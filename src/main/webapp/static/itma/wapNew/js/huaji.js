define(function (require,exports){
	var flipview = document.querySelector('.wrapper');  
	var container = flipview && flipview.firstElementChild; 
	
	//翻页按钮
	function getid(id){
	  return document.getElementById(id);
	}
	
	function addLoadEvent(func) {
	  var oldonload = window.onload;
	  if (typeof window.onload != 'function') {
		window.onload = func;
	  } else {  
		window.onload = function() {
		  oldonload();
		  func();
		}
	  }
	} 
	
	function btnEvent(){
		document.getElementById('main').addEventListener('changed', function (e) {
			switch (e.currentIndex){
				case 7:
					nextBtn.style.display='none';
					prevBtn.style.display='block';
					break;
				default:				
					nextBtn.style.display='block';
					prevBtn.style.display='none';
			}
		}); 
		
		nextBtn.addEventListener('tap', function () {
			document.querySelector('.flipview').next(1);  
		});
		
		prevBtn.addEventListener('tap', function () {
			document.querySelector('.flipview').previous(1); 
		});  
		
		//控制show
		var container = document.querySelector('.container'),list = container && container.children;
		function init(){
			list[0].classList.add('show');	
	
		}
		function addClass(){
			var index = container.index;
			if(container.querySelector('.show') && container.querySelector('.show')!==list[index]) {
				container.querySelector('.show').classList.remove('show');
			}
			!list[index].classList.contains('show') && list[index].classList.add('show');
		};
		container && container.addEventListener('changed',addClass);	
		init(); 
	}
	addLoadEvent(btnEvent);  
	
	
})