/** 
 * @Description: the script 智能运维平台 -- 知识库
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-10-21 22:56:36
 * @version： 1.0
 */
 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
		mobileTouchSlide: 'mobileTouchSlide', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader','mobileTouchSlide'], function($) {
	
	$('div.loading-wrap').delay(3000).hide();  
	
	TouchSlide({ slideCell:"#knowledgeBaseTab" });
	
	$('.item-header').on('click',function(){
		$(this).siblings('ul').toggle();
		$(this).find('.status-tag').toggleClass('closed'); 
		 
	});
	
});