/** 
 * @Description: the script 智能运维平台 -- 指派任务
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
 	mui.init({
		swipeBack:true //启用右滑关闭功能
	});
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) {
	var chargeOrderSection = $('#chargeOrderSection'),
		chargeOrderList = $('#chargeOrderList'), 
		chargeOrderMore = $('#chargeOrderMore'),
		chargeLoading = $('#chargeLoading');
				
		sessionStorage.chargeClickNum = 1;
		//表单赋值
		var formData = {
			"userId":  sessionStorage.wxUserId
                        //"userId":"huangsd@itma.com.cn"		
		};		
	//页面初始的时候加载数据
	$(document).ready(function() { 
		$.ajax({
			type: "post",
			url:"http://wechat.itma.com.cn/jeesite/f/weixin/grabTaskList",
                        dataType: 'jsonp',
                        data:formData,
			jsonpCallback: 'jsonpCallback',
			success: function(data) {  
                                if(data.order.length==0){
                                        mui.alert("未查询到抢单任务");
                                        wx.closeWindow();
                                        return;
                                }
				
				for(var i = 0; i < data.order.length; i++){
					var od = data.order[i];
					var orderStr = '<li class="mui-table-view-cell" id="'+ od.orderId +'">' +
											'<div class="mui-table" rel="'+ od.orderHref +'">' +
												'<div class="mui-table-cell mui-col-xs-6">' +
													'<h5 class="mui-ellipsis">'+ od.orderTitle +'</h5>' +
													'<p class="mui-h6 mui-ellipsis">'+ od.orderSubTitle +'</p>' +
												'</div>' +
												'<div class="mui-table-cell mui-col-xs-1 mui-text-center"><span class="mui-h5 price">'+ od.orderPrice +'</span></div>' +
												'<div class="mui-table-cell mui-col-xs-5 mui-text-center">' +
													'<span class="mui-btn mui-btn-link">受理</span>' +
													'<div class="mui-h5">'+ od.orderTime +'</div>' +
												'</div>' +
											'</div>' +
										'</li>'; 
										
					chargeOrderSection.css('display','block');						
					chargeOrderList.append(orderStr).siblings('.more-link').show();
					if($('.mui-table')){
						$('.mui-table').on('click',function(){ 
							window.location.href = $(this).attr('rel');
							sessionStorage.setItem('orderId', $(this).parent('.mui-table-view-cell').attr('id'));
						})
					}					
				}
			},
			error: function() { 
				loadingWrap.hide();
				errorWrap.show(); 
			}
		}); 
	});
	
	//点击加载--收费任务
	chargeOrderMore.on('click',function(){
		chargeOrderMore.hide();
		chargeLoading.css('display','block'); 
		var chargeClickNum = Number(sessionStorage.chargeClickNum) + 1;
	        //表单赋值
                var formData = {
                        "userId":  sessionStorage.wxUserId,
                        //"userId":"huangsd@itma.com.cn",
                        "serviceType": "",
                        "clickNum":    chargeClickNum
                };
                pageUrl = "http://wechat.itma.com.cn/jeesite/f/weixin/assignTaskList";
		
		$.ajax({
			type: "post",
			url: pageUrl,
			dataType: 'jsonp',
                        data:formData,
			jsonpCallback: 'jsonpCallback',
			success: function(data) { 
				sessionStorage.chargeClickNum = Number(sessionStorage.chargeClickNum) +1;
				
				chargeLoading.hide();
				chargeOrderMore.css('display','block');
				for(var i = 0; i < data.order.length; i++){
					var od = data.order[i];
					var orderStr = '<li class="mui-table-view-cell" id="'+ od.orderId +'">' +
					'<div class="mui-table" rel="'+ od.orderHref +'">' +
						'<div class="mui-table-cell mui-col-xs-6">' +
							'<h5 class="mui-ellipsis">'+ od.orderTitle +'</h5>' +
							'<p class="mui-h6 mui-ellipsis">'+ od.orderSubTitle +'<span class="mui-h5 price">'+ od.orderPrice +'</span></p>' +
						'</div>' +
						'<div class="mui-table-cell mui-col-xs-5 mui-text-center">' +
							'<span class="mui-btn mui-btn-link">受理</span>' +
							'<div class="mui-h5">'+ od.orderTime +'</div>' +
						'</div>' +
					'</div>' +
				'</li>';  
					 				
					chargeOrderList.append(orderStr);
					
					if($('.mui-table')){
						$('.mui-table').on('click',function(){
							console.log($(this).attr('rel'))
							window.location.href = $(this).attr('rel');
							sessionStorage.setItem('orderId', $(this).parent('.mui-table-view-cell').attr('id'));
						})
					}
					 
				}
			},
			error: function() {  
				return false;
			} 
		});
	});	
});
