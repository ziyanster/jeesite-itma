/** 
 * @Description: the script 智能运维平台 -- 填写回单
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
mui.init({
	swipeBack : false
// 启用右滑关闭功能
});
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) {	
	var  taskId = getUrlParamByArray('taskId');
	var  orderId = getUrlParamByArray('orderId');
	//页面初始的时候加载数据
	$(document).ready(function() {
		$.ajax({
			type: "get",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat03grabOrderDetail.txt",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/getGrabTaskContent?orderId="+ taskId,
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				if(data.orderId == taskId){
					document.getElementById('customerName').innerText = data.customerName;
					document.getElementById('date').innerText = data.orderTime;
				}else{
					mui.toast('请求错误');
				}
			},
			error: function(xhr,status,e) {
				mui.toast(xhr.status + ":" + status);
			}
		});
	});	
		
	//表单提交
	$('#completeButton').on('click', function() {
		
		//声明表单变量
		var completeTask,
			completeAppointmen,
			completeCal,
			taskEvaluation="",
			signature = $('#advice').val();
			
		if($('#completeTask').hasClass('mui-active')){
			completeTask = "1"; 
		}else{
			completeTask = "0"; 
		}
		
		if($('#completeAppointmen').hasClass('mui-active')){
			completeAppointmen = "1";
		}else{
			completeAppointmen = "0"; 
		}
		
		if($('#completeCal').hasClass('mui-active')){
			completeCal = "1";
		}else{
			completeCal = "0"; 
		}
		
		//对此次实施任务评价
		if($('#item1').is(':checked')){
			taskEvaluation = '0'; //非常满意
		}else if($('#item2').is(':checked')){
			taskEvaluation = '1'; //满意
		}else if($('#item3').is(':checked')){
			taskEvaluation = '2'; //不满意
		}
		if(taskEvaluation == ""){
			mui.toast('请选择任务评价'); //请输入评价意见
			return;
		}
		//是否填写表单
		if(signature == ""){
			mui.toast('请输入评价意见'); //请输入评价意见
			return;
		}
		
		//表单赋值
		var formData = {
			"formId":orderId,
			"completeTask": completeTask,
			"completeAppointmen": completeAppointmen,
			"completeCal": completeCal,
			"taskEvaluation": taskEvaluation,
			"signature": signature  
		};
		//ajax提交数据
		$.ajax({
			type: "post",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat09evaluationOrderDetail.txt",
			url:"http://wechat.itma.com.cn/jeesite/f/weixin/evaluationOrderDetail",
			data: formData,
			dataType: "jsonp",
			jsonpCallback: 'jsonpCallback',
			success: function(data) { 
				if(data.errCode == "0"){ 
					mui.alert("\u63d0\u4ea4\u6210\u529f!"); //提交成功
					wx.closeWindow();
				} else {
					mui.alert('\u63d0\u4ea4\u5931\u8d25'); //提交失败
				}
			}
		});
		return false;
		
	});
	
});

function getUrlParamByArray(name) {
	var args = new Object();
	var query=location.search.substring(1);//获取查询串   
	    var pairs=query.split("&");//在逗号处断开   
	    for(var i=0;i<pairs.length;i++) 
	    {   
	        var pos=pairs[i].indexOf('=');//查找name=value   
	        if(pos==-1)   continue;//如果没有找到就跳过   
	        var argname=pairs[i].substring(0,pos);//提取name   
	        var value=pairs[i].substring(pos+1);//提取value   
	        args[argname]=unescape(value);//存为属性   
	    }
	if(args[name]){
		return args[name];
	}
    return '';
}

