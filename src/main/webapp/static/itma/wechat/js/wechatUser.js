function getUrlParam(name) {
  var reg = new RegExp("(^|$)" + name + "=([^&]*)(&|$)");
  var r = window.location.search.substr(1).match(reg);
  if (r!= null ) return unescape(r[2]);
  return null;
}

function getUser() {
  var location = window.location.href;
  var dfd = $.Deferred();
  var code = getUrlParam('code');
  if(code != null) {
    $.get('http://wechat.itma.com.cn/jeesite/itma/get_userid?code=' + code)
      .done(function(data){
        dfd.resolve(data);
      });
  } else {
    $.get('http://wechat.itma.com.cn/jeesite/itma/get_userid?url='+ location)
      .done(function(data){
        window.location.href = data;
        dfd.resolve(data);
      });
  }
  return dfd.promise();
}

var user_id=sessionStorage.wxUserId; 
if (!sessionStorage.wxUserId ||user_id == null ) {
  $.when(getUser()).then(function(data){ 
    if (data != null && data.indexOf('https://') == -1) {
      sessionStorage.setItem('wxUserId',data);
    }
  });
}
