/** 
 * @Description: the script 
 * @authors: bradenhan (bradenhan@126.com)
 * @dateﺿ   2015-09-17 22:04:29
 * @versionﺿ1.0
 */

 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
		mobileTouchSlide: 'mobileTouchSlide', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader','mobileTouchSlide'], function($) {
	
	$('div.loading-wrap').delay(3000).hide();  
	
	TouchSlide({ slideCell:"#repairOrderTab" });
	
	var grabOrderDetailSection = $('#grabOrderDetailSection'),
		loadingWrap = $('div.loading-wrap'),  
		errorWrap = $('div.error-wrap'),
		picStr;

	$(document).ready(function() {
		document.getElementById('btnAbandon').href="abandonOrder.html?formId="+urlFormId;
		$.ajax({
			type: "get",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat04receiptOrder.txt",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/getGrabTaskContent?orderId="+urlOrderId, 
                        dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				loadingWrap.delay(3000).hide();   
                                if(typeof(data.orderId) != "undefined" && data.orderId == urlOrderId ){ 
					urlCustomer=data.customerName;
				  var grabOrderStr = '<ul class="grabOrder-summary">'+
										'<li>'+data.customerName+'</li>'+
										'<li class="address"><a href="'+data.addressLink+ '?address='+ encodeURIComponent(data.customerAddress)+ '">客户地址：'+data.customerAddress+'</a></li>'+
										'<li class="telphone"><a href="tel:'+data.phoneLink+'">联系方式：'+data.customerPhone+'</a></li>'+
										'<li>'+data.serviceType+'</li>'+
										'<li>'+data.faultType+'</li>'+
										'<li class="price">'+data.price+'</li>'+
									'</ul>'+
						
									'<div class="part-header">'+
										'<h3 class="part-title">'+data.callTitle+'</h3>'+
									'</div>'+
						
									'<div class="call-log">'+
										'<p>'+data.callLog+'</p>'+
									'</div>'+
									
									'<div class="part-header">'+
										'<span class="date">'+data.orderResponseTime+'</span>'+
										'<h3 class="part-title">'+data.orderResponseTitle+'</h3>'+
									'</div>'; 

									if (data.orderpicLink1 == "") {
										if (data.orderpicLink2 != "") {
											picStr = '<ul class="pic-list clearfix">' +
												'<li><img src="' + data.orderpicLink2 + '" alt="" /></li>' +
												'</ul>';
										} else {
											picStr = "";
										}
									}else{
										if (data.orderpicLink2 != "") {
											picStr = '<ul class="pic-list clearfix">' +
														'<li><img src="' + data.orderpicLink1 + '" alt="" /></li>' +
														'<li><img src="' + data.orderpicLink2 + '" alt="" /></li>' +
													  '</ul>';
										} else if (data.orderpicLink2 == "") {
											picStr = '<ul class="pic-list clearfix">' +
														'<li><img src="' + data.orderpicLink1 + '" alt="" /></li>' +
													  '</ul>';
										} else {
											picStr = "";
										}
									}
					   
									

				  grabOrderDetailSection.css('display', 'block');
				  grabOrderDetailSection.append(grabOrderStr + picStr);
				}else{
					alert('请求错误');
				}
			},
			error: function() {
				loadingWrap.hide();
				errorWrap.show();  
				
			}
		});
	});
	
	// 维护报告部分
	$('.fillReceipt-items').on('click','li',function(){
		$(this).parent().find('span.tip').hide();
	});
	
	
	$("#completeButton").on('click', function() {
		//声明表单变量
		var modelType = $('#modelType').val(),
			serialNumber = $('#serialNumber').val(),
			workContent = $('#workContent').val(), 
			solveProblem = $('#solveProblem').val(),  
			workPlan = $('#workPlan').val(),  
			remarks = $('#remarks').val(), 
			tips = $('.fillReceipt-items').find('span.tip');
			
		//是否填写表单
		if(modelType == ""){
			tips.eq(0).show().text('\u8bf7\u8f93\u5165\u673a\u578b'); //请输入机圿
			return false;
		}else if(serialNumber == ""){
			tips.hide();
			tips.eq(1).show().text('\u8bf7\u8f93\u5165\u7cfb\u5217\u53f7'); //请输入系列号
			return false;
		}else if(workContent == ""){
			tips.hide();
			tips.eq(2).show().text('\u8bf7\u8f93\u5165\u5de5\u4f5c\u5185\u5bb9'); //请输入工作内嬿
			return false;
		}else if(solveProblem == ""){
				tips.hide();
				tips.eq(3).show().text('\u8bf7\u8f93\u5165\u672c\u6b21\u89e3\u51b3\u7684\u95ee\u9898'); //请输入本次解决的问题
				return false;
		}else if(workPlan == ""){
				tips.hide();
				tips.eq(4).show().text('\u8bf7\u8f93\u5165\u4e0b\u4e00\u6b65\u5de5\u4f5c\u8ba1\u5212'); //请输入下一步工作计冿
				return false;
		}

                //表单赋便
		var formData = {
			"formId":urlFormId,
                        "modelType": modelType,
			"serialNumber": serialNumber,
			"workContent": workContent,
			"solveProblem": solveProblem,
			"workPlan": workPlan,
			"remarks": remarks
		};
		//ajax提交数据
		$.ajax({
			type: "post",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat05fillReceiptOrder.txt",
			url:"http://wechat.itma.com.cn/jeesite/f/weixin/fillReceiptOrder",
                        data: formData,
			dataType: "jsonp",
			jsonpCallback: 'jsonpCallback',
			success: function(data) { 
				alert(data.errMsg);				
				if(data.errCode == "0"){ 
					//跳转到评价页
                                        window.location.href ="http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/evaluationOrderDetail.html?formId="+urlFormId+"&custName="+urlCustomer;
				} else {
                                        window.location.reload()
				}
			}
		}); 
	});
	
	//维修历史记录部分
	$('.filter-bar').on('click','#deviceNo',function(){
		$(this).parent().find('span.tip').hide();
	})
	
	var orderDetail = $('#orderDetail'),
		listTtems = $('#listTtems'),
		historyList = $('#historyList');
	
	$('#searchButton').on('click',function(){
		var deviceNo = $('#deviceNo').val(),
			tip = $('.filter-bar').find('.tip');
			
		//是否填写表单
		if(deviceNo ==  ""){
			tip.eq(0).show().text('\u8bf7\u8f93\u5165\u8bbe\u5907\u7f16\u53f7'); //请输入设备编卿
			return false;
		}else{
			//ajax提交数据
			$.ajax({
				type: "get",
				url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat06repairHistoryList.txt",
				data: {"deviceNo" : deviceNo},
				dataType: "jsonp",
				jsonpCallback: 'jsonpCallback',
				success: function(data) {
					console.log(data.listTtemData.length)
					if (data.status[0].status == "1") {
						
						//订单详情
						var orderDetailStr = '<span class="pic"><img src="'+data.orderDetail[0].orderPicSrc+'" alt="" /></span>'+
											 '<p>'+data.orderDetail[0].deviceName+'</p>'+
											 '<p>'+data.orderDetail[0].ordertext+'</p>';  
						orderDetail.append(orderDetailStr).show();
						
						//最近维修记廿
						for(var i = 0; i < data.listTtemData.length; i++){
							var listTtemStr = '<li class="list-item clearfix" id="'+data.listTtemData[i].orderId+'">'+
												'<a href="'+data.listTtemData[i].orderHref+'">'+
													'<span class="num">'+data.listTtemData[i].orderNo+'</span>'+
													'<span class="pic"><img src="'+data.listTtemData[i].orderPicSrc+'" alt="" /></span>'+
													'<span class="title">'+data.listTtemData[i].orderTitle+'</span>'+
													'<span class="date">'+data.listTtemData[i].orderDate+'</span>'+
												'</a>'+
											  '</li>';
							historyList.css('display','block');					  
							listTtems.append(listTtemStr);
						}
						 
					} else {
						alert('\u7f51\u7edc\u9519\u8bef\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5'); //网络错误，请稍后再试
					}
				}
			}); 
		} 
	}); 
	
});
