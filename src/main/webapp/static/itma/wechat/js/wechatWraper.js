function generateTimeStamp() {
	return Date.parse(new Date())/1000; 
}

function generateRandomStr( len ) {
	var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'; 
	var maxPos = chars.length; 
	var ret=''; 	
	for (var i= 0; i < len ; ++i ) {
		ret += chars.charAt(Math.floor(Math.random()*maxPos)); 
	}
	return ret; 
}

function generateSignature(nonce, ticket, timestamp, url) {
	raw_ticket = 'jsapi_ticket=' + ticket + '&'; 
	raw_nonce = 'noncestr=' + nonce + '&'; 
	raw_timestamp = 'timestamp=' +timestamp + '&'; 
	raw_url = 'url='+url ; 
	raw_result = raw_ticket + raw_nonce +  raw_timestamp +  raw_url;
	var ret =  hex_sha1(raw_result); 
	return ret; 
}

function getWxConfig() {
	var config = new Object();
	$.ajax({url:'http://wechat.itma.com.cn/jeesite/itma/get_jsticket', 
		    type: 'GET', 
		    async: false, 
		    success: function(ticket) {
				var curUrl= window.location.href;  	 
				config.debug = false; 
				config.appId = 'wx57f9d1c45c99a814'; 
				config.timestamp =  generateTimeStamp(); 
				config.nonceStr = generateRandomStr(16); 
				config.signature = generateSignature(config.nonceStr, ticket, config.timestamp, curUrl); 
				config.jsApiList = ['getLocation' , 'chooseImage', 'uploadImage', 'previewImage' ] ;
		   }
	});  
    return config;  
}

var cfg = getWxConfig(); 
wx.config(cfg); 

 

