﻿/** 
 * @Description: the script 智能运维平台 -- 任务详情
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});
require(['jquery', 'wechatHideHeader'], function($) {
	var contentTable = $('#contentTable'),
		contentTable1 = $('#contentTable1'),
		loadingWrap = $('div.loading-wrap'),  
		errorWrap = $('div.error-wrap');

	//页面初始的时候加载数据
	$(document).ready(function() {
		$.ajax({
			type: "get",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat03grabOrderDetail.txt",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/getServiceRankingContent",
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				loadingWrap.delay(3000).hide();
				var yesterday = data.listYesterday;
				var curMonth = data.listCurMonth;
				for(var i =0 ;i < yesterday.length; i ++){
					var d = yesterday[i];
					contentTable.append('<tr align="center"><td>'+d.name+'</td><td>'+d.countValue+'</td><td>'+d.sumValue+'</td><td>'+d.status+'</td></tr>');
				}
				for(var i =0 ;i < curMonth.length; i ++){
					var d = curMonth[i];
					contentTable1.append('<tr align="center"><td>'+d.name+'</td><td>'+d.countValue+'</td><td>'+d.sumValue+'</td><td>'+d.status+'</td></tr>');
				}
				$('#grabOrderDetailSection').show();
			},
			error: function() {
				loadingWrap.hide();
				errorWrap.show();
			}
		});
	});
});
