/** 
 * @Description: the script 
 * @authors: bradenhan (bradenhan@126.com)
 * @dateﺿ   2015-09-17 22:04:29
 * @versionﺿ1.0
 */
 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
		mobileTouchSlide: 'mobileTouchSlide', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader','mobileTouchSlide'], function($) {
	
	$('div.loading-wrap').delay(3000).hide();  
	
	TouchSlide({ slideCell:"#repairOrderTab" });
	
	var grabOrderDetailSection = $('#grabOrderDetailSection'),
		fillReceiptItems = $('#fillReceiptItems'),
		loadingWrap = $('div.loading-wrap'),  
		errorWrap = $('div.error-wrap'),
		picStr;

	$(document).ready(function() { 
		$.ajax({
			type: "get",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/getFormContent?formId="+urlFormId, 
 			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				loadingWrap.delay(3000).hide();   
				if(typeof(data.formId) != "undefined" && data.formId == urlFormId ){ 
					urlCustomer=data.customerName;
				  var grabOrderStr = '<ul class="grabOrder-summary">'+
										'<li>'+data.customerName+'</li>'+
										'<li class="address"><a href="'+data.addressLink+ '?address='+ encodeURIComponent(data.customerAddress)+ '">客户地址：'+data.customerAddress+'</a></li>'+
										'<li class="telphone"><a href="tel:'+data.phoneLink+'">联系方式：'+data.customerPhone+'</a></li>'+
										'<li>'+data.serviceType+'</li>'+
										'<li>'+data.faultType+'</li>'+
										'<li class="price">'+data.price+'</li>'+
									'</ul>'+
						
									'<div class="part-header">'+
										'<h3 class="part-title">'+data.callTitle+'</h3>'+
									'</div>'+
						
									'<div class="call-log">'+
										'<p>'+data.callLog+'</p>'+
									'</div>'+
									
									'<div class="part-header">'+
										'<span class="date">'+data.orderResponseTime+'</span>'+
										'<h3 class="part-title">'+data.orderResponseTitle+'</h3>'+
									'</div>'; 

									if (data.orderpicLink1 == "undefined") {
										if (data.orderpicLink2 != "undefined") {
											picStr = '<ul class="pic-list clearfix">' +
												'<li><img src="' + data.orderpicLink2 + '" alt="" /></li>' +
												'</ul>';
										} else {
											picStr = "";
										}
									}else{
										if (data.orderpicLink2 != "undefined") {
											picStr = '<ul class="pic-list clearfix">' +
														'<li><img src="' + data.orderpicLink1 + '" alt="" /></li>' +
														'<li><img src="' + data.orderpicLink2 + '" alt="" /></li>' +
													  '</ul>';
										} else if (data.orderpicLink2 == "undefined") {
											picStr = '<ul class="pic-list clearfix">' +
														'<li><img src="' + data.orderpicLink1 + '" alt="" /></li>' +
													  '</ul>';
										} else {
											picStr = "";
										}
									}  
				  grabOrderDetailSection.css('display', 'block'); 
				  grabOrderDetailSection.append(grabOrderStr + picStr);

			 	   var fillReceiptStr = '<li class="fillReceipt-item item1 clearfix">'+ 
			 	   							if(data.modelType != "undefined" ){
			 	   								'<div class="input" id="modelType">' + data.modelType + '</div>'+
			 	   							}else{
			 	   							'<div class="input" id="modelType"></div>'+
			 	   							}
									 	  if(data.serialNumber != "undefined" ){
									 		 '<div class="input" id="serialNumber">' + data.serialNumber + '</div>'+
									 	  }else{
									 		 '<div class="input" id="serialNumber"></div>'+
									 	  }
											
										'</li>'+
										'<li class="fillReceipt-item item2 clearfix">'+ 
										 if(data.workContent != "undefined" ){
											 '<div class="textarea" id="workContent">' + data.workContent + '</div>'+
										 }else{
											 '<div class="textarea" id="workContent"></div>'+
										 }
										'</li>'+
										'<li class="fillReceipt-item item3 clearfix">'+ 
										 if(data.solveProblem != "undefined" ){
												'<div class="textarea" id="solveProblem">' + data.solveProblem + '</div>'+
										 }else{
											 '<div class="textarea" id="solveProblem"></div>'+
										 }
										'</li>'+
										'<li class="fillReceipt-item item4 clearfix">'+ 
										 if(data.workPlan != "undefined" ){
												'<div class="textarea" id="workPlan">' + data.workPlan + '</div>'+
										 }else{
											 '<div class="textarea" id="workPlan">' + data.workPlan + '</div>'+
										 }
										'</li>'+
										'<li class="fillReceipt-item item5 clearfix">'+ 
										if(data.workPlan != "undefined" ){
											'<div class="textarea" id="remarks">' + data.remarks + '</div>'+
										}else{
											'<div class="textarea" id="remarks"></div>'+
										}
											
										'</li>'; 
				fillReceiptItems.append(fillReceiptStr);
				}else{
					alert('请求错误');
				}
			},
			error: function() {
				loadingWrap.hide();
				errorWrap.show();  
				
			}
		});
	}); 
});
