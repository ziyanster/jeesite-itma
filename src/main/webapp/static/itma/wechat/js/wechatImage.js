function chooseImage() {
	var dfd = $.Deferred();
	wx.chooseImage({
		count : 1,
		sizeType : [ 'original', 'compressed' ],
		sourceType : [ 'album', 'camera' ],
		success : function(res) {
			if (res.localIds.length > 0) {
				dfd.resolve(res.localIds[0]);
			}
		}
	});
	return dfd.promise(); 
}

function uploadImage(localId) {
	var dfd = $.Deferred();
	wx.uploadImage({
	    localId: localId,
	    isShowProgressTips: 1, 
	    success: function (res) {
	        var serverId= res.serverId;
	        dfd.resolve(serverId); 
	    }, 
	    fail: function(res) {
	    	alert("上传文件失败！图片尺寸应小于1M."); 
	    	dfd.reject(res); 
	    }
	  
	});
	return dfd.promise(); 
}

function fetchImage(serverId) {
	var dfd = $.Deferred(); 
	$.when($.ajax("http://wechat.itma.com.cn/jeesite/itma/fetch_media?media_id=" + serverId)).then(function(data) {
		dfd.resolve(data); 
	}); 
	return dfd.promise() ; 
}


function addListItem(imageRef) {
	var element = $('<li class="pic"><img src="'+ imageRef + '" alt=""></li>'); 
	if(mui){
		element = $('<li class="mui-table-view-cell mui-media mui-col-xs-4"><img class="mui-media-object" src="'
				+ imageRef + '" alt=""></li>');
	}
	$('#wechat-add-image').before(element);
	return element; 
}

function previewImage() {
    $.when(chooseImage()).then(function(localId){
    	var element = addListItem(localId); 
    	$.when(uploadImage(localId)).then( function(serverId){
    		        $.when(fetchImage(serverId)).then (function(imagePath){
	    		    		element.attr('src', imagePath);
	    		    		element.click(function(){
	    		    			wx.previewImage({
	    		    				current: $(this).attr('src'), 
	    		    				urls: [$(this).attr('src')] 
	    		    			});
	    		    		});
		            	var addPathUrl= 'http://wechat.itma.com.cn/jeesite/f/saveAttachments?formId=' + urlFormId + '&attUrl=' + imagePath;
		           		$.post(addPathUrl, function(result){
				    	}); 
    		        }); 
    	});
    }); 
}

/*
wx.ready( function () {
	$(document).ready( function(){
		alert('items count = ' + $('#wechat-add-image').length )
		$('#wechat-add-image').click( function(){
		    $.when(chooseImage()).then(function(localId){
		    	var element = addListItem(localId); 
		    	$.when(uploadImage(localId)).then( function(serverId){
		    		        $.when(fetchImage(serverId)).then (function(imagePath){
			    		    		element.attr('src', imagePath);
			    		    		element.click(function(){
			    		    			wx.previewImage({
			    		    		    current: $(this).attr('src'), 
			    		    		    urls: [$(this).attr('src')] 
			    		    			});
			    		    		});
                    	var addPathUrl= 'http://wechat.itma.com.cn/jeesite/f/saveAttachments?formId=' + urlFormId + '&attUrl=' + imagePath;
                   		$.post(addPathUrl, function(result){
				    		    		}); 
		    		        	}); 
		    		});
		    	});  
		    	
		    }); 
		}); 
}); 
*/						                   
