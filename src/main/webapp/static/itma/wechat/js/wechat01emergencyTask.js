/** 
 * @Description: the script 智能运维平台 -- 抢单列表页
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
mui.init({
	swipeBack : false
// 启用右滑关闭功能
});
require.config({
	paths: {
		jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader',
	},
	urlArgs: "bust=" + (new Date()).getTime()
});

var contentPicker = new mui.PopPicker();
var callPicker = new mui.PopPicker();
var workTypes;
var callTypes;
require(['jquery', 'wechatHideHeader'], function($) { 
	mui('.mui-scroll-wrapper').scroll({
		indicators: true //是否显示滚动条
	});
	var userId  = sessionStorage.wxUserId;
	if(userId == "" || typeof(userId) == "undefined" || !userId){
		mui.alert('未获取到用户相关信息，请重新访问。');
		wx.closeWindow();
		return false;
	}
	
	var myCustomers;
	$.ajax({
		type: "get",
		url: "http://wechat.itma.com.cn/jeesite/f/weixin/getMyEmergencyTaskContent?userId=" + userId,
		dataType: 'jsonp',
		jsonpCallback: 'jsonpCallback',
		success: function(data) {
			if(data!=null){
				if(typeof(data.errMsg) != "undefined"){
					mui.alert(data.errMsg);
					if(wx){
						wx.closeWindow();
					}
					return;
				}
				myCustomers = data.customers;
				if(myCustomers){
					for(var i = 0;i< myCustomers.length;i++){
						var cust = myCustomers[i];
						var li = '<li class="mui-table-view-cell" id="'+cust.customerno+'">'+ cust.customername +'</li>';
						$('#customerList').append(li);
					}
					$('#customerList li').on('click', function() {
						var id = $(this).attr('id');
						var name = $(this).html();
						$('#custNo').val(id);
						$('#spSelectCustomer').text(name);
						mui('#myCustomers').popover('hide');
					});
				}
				workTypes = data.dicts;
				callTypes = data.dicts1;
				var arr = [];
				for(var i = 0 ;i < workTypes.length;i++){
					var dic = workTypes[i];
					arr.push({'text':dic.label,'value':dic.value});					
				}
				contentPicker.setData(arr);
				
				arr = [];
				for(var i = 0 ;i < callTypes.length;i++){
					var dic = callTypes[i];
					arr.push({'text':dic.label,'value':dic.value});					
				}
				callPicker.setData(arr);
			}
		},
		error: function(xhr,status,e) {
			mui.toast(xhr.status + ":" + status);
		}
	});

	$('#searchCustomer').on('tap',function(){
		var pattern = $('#searchName').val();
		if(pattern == ""){
			$('#customerList').empty();
			if(myCustomers){
				for(var i = 0;i<myCustomers.length;i++){
					var cust = myCustomers[i];
					var li = '<li class="mui-table-view-cell" id="'+cust.customerno+'">'+ cust.customername +'</li>';
					$('#customerList').append(li);
				}
				$('#customerList li').on('click', function() {
					var id = $(this).attr('id');
					var name = $(this).html();
					$('#custNo').val(id);
					$('#spSelectCustomer').text(name);
					mui('#myCustomers').popover('hide');
				});
			}				
		}else{
			$.ajax({
				type: "get",
				url: "http://wechat.itma.com.cn/jeesite/f/weixin/searchCustomers?custName=" + pattern,
				dataType: 'jsonp',
				jsonpCallback: 'jsonpCallback',
				success: function(data) {
					$('#customerList').empty();
					if(data.customers){
						for(var i = 0;i< data.customers.length;i++){
							var cust = data.customers[i];
							var li = '<li class="mui-table-view-cell" id="'+cust.customerno+'">'+ cust.customername +'</li>';
							$('#customerList').append(li);
						}
						$('#customerList li').on('click', function() {
							var id = $(this).attr('id');
							var name = $(this).html();
							$('#custNo').val(id);
							$('#spSelectCustomer').text(name);
							mui('#myCustomers').popover('hide');
						});
					}
				},
				error: function(xhr,status,e) {
					mui.toast(xhr.status + ":" + status);
				}
			});
		}
		mui('#myCustomers').popover('show');
	});
	
	$('#submitBtn').on('tap',function(){
		var custNo = $('#custNo').val();
		var workType = $('#workType').val();
		var callType = $('#callType').val();
		var callLog = $('#callLog').val();
		var callTime = $('#callTime').val();
		var outTime = $('#outTime').val();
		
		if(custNo == ""){
			mui.toast("请选择一个客户");
			return false;
		}
		if(workType == ""){
			mui.toast("请选择工作类型");
			return false;
		}
		if(callType == ""){
			mui.toast("请选择呼入方式");
			return false;
		}
		if(callTime == ""){
			mui.toast("请输入呼入时间");
			return false;			
		}
		if(outTime == ""){
			mui.toast("请输入出发时间");
			return false;				
		}
		if(callLog == ""){
			mui.toast("请输入呼入记录");
			return false;
		}
		$.ajax({
			type: "post",
			data:{
				"userId" : userId,
				"custNo" : custNo,
				"workType" : workType,
				"callType" : callType,
				"callLog" : callLog,
				"callTime" : callTime,
				"outTime" : outTime
			},
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/saveEmergencyTask",
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				if(data.errCode == "0"){
					window.location.href="http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/receiptOrderInput.html?orderId="+data.orderId+"&formId="+data.formId;
				}
				else{
					mui.alert(data.errMsg);
				}
			},
			error: function(xhr,status,e) {
				mui.toast(xhr.status + ":" + status);
			}
		});
		
	});
});

function getWorkType(e) {
	contentPicker.show(function(items) {
		e.value = items[0].text;
		document.getElementById("workType").value = items[0].value;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getCallType(e) {
	callPicker.show(function(items) {
		e.value = items[0].text;
		document.getElementById("callType").value = items[0].value;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}
function getCallTime(e) {
	var optionsJson = e.getAttribute('data-options') || '{}';
	var options = JSON.parse(optionsJson);
	var oriV = e.value;
	if (oriV) {
		options.value = oriV;
	}

	/*
	 * 首次显示时实例化组件 示例为了简洁，将 options 放在了按钮的 dom 上 也可以直接通过代码声明 optinos 用于实例化
	 * DtPicker
	 */
	var picker = new mui.DtPicker();
	picker.show(function(rs) {
		/*
		 * rs.value 拼合后的 value rs.text 拼合后的 text rs.y 年，可以通过 rs.y.vaue 和
		 * rs.y.text 获取值和文本 rs.m 月，用法同年 rs.d 日，用法同年 rs.h 时，用法同年 rs.i 分（minutes
		 * 的第二个字母），用法同年
		 */
		e.value = rs.text;
		e.blur();
		/*
		 * 返回 false 可以阻止选择框的关闭 return false;
		 */
		/*
		 * 释放组件资源，释放后将将不能再操作组件 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
		 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
		 */
		picker.dispose();
	});
}

function showMyCustomers() {
	mui('#myCustomers').popover('show');
}