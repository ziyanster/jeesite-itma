/** 
 * @Description: the script 智能运维平台 -- 填写回单
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) {	
	$('div.loading-wrap').delay(3000).hide(); 
	$("#completeButton").on('click', function() {
		//声明表单变量
		var reason= $('#reason').val();
			
		//是否说明理由
		if(reason== ""){
			alert('请填写退单理由');
			return false;
		}
                //表单赋值
		var formData = {
			"formId":urlFormId,
                        "reason": reason,
			"quitType":"4" //主动退单
		};
		//ajax提交数据
		$.ajax({
			type: "post",
			url:"http://wechat.itma.com.cn/jeesite/f/weixin/quitOrder",
                        data: formData,
			dataType: "jsonp",
			jsonpCallback: 'jsonpCallback',
			success: function(data) { 			
				if(data.errCode == "0"){ 
					//关闭
					wx.closeWindow();
				} else {
                                        window.location.reload()
				}
			}
		});
	});		
});
