/** 
 * @Description: the script 智能运维平台 -- 填写回单
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) {
	var evaluationOrderList = $('#evaluationOrderList');
	//页面初始的时候加载数据
	$(document).ready(function() { 
		var userId  = sessionStorage.wxUserId;
		if(userId == "" || typeof(userId) == "undefined" || !userId){
			mui.alert('未获取到用户相关信息，请重新访问。');
			if(wx){
				wx.closeWindow();
			}
			return;
		}
		$.ajax({
			type: "post",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat08evaluationOrderList.txt",
			url:"http://wechat.itma.com.cn/jeesite/f/weixin/unEvaluationOrderList",
			data: {                                 
				"userId":userId
			},
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) { 
				if (data.status.errCode == "0") { 
					if(data.listTtemData.length==0){
						mui.toast("未查到待评价列表");
						return;
					}
					
					//记录
					for(var i = 0; i < data.listTtemData.length; i++){
						//收费任务
						var loadingDataStr = '<li class="mui-table-view-cell mui-media" id="'+ data.listTtemData[i].orderId +'">'+
											'<a href="'+ data.listTtemData[i].orderHref +'">'+
												'<div class="mui-table">'+
													'<div class="mui-table-cell mui-col-xs-8">'+
														'<h5 class="mui-ellipsis">'+ data.listTtemData[i].orderTitle +'</h5>'+
														'<p class="mui-h6 mui-ellipsis">'+ data.listTtemData[i].orderTime+'</p>'+
													'</div>'+
						
													'<div class="mui-table-cell mui-col-xs-4 mui-text-right">'+
														'<div class="mui-h5">'+ data.listTtemData[i].orderPrice+'</div>'+
														'<div class="mui-h6">'+ data.listTtemData[i].orderSubTitle+'</div>'+
													'</div>'+
												 '</div>';
											'</a>'+
										'</li>'; 		
						evaluationOrderList.append(loadingDataStr); 
					}
				}else{
					mui.alert(data.status.errMsg);
				}
			},
			error: function(xhr,status,e) {
				mui.toast(xhr.status + ":" + status);
			}
		}); 
	}); 
});
