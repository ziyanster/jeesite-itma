﻿/** 
 * @Description: the script 智能运维平台 -- 抢单列表页
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 	mui.init({
		swipeBack:true //启用右滑关闭功能
	});
require.config({
	paths: { 
		jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader'
	},
	urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) { 
	var userId = getUrlParamByArray('userId');
	var taskId = getUrlParamByArray('taskId');
	if (!userId || !taskId) {
		return;
	}
    sessionStorage.setItem('wxUserId',userId);
	var projServiceType;
	$('#btnSubmit').on('click',function(){
		var serviceType = $('#serviceType').val();
		var cbs = $('input[name=engineer]').filter(':checked');
		if(serviceType == ""){
			mui.toast('\u8bf7\u9009\u62e9\u670d\u52a1\u7c7b\u578b');//请选择服务类型
			return false;			
		}
		if(cbs.length == 0){
			mui.toast('\u8bf7\u81f3\u5c11\u6307\u6d3e\u4e00\u540d\u5de5\u7a0b\u5e08');//请至少指派一名工程师
			return false;			
		}
		
		var engineers=[];
		for(var i = 0 ; i < cbs.length; i ++){
			engineers.push(cbs.eq(i).val());
		}
		var str = engineers.join(',');
		$.ajax({
			type : "get",
			url : "http://wechat.itma.com.cn/jeesite/f/weixin/createOrderByProjMnger",
			data : {
				'userId' : userId,
				'taskId' : taskId,
				'serviceType' :projServiceType,
				'engineers' : str
			},
			dataType : 'jsonp',
			jsonpCallback : 'jsonpCallback',
			success : function(data) {
				if(data.errCode == "1"){
					mui.alert(data.errMsg);
				}else
				{
					wx.closeWindow();	
				}
			},
			error : function(e) {
				mui.toast('error :' + e.message);
			}
		});
	});
	
	$.ajax({
		type : "get",
		url : "http://wechat.itma.com.cn/jeesite/f/weixin/getTaskDetailFromProjMnger",
		data : {
			'userId' : userId,
			'taskId' : taskId
		},
		dataType : 'jsonp',
		jsonpCallback : 'jsonpCallback',
		success : function(data) {
			$('#custName').text(data.customerName);
			$('#custQuestion').text(data.callLog);
			$('#custContract').text(data.contract);
			$('#serviceDate').text(data.serviceDate);
			if(data.engineers.length==0){
				mui.alert('未查到可指派的工程师，请联系管理员进行添加');
				return;
			}
			
			for(var i = 0; i< data.engineers.length;i++){
				var engineer = data.engineers[i];
				var div = '<div class="mui-input-row mui-checkbox mui-left"><label>'
						+ engineer.name
						+'</label><input name="engineer" value="'
						+ engineer.id
						+'" type="checkbox"></div>';
				$('#cblEngineers').append(div);
			}
			
			var arr = [];
			for(var i = 0 ; i < data.dicts.length; i ++){
				var dic = data.dicts[i];
				var option = { value : dic.value , text : dic.label };
				arr.push(option);
			}
			var userPicker = new mui.PopPicker();
			userPicker.setData(arr);
			var showUserPickerButton = document.getElementById('serviceType');
			showUserPickerButton.addEventListener('click', function(event) {
				userPicker.show(function(items) {
					showUserPickerButton.value = items[0].text;
					projServiceType = items[0].value;
					//返回 false 可以阻止选择框的关闭
					//return false;
				});
			}, false);			
		},
		error: function(xhr,status,e) {
			mui.toast(xhr.status + ":" + status);
		}
	});
});
function getUrlParamByArray(name) {
	var args = new Object();
	var query=location.search.substring(1);//获取查询串   
	    var pairs=query.split("&");//在逗号处断开   
	    for(var i=0;i<pairs.length;i++) 
	    {   
	        var pos=pairs[i].indexOf('=');//查找name=value   
	        if(pos==-1)   continue;//如果没有找到就跳过   
	        var argname=pairs[i].substring(0,pos);//提取name   
	        var value=pairs[i].substring(pos+1);//提取value   
	        args[argname]=unescape(value);//存为属性   
	    }
	if(args[name]){
		return args[name];
	}
    return '';
}