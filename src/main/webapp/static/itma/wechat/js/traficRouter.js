function getUrlParam(name) {
  var reg = new RegExp("(^|$)" + name + "=([^&]*)(&|$)");
  var r = decodeURIComponent(window.location.search.substr(1)).match(reg);
  if (r!= null ) return unescape(r[2]);
  return null;
}

function retrieveCity(address) {
	var cityIndex = address.indexOf('市'); 
	if (cityIndex != -1) {
		var provinceIndex = address.indexOf('省');
		if (provinceIndex != -1) {
			return address.substring(provinceIndex+1, cityIndex +1); 
		} else {
			return address.substring(0, cityIndex +1); 
		}
	}
	return null; 
}

function loadMap(centerPos) {
	var mapInstance  = new AMap.Map('mapContainer'); 
	mapInstance.setZoom(10)
	mapInstance.setCenter(centerPos); 
	return mapInstance; 
}

function getDestAddr(addr, city) {
	var dest = new Object(); 
	var dfd = $.Deferred(); 
	console.log('getDestAddr'); 
	AMap.service('AMap.Geocoder', function (){
		geocoder = new AMap.Geocoder({
			city: city
		}); 	
		geocoder.getLocation(addr , function(status, result) {	
			if(status ==='complete' && result.info==='OK') {
				console.log('getDestAddr: getLocation'); 
				dest.longitude = result.geocodes[0].location.lng; 
				dest.latitude = result.geocodes[0].location.lat;
				dfd.resolve(dest); 
			}
		});  
	});
	return dfd.promise(); 
} 

function getCurrentCity(longitude , latitude) {
	var dfd = $.Deferred(); 
	var lnglatXY = [longitude, latitude]; 
	AMap.service('AMap.Geocoder', function(){
		geocoder = new AMap.Geocoder();
		geocoder.getAddress(lnglatXY, function(status, result){
			if(status === 'complete' && result.info ==='OK') {
                var address = result.regeocode.formattedAddress; 
                var cityIndex = address.indexOf('市'); 
                if(cityIndex != -1 ) {
                	dfd.resolve(address.substring(0,cityIndex)); 
                }
			} else {
				alert('getCurrentCity failed'); 
			}
		}); 
	});
	return dfd.promise(); 
}
function caculatePath(city_s, city_d,  from , to, mapInstance) {	
	var path = ""; 
	var dfd = $.Deferred(); 
	console.log('caculatePaht'); 
	AMap.service('AMap.Transfer', function(){
		var trans; 
		transOptions = {
				map:mapInstance, 
				city:city_s, 
				cityd:city_d,
				policy:AMap.TransferPolicy.LEAST_TIME }; 		
		trans = new AMap.Transfer(transOptions); 
		trans.search (from, to, function (status, result ){
			var segments = result.plans[0].segments; 
			for (var i= 0; i < segments.length ; i++ ) {
				path = path+ segments[i].instruction + "<br\>"; 
			}
			dfd.resolve(path)
		}); 
	}); 
	return dfd.promise(); 
}
wx.ready(function(){
	var dfd = $.Deferred(); 
	//user address
	wx.getLocation({
	    type: 'wgs84', 
	    success: function (res) {
	    	var myLocation = new Object();  
	    	myLocation.latitude = res.latitude; 
	    	myLocation.longitude = res.longitude;
	    	dfd.resolve(myLocation); 
	    }
	}); 
	
	$.when(dfd.promise()).then(function( myLocation ) {
			var location =[myLocation.longitude, myLocation.latitude]; 
			$(document).ready( function (){
				var mapInstance = loadMap(location); 
				var addr = getUrlParam('address'); 
				var city_d = retrieveCity(addr);		
				$.when(getDestAddr(addr, city_d), getCurrentCity(location[0], location[1]))
				 .then(function(destAddr, city_s){
					 return caculatePath(city_s, city_d, location, [destAddr.longitude, destAddr.latitude], mapInstance);})
				 .then (function (pathDesc) {
					 $("#pathDescription").html(pathDesc);
				  });  
			}); 
	}); 
}); 


