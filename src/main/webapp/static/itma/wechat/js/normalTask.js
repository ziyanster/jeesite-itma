/** 
 * @Description: the script 智能运维平台 -- 抢单列表页
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
mui.init({
	swipeBack : false
// 启用右滑关闭功能
});
require.config({
	paths: {
		jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader',
	},
	urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) { 
	mui('.mui-scroll-wrapper').scroll({
		indicators: true //是否显示滚动条
	});
	var userId  = sessionStorage.wxUserId;
	if(userId == "" || typeof(userId) == "undefined" || !userId){
		mui.alert('未获取到用户相关信息，请重新访问。');
		wx.closeWindow();
		return false;
	}
	var myCustomers;

	$('#searchCustomer').on('tap',function(){
		var pattern = $('#searchName').val();
		if(pattern == ""){
			return false;			
		}else{
			$.ajax({
				type: "get",
				url: "http://wechat.itma.com.cn/jeesite/f/weixin/searchCustomers?custName=" + pattern,
				dataType: 'jsonp',
				jsonpCallback: 'jsonpCallback',
				success: function(data) {
					$('#customerList').empty();
					if(data.customers){
						for(var i = 0;i< data.customers.length;i++){
							var cust = data.customers[i];
							var li = '<li class="mui-table-view-cell" id="'+cust.customerno+'">'+ cust.customername +'</li>';
							$('#customerList').append(li);
						}
						$('#customerList li').on('click', function() {
							var id = $(this).attr('id');
							var name = $(this).html();
							$('#custNo').val(id);
							$('#spSelectCustomer').val(name);
							mui('#myCustomers').popover('hide');
						});
					}
				},
				error: function(xhr,status,e) {
					mui.toast(xhr.status + ":" + status);
				}
			});
		}
		mui('#myCustomers').popover('show');
	});
	
	$('#submitBtn').on('tap',function(){
		var custNo = $('#custNo').val();
		var workType = $('#workTypeNo').val();
		var startTime = $('#startTime').val();
		var endTime = $('#endTime').val();
		var workStat = $('#workstatNo').val();
		var workway = $('#workway').val();
		var record = $('#record').val();

		if(workType == ""){
			mui.toast("请选择工作类型");
			return false;
		}
		if(custNo == ""){
			mui.toast("请选择一个客户");
			return false;
		}
		if(startTime == ""){
			mui.toast("请选择开始时间");
			return false;			
		}
		if(endTime == ""){
			mui.toast("请选择结束时间");
			return false;				
		}
		if(workStat == ""){
			mui.toast("请选择完成情况");
			return false;
		}
		if(workway == ""){
			mui.toast("请选择工作方式");
			return false;
		}
		if(record == ""){
			mui.toast("请输入工作内容");
			return false;
		}
		$.ajax({
			type: "post",
			data:{
				"userId" : userId,
				"custNo" : custNo,
				"workType" : workType,
				"startTime" : startTime,
				"endTime" : endTime,
				"workStat" : workStat,
				"workway" : workway,
				"record" : record
			},
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/saveNormalTask",
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				if(data.errCode == "0"){
					wx.closeWindow();
				}
				else{
					mui.alert(data.errMsg);
				}
			},
			error: function(xhr,status,e) {
				mui.toast(xhr.status + ":" + status);
			}
		});
		
	});
});

var workTypePicker = new mui.PopPicker();
workTypePicker.setData([{
	'text': '市场',
	'value': '05'
}, {
	'text': '支持',
	'value': '02'
}, {
	'text': '商务',
	'value': '04'
}, {
	'text': '辅助',
	'value': '01'
}, {
	'text': '公务',
	'value': '08'
}, {
	'text': '其他',
	'value': 'OT'
}]);
var workWayPicker = new mui.PopPicker();
workWayPicker.setData([{
	'text': '现场工作',
	'value': '现场工作'
}, {
	'text': '电话支持',
	'value': '电话支持'
}]);
var workStatPicker = new mui.PopPicker();
workStatPicker.setData([{
	'text': '正常结束',
	'value': '0'
}, {
	'text': '转请他人',
	'value': '1'
}, {
	'text': '暂不处理',
	'value': '2'
}, {
	'text': '持续工作',
	'value': '3'
}]);

function getWorkway(e) {
	workWayPicker.show(function(items) {
		e.value = items[0].text;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getWorkstat(e) {
	workStatPicker.show(function(items) {
		e.value = items[0].text;
		$('#workstatNo').val(items[0].value);
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getWorkType(e) {
	workTypePicker.show(function(items) {
		e.value = items[0].text;
		$('#workTypeNo').val(items[0].value)
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getDateTime(e) {
	var optionsJson = e.getAttribute('data-options') || '{}';
	var options = JSON.parse(optionsJson);
	var id = e.getAttribute('id');
	var oriV = e.value;
	if (oriV) {
		options.value = oriV;
	}
	/* 
	 * 首次显示时实例化组件
	 * 示例为了简洁，将 options 放在了按钮的 dom 上
	 * 也可以直接通过代码声明 optinos 用于实例化 DtPicker
	 */
	var picker = new mui.DtPicker(options);
	picker.show(function(rs) {
		/*
		 * rs.value 拼合后的 value
		 * rs.text 拼合后的 text
		 * rs.y 年，可以通过 rs.y.vaue 和 rs.y.text 获取值和文本
		 * rs.m 月，用法同年
		 * rs.d 日，用法同年
		 * rs.h 时，用法同年
		 * rs.i 分（minutes 的第二个字母），用法同年
		 */
		e.value = rs.text;
		e.blur();
		/* 
		 * 返回 false 可以阻止选择框的关闭
		 * return false;
		 */
		/*
		 * 释放组件资源，释放后将将不能再操作组件
		 * 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
		 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。
		 * 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
		 */
		picker.dispose();
	});
}

function showMyCustomers() {
	mui('#myCustomers').popover('show');
}