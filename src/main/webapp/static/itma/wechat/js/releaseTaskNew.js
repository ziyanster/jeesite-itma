/**
 * @Description: the script 智能运维平台 -- 客户应急任务
 * @authors: bradenhan (bradenhan@126.com)
 * @date： 2015-09-17 22:04:29
 * @version： 1.0
 */
mui.init({
	swipeBack : true
// 启用右滑关闭功能
});
require.config({
	paths : {
		jquery : 'jquery.min.2.1.4',
		wechatHideHeader : 'wechatHideHeader'
	},
	urlArgs : "bust=" + (new Date()).getTime()
});

require(
		[ 'jquery', 'wechatHideHeader' ],
		function($) {
			//'wangfy@itma.com.cn'//
			var custId = sessionStorage.wxUserId;
			if (!custId) {
				mui.toast('获取当前用户失败');
				return;
			}
			// 页面js开始
			$("#submitBtn").on(
							'click',
							function() {							
								var record = $('#record').val().replace(/(^\s*)|(\s*$)/g, "");
								var addr = $('input[name=serviceAddr]')
												.filter(':checked').first()
												.siblings('span').text().replace(/(^\s*)|(\s*$)/g, "");
								var dt = $("#serviceDate").val().replace(/(^\s*)|(\s*$)/g, "");;
								if(record == ""){
									mui.toast('\u8bf7\u8f93\u5165\u95ee\u9898');//请输入问题
									return false;
								}else if(dt == ""){
									mui.toast('\u8bf7\u8f93\u5165\u670d\u52a1\u65f6\u95f4');//请输入服务时间
									return false;
								}
								var imgsArr = [];
								var imgs = $('#imgList img');
								for(var i = 0; i < imgs.length ; i++){
									imgsArr.push(imgs[i].src);
								}
								var formData = {
									'custId' : custId,
									'record' : record,
									'serviceAddr' : addr,
									'serviceDate' : dt,
									'imgs' : imgsArr.join(';')
								}
								$.ajax({
											type : "POST",
											url : "http://wechat.itma.com.cn/jeesite/f/weixin/custEmergencyTaskSave",
											data : formData,
											dataType : 'jsonp',
											jsonpCallback : 'jsonpCallback',
											success : function(data) {
												if (data.errCode == "0") {
													window.location.reload();
												}else{
													mui.alert(data.errMsg);
												}
											},
											error: function(xhr,status,e) {
												mui.toast(xhr.status + ":" + status);
											}
										});
							});

			$.ajax({
						type : "get",
						url : "http://wechat.itma.com.cn/jeesite/f/weixin/getCustEmergencyTaskContent",
						data : {
							'custId' : custId
						},
						dataType : 'jsonp',
						jsonpCallback : 'jsonpCallback',
						success : function(data) {
							var div = '<div class="mui-input-row mui-radio mui-left"><label>地址1</label><input name="serviceAddr" type="radio"><span>'
									+ data.customerAddress + '</span></div>';
							$('#serviceAddrDiv').append(div);
						},
						error: function(xhr,status,e) {
							mui.toast(xhr.status + ":" + status);
						}
					});

		});
function doAddress(addr){
	var dv = '<div class="mui-input-row mui-radio mui-left"><label>当前地址：</label><input name="serviceAddr" type="radio" checked="checked"><span>'
				+ addr + '</span></div>';
	$('#serviceAddrDiv').append(dv);	
}
function getDateTime(e) {
	var optionsJson = e.getAttribute('data-options') || '{}';
	var options = JSON.parse(optionsJson);
	var oriV = e.value;
	if (oriV) {
		options.value = oriV;
	}

	/*
	 * 首次显示时实例化组件 示例为了简洁，将 options 放在了按钮的 dom 上 也可以直接通过代码声明 optinos 用于实例化
	 * DtPicker
	 */
	var picker = new mui.DtPicker();
	picker.show(function(rs) {
		/*
		 * rs.value 拼合后的 value rs.text 拼合后的 text rs.y 年，可以通过 rs.y.vaue 和
		 * rs.y.text 获取值和文本 rs.m 月，用法同年 rs.d 日，用法同年 rs.h 时，用法同年 rs.i 分（minutes
		 * 的第二个字母），用法同年
		 */
		e.value = rs.text;
		e.blur();
		/*
		 * 返回 false 可以阻止选择框的关闭 return false;
		 */
		/*
		 * 释放组件资源，释放后将将不能再操作组件 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
		 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
		 */
		picker.dispose();
	});
}