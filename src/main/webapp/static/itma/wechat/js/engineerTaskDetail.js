/** 
 * @Description: the script 智能运维平台 -- 抢单列表页
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 	mui.init({
		swipeBack:true //启用右滑关闭功能
	});
require.config({
	paths: { 
		jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader'
	},
	urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) { 
	var userId = getUrlParamByArray('userId');
	var orderId = getUrlParamByArray('orderId');
	if (!userId || !orderId) {
		return;
	}
    sessionStorage.setItem('wxUserId',userId);	
	$('#btnAccept,#btnAccept1').on('click',function(){
		$.ajax({
			type : "get",
			url : "http://wechat.itma.com.cn/jeesite/f/weixin/excuteTaskFromEngineer",
			data : {
				'userId' : userId,
				'orderId' : orderId
			},
			dataType : 'jsonp',
			jsonpCallback : 'jsonpCallback',
			success : function(data) {
				if(data.errCode=="1"){
					mui.alert(data.errMsg);
				}else
				{
					wx.closeWindow();
				}
			},
			error : function(e) {
				mui.toast('error :' + e.message);
			}
		});
	});
	
	$.ajax({
		type : "get",
		url : "http://wechat.itma.com.cn/jeesite/f/weixin/getTaskDetailFromEngineer",
		data : {
			'userId' : userId,
			'orderId' : orderId
		},
		dataType : 'jsonp',
		jsonpCallback : 'jsonpCallback',
		success : function(data) {
			$('#custName').text(data.customerName);
			$('#custQuestion').text(data.callLog);
			$('#custContract').text(data.contract);
			$('#serviceDate').text(data.serviceDate);

			if(data.dicts){
				for(var i = 0 ; i < data.dicts.length; i ++){
					var dic = data.dicts[i];
					var img = '<li class="mui-table-view-cell mui-media mui-col-xs-4"><img class="mui-media-object" src="'
						+ dic.label
						+'"></li>';
					$('#ulImg').append(img);
				}
			}
		},
		error : function(e) {
			mui.toast('error :' + e);
		}
	});
});
function getUrlParamByArray(name) {
	var args = new Object();
	var query=location.search.substring(1);//获取查询串   
	    var pairs=query.split("&");//在逗号处断开   
	    for(var i=0;i<pairs.length;i++) 
	    {   
	        var pos=pairs[i].indexOf('=');//查找name=value   
	        if(pos==-1)   continue;//如果没有找到就跳过   
	        var argname=pairs[i].substring(0,pos);//提取name   
	        var value=pairs[i].substring(pos+1);//提取value   
	        args[argname]=unescape(value);//存为属性   
	    }
	if(args[name]){
		return args[name];
	}
    return '';
}