﻿/** 
 * @Description: the script 智能运维平台 -- 任务详情
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */

mui.init({
	swipeBack : false
// 启用右滑关闭功能
});
 require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});
require(['jquery', 'wechatHideHeader'], function($) {
	var grabOrderDetailSection = $('#grabOrderDetailSection');
	var orderId = getUrlParamByArray("orderId");
	//页面初始的时候加载数据
	$(document).ready(function() {
		$.ajax({
			type: "get",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat03grabOrderDetail.txt",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/getGrabTaskContent?orderId="+ orderId,
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				if(data.orderId == orderId){ 
					$('#customerName').text(data.customerName);
					$('#customerAddr').attr('href', data.addressLink).html(data.customerAddress);
					$('#customerMoblie').attr('href', data.phoneLink).html(data.customerPhone);
					$('#serviceType').text(data.serviceType);
					$('#faultType').text(data.faultType);
					$('#price').text(data.price);
					$('#callInTime').text(data.callInTime);
					$('#callRecord').text(data.callLog);
				}else{
					mui.alert('请求错误');
				}
			},
			error: function(xhr,status,e) {
				mui.toast(xhr.status + ":" + status);
			}
		});
	});
});

function checkOrder(){
		var orderId = getUrlParamByArray("orderId");
    var userId  = getUrlParamByArray("userId");
//    alert("orderId="+orderId+",userId="+userId);
		//表单赋值
		var formData = {
			"orderId": orderId,
			"userId":  userId
		};	
		$.ajax({
			type: "POST",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/GrabTask",
			data: formData,
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
//				alert(data.errMsg+"---formId="+data.formId);				
				if(data.errCode == "0"){ 
					sessionStorage.setItem('formId', data.formId);
                                        //跳转到回单页	
					window.location.href ="http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/receiptOrderInput.html?formId="+data.formId+"&orderId="+data.orderId;
				}else{
					mui.alert(data.errMsg);
					//跳转回上一页
					window.history.back();
				}
			},
			error: function(xhr,status,e) {
				mui.toast(xhr.status + ":" + status);
			}
		});
	
};

function getUrlParamByArray(name) {
	var args = new Object();
	var query=location.search.substring(1);//获取查询串   
	    var pairs=query.split("&");//在逗号处断开   
	    for(var i=0;i<pairs.length;i++) 
	    {   
	        var pos=pairs[i].indexOf('=');//查找name=value   
	        if(pos==-1)   continue;//如果没有找到就跳过   
	        var argname=pairs[i].substring(0,pos);//提取name   
	        var value=pairs[i].substring(pos+1);//提取value   
	        args[argname]=unescape(value);//存为属性   
	    }
	if(args[name]){
		return args[name];
	}
    return '';
}
