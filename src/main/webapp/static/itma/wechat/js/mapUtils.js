function getAddress(lng, lat) {
	var deferred = $.Deferred();
	AMap.service('AMap.Geocoder', function() {
		var geocoder = new AMap.Geocoder();
		var lnglat = [lng, lat ];

		geocoder.getAddress(lnglat, function(status, result) {
			if (status === 'complete' && result.info === 'OK') {
				deferred.resolve(result.regeocode.formattedAddress);
			} else {
				deferred.reject("");
			}
		});

	});

	return deferred.promise();
}


wx.ready(function(){
	var dfd = $.Deferred(); 
	wx.getLocation({
	    type: 'wgs84', 
	    success: function (res) {
	    	var myLocation = new Object();  
	    	myLocation.latitude = res.latitude; 
	    	myLocation.longitude = res.longitude;
	    	dfd.resolve(myLocation); 
	    }
	}); 
	
	$.when(dfd.promise()).then(function( myLocation ) {
			$(document).ready(function(){ 
				$.when(getAddress(myLocation.longitude, myLocation.latitude)).then(function(address) {
					if(doAddress){
						doAddress(address);
					}
				}, function(address) {
					alert('get address failed!'); 
				}); 
			}); 
	}); 
}); 

