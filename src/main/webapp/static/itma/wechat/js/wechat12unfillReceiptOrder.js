/** 
 * @Description: the script 智能运维平台 -- 指派任务
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
mui.init({
	swipeBack : true
// 启用右滑关闭功能
});
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) { 
	var unfillReceiptOrderList = $('#unfillReceiptOrderList');

	$.extend({'searchList':function(userId,param){
		//mui.toast(userId);
		//ajax提交数据		
		$.ajax({
			type: "post",		
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat12unfillReceiptOrder.txt",
			url:"http://wechat.itma.com.cn/jeesite/f/weixin/unfillReceiptOrder",
			data: {                                 
				"searchParam" : param,
				"userId":userId
			},
			dataType: "jsonp",
			jsonpCallback: 'jsonpCallback',
			success: function(data) { 
				if (data.status.errCode == "0") { 
					if(data.listTtemData.length==0){
						mui.alert('未查到待填写回单');
						wx.closeWindow();
						return;
					}
					
					//最近维修记录
					unfillReceiptOrderList.empty();

					for(var i = 0; i < data.listTtemData.length; i++){
						var listTtemStr = 
							'<li class="mui-table-view-cell mui-media" id="'+data.listTtemData[i].orderId+'">'+
								'<a href="'+data.listTtemData[i].orderHref+'">'+
									'<div class="mui-table-cell mui-col-xs-4 mui-pull-left">'+
										'<p class="mui-h6 mui-ellipsis">'+data.listTtemData[i].orderTime+'</p>'+
										'<div class="mui-h5">积分:'+data.listTtemData[i].orderPrice+'</div>'+
									'</div>'+
									'<div class="mui-table-cell mui-col-xs-8 mui-text-right mui-pull-right">'+
									'<h5 class="mui-ellipsis">'+data.listTtemData[i].orderTitle+'</h5>'+
										'<div class="mui-h5">'+data.listTtemData[i].orderSubTitle+'</div>'+
									'</div>'+
								'</a>'+
							'</li>';					

							unfillReceiptOrderList.append(listTtemStr).css('display','block');
					}
						
				} else {
					mui.toast('\u7f51\u7edc\u9519\u8bef\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5'); //网络错误，请稍后再试
				}
			}			
		});

	}});


	$(document).ready(function(){ 
		var userId = sessionStorage.wxUserId;
		if(userId == "" || typeof(userId) == "undefined" || !userId){
			mui.alert('未获取到用户相关信息，请重新访问。');
			if(wx){
				wx.closeWindow();
			}
			return;
		}
		$.searchList(userId ,"" );
		$('#searchButton').on('click',function(){
			var searchParam = $('#orderNo').val();
			$.searchList(userId,searchParam );	
		});
	});
});
function getUrlParamByArray(name) {
	var args = new Object();
	var query=location.search.substring(1);//获取查询串   
	    var pairs=query.split("&");//在逗号处断开   
	    for(var i=0;i<pairs.length;i++) 
	    {   
	        var pos=pairs[i].indexOf('=');//查找name=value   
	        if(pos==-1)   continue;//如果没有找到就跳过   
	        var argname=pairs[i].substring(0,pos);//提取name   
	        var value=pairs[i].substring(pos+1);//提取value   
	        args[argname]=unescape(value);//存为属性   
	    }
	if(args[name]){
		return args[name];
	}
    return '';
}
