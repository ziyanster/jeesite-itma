/** 
 * @Description: the script 智能运维平台 -- 填写回单
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: 'bust=' + (new Date()).getTime()
});

require(['jquery', 'wechatHideHeader'], function($) { 
	$('div.loading-wrap').delay(3000).hide(); 
	
	$('.filter-bar').on('click','#deviceNo',function(){
		$(this).parent().find('span.tip').hide();
	})
	
	var orderDetail = $('#orderDetail'),
		listTtems = $('#listTtems'),
		historyList = $('#historyList');
	
	$('#searchButton').on('click',function(){
		var deviceNo = $('#deviceNo').val(),
			tip = $('.filter-bar').find('.tip');
			
		//是否填写表单
		if(deviceNo ==  ""){
			tip.eq(0).show().text('\u8bf7\u8f93\u5165\u8bbe\u5907\u7f16\u53f7'); //请输入设备编号
			return false;
		}else{
			//ajax提交数据
			$.ajax({
				type: "get",
				url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat06repairHistoryList.txt",
				data: {"deviceNo" : deviceNo},
				dataType: "jsonp",
				jsonpCallback: 'jsonpCallback',
				success: function(data) {
					console.log(data.listTtemData.length)
					if (data.status[0].status == "1") {
						
						//订单详情
						var orderDetailStr = '<span class="pic"><img src="'+data.orderDetail[0].orderPicSrc+'" alt="" /></span>'+
											 '<p>'+data.orderDetail[0].deviceName+'</p>'+
											 '<p>'+data.orderDetail[0].ordertext+'</p>';  
						orderDetail.append(orderDetailStr).show();
						
						//最近维修记录
						for(var i = 0; i < data.listTtemData.length; i++){
							var listTtemStr = '<li class="list-item clearfix" id="'+data.listTtemData[i].orderId+'">'+
												'<a href="'+data.listTtemData[i].orderHref+'">'+
													'<span class="num">'+data.listTtemData[i].orderNo+'</span>'+
													'<span class="pic"><img src="'+data.listTtemData[i].orderPicSrc+'" alt="" /></span>'+
													'<span class="title">'+data.listTtemData[i].orderTitle+'</span>'+
													'<span class="date">'+data.listTtemData[i].orderDate+'</span>'+
												'</a>'+
											  '</li>';
							historyList.css('display','block');					  
							listTtems.append(listTtemStr);
						}
						 
					} else {
						alert('\u7f51\u7edc\u9519\u8bef\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5'); //网络错误，请稍后再试
					}
				}
			}); 
		}
		
		//alert(deviceNo);
	});
	
	
});