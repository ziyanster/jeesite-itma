/**
 * @Description: the script 智能运维平台 -- 填写回单页
 * @authors: bradenhan (bradenhan@126.com)
 * @date： 2015-09-17 22:04:29
 * @version： 1.0
 */
mui.init({
	swipeBack : false
// 启用右滑关闭功能
});
require.config({
	paths : {
		jquery : 'jquery.min.2.1.4',
		wechatHideHeader : 'wechatHideHeader'
	},
	urlArgs : "bust=" + (new Date()).getTime()
});


var contentPicker = new mui.PopPicker();
var resultPicker = new mui.PopPicker();
var servicePicker = new mui.PopPicker();
var faultPicker = new mui.PopPicker();
require(
		[ 'jquery', 'wechatHideHeader' ],
		function($) {
			mui('.mui-scroll-wrapper').scroll({
				indicators: true //是否显示滚动条
			});
			var allReportData;
			contentPicker.setData([{
				'text': '性能调整',
				'value': '性能调整'
			}, {
				'text': '数据库故障',
				'value': '数据库故障'
			}, {
				'text': '系统故障',
				'value': '系统故障'
			}, {
				'text': '中间件',
				'value': '中间件'
			}, {
				'text': '硬件故障',
				'value': '硬件故障'
			}, {
				'text': '初次巡检',
				'value': '初次巡检'
			}, {
				'text': '定期巡检',
				'value': '定期巡检'
			}, {
				'text': '预防维护',
				'value': '预防维护'
			}]);
			resultPicker.setData([{
				'text': '进一步跟踪问题',
				'value': '0'
			}, {
				'text': '请求支持',
				'value': '1'
			}, {
				'text': '工作完成',
				'value': '2'
			}]);
			var orderId = getUrlParamByArray("orderId");
			var formId = getUrlParamByArray("formId");
			sessionStorage.setItem('orderId',orderId);
			sessionStorage.setItem('formId',formId);
			var custNo;
			$.ajax({
					type: "get",
					url: "http://wechat.itma.com.cn/jeesite/f/weixin/getGrabTaskContent?orderId=" + orderId,
					dataType: 'jsonp',
					jsonpCallback: 'jsonpCallback',
					success: function(data) {
						if (typeof(data.orderId) != "undefined" && data.orderId == orderId) {
							custNo = data.customerNo;
							$('#customerName').text(data.customerName);
							$('#customerAddr').attr('href', data.addressLink).html(data.customerAddress);
							$('#customerMoblie').attr('href', data.phoneLink).html(data.customerPhone);
							$('#serviceType').text(data.serviceType);
							if(data.serviceType==""){
								var services = [];
								for(var i = 0 ;i < data.dicts.length;i++){
									var dic = data.dicts[i];
									services.push({'text':dic.label,'value':dic.value});					
								}
								servicePicker.setData(services);								
								$("#serviceDiv").show();
							}else{
								$('#serviceDiv').hide();
							}
							$('#faultType').text(data.faultType);
							if(data.faultType==""){								
								var faultTypes = [];
								for(var i = 0 ;i < data.dicts1.length;i++){
									var dic = data.dicts1[i];
									faultTypes.push({'text':dic.label,'value':dic.value});					
								}
								faultPicker.setData(faultTypes);
								$("#faultDiv").show();
							}else{
								$('#faultDiv').hide();
							}
							$('#price').text(data.price);
							$('#callInTime').text(data.callInTime);
							$('#callRecord').text(data.callLog);
							$('#responseTime').text(data.orderResponseTime);
							$('#custImg1').attr('src',data.orderpicLink1);
							$('#custImg2').attr('src',data.orderpicLink2);

							if(data.workType == "XJ"){
								$('#deviceInfo').show();
							}else{
								$('#deviceInfo').hide();
							}
						} else {
							//mui.alert(window.location.href);
							mui.alert('请求错误')
						}
					},
					error: function(xhr,status,e) {
						mui.toast(xhr.status + ":" + status);
					}
				});

				$('#reportList').on('tap','li',function(){
					var no = $(this).attr('id');
					var devices = $('#devices .mui-input-row');
					devices.find("input").val('').end().find("select").val("00");
					var index = 0;
					for(var i = 0 ; i < allData.length; i++){
						if(allData[i].label == no){
							devices.eq(index++).find("input").val(allData[i].remarks)
											.end().find("select").val(allData[i].type);
						}
					}
					mui('#reports').popover("hide");
				});
				$('#devices').on('click',function(){
					mui('#reports').popover("hide");					
				});
				$('#modalClose').on('tap',function(){
					var inputs = $('#devices .mui-input-row').find("input");
					var hasValue = false;
					for (var i = 0; i < inputs.length ; i++) {
						if(inputs[i].value != ""){
							hasValue = true;
							break;
						}
					}
					if(hasValue){
						$('#addDevices').html('查看');
					}else{
						$('#addDevices').html('新增');
					}
					
				});
				$('#getReports').on('tap', function() {
					$.ajax({
						type: "get",
						url: "http://wechat.itma.com.cn/jeesite/f/weixin/getXjReports?custNo="+custNo,
						dataType: 'jsonp',
						jsonpCallback: 'jsonpCallback',
						success: function(data) {
							allData = data.dicts;
							var result = [];
							var temp = [];
							for(var i = 0 ; i < data.dicts.length ; i++){
								var dict = data.dicts[i];
								if(result.indexOf(dict.label) >= 0){
									continue;
								}
								result.push(dict.label);
								temp.push(dict);
							}							
							result = [];							
							for (var i = 0; i < temp.length; i++) {
								var dict = temp[i];
								var listTtemStr =
									'<li class="mui-table-view-cell mui-media" id="' + dict.label + '">' +
									'<a>' +
									'<div class="mui-table-cell mui-col-xs-4 mui-pull-left">' +
									'<p class="mui-h6 mui-ellipsis">' + dict.label + '</p>' +
									'<div class="mui-h5">日期:' + dict.value + '</div>' +
									'</div>' +
									'<div class="mui-table-cell mui-col-xs-8 mui-text-right mui-pull-right">' +
									'<h5 class="mui-ellipsis">' + dict.description + '</h5>' +
									'<div class="mui-h5">' + dict.remarks + '</div>' +
									'</div>' +
									'</a>' +
									'</li>';
								result.push(listTtemStr);
							}
							$('#reportList').append(result.join(''));
							mui('#reports').popover("show");
						},
						error: function(xhr, status, e) {
							mui.toast(xhr.status + ":" + status);
						}
					});
				});
				$('#returnOrder').on('click',function(){
					window.location.href = 'http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/abandonOrder.html?formId='+formId;
				});
				
				$('#completeBtn').on('click',function(){
					var inputs = $('#devices .mui-input-row').find("input:not('.errCode')");
					var selects = $('#devices .mui-input-row').find("select:not('.errType'):not('.errDes')");
					var devices = [];
					for (var i = 0;i<inputs.length;i++) {
						if(inputs[i].value != ""){
							devices.push(selects[i].value+"$"+inputs[i].value);
						}
					}
					// 正常设备
					var device = devices.join(";");
					// 其他设备
					var qtsb = $('#qtsb').val();
					// 异常设备
					var errInputs = $('input.errCode');
					var errSelects = $('select.errType');
					var errDes = $('input.errDes');
					devices = [];
					for (var i = 0;i<errInputs.length;i++) {
						if(errInputs[i].value != ""){
							devices.push(errSelects[i].value+"$"+errInputs[i].value+"$"+errDes[i].value);
						}
					}
					var errDevice = devices.join(';');					
					
					
					//声明表单变量
					var arriveTime = $('#arriveTime').val(),
						backTime = $('#backTime').val(),
						workContent = $('#workContent').val(), 
						solveProblem = $('#questionDes').val(),  
						dealProg = $('#dealProg').val(),  
						workResult = $('#workRs').val(),
						serviceType, faultType;			
					
					//是否填写表单
					if(workContent == ""){
						mui.toast('\u8bf7\u8f93\u5165\u5de5\u4f5c\u5185\u5bb9'); //请输入工作内容
						return false;
					}else if(solveProblem == ""){
							mui.toast('\u8bf7\u8f93\u5165\u95ee\u9898\u63cf\u8ff0'); //请输入问题描述
							return false;
					}else if(dealProg == ""){
						mui.toast('\u8bf7\u8f93\u5165\u5904\u7406\u8fc7\u7a0b'); //请输入处理过程
						return false;
					}else if(arriveTime == ""){
							mui.toast('\u8bf7\u8f93\u5165\u5230\u8fbe\u65f6\u95f4'); //请输入到达时间
							return false;
					}else if(workResult == ""){
							mui.toast('\u8bf7\u786e\u5b9a\u5de5\u4f5c\u7ed3\u679c'); //请确定工作结果
							return false;
					}
					if($('#serviceType').text()=="" && !$('#serviceDiv').is(':hidden')){
						serviceType = $("#serviceTypeCode").val();
						if(serviceType ==""){
							mui.toast('请选择服务类型'); //请输入服务类型
							return false;							
						}
					}
					if($('#faultType').text()=="" && !$('#faultDiv').is(':hidden')){
						faultType = $("#faultTypeCode").val();
						if(faultType ==""){
							mui.toast('请选择故障类型'); //请输入故障类型
							return false;			
						}
					}
					var imgsArr = [];
					var imgs = $('#imgList img');
					for(var i = 0; i < imgs.length ; i++){
						imgsArr.push(imgs[i].src);
					}
			        //表单赋值
					var formData = {
						"formId":formId,
			                        //"formId":"f29ecf92fa4e4188844da1e80794efc0",
			            "arriveTime": arriveTime,
			            "backTime": backTime,
						"workContent": workContent,
						"solveProblem": solveProblem,
						"dealProg": dealProg,
						"workResult": workResult,
						"serviceType": serviceType,
						"faultType": faultType,
						"imgs" : imgsArr.join(';'),
						"device" : device,
						"errDevice" : errDevice,
						"qtsb" : qtsb
					};
					//ajax提交数据
					$.ajax({
						type: "post",
						//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat05fillReceiptOrder.txt",
						url:"http://wechat.itma.com.cn/jeesite/f/weixin/fillReceiptOrder",
			            data: formData,
						dataType: "jsonp",
						jsonpCallback: 'jsonpCallback',
						success: function(data) { 
							alert(data.errMsg);				
							if(data.errCode == "0"){ 
								//跳转到待回单页
			                                        window.location.href ="http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/unfillReceiptOrder.html";
							} else {
			                                        window.location.reload()
							}
						}
					}); 
				});
		});

function getWorkContent(e) {
	contentPicker.show(function(items) {
		e.value = items[0].text;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getWorkResult(e) {
	resultPicker.show(function(items) {
		e.value = items[0].text;
		var rs = document.getElementById('workRs');
		rs.value = items[0].value;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getServiceType(e) {
	servicePicker.show(function(items) {
		e.value = items[0].text;
		document.getElementById('serviceTypeCode').value = items[0].value;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getFaultType(e) {
	faultPicker.show(function(items) {
		e.value = items[0].text;
		document.getElementById('faultTypeCode').value = items[0].value;
		e.blur();
		//返回 false 可以阻止选择框的关闭
		//return false;
	});
}

function getUrlParamByArray(name) {
	var args = new Object();
	var query=location.search.substring(1);//获取查询串   
	    var pairs=query.split("&");//在逗号处断开   
	    for(var i=0;i<pairs.length;i++) 
	    {   
	        var pos=pairs[i].indexOf('=');//查找name=value   
	        if(pos==-1)   continue;//如果没有找到就跳过   
	        var argname=pairs[i].substring(0,pos);//提取name   
	        var value=pairs[i].substring(pos+1);//提取value   
	        args[argname]=unescape(value);//存为属性   
	    }
	if(args[name]){
		return args[name];
	}
    return '';
}
function getDateTime(e) {
	var optionsJson = e.getAttribute('data-options') || '{}';
	var options = JSON.parse(optionsJson);
	var oriV = e.value;
	if (oriV) {
		options.value = oriV;
	}

	/*
	 * 首次显示时实例化组件 示例为了简洁，将 options 放在了按钮的 dom 上 也可以直接通过代码声明 optinos 用于实例化
	 * DtPicker
	 */
	var picker = new mui.DtPicker();
	picker.show(function(rs) {
		/*
		 * rs.value 拼合后的 value rs.text 拼合后的 text rs.y 年，可以通过 rs.y.vaue 和
		 * rs.y.text 获取值和文本 rs.m 月，用法同年 rs.d 日，用法同年 rs.h 时，用法同年 rs.i 分（minutes
		 * 的第二个字母），用法同年
		 */
		e.value = rs.text;
		e.blur();
		/*
		 * 返回 false 可以阻止选择框的关闭 return false;
		 */
		/*
		 * 释放组件资源，释放后将将不能再操作组件 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
		 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
		 */
		picker.dispose();
	});
}