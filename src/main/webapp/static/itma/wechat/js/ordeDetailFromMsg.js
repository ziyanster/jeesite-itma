﻿/** 
 * @Description: the script 智能运维平台 -- 任务详情
 * @authors: bradenhan (bradenhan@126.com)
 * @date：    2015-09-17 22:04:29
 * @version： 1.0
 */
 
require.config({ 
    paths : {
    	jquery: 'jquery.min.2.1.4',
		wechatHideHeader: 'wechatHideHeader', 
    },
    urlArgs: "bust=" + (new Date()).getTime()
}); 

require(['jquery', 'wechatHideHeader'], function($) {
	var grabOrderDetailSection = $('#grabOrderDetailSection'),
		loadingWrap = $('div.loading-wrap'),  
		errorWrap = $('div.error-wrap'); 



	//页面初始的时候加载数据
	$(document).ready(function() {
		//alert("ajax start ...");
		$.ajax({
			type: "get",
			//url: "http://4.bradenwork.sinaapp.com/poject/intelligentOperationData/wechat03grabOrderDetail.txt",
			url: "http://wechat.itma.com.cn/jeesite/f/weixin/getGrabTaskContent?orderId="+sessionStorage.orderId,
			dataType: 'jsonp',
			jsonpCallback: 'jsonpCallback',
			success: function(data) {
				loadingWrap.delay(3000).hide();   
				if(data.orderId == sessionStorage.orderId){ 
					var grabOrderStr = '<ul class="grabOrder-summary">'+
											'<li>客户名称：'+data.customerName+'</li>'+
											'<li class="address"><a href="'+data.addressLink+ '?address='+ encodeURIComponent(data.customerAddress)+ '">客户地址：'+data.customerAddress+'</a></li>'+
											'<li class="telphone"><a href="tel:'+data.phoneLink+'">联系方式：'+data.customerPhone+'</a></li>'+
											'<li>服务类型：'+data.serviceType+'</li>'+
											'<li>故障类型：'+data.faultType+'</li>'+
											'<li class="price">'+data.price+'</li>'+
										'</ul>'+
							
										'<div class="part-header">'+
											'<h3 class="part-title">'+data.callTitle+'</h3>'+
										'</div>'+
							
										'<div class="call-log">'+
											'<p>'+data.callLog+'</p>'+
										'</div>';

					grabOrderDetailSection.css('display', 'block');
					grabOrderDetailSection.append(grabOrderStr);
				}else{
					alert('请求错误');
				}
			},
			error: function() {
				loadingWrap.hide();
				errorWrap.show();
			}
		});
	});
});
