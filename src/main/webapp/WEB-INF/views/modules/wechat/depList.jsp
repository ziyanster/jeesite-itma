<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>部门用户</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			var id = $('#selectId', parent.document).val();
			var dep = parent.window.selectNode;
			if(id != ""){
				$('#btnSend').removeAttr('disabled');
				$('#btnUpdate').removeAttr('disabled');
				if(id != "1")
				{
					$('#btnDel').removeAttr('disabled');
				}else{
					$('#btnDel').attr('disabled','disabled');					
				}
			}else{
				$('#btnDel').attr('disabled','disabled');
				$('#btnUpdate').attr('disabled','disabled');
				$('#btnSend').attr('disabled','disabled');						
			}
			if(dep)
			{
				$('#depName').html(dep.name);
			}
			$('#btnUpdate').on('click',function(){
				window.location.href = "${ctx}/wechat/depForm?depId="+id+"&parentid="+ dep.pId+"&name=" + dep.name;
			});
			$('#btnSend').on('click',function(){
				window.location.href = '${ctx}/cms/article/form?category.name=%e5%85%ac%e5%8f%b8%e6%96%b0%e9%97%bb&id=&category.id=11&scopeOffice='+id+'&scopeOfficeName='+ escape(dep.name);
			});
			$('#btnDel').on('click',function(){
				var submit = function (v, h, f) {
				    if (v == 'ok'){
						$.ajax({
							url : '${ctx}/wechat/deleteDepartment?depId='+id,
							type : 'post',
							dataType : 'jsonp',
							jsonpCallback : 'jsonpCallback',
							success : function(data){
								if(data == "Success"){
									refreshTree();
								}else{
									alert(data);
								}
							},
							error : function(xhr,status,e){
								alert(xhr + ":" + e);						
							}
						});
				    }
				    else if (v == 'cancel'){
				    	
				    }
				    return true; //close
				};
				$.jBox.confirm("确定删除部门吗？", "提示", submit);
			});
			$('#btnSubmit').on('click',function(){
				var name = $('#name').val();
				var trs = $('tbody tr');
				var nameTds = trs.find('td:first');
				
				if(name != ""){
					for(var i = 0; i < nameTds.length; i++){
						var tdV = nameTds[i].innerHTML;
						if(tdV.indexOf(name) < 0){
							$(nameTds[i]).parent('tr').hide();
						}
					}
				}else{
					trs.show();
				}
				$('tbody td').css('background-color','#fdfdfd');
				$('tbody tr:visible').filter(':odd').find('td').css('background-color','#f9f9f9');
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/wechat/depList">部门用户列表</a></li>
		<li><a href="${ctx}/wechat/depForm">部门编辑</a></li>
		<li><a href="${ctx}/wechat/depSync">部门同步</a></li>
	</ul>
	<div id="searchForm" class="breadcrumb form-search ">
		<ul class="ul-form">
			<li><label>部门名称：</label>
			<label id="depName" style="width:150px"></label>
			</li>
			<li><label>姓&nbsp;&nbsp;&nbsp;名：</label>
			<input id="name" type="text" value="" />
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="button" value="查询用户"/>
				<input id="btnUpdate" class="btn btn-primary" type="button" value="更新部门" disabled="disabled"/>
				<input id="btnDel" class="btn btn-primary" type="button" value="删除部门" disabled="disabled"/>
				<input id="btnSend" class="btn btn-primary" type="button" value="发送消息" disabled="disabled"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</div>	
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead><tr><th>姓名</th><th>帐号</th><th>微信号</th><th class="sort-column login_name">职位</th><th class="sort-column name">手机</th><th>email</th></tr></thead>
		<tbody>
		<c:forEach items="${list}" var="user">
			<tr>
				<td>${user.name}</td>
				<td>${user.id}</td>
				<td>${user.weChatId}</td>
				<td>${user.loginName}</td>
				<td>${user.mobile}</td>
				<td>${user.email}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>
</html>