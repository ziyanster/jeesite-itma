<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>部门同步</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			var id = $('#selectId', parent.document).val();
			$("#btnImport").click(function(){
				$.jBox($("#importBox").html(), {title:"上传同步文件", buttons:{"关闭":true}, 
					bottomText:"上传文件应为所下载的模板文件！"});
			});	
			$('#btnSyncUser').on('click',function(){
				$.ajax({
					url : '${ctx}/wechat/syncUser?depId='+id,
					type : 'post',
					dataType : 'jsonp',
					jsonpCallback : 'jsonpCallback',
					success : function(data){
						if(data == "Success"){
							refreshTree();
						}else{
							alert(data);
						}
					},
					error : function(xhr,status,e){
						alert(xhr + ":" + e);						
					}
				});				
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/wechat/depList">部门用户列表</a></li>
		<li><a href="${ctx}/wechat/depForm">部门添加</a></li>
		<li class="active"><a href="${ctx}/wechat/depSync">部门同步</a></li>
	</ul>	
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/wechat/uploadAndSync" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在上传，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   上    传   "/>
			<a href="http://qydev.weixin.qq.com/download/batch_party_sample.csv">下载模板</a>
		</form>
	</div>
	<div id="searchForm"  class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns">
				<input id="btnImport" class="btn btn-primary" type="button" value="上传模板并执行同步部门"/>
				<input id="btnSyncUser" class="btn btn-primary" type="button" value="同步用户"/>
			</li>
			<li class="clearfix"></li>
		</ul>
	</div>
</body>
</html>