<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>${category.name}</title>
	<meta name="decorator" content="cms_default_${site.theme}"/>
	<meta name="description" content="${category.description}" />
	<meta name="keywords" content="${category.keywords}" /> 
	<script>!function(c){function f(){var a=g.getBoundingClientRect().width;540<a/b&&(a=540*b);c.rem=a/16;Object.defineProperty(window,"rem",{value:c.rem,writable:!1});g.style.fontSize=c.rem+"px"}var b,a,e,g=document.documentElement,d=document.querySelector('meta[name="viewport"]');b||a||(a=c.navigator.appVersion.match(/android/gi)||c.navigator.appVersion.match(/iphone/gi),b=c.devicePixelRatio,b=a?3<=b?3:2<=b?2:1:1,a=1/b);d?(d=d.getAttribute("content").match(/initial\-scale=([^\,$]+)/))&&(a=parseFloat(d[1]),b=parseInt(1/a)):(d=document.createElement("meta"),d.setAttribute("name","viewport"),d.setAttribute("content","initial-scale="+a+", maximum-scale="+a+", minimum-scale="+a+", user-scalable=no"),g.firstElementChild.appendChild(d));Object.defineProperty(window,"dpr",{value:b,writable:!1});c.addEventListener("resize",function(){clearTimeout(e);e=setTimeout(f,300)},!1);c.addEventListener("pageshow",function(a){a.persisted&&(clearTimeout(e),e=setTimeout(f,300))},!1);"complete"===document.readyState?document.body.style.fontSize=16*b+"px":document.addEventListener("DOMContentLoaded",function(){document.body.style.fontSize=16*b+"px"},!1);f()}(window);
  function setupFontSize(a){2!==window.dpr&&(a||(a=document.body),a=a.querySelectorAll("[data-changed]"),[].forEach.call(a,function(a){if(!a.style.fontSize){var b=+getComputedStyle(a)["font-size"].replace(/px/,"");a.style.fontSize=b/2*window.dpr+"px"}}))};
		</script>
		<style>
		html,body{color:#222; font-family:Microsoft YaHei,Helvitica,Verdana,Tohoma,Arial,san-serif; background: #77c8f5; margin:0; padding:0; text-decoration:none;}
		body >.tips{position:fixed; display:none; top:50%; left:50%; z-index:100; text-align:center; padding:20px; width:200px;}
		body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0; padding:0;}
		table{border-collapse:collapse; border-spacing:0;}
		fieldset,img{border:0;}
		address,caption,cite,code,dfn,em,th,var{font-style:normal; font-weight:normal;}
		ol,ul{list-style:none outside none; margin:0; padding:0;}
		caption,th{text-align:left;}
		h1,h2,h3,h4,h5,h6{font-size:100%; font-weight:normal;}
		a{color:#fff; text-decoration:none;} 
		 
		/*knowledgeBaseTab*/
		.knowledgeBaseTab{margin: 0;}
		 		
		/* fucus-slide */
		.fucus-slide{-webkit-box-sizing: border-box; box-sizing: border-box; position: relative; width: 100%; height: 4rem; overflow: hidden; margin: 0 auto; paddinsg: 0 .5rem;}
		.fucus-slide .slide-mark{position: absolute; width: 2rem; height: 2rem; line-height: 2rem; bottom: 0; right: 0; z-index: 1; text-align: center;}
		.fucus-slide .slide-mark li{display: inline-block; width: .3rem; height: .3rem; overflow: hidden; margin: 0 0.05rem; -webkit-border-radius: 50%; border-radius: 50%; background: #8f9091; vertical-align: middle; text-indent: -9999px;}
		.fucus-slide .slide-mark .current{background: #0fb08f;}
		.fucus-slide .slide-data{position: relative; z-index: 0;}
		.fucus-slide .slide-data li{position: relative;}
		.fucus-slide .slide-data li img{width: 100%; height: 4rem; display: block;} 
		.fucus-slide .slide-data li .pic-title{-webkit-box-sizing: border-box; box-sizing: border-box; display: block; position: absolute; left: 0; bottom: 0; width: 100%; height: 2rem; overflow: hidden; padding: 0 2.5rem 0 .5rem; background: rgba(0,0,0,.5); font-size: .7rem; line-height: 2rem; color: #fff;}

		
		/* section */
		.section{margin: 0 .5rem;}
		.section .section-header{margin: 1rem 0 0; padding: .2rem .5rem 0; -webkit-border-radius: .2rem .2rem 0 0; border-radius: .2rem .2rem 0 0; border: .025rem solid #666; border-bottom: 0 none; background: #fff; font-size: .6rem; line-height: 1rem;}
		.section .section-header .pull-right{float: right; color: #999; font-size: .4rem;}
		.section .section-header .section-title{font-size: .6rem;}
		.section .news-list{padding: .2rem .5rem .5rem; -webkit-border-radius: 0 0 .2rem .2rem; border-radius: 0 0 .2rem .2rem; border: .025rem solid #666; border-bottom: 0 none; background: #fff;}
		.section .news-list li{height: .9rem; overflow: hidden; margin: .1rem 0 0; font-size: .55rem; line-height: .9rem;}
		.section .news-list li a{color: #666; font-size: .55rem;}
		.section .news-list .pull-right{float: right; padding: 0 0 0 .5rem; color: #999; font-size: .4rem;}
		</style>
		
	<script src="${ctxStaticTheme}/wz/js/TouchSlide.1.1.min.js" ></script>
</head>
<body>
	<div class="knowledgeBaseTab">
	<div id="slideBox" class="fucus-slide">
		<div class="slide-data">
			<ul>
				<li>
					<a class="pic"  href="javascript::"><img src="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/banner1.jpg" /></a> 
				</li>
				<li>
					<a class="pic"  href="javascript::"><img src="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/banner2.jpg" /></a> 
				</li>
				<li>
					<a class="pic"  href="javascript::"><img src="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/banner3.jpg" /></a> 
				</li>
				<li>
					<a class="pic"  href="javascript::"><img src="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/banner4.jpg" /></a> 
				</li>
			</ul>
		</div>

		<div class="slide-mark">
			<ul></ul>
		</div>
	</div>
	<script type="text/javascript">
		TouchSlide({
			slideCell: "#slideBox",
			titCell: ".slide-mark ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
			mainCell: ".slide-data ul",
			effect: "leftLoop",
			autoPage: true, //自动分页
			autoPlay: true,
			delayTime: '1000'
		});
	</script>
	<c:set var="index" value="1" />
	<c:forEach items="${categoryList}" var="tpl">
		<c:if test="${tpl.inList eq '1' && tpl.module ne ''}">
			<c:set var="index" value="${index+1}" /> ${index % 2 eq 0 ? '
			<div class="section">':''}
				<div class="section-header">
					<a href="${ctx}/list-${tpl.id}${urlSuffix}" class="pull-right">更多&gt;&gt;</a>
					<h4 class="section-title">${tpl.name}</h4>
				</div>
				<c:if test="${tpl.module eq 'article'}">
					<ul class="news-list">
						<c:forEach items="${fnc:getArticleList(site.id, tpl.id, 5, '')}" var="article">
							<li><span class="pull-right"><fmt:formatDate value="${article.updateDate}" pattern="yyyy.MM.dd"/></span><a href="${ctx}/view-${article.category.id}-${article.id}${urlSuffix}" style="color:${article.color}">${fns:abbr(article.title,40)}</a></li>
						</c:forEach>
					</ul>
				</c:if>
				<c:if test="${tpl.module eq 'link'}">
					<ul class="news-list">
						<c:forEach items="${fnc:getLinkList(site.id, tpl.id, 5, '')}" var="link">
							<li><span class="pull-right"><fmt:formatDate value="${link.updateDate}" pattern="yyyy.MM.dd"/></span><a target="_blank" href="${link.href}" style="color:${link.color}">${fns:abbr(link.title,40)}</a></li>
						</c:forEach>
					</ul>
				</c:if>
				${index % 2 ne 0 ? '</div>':''}
		</c:if>
	</c:forEach>
</div>
</body>
</html>