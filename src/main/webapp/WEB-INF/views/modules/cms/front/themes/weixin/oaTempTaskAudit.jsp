<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>临时审批任务管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="decorator" content="blank" />
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script src="${pageContext.request.contextPath}/static/jquery-validation/1.11.1/lib/jquery.form.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					//form.submit();
					$(form).ajaxSubmit({
						type: 'post', // 提交方式 get/post
			            url: $('#inputForm').attr('action'), // 需要提交的 url
			            data: $('#inputForm').serialize(),
			            success: function(data) { // data 保存提交后返回的数据，一般为 json 数据
			                alert('提交成功！');
			            	if(wx){
			            		wx.closeWindow();
			            	}else{
			            		window.close();
			            	}
			            },
			            error: function(XmlHttpRequest, textStatus, errorThrown){  
	                        alert( "重复提交");  
	            			closeTip();
	                    } 
					});
					return false; // 阻止表单自动提交事件
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="#">审批</a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="oaTempTask" action="${pageContext.request.contextPath}/f/oa/saveAudit" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<sys:message content="${message}"/>		
		<fieldset>
			<legend>${testAudit.act.taskName}</legend>
			<table class="table-form">
				<tr>
					<td class="tit">申请类型</td><td>${fns:getDictLabel(oaTempTask.type, 'mis_approvalType', '')}</td>
				</tr>
				<tr>					
					<td class="tit">MIS编号</td><td>${oaTempTask.misno}</td>
				</tr>
				<tr>					
					<td class="tit">申请者</td><td>${fns:getUserById(oaTempTask.createBy).name}</td>
				</tr>
				<tr>
					<td class="tit">申请原因</td>
					<td >${oaTempTask.content}</td>
				</tr>
				<tr>
					<td class="tit">缩略图</td>
					<td >			
						<img src="${oaTempTask.image}" class="img-rounded">
					</td>
				</tr>				
				<tr>
					<td class="tit">分管领导意见</td>
					<td >
						${oaTempTask.leaderText}
					</td>
				</tr>	
				<tr>
					<td class="tit">您的意见</td>
					<td>
						<form:textarea path="act.comment" class="required" rows="5" maxlength="20" cssStyle="width:500px"/>
					</td>
				</tr>			
			</table>
		</fieldset>				

		<div class="form-actions">
				<c:if test="${oaTempTask.act.taskDefKey ne 'apply_end'}">
					<input id="btnSubmit" class="btn btn-primary" type="submit" value="同 意" onclick="$('#flag').val('yes')"/>&nbsp;
					<input id="btnSubmit" class="btn btn-inverse" type="submit" value="驳 回" onclick="$('#flag').val('no')"/>&nbsp;
				</c:if>			

			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
		<act:histoicFlow_front procInsId="${oaTempTask.act.procInsId}"/>
	</form:form>
</body>
</html>