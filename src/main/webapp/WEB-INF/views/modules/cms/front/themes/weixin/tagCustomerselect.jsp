<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>数据选择</title>
	<meta name="decorator" content="blank"/>
	<script type="text/javascript">
		//搜索节点
		function selectNode(id,name) {
			// 取得输入的关键字的值
			//var value = $.trim(key.get(0).value);	
			$("#customerno").val(id);
			$("#customername").val(name);
			top.$.jBox.getBox().find("button[value='ok']").trigger("click");
		}
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }		
	</script>
</head>
<body>
	<input type="hidden" id="customerno" value="" />
	<input type="hidden" id="customername" value="" />	
	<div id="tree" class="ztree" style="padding:15px 20px;">
	<form:form id="searchForm" modelAttribute="smartCustomer" action="${pageContext.request.contextPath}/f/customerSearch/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><form:input path="customername" htmlEscape="false" maxlength="20" class="input-medium" /></li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead><tr><th>客户名称</th></tr></thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartCustomer">
			<tr>
				<td>
					<a href="#" onclick="selectNode('${smartCustomer.customerno}','${smartCustomer.customername}');">
						${smartCustomer.customername}
					</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>	
	</div>
</body>