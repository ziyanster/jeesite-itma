<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<html>
<head>
	<title>待办任务</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="decorator" content="blank" />
	<script type="text/javascript">
		$(document).ready(function() {
			$("#userEmail").val(sessionStorage.wxUserId);
		});
		/**
		 * 签收任务
		 */
		function claim(taskId) {
			$.get('${ctx}/act/task/claim' ,{taskId: taskId}, function(data) {
				if (data == 'true'){
		        	top.$.jBox.tip('签收完成');
		            location = '${ctx}/act/task/todo/';
				}else{
		        	top.$.jBox.tip('签收失败');
				}
		    });
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="#">待办任务</a></li>
	</ul>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>标题</th>
				<th>当前环节</th><%--
				<th>任务内容</th> --%>
				<th>流程名称</th>
				<th>创建时间</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${list}" var="act">
				<c:set var="task" value="${act.task}" />
				<c:set var="vars" value="${act.vars}" />
				<c:set var="procDef" value="${act.procDef}" /><%--
				<c:set var="procExecUrl" value="${act.procExecUrl}" /> --%>
				<c:set var="status" value="${act.status}" />
				<tr>
					<td>
						<c:if test="${empty task.assignee}">
							${fns:abbr(not empty act.vars.map.title ? act.vars.map.title : task.id, 60)}
						</c:if>
						<c:if test="${not empty task.assignee}">
							${fns:abbr(not empty vars.map.title ? vars.map.title : task.id, 60)}
						</c:if>
					</td>
					<td>
						${task.name}
					</td><%--
					<td>${task.description}</td> --%>
					<td>${procDef.name}</td>
					<td>
						<fmt:formatDate value="${task.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
					</td>
					<td>
						<a href="${pageContext.request.contextPath}/f/act/form?taskId=${task.id}&taskName=${fns:urlEncode(task.name)}&taskDefKey=${task.taskDefinitionKey}&procInsId=${task.processInstanceId}&procDefId=${task.processDefinitionId}&status=${status}">任务办理</a>
						<a target="_blank" href="${pageContext.request.contextPath}/f/act/trace/photo/${task.processDefinitionId}/${task.executionId}">跟踪</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>
