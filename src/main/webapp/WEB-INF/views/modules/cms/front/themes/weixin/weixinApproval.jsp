<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>
<html>
<head>
	<title>应急审批管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="decorator" content="blank"/>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<br/>
	<form:form id="inputForm" modelAttribute="smartApproval" action="${ctx}/formApproval" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<div class="control-group">
			<label class="control-label">申请者：</label>
			<div class="controls">
				<!--form:input path="createBy.id" htmlEscape="false" maxlength="64" class="input-xlarge "  disabled="true"/-->
				${fns:getUserById(smartApproval.createBy.id).name}
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">申请时间：</label>
			<div class="controls">
				<fmt:formatDate value="${smartApproval.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
			</div>
		</div>			
		<div class="control-group">
			<label class="control-label">MIS编号：</label>
			<div class="controls">
				${smartApproval.misno}
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">申请类型：</label>
			<div class="controls">
				${fns:getDictLabel(smartApproval.type, 'mis_approvalType', '')}
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">申请信息：</label>
			<div class="controls">
				${smartApproval.remarks}			
			</div>
		</div>		

		<div class="form-actions">
			<a href="${ctx}/agreeApproval?id=${smartApproval.id}" onclick="return confirmx('确认同意该应急审批吗？', this.href)">同意</a>
    		&nbsp;&nbsp;&nbsp;
    		<a href="${ctx}/disagreeApproval?id=${smartApproval.id}" onclick="return confirmx('确认不同意该应急审批吗？', this.href)">不同意</a>
		</div>
	</form:form>
</body>
</html>