<%@ page contentType="text/html;charset=UTF-8" %>
	<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
		<html>

		<head>
			<title>知识库管理</title>
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
			<meta name="decorator" content="blank" />
			<style>
			body{position: relative; width: 100%; overflow: hidden;}
			#right{-webkit-box-sizing: border-box; box-sizing: border-box; float: none; position: absolute; top: 0; right: 0; width: 100%!important; padding: 0 5px 0 25px!important;}
			.left-wrap{position: absolute; top: 0; left: 0; z-index: 100; padding: 0 0 0 21px; -webkit-transform: translateX(-100%); transform: translateX(-100%); -webkit-transition: all 1s ease; transition: all 1s ease;}
			#openClose{position: relative; z-index: 101; width: 20px; overflow: hidden; background: #efefef; opacity: 1!important;}
			#openClose.close:after{content:''; position: absolute; left: -6px; top: 50%; width: 0; height: 0; overflow: hidden; border: 10px solid #efefef; border-right-color: #333;}
			#openClose, #openClose.close{background: #efefef}
			#openClose.open:after{content:''; position: absolute; left: 7px; top: 50%; width: 0; height: 0; overflow: hidden; border: 10px solid #efefef; border-left-color: #333;}
			body.haslayer .left-wrap{-webkit-transform: translateX(0); transform: translateX(0);}
			*{-webkit-tap-highlight-color:rgba(255,0,0,0); outline: 0 none;}
			</style>
		</head>

		<body>
			<div id="content" class="row-fluid"> 
				<div class="left-wrap" style="width: 60%;">
					<iframe id="cmsMenuFrame" name="cmsMenuFrame" src="${pageContext.request.contextPath}/f/knowledgeTree" style="overflow:visible;" scrolling="yes" frameborder="no" width="100%"></iframe>
				</div>
				<div id="openClose" class="close">&nbsp;</div>
			</div>
			<div id="right">
				<iframe id="cmsMainFrame" name="cmsMainFrame" src="${pageContext.request.contextPath}/f/knowledgeNone" style="overflow:visible;" scrolling="yes" frameborder="no" width="100%"></iframe>
			</div>
			</div>
			<script type="text/javascript">
				var leftWidth = "60%"; // 左侧窗口大小
				function wSize(){
					var strs=getWindowSize().toString().split(",");
					$("#cmsMenuFrame, #cmsMainFrame, #openClose").height(strs[0]-5);
					$("#right").width($("body").width()-$("#openClose").width());
				}
				// 鼠标移动到边界自动弹出左侧菜单
				// $("#openClose").mouseover(function(){
				// 	if($(this).hasClass("open")){
				// 		$(this).click();
				// 	}
				// });
				$("#openClose").on('click',function(){
					$(this).toggleClass('open');
					$('body').toggleClass('haslayer');
				}); 
			</script>
			<script src="${ctxStatic}/common/wsize.min.js" type="text/javascript"></script>
		</body>

		</html>