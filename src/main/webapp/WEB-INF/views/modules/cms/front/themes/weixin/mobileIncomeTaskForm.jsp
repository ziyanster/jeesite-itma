<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>
<html>
<head>
	<title>运维任务录入管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="decorator" content="wechat"/>
	<%@include file="/WEB-INF/views/include/wechat_head.jsp" %>	
	<script type="text/javascript">
		$(document).ready(function() {

			$("#inputForm").validate({
				submitHandler: function(form){
					var custno=$("#customerno").val();
					if(typeof(custno)=="undefined" || custno==""){
						alert("请选择客户名称！");
						return;
					}
					$("#remarks").val(sessionStorage.wxUserId);
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
	<style> 
		input, textarea, .uneditable-input{max-width: 160px!important;}
	</style>
</head>
<body>

	<form:form id="inputForm" modelAttribute="smartTask" action="${ctx}/mobileTaskSave" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<form:input path="remarks" value="" htmlEscape="false" maxlength="50" class="input-xlarge " type="hidden"  style="width:180px;" />
		<div class="control-group">
			<label class="control-label" style="width:100px;">客户名称：</label>
			<div class="controls" style="margin-left:120px;">		
				<sys:customerselect id="customerno" name="customername" value="" labelName="" labelValue="" cssClass="input-small" allowClear="true" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width:100px;">呼叫记录：</label>
			<div class="controls" style="margin-left:120px;">
				<form:textarea path="record" htmlEscape="false" rows="8" maxlength="300" class="input-xxlarge required"  style="width:180px;"/>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" style="width:100px;">呼入时间：</label>
			<div class="controls" style="margin-left:120px;">
				<input name="incomingtime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
					value="<fmt:formatDate value="${smartTask.incomingtime}" pattern="yyyy-MM-dd HH:mm:ss" />"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="width:100px;">呼入方式：</label>
			<div class="controls" style="margin-left:120px;">
				<form:radiobuttons path="incomingway" items="${fns:getDictList('mis_incomingway')}"  itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
			</div>
		</div>		
		<div class="form-actions" style="padding-left: 0; text-align: center;" ">
			<input id="btnSubmit" class="btn btn-primary" style="margin: 0 5px;" type="submit" value="保 存"/>
			<input id="btnCancel" class="btn" type="button" style="margin: 0 5px;" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>
