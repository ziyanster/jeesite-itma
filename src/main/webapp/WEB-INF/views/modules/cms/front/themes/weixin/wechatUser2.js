
function getParameterByName(name){  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");  var regexS = "[\\?&]" + name + "=([^&#]*)";  var regex = new RegExp(regexS);  var results = regex.exec(window.location.search);  if(results == null)    return "";  else    return decodeURIComponent(results[1].replace(/\+/g, " "));}



function getUser() {
  var location = window.location.href;
  var dfd = $.Deferred();
  var code = getParameterByName('code');
  if(code.length >0 ) {
    $.get('http://wechat.itma.com.cn/jeesite/itma/get_userid?code=' + code)
      .done(function(data){
        dfd.resolve(data);
      });
  } else {
    $.get('http://wechat.itma.com.cn/jeesite/itma/get_userid?url='+ location)
      .done(function(data){
        window.location.href = data;
        dfd.resolve(data);
      });
  }
  return dfd.promise();
}

var user_id=sessionStorage.wxUserId;
if (user_id == null ) {
  $.when(getUser()).then(function(data){
    if (data != null && data.indexOf('https://') == -1) {
      sessionStorage.setItem('wxUserId',data);
    }
  });
}

//页面初始的时候加载数据
$(document).ready(function() {
	document.getElementById('remarks').innerText=sessionStorage.wxUserId;
});
}