<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>临时审批任务管理</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<meta name="decorator" content="wechat" />
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script src="${pageContext.request.contextPath}/static/jquery-validation/1.11.1/lib/jquery.form.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var userId  = sessionStorage.wxUserId;
			if(userId == "" || typeof(userId) == "undefined" || !userId){
				mui.alert('未获取到用户相关信息，请重新访问。');
				if(wx){
					wx.closeWindow();
				}
				return;
			}
			//$("#name").focus();
			$('#btnSubmit').on('click',function(){
				loading('正在提交，请稍等...');
				$('#flag').val('yes');
				$("#remarks").val(sessionStorage.wxUserId);
				$('#inputForm').ajaxSubmit({
					type: 'post', // 提交方式 get/post
		            url: $('#inputForm').attr('action'), // 需要提交的 url
		            data: $('#inputForm').serialize(),
		            success: function(data) { // data 保存提交后返回的数据，一般为 json 数据
		                // 此处可对 data 作相关处理
		                alert('提交成功！');
		            	if(wx){
		            		wx.closeWindow();
		            	}else{
		            		window.close();
		            	}
		            },
		            error: function(XmlHttpRequest, textStatus, errorThrown){  
                        alert( "error");  
            			closeTip();
                    } 
				});
				return false;				
			});
			
			$("#inputForm").validate({
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
	<style>
		body{width: 100%; overflow: hidden;}
		input, textarea, .uneditable-input{max-width: 160px!important;}
		.form-horizontal .form-actions{padding-left: 0; text-align: center;} 
		.jbox-button-panel{-webkit-transform: scale(1.5); transform: scale(1.5);}
	</style>
</head>
<body>
	<ul class="nav nav-tabs">
	</ul> 
	<form:form id="inputForm" modelAttribute="oaTempTask" action="${pageContext.request.contextPath}/f/oa/save" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="remarks"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<sys:message content="${message}"/>		
		<fieldset>
			<legend>审批申请</legend>
			<table class="table-form">
				<tr>
					<td class="tit">申请类型</td><td>
						<form:select path="type" class="input-large ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('mis_approvalType')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>					
					<td class="tit">MIS编号</td><td>
						<form:input path="misno" htmlEscape="false" maxlength="64" class="input-large "/>
					</td>
				</tr>
				<tr>					
					<td class="tit">申请者</td><td>${fns:getUserById(oaTempTask.createBy).name}</td>
				</tr>				
				<tr>					
					<td class="tit">审批者</td><td>
						<sys:treeselect_front id="leaderId" name="leaderId" value="${oaTempTask.leaderId.id}" labelName="" labelValue="${oaTempTask.leaderId.id}"
							title="用户" url="${pageContext.request.contextPath}/f/oa/treeData?type=3&officeId=2" cssClass="" allowClear="true" notAllowSelectParent="true"/>
					</td>
				</tr>
				<tr>
					<td class="tit">申请原因</td>
					<td >
						<form:textarea path="content" class="required" rows="5" maxlength="200" cssStyle="width:200px"/>
					</td>
				</tr>
				<tr>
					<td class="tit">缩略图</td>
					<td >
		                                <input type="hidden" id="image" name="image" value="${article.imageSrc}" />
						<!--sys:ckfinder input="image" type="thumb" uploadPath="/cms/article" selectMultiple="false"/-->
						<input type="button" value="上传照片" onclick="previewImage()" />
						<ul class="pic-list clearfix">
							<li class="add-item" id="wechat-add-image" ></li>
						</ul>
					</td>
				</tr>				
				<tr>
					<td class="tit">分管领导意见</td>
					<td >
						${oaTempTask.leaderText}
					</td>
				</tr>				
			</table>
		</fieldset>				

		<div class="form-actions">
				<input id="btnSubmit" class="btn btn-primary" type="button" value="提交申请" />&nbsp;
				<c:if test="${not empty oaTempTask.id}">
					<input id="btnSubmit2" class="btn btn-inverse" type="submit" value="销毁申请" onclick="$('#flag').val('no')"/>&nbsp;
				</c:if>				
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html> 
