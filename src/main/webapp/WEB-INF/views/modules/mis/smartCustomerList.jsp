<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>客户资料维护管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出销售任务数据吗？","系统提示",function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/mis/smartCustomer/export");
						$("#searchForm").submit();
					}
				},{buttonsFocus:1});
				top.$('.jbox-body .jbox-icon').css('top','55px');
			});	
			$("#btnImport").click(function(){
				$.jBox($("#importBox").html(), {title:"导入数据", buttons:{"关闭":true}, 
					bottomText:"导入文件不能超过5M，仅允许导入“xls”或“xlsx”格式文件！"});
			});			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<div id="importBox" class="hide">
		<form id="importForm" action="${ctx}/mis/smartCustomer/import" method="post" enctype="multipart/form-data"
			class="form-search" style="padding-left:20px;text-align:center;" onsubmit="loading('正在导入，请稍等...');"><br/>
			<input id="uploadFile" name="file" type="file" style="width:330px"/><br/><br/>　　
			<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   "/>
			<a href="${ctx}/mis/smartCustomer/import/template">下载模板</a>
		</form>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartCustomer/">客户资料维护列表</a></li>
		<shiro:hasPermission name="mis:smartCustomer:edit"><li><a href="${ctx}/mis/smartCustomer/form">客户资料维护添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartCustomer" action="${ctx}/mis/smartCustomer/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>选择客户：</label>
				<sys:treeselect id="customerno" name="smartCustomer.customerno" value="${smartCustomer.customerno}" labelName="" labelValue="${smartOrder.customername}"
					title="客户" url="/mis/smartCustomer" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>	
			<li><label>受理人：</label>
				<sys:treeselect id="assignedno" name="assignedno.id" value="${smartOrder.assignedno.id}" labelName="${smartOrder.assignedno.name}" labelValue="${smartOrder.assignedno.id}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>				
			<li><label>客户编号：</label>
				<form:input path="customerno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>客户名称：</label>
				<form:input path="customername" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>所属区域：</label>
				<form:radiobuttons path="area" items="${fns:getDictList('mis_area')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li><label>所属行业：</label>
				<form:select path="industry" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_industry')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			<li class="btns"><input id="btnImport" class="btn btn-primary" type="button" value="导入"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>客户编号</th>
				<th>客户名称</th>
				<th>客户地址</th>
				<th>经度</th>
				<th>纬度</th>
				<th>联系电话</th>
				<th>联系人</th>
				<th>所属区域</th>
				<th>所属行业</th>
				<shiro:hasPermission name="mis:smartCustomer:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartCustomer">
			<tr>
				<td><a href="${ctx}/mis/smartCustomer/form?id=${smartCustomer.id}">
					${smartCustomer.customerno}
				</a></td>
				<td>
					${smartCustomer.customername}
				</td>
				<td>
					${smartCustomer.address}
				</td>
				<td>
					${smartCustomer.longitude}
				</td>
				<td>
					${smartCustomer.latitude}
				</td>
				<td>
					${smartCustomer.phone}
				</td>
				<td>
					${smartCustomer.personname}
				</td>
				<td>
					${fns:getDictLabel(smartCustomer.area, 'mis_area', '')}
				</td>
				<td>
					${fns:getDictLabel(smartCustomer.industry, 'mis_industry', '')}
				</td>
				<shiro:hasPermission name="mis:smartCustomer:edit"><td>
    				<a href="${ctx}/mis/smartCustomer/form?id=${smartCustomer.id}">修改</a>
					<a href="${ctx}/mis/smartCustomer/delete?id=${smartCustomer.id}" onclick="return confirmx('确认要删除该客户资料维护吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>