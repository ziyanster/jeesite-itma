<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>工单管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/smartOrder/list_person">工单管理列表</a></li>
		<li class="active"><a href="${ctx}/mis/smartOrder/form_person?id=${smartOrder.id}">工单管理详细内容</a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="smartOrder" action="${ctx}/mis/smartOrder/save_person" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		
		<div class="accordion" id="accordion_all">
		  <div class="accordion-group">
		    <div class="accordion-heading">
		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_all" href="#collapse1">任务详情</a>
		    </div>
		    <div id="collapse1" class="accordion-body collapse in">
		      <div class="accordion-inner">
				<div class="row">
				  <div class="span5">
			      	<dl class="dl-horizontal">
	  					<dt>客户名称:</dt><dd>${fns:getCustomerName(smartTask.customerno,  '')}</dd>
	  					<dt>呼入类型:</dt><dd>${fns:getDictLabel(smartTask.incomingway, 'mis_incomingway', '')}</dd>
	  					<dt>呼入时间:</dt><dd><fmt:formatDate value="${smartTask.incomingtime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>
					</dl>					  
				  </div>
				  <div class="span4">
			      	<dl class="dl-horizontal">
	  					<dt>任务状态:</dt><dd>${fns:getDictLabel(smartTask.status,   'mis_taskstatus', '')}</dd>
	  					<dt>消息类型:</dt><dd>${fns:getDictLabel(smartTask.sendtype, 'mis_sendtype', '')}</dd>
	  					<dt>积分:</dt><dd>${smartTask.integral}</dd>	  					
					</dl>					  
				  </div>
				</div>  	
				<div class="row">
					<div class="span9">	
						<dl class="dl-horizontal">		
							<dt>呼入内容:</dt><dd>${smartTask.record}</dd>
						</dl>
						<c:if test="${attList.size()>0}">
							<div id="myCarousel" class="carousel slide">
							  <ol class="carousel-indicators">
							  	<c:forEach items="${attList}" var="SmartAttachments" varStatus="status">
							    	<li data-target="#myCarousel" data-slide-to="${status.index}" <c:if test="${status.index=='0'}">class="active"</c:if> ></li>
							    </c:forEach>
							  </ol>
							  <!-- Carousel items -->
							  <div class="carousel-inner">
							  	<c:forEach items="${attList}" var="SmartAttachments" varStatus="status">
							  		<div class="item <c:if test="${status.index=='0'}">active</c:if>">
							    		<img	src="${SmartAttachments.remarks}" alt >
							    	</div>
							  	</c:forEach>
							  </div>
							  <!-- Carousel nav -->
							  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
							  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
							</div>
						</c:if>
					</div>
				</div>
				
		      </div>
		    </div>
		  </div>
		  <div class="accordion-group">
		    <div class="accordion-heading">
		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_all" href="#collapse2">回单详情</a>
		    </div>
		    <div id="collapse2" class="accordion-body collapse">
		      <div class="accordion-inner">
				<div class="control-group">				      
					<label class="control-label">回单时间：</label>
					<div class="controls">
						<input name="responsetime" type="text" maxlength="20" class="input-medium Wdate required"
							value="<fmt:formatDate value="${smartOrder.responsetime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
							onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
						<span class="help-inline"><font color="red">*</font> </span>
					</div>
				</div>	
				<div class="control-group">				
					<label class="control-label">机型：</label>
					<div class="controls">
						<form:input path="modeltype" htmlEscape="false" maxlength="64" class="input-samll "/>
					</div>
				</div>
				<div class="control-group">							
					<label class="control-label">系列号：</label>
					<div class="controls">
						<form:input path="equipmentno" htmlEscape="false" maxlength="64" class="input-samll "/>
					</div>
				</div>				 
				<div class="row">
					<div class="span9">	
						<dl class="dl-horizontal">		
							<dt>工作内容:</dt><dd><form:textarea path="taskcontent" htmlEscape="false" rows="2" maxlength="200" class="input-xxlarge "/></dd>
							<dt>本次解决的问题:</dt><dd><form:textarea path="thistimejob" htmlEscape="false" rows="2" maxlength="200" class="input-xxlarge "/></dd>
							<dt>下一步工作计划:</dt><dd><form:textarea path="nexttimeplan" htmlEscape="false" rows="2" maxlength="200" class="input-xxlarge "/></dd>
							<dt>备注:</dt><dd><form:textarea path="remarks" htmlEscape="false" rows="2" maxlength="200" class="input-xxlarge "/></dd>
						</dl>
					</div>					
				</div>	
				<c:if test="${not empty smartOrder.isback}">
					<div class="control-group">				      
						<label class="control-label">退单时间：</label>
						<div class="controls">
							<input name="responsetime" type="text" maxlength="20" class="input-medium Wdate required"
								value="<fmt:formatDate value="${smartOrder.responsetime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
								onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
							<span class="help-inline"><font color="red">*</font> </span>
						</div>
					</div>	
					<div class="control-group">
						<label class="control-label">退单说明：</label>
						<div class="controls">
							<form:textarea path="backremark" htmlEscape="false" rows="4" maxlength="200" class="input-xxlarge "/>
						</div>
					</div>		
				</c:if>																	      
		      </div>
		    </div>
		  </div>
		  <c:if test="${not empty smartOrder.isback}">
			  <div class="accordion-group">
			    <div class="accordion-heading">
			      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_all" href="#collapse3">客户评价</a>
			    </div>
			    <div id="collapse3" class="accordion-body collapse">
			      <div class="accordion-inner">
					<div class="row">
					  <div class="span5">
				      	<dl class="dl-horizontal">
							<dt>用户确认是否完成实施:</dt><dd>${fns:getDictLabel(smartOrder.iscomplete,   'yes_no', '')}</dd>
							<dt>是否在规定时间与客户预约:</dt><dd>${fns:getDictLabel(smartOrder.isappointment,   'yes_no', '')}</dd>
						</dl>					  
					  </div>
					  <div class="span4">
				      	<dl class="dl-horizontal">
							<dt>是否在预约时间上门服务:</dt><dd>${fns:getDictLabel(smartOrder.isontime,   'yes_no', '')}</dd>
							<dt>客户满意度评价:</dt><dd>${fns:getDictLabel(smartOrder.opinion,   'mis_opinion', '')}</dd>				
						</dl>					  
					  </div>
					</div>  	
					<div class="row">
						<div class="span9">	
							<dl class="dl-horizontal">		
								<dt>客户建议:</dt><dd>${smartOrder.advice}</dd>
							</dl>
						</div>
					</div>		      
			      </div>
			    </div>
			  </div>
		</c:if>	

		<div class="form-actions">
			<shiro:hasPermission name="mis:smartOrder:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>