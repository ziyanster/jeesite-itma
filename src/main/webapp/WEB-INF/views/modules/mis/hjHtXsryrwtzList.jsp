<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销售人员任务台账管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/hjHtXsryrwtz/">销售人员任务台账列表</a></li>
		<shiro:hasPermission name="mis:hjHtXsryrwtz:edit"><li><a href="${ctx}/mis/hjHtXsryrwtz/form">销售人员任务台账添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="hjHtXsryrwtz" action="${ctx}/mis/hjHtXsryrwtz/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>销售季度：</label>
				<form:input path="yearq" htmlEscape="false" maxlength="6" class="input-medium"/>
			</li>
			<li><label>业务员姓名：</label>
				<form:input path="ywryxm" htmlEscape="false" maxlength="8" class="input-medium"/>
			</li>
			<li><label>任务额：</label>
				<form:input path="rwe" htmlEscape="false" class="input-medium"/>
			</li>
			<li><label>完成额：</label>
				<form:input path="wce" htmlEscape="false" class="input-medium"/>
			</li>
			<li><label>培训费用：</label>
				<form:input path="pxfy" htmlEscape="false" class="input-medium"/>
			</li>
			<li><label>实发工资比：</label>
				<form:input path="sjyfgzbl" htmlEscape="false" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>销售季度</th>
				<th>业务员姓名</th>
				<th>任务额</th>
				<th>完成额</th>
				<th>完成比例</th>
				<th>费用报销额</th>
				<th>实际费用</th>
				<th>培训费用</th>
				<th>差旅费用</th>
				<th>实发工资比</th>
				<shiro:hasPermission name="mis:hjHtXsryrwtz:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="hjHtXsryrwtz">
			<tr>
				<td><a href="${ctx}/mis/hjHtXsryrwtz/form?id=${hjHtXsryrwtz.id}">
					${hjHtXsryrwtz.yearq}
				</a></td>
				<td>
					${hjHtXsryrwtz.ywryxm}
				</td>
				<td>
					${hjHtXsryrwtz.rwe}
				</td>
				<td>
					${hjHtXsryrwtz.wce}
				</td>
				<td>
					${hjHtXsryrwtz.wcbl}
				</td>
				<td>
					${hjHtXsryrwtz.fybxed}
				</td>
				<td>
					${hjHtXsryrwtz.sjfybx}
				</td>
				<td>
					${hjHtXsryrwtz.pxfy}
				</td>
				<td>
					${hjHtXsryrwtz.clfy}
				</td>
				<td>
					${hjHtXsryrwtz.sjyfgzbl}
				</td>
				<shiro:hasPermission name="mis:hjHtXsryrwtz:edit"><td>
    				<a href="${ctx}/mis/hjHtXsryrwtz/form?id=${hjHtXsryrwtz.id}">修改</a>
					<a href="${ctx}/mis/hjHtXsryrwtz/delete?id=${hjHtXsryrwtz.id}" onclick="return confirmx('确认要删除该销售人员任务台账吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>