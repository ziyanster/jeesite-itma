<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销售人员任务台账管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/hjHtXsryrwtz/">销售人员任务台账列表</a></li>
		<li class="active"><a href="${ctx}/mis/hjHtXsryrwtz/form?id=${hjHtXsryrwtz.id}">销售人员任务台账<shiro:hasPermission name="mis:hjHtXsryrwtz:edit">${not empty hjHtXsryrwtz.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="mis:hjHtXsryrwtz:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="hjHtXsryrwtz" action="${ctx}/mis/hjHtXsryrwtz/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">销售季度：</label>
			<div class="controls">
				<form:input path="yearq" htmlEscape="false" maxlength="6" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">业务员代码：</label>
			<div class="controls">
				<form:input path="ywrydm" htmlEscape="false" maxlength="6" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">业务员姓名：</label>
			<div class="controls">
				<form:input path="ywryxm" htmlEscape="false" maxlength="8" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">任务额：</label>
			<div class="controls">
				<form:input path="rwe" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">完成额：</label>
			<div class="controls">
				<form:input path="wce" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">完成比例：</label>
			<div class="controls">
				<form:input path="wcbl" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">费用报销额：</label>
			<div class="controls">
				<form:input path="fybxed" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">实际费用：</label>
			<div class="controls">
				<form:input path="sjfybx" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">培训费用：</label>
			<div class="controls">
				<form:input path="pxfy" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">差旅费用：</label>
			<div class="controls">
				<form:input path="clfy" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">实发工资比：</label>
			<div class="controls">
				<form:input path="sjyfgzbl" htmlEscape="false" class="input-xlarge  number"/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="mis:hjHtXsryrwtz:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>