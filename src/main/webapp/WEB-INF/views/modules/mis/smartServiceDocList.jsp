<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>运维任务信息管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartServiceDoc/">运维任务信息列表</a></li>
		<shiro:hasPermission name="mis:smartServiceDoc:edit"><li><a href="${ctx}/mis/smartServiceDoc/form">运维任务信息添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartServiceDoc" action="${ctx}/mis/smartServiceDoc/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>服务单号：</label>
				<form:input path="servicedocno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>客户名称：</label>
				<form:input path="customername" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>呼入方式：</label>
				<form:input path="incomingway" htmlEscape="false" maxlength="30" class="input-medium"/>
			</li>
			<li><label>机型：</label>
				<form:input path="model" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>品牌：</label>
				<form:input path="brand" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>客户姓名：</label>
				<form:input path="personname" htmlEscape="false" maxlength="30" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>服务单号</th>
				<th>客户名称</th>
				<th>客户编号</th>
				<th>呼入方式</th>
				<th>呼入时间</th>
				<th>机型</th>
				<th>系列号</th>
				<th>品牌</th>
				<th>客户姓名</th>
				<th>联系电话</th>
				<th>地址</th>
				<shiro:hasPermission name="mis:smartServiceDoc:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartServiceDoc">
			<tr>
				<td><a href="${ctx}/mis/smartServiceDoc/form?id=${smartServiceDoc.id}">
					${smartServiceDoc.servicedocno}
				</a></td>
				<td>
					${smartServiceDoc.customername}
				</td>
				<td>
					${smartServiceDoc.customerno}
				</td>
				<td>
					${smartServiceDoc.incomingway}
				</td>
				<td>
					<fmt:formatDate value="${smartServiceDoc.incomingtime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${smartServiceDoc.model}
				</td>
				<td>
					${smartServiceDoc.seriesno}
				</td>
				<td>
					${smartServiceDoc.brand}
				</td>
				<td>
					${smartServiceDoc.personname}
				</td>
				<td>
					${smartServiceDoc.phone}
				</td>
				<td>
					${smartServiceDoc.address}
				</td>
				<shiro:hasPermission name="mis:smartServiceDoc:edit"><td>
    				<a href="${ctx}/mis/smartServiceDoc/form?id=${smartServiceDoc.id}">修改</a>
					<a href="${ctx}/mis/smartServiceDoc/delete?id=${smartServiceDoc.id}" onclick="return confirmx('确认要删除该运维任务信息吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>