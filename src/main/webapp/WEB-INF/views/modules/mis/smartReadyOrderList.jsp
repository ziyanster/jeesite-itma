<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>工单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartOrder/">工单管理列表</a></li>
		<shiro:hasPermission name="mis:smartOrder:edit"><li><a href="#">工单管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartOrder" action="${ctx}/mis/smartOrder/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>创建时间：</label>
				<input name="beginCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.beginCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.endCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>		
			<li><label>任务编号：</label>
				<form:input path="taskno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>是否指派：</label>
				<form:radiobuttons path="isassigned" items="${fns:getDictList('mis_sendtype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li><label>受理人：</label>
				<sys:treeselect id="assignedno" name="assignedno.id" value="${smartOrder.assignedno.id}" labelName="${smartOrder.assignedno.name}" labelValue="${smartOrder.assignedno.id}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>服务类型：</label>
				<form:select path="servicetype" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_servicetype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_orderstatus')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>						
			<li><label>工作内容：</label>
				<form:input path="taskcontent" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>服务单号</th>
				<th>工单流水号</th>
				<th>客户名称</th>
				<th>任务描述</th>
				<th>是否指派</th>
				<th>分发时间</th>				
				<th>受理人</th>
				<th>状态</th>
				<th>服务类型</th>
				<th>积分</th>
				<th>呼叫记录</th>				
				<shiro:hasPermission name="mis:smartOrder:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartOrder">
			<tr>
				<td><a href="${ctx}/mis/smartOrder/form?id=${smartOrder.id}">
					${smartOrder.servicedocno}
				</a></td>
				<td><a href="${ctx}/mis/smartOrder/form?id=${smartOrder.id}">
					${smartOrder.orderserialno}
				</a></td>
				<td>
					${fns:getCustomerName(smartOrder.customerno,  '')}
				</td>
				<td>
					${fns:getTask(smartOrder.taskno).record}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isassigned, 'mis_sendtype', '')}
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>				
				<td>
					${fns:getUserById(smartOrder.assignedno).name}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.status, 'mis_orderstatus', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.servicetype, 'mis_servicetype', '')}
				</td>
				<td>
					${smartOrder.integral}
				</td>
				<td>
					${smartOrder.record}
				</td>				
				<shiro:hasPermission name="mis:smartOrder:edit"><td>
    				<a href="${ctx}/mis/smartOrder/form?id=${smartOrder.id}">修改</a>
					<a href="${ctx}/mis/smartOrder/delete?id=${smartOrder.id}" onclick="return confirmx('确认要删除该工单管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>