<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>个人工单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartOrder/list_person">个人工单列表</a></li>
		<shiro:hasPermission name="mis:smartOrder:edit"><li><a href="${ctx}/mis/smartOrder/form_person">工单详细信息</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartOrder" action="${ctx}/mis/smartOrder/list_person" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>受理时间：</label>
				<input name="beginCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.beginCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.endCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>客户名称：</label>	
				<sys:customerselect id="customerno" name="customername" value="" labelName="" labelValue="" cssClass="input-small" allowClear="true" />
			</li>
			<li><label>是否指派：</label>
				<form:select path="isassigned" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_sendtype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>客户评价：</label>
				<form:select path="opinion" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_opinion')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_orderstatus')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th style="width:200px;">服务单号</th>
				<th style="width:200px;">客户名称</th>
				<th style="width:200px;">呼叫内容</th>
				<th>是否指派</th>
				<th>积分</th>
				<th>客户评价</th>
				<th>状态</th>
				<th>呼入时间</th>
				<th>受理时间</th>
				<th>完成时间</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartOrder">
			<tr>
				<td>
					<a href="${ctx}/mis/smartOrder/form_person?id=${smartOrder.id}">${smartOrder.id}</a>
				</td>
				<td>
					${fns:getCustomerName(smartOrder.customerno,  '')}
				</td>
				<td>
					${fns:getTask(smartOrder.taskno).record}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isassigned, 'mis_sendtype', '')}
				</td>
				<td>
					${smartOrder.integral}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.opinion, 'mis_opinion', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.status, 'mis_orderstatus', '')}
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>