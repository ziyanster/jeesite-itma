<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>巡检任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/misChecktask/">巡检任务列表</a></li>
		<li class="active"><a href="${ctx}/mis/misChecktask/form?id=${misChecktask.id}">巡检任务<shiro:hasPermission name="mis:misChecktask:edit">${not empty misChecktask.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="mis:misChecktask:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="misChecktask" action="${ctx}/mis/misChecktask/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">合同：</label>
			<!-- <div class="controls">
				<form:select path="contractid" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_cust')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div> -->
				<div class="controls">
				<form:select path="contractid" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getContractList()}" itemLabel="projectname" itemValue="id" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">提前一周提醒：</label>
			<div class="controls">
				<form:radiobuttons path="weekremind" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">早上提醒：</label>
			<div class="controls">
				<form:radiobuttons path="morningremind" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">晚上提醒：</label>
			<div class="controls">
				<form:radiobuttons path="nightremind" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">是否可用：</label>
			<div class="controls">
				<form:radiobuttons path="isenabled" items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="mis:misChecktask:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>