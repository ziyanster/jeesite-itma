<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>服务排名管理</title>
	<meta name="decorator" content="blank"/>
	<script type="text/javascript">
		$(document).ready(function() {
			var selectObject=$("#status option[value='${param.status}']");
			selectObject.attr("selected",true);
			$(".select2-chosen").text(selectObject.text());
			
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<form:form id="searchForm" modelAttribute="article" action="${ctx}/mis/smartOrder/chart_index" method="post" class="breadcrumb form-search">
		<div>
			<label>开始日期：</label>
			<input id="beginDate" name="beginDate" type="text" readonly="readonly" maxlength="20" class="input-small Wdate"
				value="${paramMap.beginDate}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			<label>结束日期：</label>
			<input id="endDate" name="endDate" type="text" readonly="readonly" maxlength="20" class="input-small Wdate"
				value="${paramMap.endDate}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>&nbsp;&nbsp;
			<label>工单状态：</label>
			<select id="status" name="status" class="input-medium select2-offscreen"  >
					<option value="" selected="selected"></option>
					<option value="0">待受理</option>
					<option value="1">已受理</option>
					<option value="2">已回单</option>
					<option value="3">已评价</option>
					<option value="4">主动退单</option>
					<option value="5">超时退单</option>
					<option value="9">关闭</option>
			</select>
			<label>排序：</label>
			<select id="orderBy" name="orderBy" class="input-medium select2-offscreen"  >
					<option value="DESC" selected="selected">倒序</option>
					<option value="ASC">正序</option>
			</select>
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
		</div>
	</form:form>
	<sys:message content="${message}"/>
	<div class="row">
	  <div class="span3">
		<table id="contentTable" class="table table-striped table-bordered table-condensed">
			<caption>维护与巡检服务排名统计</caption>
			<thead><tr><th>服务人员</th><th>完成数量</th><th>完成积分</th>
			<tbody>
			<c:forEach items="${list}" var="list">
				<tr>
					<td>${list.name}</td>
					<td>${list.countValue}</td>
					<td>${list.sumValue}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>					  
	  </div>

	  <div class="span9">
    	<div id="main" style="height:400px"></div>				  
	  </div>
	</div>
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->

    <!-- ECharts单文件引入 -->
    <script src="${ctxStatic}/baidu-echarts/echarts.js"></script>
    <script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/baidu-echarts'
            }        
        });
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/scatter' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                option = {
                	    title : {
                	        text: '服务完成数量及积分排名分布',
                	        subtext: '抽样调查来自: 智能运维平台'
                	    },
                	    tooltip : {
                	        trigger: 'axis',
                	        showDelay : 0,
                	        formatter : function (params) {
                	            if (params.value.length > 1) {
                	                return params.seriesName + ' :'+params.value[2] +'<br/>'
                	                   + params.value[0] + '分 ' 
                	                   + params.value[1] + '次 ';
                	            }
                	            else {
                	                return params.seriesName + ' :<br/>'
                	                   + params.name + ' : '
                	                   + params.value + 'kg ';
                	            }
                	        },  
                	        axisPointer:{
                	            show: true,
                	            type : 'cross',
                	            lineStyle: {
                	                type : 'dashed',
                	                width : 1
                	            }
                	        }
                	    },
                	    legend: {
                	        data:['华际','外协']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataZoom : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    xAxis : [
                	        {
                	            type : 'value',
                	            scale:true,
                	            axisLabel : {
                	                formatter: '{value} 分'
                	            }
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'value',
                	            scale:true,
                	            axisLabel : {
                	                formatter: '{value} 次'
                	            }
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'华际',
                	            type:'scatter',
                	            data: ${arrayData},
                	            markPoint : {
                	                data : [
                	                    {type : 'max', name: '最大值'},
                	                    {type : 'min', name: '最小值'}
                	                ]
                	            },
                	            markLine : {
                	                data : [
                	                    {type : 'average', name: '平均值'}
                	                ]
                	            }
                	        }
                	    ]
                	};
                	                    
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );        
    </script>	
</body>
</html>