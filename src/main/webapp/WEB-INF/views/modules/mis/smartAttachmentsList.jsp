<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>附件管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartAttachments/">附件管理列表</a></li>
		<shiro:hasPermission name="mis:smartAttachments:edit"><li><a href="${ctx}/mis/smartAttachments/form">附件管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartAttachments" action="${ctx}/mis/smartAttachments/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>附件地址：</label>
				<form:input path="remarks" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>任务编号：</label>
				<form:input path="taskno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>工单编号：</label>
				<form:input path="orderid" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>附件类型：</label>
				<form:select path="type" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_attachment')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>编号</th>
				<th>更新时间</th>
				<th>附件地址</th>
				<th>任务编号</th>
				<th>工单编号</th>
				<th>附件类型</th>
				<shiro:hasPermission name="mis:smartAttachments:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartAttachments">
			<tr>
				<td><a href="${ctx}/mis/smartAttachments/form?id=${smartAttachments.id}">
					${smartAttachments.id}
				</a></td>
				<td>
					<fmt:formatDate value="${smartAttachments.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${smartAttachments.remarks}
				</td>
				<td>
					${smartAttachments.taskno}
				</td>
				<td>
					${smartAttachments.orderid}
				</td>
				<td>
					${fns:getDictLabel(smartAttachments.type, 'mis_attachment', '')}
				</td>
				<shiro:hasPermission name="mis:smartAttachments:edit"><td>
    				<a href="${ctx}/mis/smartAttachments/form?id=${smartAttachments.id}">修改</a>
					<a href="${ctx}/mis/smartAttachments/delete?id=${smartAttachments.id}" onclick="return confirmx('确认要删除该附件管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>