<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>运维任务预处理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartTask/readylist">待处理运维任务列表</a></li>
		<shiro:hasPermission name="mis:smartTask:edit"><li><a href="#">运维任务预处理</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartTask" action="${ctx}/mis/smartTask/readylist" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>呼入时间：</label>
				<input name="beginIncomingtime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartTask.beginIncomingtime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endIncomingtime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartTask.endIncomingtime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>呼入方式：</label>
				<form:select path="incomingway" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_incomingway')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>				
			</li>
			<li><label>系列号：</label>
				<form:input path="seriesno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>品牌：</label>
				<form:select path="brand" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>服务类型：</label>
				<form:select path="servicetype" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_servicetype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>					
			</li>
			<li><label>故障类型：</label>
				<form:select path="faulttype" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_faulttype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>					
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>任务编号</th>
				
				<th>客户名称</th>
				<th>呼入时间</th>
				<th>呼入方式</th>
				<th>机型</th>
				<th>系列号</th>
				<th>品牌</th>
				<th>呼叫记录</th>
				<th>状态</th>
				<th>积分</th>
				<th>服务类型</th>
				<th>故障类型</th>
				<th>服务评级</th>
				<th>消息类型</th>				
				<shiro:hasPermission name="mis:smartTask:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartTask">
			<tr>
				<td><a href="${ctx}/mis/smartTask/readyForm?id=${smartTask.id}">
					${smartTask.id}
				</a></td>
				<td>
					${fns:getCustomerName(smartTask.customerno,  '')}
				</td>
				<td>
					<fmt:formatDate value="${smartTask.incomingtime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${fns:getDictLabel(smartTask.incomingway, 'mis_incomingway', '')}
				</td>
				<td>
					${smartTask.model}
				</td>
				<td>
					${smartTask.seriesno}
				</td>
				<td>
					${fns:getDictLabel(smartTask.brand, 'mis_brand', '')}
				</td>
				<td>
					${smartTask.record}
				</td>
				<td>
					${fns:getDictLabel(smartTask.status, 'mis_taskstatus', '')}
				</td>
				<td>
					${smartTask.integral}
				</td>
				<td>
					${fns:getDictLabel(smartTask.servicetype, 'mis_servicetype', '')}
				</td>
				<td>
					${fns:getDictLabel(smartTask.faulttype, 'mis_faulttype', '')}
				</td>
				<td>
					${fns:getDictLabel(smartTask.servicelevel, 'mis_servicelevel', '')}
				</td>
				<td>
					${fns:getDictLabel(smartTask.sendtype, 'mis_sendtype', '')}
				</td>				
				<shiro:hasPermission name="mis:smartTask:edit"><td>
    				<a href="${ctx}/mis/smartTask/form?id=${smartTask.id}">修改</a>
					<a href="${ctx}/mis/smartTask/delete?id=${smartTask.id}" onclick="return confirmx('确认要删除该运维任务录入吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>
