<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销售任务完成情况</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		$(document).ready(function() {
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出用户数据吗？","系统提示",function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/sys/user/export");
						$("#searchForm").submit();
					}
				},{buttonsFocus:1});
				top.$('.jbox-body .jbox-icon').css('top','55px');
			});
		});		
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li ><a href="${ctx}/mis/hjMarkerterTask/">销售任务列表</a></li>
		<li class="active"><a href="${ctx}/mis/hjHtSwhtbh/">个人任务完成情况</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="hjHtSwhtbh" action="${ctx}/mis/hjHtSwhtbh/tasklist" method="post" class="breadcrumb form-search">
		<form:hidden path="ywrydm" htmlEscape="false" maxlength="50" class="input-medium"/>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th rowspan="2">销售姓名</th>
				<th rowspan="2">市场范围</th>
				<th rowspan="2">销售年度</th>
				<th rowspan="2">销售任务额</th>
				<th rowspan="2">完成任务额</th>
				<th rowspan="2">完成比例</th>
				<th colspan="3" style="text-align:center";>一季度</th>
				<th colspan="3" style="text-align:center";>二季度</th>
				<th colspan="3" style="text-align:center";>三季度</th>
				<th colspan="3" style="text-align:center";>四季度</th>
			</tr>
			<tr>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
			</tr>			
		</thead>		
		<tbody>
		<c:forEach items="${pageTask.list}" var="hjMarkerterTask">
			<tr>
				<td>
					${hjMarkerterTask.salesname}
				</td>
				<td>
					${hjMarkerterTask.salerange}
				</td>
				<td>
					${hjMarkerterTask.year}
				</td>
				<td>
					${hjMarkerterTask.task}
				</td>
				<td>
					${hjMarkerterTask.saleTask}万
				</td>
				<td>
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.conPercent -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.conPercent -0) <100 }">red</c:if>	
						">
						${100*hjMarkerterTask.conPercent}%
						</font> 
					</span>	
				</td>
				<td>
					${hjMarkerterTask.s1Task}
				</td>
				<td>
					${hjMarkerterTask.s1TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s1 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s1 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s1}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s2Task}
				</td>
				<td>
					${hjMarkerterTask.s2TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s2 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s2 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s2}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s3Task}
				</td>
				<td>
					${hjMarkerterTask.s3TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s3 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s3 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s3}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s4Task}
				</td>
				<td>
					${hjMarkerterTask.s4TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s4 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s4 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s4}%
						</font> 
					</span>		
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<table id="contentTable" class="table table-bordered">
		<thead>
			<tr>
				<th>商务合同号</th>
				<th>合同开始日</th>
				<th>合同终止日</th>
				<th>客户名称</th>
				<th>项目名称</th>
				<th>合同金额</th>
				<th>资金已回款</th>
				<th>外购费用</th>
				<th>资金占用</th>
				<th>培训费用</th>
				<th>合同类型</th>
				<th>业务人员</th>
				<th>税率</th>
				<th>资金回款率</th>
				<th>分摊毛利额比例</th>
				<th>毛利额</th>
				<th>毛利率</th>				
				<th>年度平均毛利额</th>				
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page}" var="hjHtSwhtbh">
			<tr <c:if test="${hjHtSwhtbh.mll==1.0 || hjHtSwhtbh.sl==0.0 }">class="error"</c:if>	>
				<td>
					${hjHtSwhtbh.swhtbh}
				</td>
				<td>
					<fmt:formatDate value="${hjHtSwhtbh.htksrq}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${hjHtSwhtbh.htzzrq}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${hjHtSwhtbh.khmc}
				</td>
				<td>
					${hjHtSwhtbh.xmmc}
				</td>
				<td style="text-align:right";>			
					${hjHtSwhtbh.htje}
				</td>
				<td>
					${hjHtSwhtbh.yjyje}
				</td>
				<td>
					${hjHtSwhtbh.wgfy}
				</td>
				<td>
					${hjHtSwhtbh.zjzy}
				</td>
				<td>
					${hjHtSwhtbh.pxfy}
				</td>
				<td>
					${hjHtSwhtbh.htlxmc}
				</td>
				<td>
					${hjHtSwhtbh.ywryxm}
				</td>
				<td>
					${hjHtSwhtbh.sl}
				</td>
				<td>
					${hjHtSwhtbh.zjhkl}
				</td>
				<td>
					${hjHtSwhtbh.ftmlebl}
				</td>
				<td>
					${hjHtSwhtbh.mle}
				</td>
				<td>
					${hjHtSwhtbh.mll}
				</td>				
				<td>
					${hjHtSwhtbh.avgmle}
				</td>		
			</tr>
		</c:forEach>
			<tr>
				<td>
					年度毛利额小计	
				</td>				
				<td colspan=17>
					${yearMle}万
				</td>
			</tr>		
		</tbody>
	</table>

</body>
</html>