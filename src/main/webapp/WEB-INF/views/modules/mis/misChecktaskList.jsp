<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>巡检任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/misChecktask/">巡检任务列表</a></li>
		<shiro:hasPermission name="mis:misChecktask:edit"><li><a href="${ctx}/mis/misChecktask/form">巡检任务添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="misChecktask" action="${ctx}/mis/misChecktask/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>合同编号：</label>
				
				<form:select path="contractid" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getContractList()}" itemLabel="projectname" itemValue="id" htmlEscape="false"/>
				</form:select>
		
			</li>
			<li><label>巡检日期：</label>
				<input name="beginCheckdata" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${misChecktask.beginCheckdata}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endCheckdata" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${misChecktask.endCheckdata}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>编号</th>
				<th>任务流水号</th>
				<th>合同编号</th>
				<th>巡检日期</th>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="mis:misChecktask:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="misChecktask">
			<tr>
				<td><a href="${ctx}/mis/misChecktask/form?id=${misChecktask.id}">
					${misChecktask.id}
				</a></td>
				<td>
					${misChecktask.taskserialno}
				</td>
				<td>
				<a href="${ctx}/mis/misContract/form?id=${misChecktask.contractid}">
				${misChecktask.contractid}
				</a>
				</td>
				
				<td>
					<fmt:formatDate value="${misChecktask.checkdata}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${misChecktask.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${misChecktask.remarks}
				</td>
				<shiro:hasPermission name="mis:misChecktask:edit"><td>
    				<a href="${ctx}/mis/misChecktask/form?id=${misChecktask.id}">修改</a>
					<a href="${ctx}/mis/misChecktask/delete?id=${misChecktask.id}" onclick="return confirmx('确认要删除该巡检任务吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>