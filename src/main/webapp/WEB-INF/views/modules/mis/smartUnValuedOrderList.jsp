<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>工单管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartOrder/unValuedList">工单管理列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="smartOrder" action="${ctx}/mis/smartOrder/unValuedList" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>创建时间：</label>
				<input name="beginCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.beginCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.endCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>服务单号：</label>
				<form:input path="servicedocno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>客户编号：</label>
				<form:input path="customerno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>受理人：</label>
				<sys:treeselect id="assignedno" name="assignedno.id" value="${smartOrder.assignedno.id}" labelName="" labelValue="${smartOrder.assignedno.name}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>受理人</th>
				<th>创建时间</th>
				<th>服务单号</th>
				<th>客户编号</th>
				<th>工作内容</th>
				<th>本次解决的问题</th>
				<th>状态</th>
				<th>是否退单</th>
				<th>积分</th>
				<shiro:hasPermission name="mis:smartOrder:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartOrder">
			<tr>
				<td><a href="${ctx}/mis/smartOrder/unValuedForm?id=${smartOrder.id}">
					${smartOrder.assignedno.name}
				</a></td>
				<td>
					<fmt:formatDate value="${smartOrder.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${smartOrder.servicedocno}
				</td>
				<td>
					${smartOrder.customerno}
				</td>	
				<td>
					${smartOrder.taskcontent}
				</td>
				<td>
					${smartOrder.thistimejob}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.status, 'mis_orderstatus', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isback, 'yes_no', '')}
				</td>
				<td>
					${smartOrder.integral}
				</td>
				<shiro:hasPermission name="mis:smartOrder:edit"><td>
    				<a href="${ctx}/mis/smartOrder/unValuedForm?id=${smartOrder.id}">修改</a>
					<a href="${ctx}/mis/smartOrder/delete?id=${smartOrder.id}" onclick="return confirmx('确认要删除该工单管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>