<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销售任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/hjHtSwhtbh/">销售任务列表</a></li>	
	</ul>
	<form:form id="searchForm" modelAttribute="hjHtSwhtbh" action="${ctx}/mis/hjHtSwhtbh/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>销售年度：</label>
				<form:select id="yearsale" path="yearsale" class="input-small"><form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_saleyear')}" itemValue="value" itemLabel="label" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>商务合同号：</label>
				<form:input path="swhtbh" htmlEscape="false" maxlength="20" class="input-medium"/>
			</li>			
			<li><label>客户名称：</label>
				<form:input path="khmc" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>销售姓名：</label>
				<sys:treeselect id="ywryxm" name="ywryxm" value="" labelName="" labelValue=""
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>								
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商务合同号</th>
				<th>合同开始日</th>
				<th>合同终止日</th>
				<th>客户名称</th>
				<th>项目名称</th>
				<th>合同金额</th>
				<th>资金已回款</th>
				<th>外购费用</th>
				<th>资金占用</th>
				<th>培训费用</th>
				<th>合同类型</th>
				<th>业务人员</th>
				<th>税率</th>
				<th>资金回款率</th>
				<th>分摊毛利额比例</th>
				<th>毛利额</th>
				<th>年度平均毛利额</th>				
				<th>毛利率</th>
				<shiro:hasPermission name="mis:hjHtSwhtbh:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="hjHtSwhtbh">
			<tr <c:if test="${hjHtSwhtbh.mll==1.0 || hjHtSwhtbh.sl==0.0 }">class="error"</c:if>	>
				<td>
					${hjHtSwhtbh.swhtbh}
				</td>
				<td>
					<fmt:formatDate value="${hjHtSwhtbh.htksrq}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${hjHtSwhtbh.htzzrq}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${hjHtSwhtbh.khmc}
				</td>
				<td>
					${hjHtSwhtbh.xmmc}
				</td>
				<td>
					${hjHtSwhtbh.htje}
				</td>
				<td>
					${hjHtSwhtbh.yjyje}
				</td>
				<td>
					${hjHtSwhtbh.wgfy}
				</td>
				<td>
					${hjHtSwhtbh.zjzy}
				</td>
				<td>
					${hjHtSwhtbh.pxfy}
				</td>
				<td>
					${hjHtSwhtbh.htlxmc}
				</td>
				<td>
					${hjHtSwhtbh.ywryxm}
				</td>
				<td>
					${hjHtSwhtbh.sl}
				</td>
				<td>
					${hjHtSwhtbh.zjhkl}
				</td>
				<td>
					${hjHtSwhtbh.ftmlebl}
				</td>
				<td>
					${hjHtSwhtbh.mle}
				</td>
				<td>
					${hjHtSwhtbh.avgmle}
				</td>				
				<td>
					${hjHtSwhtbh.mll}
				</td>
				<shiro:hasPermission name="mis:hjHtSwhtbh:edit"><td>
    				<a href="${ctx}/mis/hjHtSwhtbh/form?id=${hjHtSwhtbh.id}">修改</a>
					<a href="${ctx}/mis/hjHtSwhtbh/delete?id=${hjHtSwhtbh.id}" onclick="return confirmx('确认要删除该销售任务吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>