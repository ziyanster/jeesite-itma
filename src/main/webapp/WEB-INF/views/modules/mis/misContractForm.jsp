<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>巡检合同管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/misContract/">巡检合同列表</a></li>
		<li class="active"><a href="${ctx}/mis/misContract/form?id=${misContract.id}">巡检合同<shiro:hasPermission name="mis:misContract:edit">${not empty misContract.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="mis:misContract:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="misContract" action="${ctx}/mis/misContract/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">客户名称：</label>
			<div class="controls">
				<form:select path="customerno" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getCustomerList()}" itemLabel="customername" itemValue="customerno" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">开始日期：</label>
			<div class="controls">
				<input name="begindate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${misContract.begindate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">结束日期：</label>
			<div class="controls">
				<input name="enddate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${misContract.enddate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">地址：</label>
			<div class="controls">
				<form:input path="address" htmlEscape="false" maxlength="200" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">服务内容：</label>
			<div class="controls">
				<form:textarea path="content" htmlEscape="false" rows="4" maxlength="250" class="input-xxlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">销售主管：</label>
			<div class="controls">
				<sys:treeselect id="salesdirector" name="salesdirector" value="${misContract.salesdirector}" labelName="" labelValue="${misContract.salesdirector}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="" allowClear="true" notAllowSelectParent="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">巡检频次：</label>
			<div class="controls">
				<form:radiobuttons path="checktype" items="${fns:getDictList('mis_frequency')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">项目名称：</label>
			<div class="controls">
				<form:input path="projectname" htmlEscape="false" maxlength="150" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">客户联系人：</label>
			<div class="controls">
				<form:input path="cuscontact" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">客户电话：</label>
			<div class="controls">
				<form:input path="phone" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
			<div class="control-group">
				<label class="control-label">设备清单：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>机型</th>
								<th>产品描述</th>
								<th>数量</th>
								<th>服务时限</th>
								<th>序列号</th>
								<th>用途</th>
								<shiro:hasPermission name="mis:misContract:edit"><th width="10">&nbsp;</th></shiro:hasPermission>
							</tr>
						</thead>
						<tbody id="misContractdetailList">
						</tbody>
						<shiro:hasPermission name="mis:misContract:edit"><tfoot>
							<tr><td colspan="8"><a href="javascript:" onclick="addRow('#misContractdetailList', misContractdetailRowIdx, misContractdetailTpl);misContractdetailRowIdx = misContractdetailRowIdx + 1;" class="btn">新增</a></td></tr>
						</tfoot></shiro:hasPermission>
					</table>
					<script type="text/template" id="misContractdetailTpl">//<!--
						<tr id="misContractdetailList{{idx}}">
							<td class="hide">
								<input id="misContractdetailList{{idx}}_id" name="misContractdetailList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="misContractdetailList{{idx}}_delFlag" name="misContractdetailList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td>
								<input id="misContractdetailList{{idx}}_model" name="misContractdetailList[{{idx}}].model" type="text" value="{{row.model}}" maxlength="100" class="input-small "/>
							</td>
							<td>
								<input id="misContractdetailList{{idx}}_productdes" name="misContractdetailList[{{idx}}].productdes" type="text" value="{{row.productdes}}" maxlength="200" class="input-small "/>
							</td>
							<td>
								<input id="misContractdetailList{{idx}}_qty" name="misContractdetailList[{{idx}}].qty" type="text" value="{{row.qty}}" class="input-small "/>
							</td>
							<td>
								<input id="misContractdetailList{{idx}}_servicetime" name="misContractdetailList[{{idx}}].servicetime" type="text" value="{{row.servicetime}}" maxlength="100" class="input-small "/>
							</td>
							<td>
								<input id="misContractdetailList{{idx}}_serialno" name="misContractdetailList[{{idx}}].serialno" type="text" value="{{row.serialno}}" maxlength="100" class="input-small "/>
							</td>
							<td>
								<input id="misContractdetailList{{idx}}_useful" name="misContractdetailList[{{idx}}].useful" type="text" value="{{row.useful}}" maxlength="150" class="input-small "/>
							</td>
							<shiro:hasPermission name="mis:misContract:edit"><td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#misContractdetailList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>
						</tr>//-->
					</script>
					<script type="text/javascript">
						var misContractdetailRowIdx = 0, misContractdetailTpl = $("#misContractdetailTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(misContract.misContractdetailList)};
							for (var i=0; i<data.length; i++){
								addRow('#misContractdetailList', misContractdetailRowIdx, misContractdetailTpl, data[i]);
								misContractdetailRowIdx = misContractdetailRowIdx + 1;
							}
						});
					</script>
				</div>
			</div>
		<div class="form-actions">
			<shiro:hasPermission name="mis:misContract:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>