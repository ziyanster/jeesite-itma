<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>运维任务预处理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
	function retrieveCity(address) {
		var cityIndex = address.indexOf('市'); 
		if (cityIndex != -1) {
			var provinceIndex = address.indexOf('省');
			if (provinceIndex != -1) {
				return address.substring(provinceIndex+1, cityIndex +1); 
			} else {
				return address.substring(0, cityIndex +1); 
			}
		}
		return null; 
	}	
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					//判断当前客户地址是否合法，提示补充客户资料
					var address=$('#address').val();
					if(retrieveCity(address)==null){
						alert('客户地址不符合地图索引要求，请在客户资料管理模块补充完整');
						return;
					}
					//判断指派操作是否指定用户
					var sendtype=$('#sendtype').val();
					var assignuserId=$('#assignuserId').val();
					if(sendtype=='0' && assignuserId==''){
						alert('请选择被指派的用户');
						return;						
					}
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/smartTask/readylist">待处理运维任务列表</a></li>
		<li class="active"><a href="#">运维任务详情</a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="smartTask" action="${ctx}/mis/smartTask/readySave" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
<div class="accordion" id="accordion2">
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
        客户呼入记录
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse in">
      <div class="accordion-inner">
		    <div class="control-group ">
		    	<label class="control-label">客户名称：</label>
		    	<div class="controls">
				<form:input path="customerno" value="${fns:getCustomerName(smartTask.customerno,  '')}" htmlEscape="false" maxlength="50" class="input-xlarge " disabled="true"/>
		    	</div>
		    </div>
		    <div class="control-group ">
		    	<label class="control-label">客户地址：</label>
		    	<div class="controls">
				<input id="address" name="address" value="${fns:getCustomer(smartTask.customerno).getAddress()}" class="input-xlarge " disabled="true"/>
		    	</div>
		    </div>		    
		    <div class="control-group">
		    	<label class="control-label">呼入时间：</label>
		    	<div class="controls">
		    		<input name="incomingtime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required"
		    			value="<fmt:formatDate value="${smartTask.incomingtime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
		    			onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});" disabled/>
		    		<span class="help-inline"><font color="red">*</font> </span>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">呼入方式：</label>
		    	<div class="controls">
		    		<form:radiobuttons path="incomingway" items="${fns:getDictList('mis_incomingway')}" itemLabel="label" itemValue="value" htmlEscape="false" disabled="true"/>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">机型：</label>
		    	<div class="controls">
		    		<form:input path="model" htmlEscape="false" maxlength="50" class="input-xlarge " disabled="true"/>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">系列号：</label>
		    	<div class="controls">
		    		<form:input path="seriesno" htmlEscape="false" maxlength="64" class="input-xlarge "  disabled="true"/>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">品牌：</label>
		    	<div class="controls">
		    		<form:select path="brand" class="input-xlarge " disabled="true">
		    			<form:option value="" label=""/>
		    			<form:options items="${fns:getDictList('mis_brand')}" itemLabel="label" itemValue="value" htmlEscape="false"  />
		    		</form:select>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">呼叫记录：</label>
		    	<div class="controls">
		    		<form:textarea path="record" htmlEscape="false" rows="4" maxlength="300" class="input-xxlarge "  disabled="true"/>
		    	</div>
		    </div>

      </div>
    </div>
  </div>
  <div class="accordion-group">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        预处理信息
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
      <!-- <div class="control-group">
		    	<label class="control-label">是否巡检：</label>
		    	<div class="controls">
		    		<form:radiobuttons path="ontimecheck" items="${fns:getDictList('ontimecheck')}" itemLabel="label" itemValue="value" htmlEscape="false" />
		    	</div>
		    </div>
		    <div class="control-group">
			<label class="control-label">巡检日期：</label>
			<div class="controls">
				<input name="checkdate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartTask.checkdate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>  -->
		    <div class="control-group">
		    	<label class="control-label">服务类型：</label>
		    	<div class="controls">
		    		<form:radiobuttons path="servicetype" items="${fns:getDictList('mis_servicetype')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">故障类型：</label>
		    	<div class="controls">
		    		<form:radiobuttons path="faulttype" items="${fns:getDictList('mis_faulttype')}" itemLabel="label" itemValue="value" htmlEscape="false" class=""/>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">服务评级：</label>
		    	<div class="controls">
		    		<form:select path="servicelevel" class="input-xlarge ">
		    			<form:option value="" label=""/>
		    			<form:options items="${fns:getDictList('mis_servicelevel')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
		    		</form:select>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">积分：</label>
		    	<div class="controls">
		    		<form:input path="integral" htmlEscape="false" maxlength="50" class="input-xlarge  digits"/>
		    	</div>
		    </div>
		    <div class="control-group">
		    	<label class="control-label">消息类型：</label>
		    	<div class="controls">
		    		<form:select path="sendtype" class="input-xlarge ">
		    			<form:option value="" label=""/>
		    			<form:options items="${fns:getDictList('mis_sendtype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
		    		</form:select>
		    	</div>
		    </div>	
		<div class="control-group">
			<label class="control-label">指派用户：</label>
			<div class="controls">
				<sys:treeselect id="assignuser" name="assignuser.id" value="${smartTask.assignuser.id}" labelName="${smartTask.assignuser.name}" labelValue="${smartTask.assignuser.id}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="" allowClear="true" notAllowSelectParent="true"/>
			</div>
		</div>			    	    
      </div>
    </div>
  </div>
</div>



		<div class="form-actions">
			<shiro:hasPermission name="mis:smartTask:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>
