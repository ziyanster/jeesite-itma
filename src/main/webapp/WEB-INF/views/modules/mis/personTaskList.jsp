<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销售任务完成情况</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/hjHtSwhtbh/">销售任务完成情况</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="hjHtSwhtbh" action="${ctx}/mis/hjHtSwhtbh/tasklist" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<form:hidden path="ywrydm" htmlEscape="false" maxlength="50" class="input-medium"/>
		<ul class="ul-form">	
			<li><label>销售年度：</label>
				<form:select id="bz3" path="bz3" class="input-small"><form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_saleyear')}" itemValue="value" itemLabel="label" htmlEscape="false"/>
				</form:select>
			</li>				
			<li><label>客户名称：</label>
				<form:input path="khmc" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>项目名称：</label>
				<form:input path="xmmc" htmlEscape="false" maxlength="60" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th rowspan="2">销售ID</th>
				<th rowspan="2">市场范围</th>
				<th rowspan="2">销售年度</th>
				<th rowspan="2">任务</th>
				<th rowspan="2">销售任务</th>
				<th rowspan="2">任务比例</th>
				<th rowspan="2">任务额度</th>
				<th colspan="3" style="text-align:center";>一季度</th>
				<th colspan="3" style="text-align:center";>二季度</th>
				<th colspan="3" style="text-align:center";>三季度</th>
				<th colspan="3" style="text-align:center";>四季度</th>
			</tr>
			<tr>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
			</tr>			
		</thead>		
		<tbody>
		<c:forEach items="${pageTask.list}" var="hjMarkerterTask">
			<tr>
				<td><a href="${ctx}/mis/hjMarkerterTask/form?id=${hjMarkerterTask.id}">
					${hjMarkerterTask.salesid}
				</a></td>
				<td>
					${hjMarkerterTask.salerange}
				</td>
				<td>
					${hjMarkerterTask.year}
				</td>
				<td>
					${hjMarkerterTask.task}
				</td>
				<td>
					${hjMarkerterTask.saleTask}
				</td>
				<td>
					${hjMarkerterTask.taskPercent}
				</td>
				<td>
					${hjMarkerterTask.conPercent}
				</td>
				<td>
					${hjMarkerterTask.s1Task}
				</td>
				<td>
					${hjMarkerterTask.s1TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s1 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s1 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s1}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s2Task}
				</td>
				<td>
					${hjMarkerterTask.s2TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s2 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s2 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s2}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s3Task}
				</td>
				<td>
					${hjMarkerterTask.s3TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s3 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s3 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s3}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s4Task}
				</td>
				<td>
					${hjMarkerterTask.s4TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s4 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s4 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s4}%
						</font> 
					</span>		
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商务合同号</th>
				<th>合同开始日</th>
				<th>合同终止日</th>
				<th>客户名称</th>
				<th>项目名称</th>
				<th>合同金额</th>
				<th>资金已回款</th>
				<th>外购费用</th>
				<th>资金占用</th>
				<th>培训费用</th>
				<th>合同类型</th>
				<th>业务人员</th>
				<th>税率</th>
				<th>资金回款率</th>
				<th>分摊毛利额比例</th>
				<th>毛利额</th>
				<th>毛利率</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="hjHtSwhtbh">
			<tr <c:if test="${hjHtSwhtbh.mll==1.0 || hjHtSwhtbh.sl==0.0 }">class="error"</c:if>	>
				<td><a href="${ctx}/mis/hjHtSwhtbh/form?id=${hjHtSwhtbh.id}">
					${hjHtSwhtbh.swhtbh}
				</a></td>
				<td>
					<fmt:formatDate value="${hjHtSwhtbh.htksrq}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${hjHtSwhtbh.htzzrq}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${hjHtSwhtbh.khmc}
				</td>
				<td>
					${hjHtSwhtbh.xmmc}
				</td>
				<td style="text-align:right";>
					${hjHtSwhtbh.htje/10000}万
				</td>
				<td>
					${hjHtSwhtbh.yjyje}
				</td>
				<td>
					${hjHtSwhtbh.wgfy}
				</td>
				<td>
					${hjHtSwhtbh.zjzy}
				</td>
				<td>
					${hjHtSwhtbh.pxfy}
				</td>
				<td>
					${hjHtSwhtbh.htlxmc}
				</td>
				<td>
					${hjHtSwhtbh.ywryxm}
				</td>
				<td>
					${hjHtSwhtbh.sl}
				</td>
				<td>
					${hjHtSwhtbh.zjhkl}
				</td>
				<td>
					${hjHtSwhtbh.ftmlebl}
				</td>
				<td>
					${hjHtSwhtbh.mle}
				</td>
				<td>
					${hjHtSwhtbh.mll}
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>