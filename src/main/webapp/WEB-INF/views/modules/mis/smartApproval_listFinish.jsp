<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>应急审批管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/smartApproval/listReady">待应急审批列表</a></li>
		<li class="active" ><a href="${ctx}/mis/smartApproval/listFinish">已应急审批列表</a></li>			
	</ul>
	<form:form id="searchForm" modelAttribute="smartApproval" action="${ctx}/mis/smartApproval/listApply" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>申请者：</label>
				<form:input path="createBy.id" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>申请信息：</label>
				<form:input path="remarks" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>MIS编号：</label>
				<form:input path="misno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>申请类型：</label>
				<form:select path="type" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_approvalType')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>审批状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_approvalStatus')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>审批时间：</label>
				<input name="approvalDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartApproval.approvalDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>编号</th>
				<th>申请者</th>
				<th>申请时间</th>
				<th>申请信息</th>
				<th>MIS编号</th>
				<th>申请类型</th>
				<th>审批状态</th>
				<th>是否审批通过</th>			
				<th>审批时间</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartApproval">
			<tr>
				<td>
					${smartApproval.id}
				</td>
				<td>
					${fns:getUserById(smartApproval.createBy.id).name}
				</td>
				<td>
					<fmt:formatDate value="${smartApproval.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${smartApproval.remarks}
				</td>
				<td>
					${smartApproval.misno}
				</td>
				<td>
					${fns:getDictLabel(smartApproval.type, 'mis_approvalType', '')}
				</td>
				<td>
					${fns:getDictLabel(smartApproval.status, 'yes_no', '')}
				</td>
				<td>
					${fns:getDictLabel(smartApproval.status, 'yes_no', '')}
				</td>				
				<td>
					<fmt:formatDate value="${smartApproval.approvalDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>