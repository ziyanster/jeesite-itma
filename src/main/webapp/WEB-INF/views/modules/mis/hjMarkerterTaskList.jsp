<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>销售任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出销售任务数据吗？","系统提示",function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/mis/hjMarkerterTask/export");
						$("#searchForm").submit();
					}
				},{buttonsFocus:1});
				top.$('.jbox-body .jbox-icon').css('top','55px');
			});
		});		
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/hjMarkerterTask/">销售任务列表</a></li>
		<li><a href="${ctx}/mis/hjMarkerterTask/persontasklist">个人任务完成情况</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="hjMarkerterTask" action="${ctx}/mis/hjMarkerterTask/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>销售ID：</label>
				<form:input path="salesid" htmlEscape="false" maxlength="6" class="input-medium"/>					
			</li>
			<li><label>销售姓名：</label>
				<form:input path="salesname" htmlEscape="false" maxlength="6" class="input-medium"/>					
			</li>			
			<li><label>销售年度：</label>
				<form:select id="year" path="year" class="input-small"><form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_saleyear')}" itemValue="value" itemLabel="label" htmlEscape="false"/>
				</form:select>				
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th rowspan="2">销售ID</th>
				<th rowspan="2">销售姓名</th>
				<th rowspan="2">市场范围</th>
				<th rowspan="2">销售年度</th>
				<th rowspan="2">销售任务额</th>
				<th rowspan="2">完成任务额</th>
				<th rowspan="2">完成比例</th>
				<th colspan="3" style="text-align:center"; >一季度</th>
				<th colspan="3" style="text-align:center";>二季度</th>
				<th colspan="3" style="text-align:center";>三季度</th>
				<th colspan="3" style="text-align:center";>四季度</th>
			</tr>
			<tr>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
				<th style="text-align:center";>任务额</th>
				<th style="text-align:center";>完成额</th>
				<th style="text-align:center";>完成比</th>
			</tr>			
		</thead>		
		<tbody>
		<c:forEach items="${page.list}" var="hjMarkerterTask">
			<tr>
				<td><a href="${ctx}/mis/hjMarkerterTask/persontasklist?ywrydm=${hjMarkerterTask.salesid}&bz3=${hjMarkerterTask.year}">
					${hjMarkerterTask.salesid}
				</a></td>
				<td>
					${hjMarkerterTask.salesname}
				</td>				
				<td>
					${hjMarkerterTask.salerange}
				</td>
				<td>
					${hjMarkerterTask.year}
				</td>
				<td>
					${hjMarkerterTask.task}
				</td>
				<td>
					${hjMarkerterTask.saleTask}万
				</td>
				<td>
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.conPercent -100) >=1 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.conPercent -100) <1 }">red</c:if>	
						">
						${hjMarkerterTask.conPercent}%
						<!--fmt:formatNumber type="number" value="${hjMarkerterTask.conPercent} " maxFractionDigits="2"/-->
						</font> 
					</span>						
				</td>
				<td>
					${hjMarkerterTask.s1Task}
				</td>
				<td>
					${hjMarkerterTask.s1TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s1 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s1 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s1}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s2Task}
				</td>
				<td>
					${hjMarkerterTask.s2TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s2 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s2 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s2}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s3Task}
				</td>
				<td>
					${hjMarkerterTask.s3TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s3 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s3 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s3}%
						</font> 
					</span>		
				</td>
				<td>
					${hjMarkerterTask.s4Task}
				</td>
				<td>
					${hjMarkerterTask.s4TaskCom}
				</td>
				<td style="text-align:right";>		
					<span class="help-inline">
						<font color="									
						<c:if test="${(hjMarkerterTask.s4 -0) >=100 }">green</c:if>		
						<c:if test="${(hjMarkerterTask.s4 -0) <100 }">red</c:if>	
						">
						${hjMarkerterTask.s4}%
						</font> 
					</span>		
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>