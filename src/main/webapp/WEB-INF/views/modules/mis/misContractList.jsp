<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>巡检合同管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/misContract/">巡检合同列表</a></li>
		<shiro:hasPermission name="mis:misContract:edit"><li><a href="${ctx}/mis/misContract/form">巡检合同添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="misContract" action="${ctx}/mis/misContract/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>开始日期：</label>
				<input name="beginBegindate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${misContract.beginBegindate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endBegindate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${misContract.endBegindate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>结束日期：</label>
				<input name="beginEnddate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${misContract.beginEnddate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endEnddate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${misContract.endEnddate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>销售主管：</label>
				<sys:treeselect id="salesdirector" name="salesdirector" value="${misContract.salesdirector}" labelName="" labelValue="${misContract.salesdirector}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>编号</th>
				<th>更新时间</th>
				<th>客户名称</th>
				<th>开始日期</th>
				<th>结束日期</th>
				<th>地址</th>
				<th>服务内容</th>
				<th>销售主管</th>
				<th>巡检频次</th>
				<th>项目名称</th>
				<th>客户联系人</th>
				<shiro:hasPermission name="mis:misContract:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="misContract">
			<tr>
				<td><a href="${ctx}/mis/misContract/form?id=${misContract.id}">
					${misContract.id}
				</a></td>
				<td>
					<fmt:formatDate value="${misContract.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${fns:getCustomerName(misContract.customerno,  '')}
				</td>
				<td>
					<fmt:formatDate value="${misContract.begindate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${misContract.enddate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${misContract.address}
				</td>
				<td>
					${misContract.content}
				</td>
				<td>
					${misContract.salesdirector}
				</td>
				<td>
					${fns:getDictLabel(misContract.checktype, 'mis_frequency', '')}
				</td>
				<td>
					${misContract.projectname}
				</td>
				<td>
					${misContract.cuscontact}
				</td>
				<shiro:hasPermission name="mis:misContract:edit"><td>
    				<a href="${ctx}/mis/misContract/form?id=${misContract.id}">修改</a>
					<a href="${ctx}/mis/misContract/delete?id=${misContract.id}" onclick="return confirmx('确认要删除该巡检合同吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>