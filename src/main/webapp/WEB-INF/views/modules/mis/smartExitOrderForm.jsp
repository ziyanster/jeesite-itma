<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>退单管理</title>
	<meta name="decorator" content="blank"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/mis/smartOrder/list_exit">退单列表</a></li>
		<li class="active"><a href="${ctx}/mis/smartOrder/chart_exit">退单统计</a></li>
	</ul><br/>
	<form:form id="searchForm" modelAttribute="article" action="${ctx}/mis/smartOrder/chart_exit" method="post" class="breadcrumb form-search">
		<div>
			<label>开始日期：</label><input id="beginDate" name="beginDate" type="text" readonly="readonly" maxlength="20" class="input-small Wdate"
				value="${paramMap.beginDate}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			<label>结束日期：</label><input id="endDate" name="endDate" type="text" readonly="readonly" maxlength="20" class="input-small Wdate"
				value="${paramMap.endDate}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>&nbsp;&nbsp;
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/>
		</div>
	</form:form>
	<sys:message content="${message}"/>
	<div class="row">
	  <div class="span4">
		<table id="contentTable" class="table table-striped table-bordered table-condensed">
			<caption>维修状态分类统计</caption>
			<thead><tr><th>状态名称</th><th>数量</th>
			<tbody>
			<c:forEach items="${list_status}" var="statsStatusList">
				<tr>
					<td>${statsStatusList.name}</td>
					<td>${statsStatusList.value}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>					  
	  </div>
	  <div class="span1"></div>
	  <div class="span4">
		<table id="contentTable" class="table table-striped table-bordered table-condensed">
			<caption>退单人员排名统计</caption>
			<thead><tr><th>维修人员</th><th>退单次数</th>
			<tbody>
			<c:forEach items="${list_exit}" var="statsExitList">
				<tr>
					<td>${statsExitList.name}</td>
					<td>${statsExitList.value}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>					  
	  </div>
	</div>  
	<div class="row">
	  <div class="span9">
    	<div id="main" style="height:400px"></div>
	  </div>
	</div>
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->

    <!-- ECharts单文件引入 -->
    <script src="${ctxStatic}/baidu-echarts/echarts.js"></script>
    <script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/baidu-echarts'
            }        
        });
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/pie' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                option = {
                	    tooltip : {
                	        trigger: 'item',
                	        formatter: "{a} <br/>{b} : {c} ({d}%)"
                	    },
                	    legend: {
                	        orient : 'vertical',
                	        x : 'left',
                	        data:${legend}
                	        //data:['直达','营销广告','搜索引擎','邮件营销','联盟广告','视频广告','百度','谷歌','必应','其他']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            magicType : {
                	                show: true, 
                	                type: ['pie', 'funnel']
                	            },
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : false,
                	    series : [
                	        {
                	            name:'退单人员排名统计',
                	            type:'pie',
                	            selectedMode: 'single',
                	            radius : [0, 70],
                	            
                	            // for funnel
                	            x: '20%',
                	            width: '40%',
                	            funnelAlign: 'right',
                	            //max: 1548,
                	            
                	            itemStyle : {
                	                normal : {
                	                    label : {
                	                        position : 'inner'
                	                    },
                	                    labelLine : {
                	                        show : false
                	                    }
                	                }
                	            },
                	            data:${jsondata1}
                	        },
                	        {
                	            name:'维修状态分类统计',
                	            type:'pie',
                	            radius : [100, 140],
                	            
                	            // for funnel
                	            x: '60%',
                	            width: '35%',
                	            funnelAlign: 'left',
                	            //max: 1048,
                	            data:${jsondata2}
                	        }
                	    ]
                	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );        
    </script>	
</body>
</html>