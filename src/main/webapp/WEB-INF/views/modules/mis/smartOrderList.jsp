<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>工单管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/mis/smartOrder/">工单管理列表</a></li>
		<shiro:hasPermission name="mis:smartOrder:edit"><li><a href="${ctx}/mis/smartOrder/form">工单管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smartOrder" action="${ctx}/mis/smartOrder/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>创建时间：</label>
				<input name="beginCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.beginCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endCreateDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${smartOrder.endCreateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>服务单号：</label>
				<form:input path="servicedocno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>客户编号：</label>
				<form:input path="customerno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>任务编号：</label>
				<form:input path="taskno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>是否指派：</label>
				<form:radiobuttons path="isassigned" items="${fns:getDictList('mis_sendtype')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li><label>受理人：</label>
				<sys:treeselect id="assignedno" name="assignedno.id" value="${smartOrder.assignedno.id}" labelName="" labelValue="${smartOrder.}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>工作内容：</label>
				<form:input path="taskcontent" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>本次解决的问题：</label>
				<form:input path="thistimejob" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>下一步工作计划：</label>
				<form:input path="nexttimeplan" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>客户意见：</label>
				<form:radiobuttons path="opinion" items="${fns:getDictList('mis_opinion')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			</li>
			<li><label>客户建议：</label>
				<form:input path="advice" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_orderstatus')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>退单说明：</label>
				<form:input path="backremark" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>servicetype：</label>
				<form:select path="servicetype" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_serviceType')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>呼叫记录：</label>
				<form:input path="record" htmlEscape="false" maxlength="512" class="input-medium"/>
			</li>
			<li><label>机型：</label>
				<form:input path="modeltype" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>创建时间</th>
				<th>服务单号</th>
				<th>客户编号</th>
				<th>任务编号</th>
				<th>设备编号</th>
				<th>是否指派</th>
				<th>受理人</th>
				<th>工作内容</th>
				<th>本次解决的问题</th>
				<th>下一步工作计划</th>
				<th>客户意见</th>
				<th>客户建议</th>
				<th>服务响应时间</th>
				<th>到达现场时间</th>
				<th>问题解决时间</th>
				<th>审批人</th>
				<th>审批时间</th>
				<th>审批意见</th>
				<th>状态</th>
				<th>请用户确认是否完成实施</th>
				<th>是否在规定时间与客户预约</th>
				<th>是否在预约时间上门服务</th>
				<th>是否退单</th>
				<th>退单说明</th>
				<th>servicetype</th>
				<th>积分</th>
				<th>呼叫记录</th>
				<th>时效（分）</th>
				<th>机型</th>
				<shiro:hasPermission name="mis:smartOrder:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smartOrder">
			<tr>
				<td><a href="${ctx}/mis/smartOrder/form?id=${smartOrder.id}">
					<fmt:formatDate value="${smartOrder.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</a></td>
				<td>
					${smartOrder.servicedocno}
				</td>
				<td>
					${smartOrder.customerno}
				</td>
				<td>
					${smartOrder.taskno}
				</td>
				<td>
					${smartOrder.equipmentno}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isassigned, 'mis_sendtype', '')}
				</td>
				<td>
					${smartOrder.}
				</td>
				<td>
					${smartOrder.taskcontent}
				</td>
				<td>
					${smartOrder.thistimejob}
				</td>
				<td>
					${smartOrder.nexttimeplan}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.opinion, 'mis_opinion', '')}
				</td>
				<td>
					${smartOrder.advice}
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.responsetime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.arrivedtime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.solvetime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${smartOrder.}
				</td>
				<td>
					<fmt:formatDate value="${smartOrder.audittime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${smartOrder.auditopinion}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.status, 'mis_orderstatus', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.iscomplete, 'yes_no', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isappointment, 'yes_no', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isontime, 'yes_no', '')}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.isback, 'yes_no', '')}
				</td>
				<td>
					${smartOrder.backremark}
				</td>
				<td>
					${fns:getDictLabel(smartOrder.servicetype, 'mis_serviceType', '')}
				</td>
				<td>
					${smartOrder.integral}
				</td>
				<td>
					${smartOrder.record}
				</td>
				<td>
					${smartOrder.timeout}
				</td>
				<td>
					${smartOrder.modeltype}
				</td>
				<shiro:hasPermission name="mis:smartOrder:edit"><td>
    				<a href="${ctx}/mis/smartOrder/form?id=${smartOrder.id}">修改</a>
					<a href="${ctx}/mis/smartOrder/delete?id=${smartOrder.id}" onclick="return confirmx('确认要删除该工单管理吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>