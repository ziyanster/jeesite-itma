<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>临时审批任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/oa/oaTempTask/">临时审批任务列表</a></li>
		<li class="active"><a href="${ctx}/oa/oaTempTask/form/?procInsId=${oaTempTask.procInsId}">审批详情</a></li>
	</ul>
	<form:form class="form-horizontal">
		<sys:message content="${message}"/>
		<fieldset>
			<legend>审批详情</legend>
			<table class="table-form">
				<tr>
					<td class="tit">申请类型</td><td>${fns:getDictLabel(oaTempTask.type, 'mis_approvalType', '')}</td>
					<td class="tit">MIS编号</td><td>${oaTempTask.misno}</td>
					<td class="tit">审批者</td><td>${fns:getUserById(oaTempTask.leaderId).name}</td>
				</tr>
				<tr>
					<td class="tit">申请原因</td>
					<td colspan="5">${oaTempTask.content}</td>
				</tr>
				<tr>
					<td class="tit">缩略图</td>
					<td colspan="5">			
						<img src="${oaTempTask.image}" class="img-rounded">
					</td>
				</tr>				
				<tr>
					<td class="tit">分管领导意见</td>
					<td colspan="5">
						${oaTempTask.leaderText}
					</td>
				</tr>				
			</table>
		</fieldset>				
		<act:histoicFlow procInsId="${oaTempTask.act.procInsId}" />
		<div class="form-actions">
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>