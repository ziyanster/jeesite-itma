<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>临时审批任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/oa/oaTempTask/">临时审批任务列表</a></li>
		<li class="active"><a href="${ctx}/oa/oaTempTask/form?id=${oaTempTask.id}">临时审批任务<shiro:hasPermission name="oa:oaTempTask:edit">${not empty oaTempTask.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="oa:oaTempTask:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="oaTempTask" action="${ctx}/oa/oaTempTask/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<sys:message content="${message}"/>		
		<fieldset>
			<legend>审批申请</legend>
			<table class="table-form">
				<tr>
					<td class="tit">申请类型</td><td>
						<form:select path="type" class="input-xlarge ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('mis_approvalType')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td><td class="tit">MIS编号</td><td>
						<form:input path="misno" htmlEscape="false" maxlength="64" class="input-xlarge "/>
					</td><td class="tit">审批者</td><td>
						<sys:treeselect id="leaderId" name="leaderId" value="${oaTempTask.leaderId.id}" labelName="" labelValue="${oaTempTask.leaderId.id}"
							title="用户" url="/sys/user/treeData?type=3&officeId=2" cssClass="" allowClear="true" notAllowSelectParent="true"/>
					</td>
				</tr>
				<tr>
					<td class="tit">申请原因</td>
					<td colspan="5">
						<form:textarea path="content" class="required" rows="5" maxlength="200" cssStyle="width:500px"/>
					</td>
				</tr>
				<tr>
					<td class="tit">缩略图</td>
					<td colspan="5">
		                <input type="hidden" id="image" name="image" value="${article.imageSrc}" />
						<sys:ckfinder input="image" type="thumb" uploadPath="/cms/article" selectMultiple="false"/>
					</td>
				</tr>				
				<tr>
					<td class="tit">分管领导意见</td>
					<td colspan="5">
						${oaTempTask.leaderText}
					</td>
				</tr>				
			</table>
		</fieldset>				

		<div class="form-actions">
			<shiro:hasPermission name="oa:oaTempTask:edit">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="提交申请" onclick="$('#flag').val('yes')"/>&nbsp;
				<c:if test="${not empty oaTempTask.id}">
					<input id="btnSubmit2" class="btn btn-inverse" type="submit" value="销毁申请" onclick="$('#flag').val('no')"/>&nbsp;
				</c:if>				
			</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>