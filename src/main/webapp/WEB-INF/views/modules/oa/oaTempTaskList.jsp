<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>临时审批任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/oa/oaTempTask/">临时审批任务列表</a></li>
		<shiro:hasPermission name="oa:oaTempTask:edit"><li><a href="${ctx}/oa/oaTempTask/form">临时审批任务添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="oaTempTask" action="${ctx}/oa/oaTempTask/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>申请者：</label>
				<form:input path="createBy.id" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>申请时间：</label>
				<input name="createDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${oaTempTask.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>申请类型：</label>
				<form:select path="type" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mis_approvalType')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>MIS编号：</label>
				<form:input path="misno" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>申请原因：</label>
				<form:input path="content" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>审批者：</label>
				<sys:treeselect id="leaderId" name="leaderId" value="${oaTempTask.leaderId.id}" labelName="" labelValue="${oaTempTask.leaderId.id}"
					title="用户" url="/sys/office/treeData?type=3" cssClass="input-small" allowClear="true" notAllowSelectParent="true"/>
			</li>
			<li><label>流程ID：</label>
				<form:input path="procInsId" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>申请者</th>
				<th>申请时间</th>
				<th>申请类型</th>
				<th>MIS编号</th>
				<th>申请原因</th>
				<th>审批者</th>
				<th>审批结果</th>
				<th>缩略图</th>
				<shiro:hasPermission name="oa:oaTempTask:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="oaTempTask">
			<tr>
				<td><a href="${ctx}/oa/oaTempTask/form?id=${oaTempTask.id}">
					${fns:getUserById(oaTempTask.createBy).name}
				</a></td>
				<td>
					<fmt:formatDate value="${oaTempTask.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${fns:getDictLabel(oaTempTask.type, 'mis_approvalType', '')}
				</td>
				<td>
					${oaTempTask.misno}
				</td>
				<td>
					${oaTempTask.content}
				</td>
				<td>
					${fns:getUserById(oaTempTask.leaderId).name}
				</td>
				<td>
					${oaTempTask.leaderText}
				</td>
				<td>
					<img src="${oaTempTask.image}" class="img-rounded">
				</td>
				<shiro:hasPermission name="oa:oaTempTask:edit"><td>
    				<a href="${ctx}/oa/oaTempTask/form?id=${oaTempTask.id}">修改</a>
					<a href="${ctx}/oa/oaTempTask/delete?id=${oaTempTask.id}" onclick="return confirmx('确认要删除该临时审批任务吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>