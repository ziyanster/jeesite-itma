<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>临时审批任务管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/oa/oaTempTask/">临时审批任务列表</a></li>
		<li class="active"><a href="#"><shiro:hasPermission name="oa:oaTempTask:edit">${oaTempTask.act.taskName}</shiro:hasPermission><shiro:lacksPermission name="oa:oaTempTask:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="oaTempTask" action="${ctx}/oa/oaTempTask/saveAudit" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="act.taskName"/>
		<form:hidden path="act.taskDefKey"/>
		<form:hidden path="act.procInsId"/>
		<form:hidden path="act.procDefId"/>
		<form:hidden id="flag" path="act.flag"/>
		<sys:message content="${message}"/>		
		<fieldset>
			<legend>${testAudit.act.taskName}</legend>
			<table class="table-form">
				<tr>
					<td class="tit">申请类型</td><td>${fns:getDictLabel(oaTempTask.type, 'mis_approvalType', '')}</td>
					<td class="tit">MIS编号</td><td>${oaTempTask.misno}</td>
					<td class="tit">申请者</td><td>${fns:getUserById(oaTempTask.createBy).name}</td>
				</tr>
				<tr>
					<td class="tit">申请原因</td>
					<td colspan="5">${oaTempTask.content}</td>
				</tr>
				<tr>
					<td class="tit">缩略图</td>
					<td colspan="5">			
						<img src="${oaTempTask.image}" class="img-rounded">
					</td>
				</tr>				
				<tr>
					<td class="tit">分管领导意见</td>
					<td colspan="5">
						${oaTempTask.leaderText}
					</td>
				</tr>	
				<tr>
					<td class="tit">您的意见</td>
					<td colspan="5">
						<form:textarea path="act.comment" class="required" rows="5" maxlength="20" cssStyle="width:500px"/>
					</td>
				</tr>			
			</table>
		</fieldset>				

		<div class="form-actions">
			<shiro:hasPermission name="oa:oaTempTask:edit">
				<c:if test="${oaTempTask.act.taskDefKey ne 'apply_end'}">
					<input id="btnSubmit" class="btn btn-primary" type="submit" value="同 意" onclick="$('#flag').val('yes')"/>&nbsp;
					<input id="btnSubmit" class="btn btn-inverse" type="submit" value="驳 回" onclick="$('#flag').val('no')"/>&nbsp;
				</c:if>			
			</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
		<act:histoicFlow procInsId="${oaTempTask.act.procInsId}"/>
	</form:form>
</body>
</html>