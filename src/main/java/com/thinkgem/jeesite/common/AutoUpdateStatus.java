package com.thinkgem.jeesite.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.modules.mis.dao.MisChecktaskDao;
import com.thinkgem.jeesite.modules.mis.dao.MisContractDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartOrderDao;
import com.thinkgem.jeesite.modules.mis.entity.MisChecktask;
import com.thinkgem.jeesite.modules.mis.entity.MisContract;
import com.thinkgem.jeesite.modules.mis.entity.StatsParam;
import com.thinkgem.jeesite.modules.mis.entity.StatsResult;
import com.thinkgem.jeesite.modules.sys.dao.UserDao;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

public class AutoUpdateStatus extends TimerTask {
	private static Logger logger = LoggerFactory
			.getLogger(AutoUpdateStatus.class);
	private static MisContractDao mcDao = SpringContextHolder
			.getBean(MisContractDao.class);
	private static MisChecktaskDao mctDao = SpringContextHolder
			.getBean(MisChecktaskDao.class);
	
	private static SmartOrderDao soDao = SpringContextHolder
			.getBean(SmartOrderDao.class);
	
	private static UserDao userDao = SpringContextHolder
			.getBean(UserDao.class);
	
	MisContract misContract = new MisContract();
	//MisChecktask misChecktask = new MisChecktask();

	@Override
	public void run() {
		
		//发送给公司领导微信提醒功能
		SendWeChat();
		
		// logger.debug("Auto Run Exec Code ..:" );
		try {
			// MisContract misContract=new MisContract();
		MisChecktask misChecktask = new MisChecktask();
			String strContractid = "";
			misChecktask.setIsenabled("1");// 是否可用设置为可用
			// 任务列表
			List<MisChecktask> mctList = mctDao.findList(misChecktask);
			for (int i = 0; i < mctList.size(); i++) {

				strContractid = mctList.get(i).getContractid();
				// misContract=mcDao.get(strContractid);
				MisChecktask task = new MisChecktask();
				task = mctList.get(i);
				dealCon(strContractid, task.getWeekremind(),
						task.getMorningremind(),
						task.getNightremind(), task.getId(),
						task);

				// if(){}
			}
			// 合同列表
			// List<MisContract> mcList =mcDao.findAllList(misContract);

			// 处理代码
		} catch (Exception e) {
			logger.debug("处理失败:", e);
		} finally {

		}
		AutoUpdateStatus task = new AutoUpdateStatus();
		String strInterval = Global.getConfig("auto.run.interval");
		int interval = Integer.parseInt(strInterval);// 轮询时间
		AutoRunServlet.timer.schedule(task, interval * 60 * 1000);// 循环一次
	}
	
	//发送给公司领导微信提醒功能
	private synchronized void SendWeChat(){
//		Calendar rightNow = Calendar.getInstance();
//		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		Date date = new Date();
		String formatDate = DateUtils.formatDate(date, "HH:mm");
		
		String time = "09:00" ;
		if (formatDate.equals(time) && formatDate != null){			
			User user = new User();
			user.setOffice(new Office("2"));
			List<User> list  = userDao.findUserByOfficeId(user);
			for (User user2 : list) {
				String weixinUser = user2.getEmail();
//				String loginName = user2.getLoginName();
//				String name = user2.getName();
				String name = "点击全文查看";
				String serviceContent ="昨天服务排名统计查看";
				String tmp ="";
				DictUtils.postWeixinTask(tmp, "11", weixinUser, name, serviceContent, 0);
				//System.out.println(weixinUser);
			}
			//String weixinUser = "464560551@qq.com";
		}
	}

	// 参数：合同ID、提前几天提醒
	@SuppressWarnings("deprecation")
	private void dealCon(String strContractid, String strweek, String strMor,
			String strNight, String taskId, MisChecktask Checktask)
			throws ParseException {
		String ChecktaskId= Checktask.getId();
		// 根据合同ID得到合同信息
		misContract = mcDao.get(strContractid);
		Calendar rightNow = Calendar.getInstance();
		int hour = rightNow.get(Calendar.HOUR_OF_DAY);
		// Date now = new Date();
		// 时间格式
		SimpleDateFormat datehour = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dConEnd = misContract.getEnddate();// 合同结束日期
		Date dConStart = misContract.getBegindate();// 合同结束日期
		Date dNow = new Date();// 当前日期
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");

		// 当前时间必须在合同有效期之内即结束日期要在当前时期之后
		if (dConEnd.getTime() >= dNow.getTime()) {
			// 巡检方式一周两周一个月两个月一季度
			String strCheckType = misContract.getChecktype();
			String xjDate = "";
			// 循环合同中需要巡检的日期
			Calendar cd = Calendar.getInstance();
			cd.setTime(dConStart);
			// 在结束日期之前每天循环开始日期和结束日期之间循环
			// misChecktask= mctDao.get(taskId);
			while (cd.getTime().before(dConEnd)) {
				xjDate = sf.format(cd.getTime());
				Date datexjDate = sf.parse(xjDate);
				if (strweek.equals("1")) {
					// 判断是否提前一周发过
					if (Checktask.getAdvweek() != null) {
						String Advweek = sf.format(Checktask.getAdvweek());
						if (Advweek.equals(sf.format(dNow))) {
							break;
						}
					}
					Calendar cdWeek = Calendar.getInstance();
					cdWeek.setTime(datexjDate);
					cdWeek.add(Calendar.WEEK_OF_YEAR, -1);
					String rDateWeek = sf.format(cdWeek.getTime());
					// Date a= misChecktask.getAdvweek();
					if (rDateWeek.equals(sf.format(dNow))) {
						// 提前一周提醒
						String userId = misContract.getSalesdirector();
						String weixinUser = UserUtils.get(userId).getEmail();
					
					//	MisChecktask	temptask = new MisChecktask();
						// MisChecktask misChecktask=new MisChecktask();\
						Checktask.setId(ChecktaskId);
					//	Checktask.setContractid(strContractid);
						Checktask.setAdvweek(dNow);
						mctDao.update(Checktask);
						
						String custName = DictUtils.getCustomerName(
								misContract.getCustomerno(), "");
						String strProjectname = misContract.getProjectname();
						DictUtils
								.postWeixinTask("", "10", weixinUser, custName,
										strProjectname + "下周即将巡检！请按时安排人员！", 0);
						break;
					}
				}

				// SimpleDateFormat dateHour = new
				// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//可以方便地修改日期格式
				// 当天早上提醒 如果现在是9点进入循环
				if (strMor.equals("1") && hour == 11) {
					String Advmorning = datehour.format(Checktask
							.getAdvmorning());
					if (Advmorning.equals(datehour.format(dNow))) {
						break;
					}
					if (sf.format(sf.parse(xjDate)).equals(sf.format(dNow))) {
						// 提前一周提醒
						String userId = misContract.getSalesdirector();
						String weixinUser = UserUtils.get(userId).getEmail();
						//MisChecktask	temptask = new MisChecktask();
						Checktask = new MisChecktask();
						Checktask.setId(ChecktaskId);
						Checktask.setAdvmorning(dNow);
						mctDao.update(Checktask);
					//	temptask=null;
						String custName = DictUtils.getCustomerName(
								misContract.getCustomerno(), "");
						String strProjectname = misContract.getProjectname();
						DictUtils
								.postWeixinTask("", "10", weixinUser, custName,
										strProjectname + "今天即将巡检！请按时安排人员！", 0);
						break;
					}
					// Date a= misChecktask.getAdvweek();
				}
				// 当天晚上提醒
				if (strNight.equals("1") && hour == 18) {
					if (Checktask.getAdvnight() != null) {
						String AdvNight = sf.format(Checktask.getAdvnight());
						if (AdvNight.equals(sf.format(dNow))) {
							break;
						}
					}
					if (sf.format(sf.parse(xjDate)).equals(sf.format(dNow))) {
						// 当天晚上提醒
						String userId = misContract.getSalesdirector();
						String weixinUser = UserUtils.get(userId).getEmail();
						//MisChecktask	temptask = new MisChecktask();
						//Checktask = new MisChecktask();
						Checktask.setId(ChecktaskId);
						Checktask.setAdvnight(dNow);
						mctDao.update(Checktask);
						//Checktask=null;
						String custName = DictUtils.getCustomerName(
								misContract.getCustomerno(), "");
						String strProjectname = misContract.getProjectname();
						DictUtils.postWeixinTask("", "10", weixinUser,
								custName, strProjectname + "今天白天巡检！请检查是否按时安排人员！", 0);
						break;
					}
				}
				System.out.println(Checktask.getAdvday()+"###########");
				
				if (Checktask.getAdvday()!=null) {
					String Advday = sf.format(Checktask.getAdvday());
					if (Advday.equals(sf.format(dNow))) {
						break;
					}
				}
				// 提前一天必须提醒
				Calendar cdDay = Calendar.getInstance();
				cdDay.setTime(datexjDate);
				cdDay.add(Calendar.DAY_OF_YEAR, -1);
				String rDateDay = sf.format(cdDay.getTime());
				// Date a= misChecktask.getAdvweek();
				if (rDateDay.equals(sf.format(dNow))) {
					// 提前一周提醒
					String userId = misContract.getSalesdirector();
					String weixinUser = UserUtils.get(userId).getEmail();
					//MisChecktask	temptask = new MisChecktask();
					//Checktask=new MisChecktask();
					//System.out.println(ChecktaskId+"$$$$$$$$$$$$$");
					Checktask.setId(ChecktaskId);
					Checktask.setAdvday(dNow);
					mctDao.update(Checktask);
					//Checktask=null;
					String custName = DictUtils.getCustomerName(
							misContract.getCustomerno(), "");
					String strProjectname = misContract.getProjectname();
					DictUtils.postWeixinTask("", "10", weixinUser, custName,
							strProjectname + "明天即将巡检！请按时安排人员！", 0);
					break;
				}

				// 当前合同巡检日期

				if (strCheckType.equals("1")) {// 一周巡检一次
					cd.add(Calendar.WEEK_OF_YEAR, 1);
				} else if (strCheckType.equals("2")) {// 二周巡检一次
					cd.add(Calendar.WEEK_OF_YEAR, 2);
				} else if (strCheckType.equals("3")) {// 一个月巡检一次
					cd.add(Calendar.MONTH, 1);
				} else if (strCheckType.equals("4")) {// 两个月巡检一次
					cd.add(Calendar.MONTH, 2);
				} else if (strCheckType.equals("5")) {// 一季度巡检一次
					cd.add(Calendar.MONTH, 3);
				}
			}
		}
	}

}
