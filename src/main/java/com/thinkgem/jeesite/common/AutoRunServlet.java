package com.thinkgem.jeesite.common;

import java.io.IOException;
import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AutoRunServlet
 */
public class AutoRunServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String autoPayRunTime;  
    static Timer timer = null;  
    static {  
        timer = new Timer();  
        AutoUpdateStatus task = new AutoUpdateStatus();  
        timer.schedule(task, 1000);  
    }        
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutoRunServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


}
