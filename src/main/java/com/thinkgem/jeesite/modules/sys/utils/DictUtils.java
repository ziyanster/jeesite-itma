﻿/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.commons.lang3.StringUtils;



import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.mapper.JsonMapper;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.CacheUtils;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.modules.cms.entity.Article;
import com.thinkgem.jeesite.modules.mis.dao.MisContractDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartCustomerDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartOrderDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartTaskDao;
import com.thinkgem.jeesite.modules.mis.entity.MisContract;
import com.thinkgem.jeesite.modules.mis.entity.SmartApproval;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.mis.entity.jsonTask;
import com.thinkgem.jeesite.modules.oa.entity.OaTempTask;
import com.thinkgem.jeesite.modules.sys.dao.DictDao;
import com.thinkgem.jeesite.modules.sys.dao.RoleDao;
import com.thinkgem.jeesite.modules.sys.dao.UserDao;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.sys.entity.Role;
import com.thinkgem.jeesite.modules.sys.entity.User;


/**
 * 字典工具类
 * @author ThinkGem
 * @version 2013-5-29
 */
public class DictUtils {

	private static  DictDao dictDao = SpringContextHolder.getBean(DictDao.class);
	private static  UserDao userDao= SpringContextHolder.getBean(UserDao.class);
	private static  RoleDao roleDao= SpringContextHolder.getBean(RoleDao.class);
	public static final String CACHE_DICT_MAP = "dictMap";
	
	public static String getDictLabel(String value, String type, String defaultValue){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(value)){
			for (Dict dict : getDictList(type)){
				if (type.equals(dict.getType()) && value.equals(dict.getValue())){
					return dict.getLabel();
				}
			}
		}
		return defaultValue;
	}
	
	public static String getDictLabels(String values, String type, String defaultValue){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(values)){
			List<String> valueList = Lists.newArrayList();
			for (String value : StringUtils.split(values, ",")){
				valueList.add(getDictLabel(value, type, defaultValue));
			}
			return StringUtils.join(valueList, ",");
		}
		return defaultValue;
	}

	public static String getDictValue(String label, String type, String defaultLabel){
		if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(label)){
			for (Dict dict : getDictList(type)){
				if (type.equals(dict.getType()) && label.equals(dict.getLabel())){
					return dict.getValue();
				}
			}
		}
		return defaultLabel;
	}
	
	public static List<Dict> getDictList(String type){
		@SuppressWarnings("unchecked")
		Map<String, List<Dict>> dictMap = (Map<String, List<Dict>>)CacheUtils.get(CACHE_DICT_MAP);
		if (dictMap==null){
			dictMap = Maps.newHashMap();
			for (Dict dict : dictDao.findAllList(new Dict())){
				List<Dict> dictList = dictMap.get(dict.getType());
				if (dictList != null){
					dictList.add(dict);
				}else{
					dictMap.put(dict.getType(), Lists.newArrayList(dict));
				}
			}
			CacheUtils.put(CACHE_DICT_MAP, dictMap);
		}
		List<Dict> dictList = dictMap.get(type);
		if (dictList == null){
			dictList = Lists.newArrayList();
		}
		return dictList;
	}
	
	/**
	 * 返回字典列表（JSON）
	 * @param type
	 * @return
	 */
	public static String getDictListJson(String type){
		return JsonMapper.toJsonString(getDictList(type));
	}
	
	private static SmartCustomerDao scDao = SpringContextHolder.getBean(SmartCustomerDao.class);
	private static MisContractDao mcDao = SpringContextHolder.getBean(MisContractDao.class);
	public static String getCustomerName(String value,  String defaultValue){
		if (StringUtils.isNotBlank(value)){
			SmartCustomer sc_in= new SmartCustomer();
			sc_in.setCustomerno(value);
			List<SmartCustomer> scList =scDao.findList(sc_in);
			if(scList.size()==1)
				return scList.get(0).getCustomername();
		}
		return defaultValue;
	}	
	public static String getCustomerArea(String value,  String defaultValue){
		if (StringUtils.isNotBlank(value)){
			SmartCustomer sc_in= new SmartCustomer();
			sc_in.setCustomerno(value);
			List<SmartCustomer> scList =scDao.findList(sc_in);
			if(scList.size()==1)
				return scList.get(0).getArea();
		}
		return defaultValue;
	}	

	public static List<SmartCustomer> getCustomerList(){
		//Page<SmartCustomer> pageParam=new Page<SmartCustomer>();
		//pageParam.setPageSize(10);		
		SmartCustomer sc= new SmartCustomer();
		//sc.setPage(pageParam);
		List<SmartCustomer> wsList = scDao.findAllList(sc);
		if(wsList!=null)
			return wsList;
		return Lists.newArrayList();
	}	
	//查询巡检合同
	public static List<MisContract> getContractList(){
		//Page<SmartCustomer> pageParam=new Page<SmartCustomer>();
		//pageParam.setPageSize(10);		
		MisContract mc= new MisContract();
		//sc.setPage(pageParam);
		List<MisContract> wsList = mcDao.findAllList(mc);
		if(wsList!=null)
			return wsList;
		return Lists.newArrayList();
	}	

	//DictUtils.postWeixinTask(0,"huangsd@itma.com.cn",smartServiceDoc.getCustomername(),smartServiceDoc.getRemarks());
	public static String  postWeixinTask( String orderId,String type,String userName,String customerName,String serviceContent,int service_reward) { 
    	
    	HttpURLConnection conn = null ; 
    	String ret = ""; 
    	String urlTaskName="";
    	String title="";
    	String picName="";
    	String cilckUrl= "";
    	try {
    		if(type.equals("0")){//指派任务
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_task?agentId=2&user="+userName;
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/assignmentTaskDetailFromMsg.html?orderId="+orderId+"&userId="+userName;
    			title="您收到一个新的指派任务，请点击确认?";
    			//title="New Task Here, Take it?";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-zhipai.jpg";
    		}else if(type.equals("1")){//抢单任务
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/grab_task?agentId=2";
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/grabOrderDetailFromMsg.html?orderId="+orderId;
    			title="刚刚发布抢单任务，赶紧下手抢啊！";
    			//title="New Assign Task,Take it?";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-qiangdan.jpg";
    		}if(type.equals("3")){//成功
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_task?agentId=2&user="+userName;
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/orderDetailFromMsg.html?orderId="+orderId;
    			title="您的申请已生效！";
    			//title="New Task Here, Take it?";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-success.png";    			 			
    		}if(type.equals("4")){//主动退单
        			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_task?agentId=2&user="+userName;
        			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/wechatTaskInfo.html?formId="+orderId;
        			title="您的退单申请已生效！";
        			//title="New Task Here, Take it?";
        			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-quit.png";    		
    		}if(type.equals("9")){//新闻通知
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/grab_task?agentId=2";
    			//cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/orderDetailFromMsg.html?orderId="+orderId;
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/f/view-3-1.html";
    			title="请查阅新闻通知！";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-quit.png";           			
    		}
    		if(type.equals("10")){//巡检任务通知
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_task?agentId=2&user="+userName;
    			//cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/orderDetailFromMsg.html?orderId="+orderId;
    			cilckUrl= "";
    			title="巡检任务通知！";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-zhipai.jpg";           			
    		}
    		if(type.equals("11")){
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/service_ranking?agentId=2&user="+userName;
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/serviceRankingDetailFromMsg.html?orderId="+orderId+"&userId="+userName;
    			title="服务排名统计通知！";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-tongji.jpg";
    		}
    		if(type.equals("12")){
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_task?agentId=2&user="+userName;
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/projMngerAssignDetail.html?taskId="+orderId+"&userId="+userName;
    			title="指派运维任务！";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-zhipai.jpg";
    		}
    		if(type.equals("13")){
    			urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_task?agentId=2&user="+userName;
    			cilckUrl= "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/engineerTaskDetail.html?orderId="+orderId+"&userId="+userName;
    			title="您收到一个新的指派任务，请点击确认?";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-zhipai.jpg";
    		}
    		
    		// 为指定人发送系统提示
    		// DictUtils.postWeixinTask("", "99", "wangfy@itma.com.cn", "", "这是提示的内容", 0);
    		if(type.equals("99")){
    			urlTaskName = "http://wechat.itma.com.cn/jeesite/itma/send_prompt?agentId=2&user="+userName;
    			title = "提示信息";
    			picName="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-prompt.jpg";
    		}
    		URL url = new URL(urlTaskName); 
    		conn = (HttpURLConnection)url.openConnection();
    		conn.setDoOutput(true); 
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json");
    		
    		JSONObject jo = new JSONObject();
    		jo.put("title",title); 
            jo.put("customer_name",  customerName );
            jo.put("service_content",serviceContent); 
            jo.put("service_reward", service_reward);
            if(!type.equals("99")){
            	jo.put("url",cilckUrl);
            }
            jo.put("picurl", picName); 
            
    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		out.write(jo.toString().getBytes());
    		//out.write(jo.toString().getBytes("utf-8"));
    		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
    		char[] buffer = new char[1024]; 
    		int count = 0;
    		StringBuilder strBuilder = new StringBuilder(); 
    		while ((count = reader.read(buffer)) > 0) {
    			strBuilder.append(buffer, 0, count); 
    		}
    		ret = strBuilder.toString(); 
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(conn != null ) {
    			conn.disconnect();
    		}
    	}
    	return ret; 
    	
    	
    }	
	
	private static SmartTaskDao stDao = SpringContextHolder.getBean(SmartTaskDao.class);
	public static SmartTask getTask(String value){
		SmartTask st_in =new SmartTask();
		SmartTask st_out=new SmartTask();
		st_in.setId(value);
		st_out=stDao.get(st_in);
		if(st_out!=null)
			return st_out;
		else
			return st_in;

	}	

	public static SmartCustomer getCustomer(String value){
		SmartCustomer sc_in= new SmartCustomer();
		SmartCustomer sc_out= new SmartCustomer();		
		if (StringUtils.isNotBlank(value)){
			sc_in.setCustomerno(value);
			List<SmartCustomer> scList =scDao.findList(sc_in);
			if(scList.size()==1)
				return scList.get(0);
		}
		return sc_out;
	}	
	public  static String getReadyGrabList(String serviceType,int pageNo){
		List<jsonTask> jsonPage  = Lists.newArrayList();
		Page<SmartTask> pageParam=new Page<SmartTask>();
		pageParam.setPageSize(2);
		pageParam.setPageNo(pageNo);
		pageParam.setOrderBy("incomingtime");
		
		SmartTask st = new SmartTask();
		st.setSendtype("1");//抢单任务
		st.setStatus("1");//待抢单
		st.setServicetype(serviceType);
		st.setPage(pageParam);
		pageParam.setList(stDao.findList(st));
		long totalCount=pageParam.getCount();
		long checkCount =(pageNo-1)*2;
		if(checkCount<totalCount){
			List<SmartTask> listOrder = stDao.findList(st);
			for(int i=0;i<listOrder.size();i++){
				SmartTask st_out = listOrder.get(i) ;
				jsonTask jsonT=new jsonTask();
				jsonT.setOrderId(st_out.getId());
				jsonT.setOrderHref("http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/grabOrderDetail.html");
				jsonT.setOrderPrice(Integer.toString(st_out.getIntegral()));
				jsonT.setOrderSubTitle(DictUtils.getCustomerName(st_out.getCustomerno(),""));
				jsonT.setOrderTime("30");
				jsonT.setOrderTitle(st_out.getRecord());
				jsonPage.add(jsonT);
			}		
		}
		return  JsonMapper.toJsonString(jsonPage);
	}	
	private static SmartOrderDao soDao = SpringContextHolder.getBean(SmartOrderDao.class);
	public  static String getReadyAssignList(String userId,String serviceType,int pageNo){
		List<jsonTask> jsonPage  = Lists.newArrayList();
		Page<SmartOrder> pageParam=new Page<SmartOrder>();
		pageParam.setPageSize(2);
		pageParam.setPageNo(pageNo);
		pageParam.setOrderBy("createDate");
		
		SmartOrder so = new SmartOrder();
		so.setIsassigned("0");//指派任务
		so.setStatus("0");//待受理
		so.setAssignedno(UserUtils.getByEmail(userId));
		so.setServicetype(serviceType);
		so.setPage(pageParam);
		pageParam.setList(soDao.findList(so));
		long totalCount=pageParam.getCount();
		long checkCount =(pageNo-1)*2;
		if(checkCount<totalCount){
			List<SmartOrder> listOrder = soDao.findList(so);
			for(int i=0;i<listOrder.size();i++){
				SmartOrder so_out = listOrder.get(i) ;
				String sTaskId=listOrder.get(i).getTaskno();
				SmartTask st=stDao.get(sTaskId);				
				jsonTask jsonT=new jsonTask();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				jsonT.setOrderId(sTaskId);
				jsonT.setOrderHref("http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/assignmentTaskDetail.html?orderId="+sTaskId);
				jsonT.setOrderPrice(Integer.toString(st.getIntegral()==null ? 0:st.getIntegral()));
				jsonT.setOrderSubTitle(DictUtils.getCustomerName(so_out.getCustomerno(),""));
				jsonT.setOrderTime(sdf.format(st.getCreateDate()));
				jsonT.setOrderTitle(st.getRecord());
				jsonPage.add(jsonT);
			}		
		}
		return  JsonMapper.toJsonString(jsonPage);
	}	
	/**
	 *同步微信用户
	 *@param type 0新增、1更新
	 *@return
	 *@exception  (方法有异常的话加)
	 *@author YM
	 *@Time2015-10-19
	 */
	public static String postSynWeixinUser(String userId, String name,
			int[] department, String position, String mobile, String gender,
			String email, String weiXinId, String enable) throws IOException {

		HttpURLConnection conn = null;
		String ret = "";
		String type = "1";
		String sub = "errcode";
		String jsonUser = getWeChatUser(email);
		int a = jsonUser.indexOf(sub);
		if (a >= 0) {
			type = "0";
		}

		String urlSynWeixinUser = "";

		try {
			if (type.equals("0")) {// 新增用户
				urlSynWeixinUser = "http://wechat.itma.com.cn/jeesite/itma/create_user";
			} else {// 更新用户
				urlSynWeixinUser = "http://wechat.itma.com.cn/jeesite/itma/update_user";
			}
			URL url = new URL(urlSynWeixinUser);
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			// 拼凑用户json
			org.activiti.engine.impl.util.json.JSONObject jo = new org.activiti.engine.impl.util.json.JSONObject();
			jo.put("avatar_url", "");
			jo.put("userid", email);
			jo.put("name", name);
			jo.put("department", department);
			jo.put("position", position);
			jo.put("mobile", mobile);
			jo.put("gender", gender);
			jo.put("email", email);
			jo.put("weixinid", weiXinId);
			System.out.print(weiXinId+"微信号");
			if (type.equals("1")) {// 更新用户的是否可用
				jo.put("enable", enable);
			}
			OutputStream out = new DataOutputStream(conn.getOutputStream());
			out.write(jo.toString().getBytes());
			//out.write(jo.toString().getBytes("utf-8"));
			InputStreamReader reader = new InputStreamReader(
					conn.getInputStream());
			char[] buffer = new char[1024];
			int count = 0;
			StringBuilder strBuilder = new StringBuilder();
			while ((count = reader.read(buffer)) > 0) {
				strBuilder.append(buffer, 0, count);
			}
			ret = strBuilder.toString();
			if (type.equals("0")) {// 新增用户
				getInviteFriend(email);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		System.out.print(ret);
		return ret;
	}
	public static String getWeChatUser(String userId) throws IOException {
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		String lines = "";
		String ret = "";
		String getURL = "http://wechat.itma.com.cn/jeesite/itma/get_userinfo?user="
				+ userId;
		URL getUrl;
		try {
			getUrl = new URL(getURL);
			connection = (HttpURLConnection) getUrl.openConnection();
			connection.connect();
			reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "utf-8"));

			while ((lines = reader.readLine()) != null) {
				ret = lines;
				System.out.println(lines);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			reader.close();
			// 断开连接
			connection.disconnect();
		}
		System.out.println(ret);
		return ret;
	}
	public static String getInviteFriend(String userId) throws IOException {
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		String lines = "";
		String ret = "";
		String getURL = "http://wechat.itma.com.cn/jeesite/itma/invite_friend?user="
				+ userId;
		URL getUrl;
		try {
			getUrl = new URL(getURL);
			connection = (HttpURLConnection) getUrl.openConnection();
			connection.connect();
			reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "utf-8"));

			while ((lines = reader.readLine()) != null) {
				ret = lines;
				System.out.println(lines);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			reader.close();
			// 断开连接
			connection.disconnect();
		}
	
		return ret;
	}
	public static String  postWeixinNewsToSomeone(String offices,String users, String categoryId,Article article) { 
    	
    	HttpURLConnection conn = null ; 
    	String ret = ""; 
    	String urlTaskName="http://wechat.itma.com.cn/jeesite/itma/send_news_tosomeone?agentId=2";
      	String cilckUrl="http://wechat.itma.com.cn/jeesite/f/view-"+categoryId+"-"+article.getId()+".html";	
      	String picUrl="http://wechat.itma.com.cn"+article.getImageSrc();
//    	String urlTaskName="http://192.168.153.65:8080/jeesite/itma/send_news_tosomeone?agentId=2";
//      String cilckUrl="http://192.168.153.65:8080/jeesite/f/view-"+categoryId+"-"+article.getId()+".html";	
//      String picUrl="http://192.168.153.65:8080"+article.getImageSrc();
    	try{
    		offices = offices.replace(",", "|");
    		String[] ids = users.split(",");
    		List<String> newIds = new ArrayList<String>();
    		for (String str : ids) {
    			if(!StringUtils.isBlank(str))
				{
    				newIds.add(userDao.get(str).getEmail());
				}
			}
    		users = StringUtils.join(newIds,"|");    		
    		urlTaskName = urlTaskName + "&users=" + users + "&parties=" + offices;
    		
    		URL url = new URL(urlTaskName); 
    		conn = (HttpURLConnection)url.openConnection();
    		conn.setDoOutput(true); 
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json");
    		
    		JSONObject jo = new JSONObject();
    		jo.put("title",article.getTitle()); 
            jo.put("customer_name",  "栏目名称："+article.getCategory().getName() );
            jo.put("service_content","摘要:"+article.getDescription()); 
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            jo.put("service_reward","发布时间:"+ sdf.format(article.getCreateDate()));
            jo.put("url",cilckUrl);
            jo.put("picurl",picUrl ); 
            
    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		//out.write(jo.toString().getBytes("utf-8"));
    		out.write(jo.toString().getBytes());
    		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
    		char[] buffer = new char[1024]; 
    		int count = 0;
    		StringBuilder strBuilder = new StringBuilder(); 
    		while ((count = reader.read(buffer)) > 0) {
    			strBuilder.append(buffer, 0, count); 
    		}
    		ret = strBuilder.toString(); 
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(conn != null ) {
    			conn.disconnect();
    		}
    	}
    	return ret; 
    	
    	
    }	
	public static String  postWeixinNews( String categoryId,Article article) { 
    	
    	HttpURLConnection conn = null ; 
    	String ret = ""; 
    	String urlTaskName="http://wechat.itma.com.cn/jeesite/itma/send_news?agentId=2";
      	String cilckUrl="http://wechat.itma.com.cn/jeesite/f/view-"+categoryId+"-"+article.getId()+".html";	
      	String picUrl="http://wechat.itma.com.cn"+article.getImageSrc();
    	try{
    		URL url = new URL(urlTaskName); 
    		conn = (HttpURLConnection)url.openConnection();
    		conn.setDoOutput(true); 
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json");
    		
    		JSONObject jo = new JSONObject();
    		jo.put("title",article.getTitle()); 
            jo.put("customer_name",  "栏目名称："+article.getCategory().getName() );
            jo.put("service_content","摘要:"+article.getDescription()); 
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            jo.put("service_reward","发布时间:"+ sdf.format(article.getCreateDate()));
            jo.put("url",cilckUrl);
            jo.put("picurl",picUrl ); 
            
    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		//out.write(jo.toString().getBytes("utf-8"));
    		out.write(jo.toString().getBytes());
    		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
    		char[] buffer = new char[1024]; 
    		int count = 0;
    		StringBuilder strBuilder = new StringBuilder(); 
    		while ((count = reader.read(buffer)) > 0) {
    			strBuilder.append(buffer, 0, count); 
    		}
    		ret = strBuilder.toString(); 
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(conn != null ) {
    			conn.disconnect();
    		}
    	}
    	return ret; 
    	
    	
    }	
	
	public static String  postWeixinApproval( SmartApproval sa) { 
    	
    	HttpURLConnection conn = null ; 
    	String ret = "";
      	String cilckUrl="http://wechat.itma.com.cn/jeesite/f/formApproval?id="+sa.getId();	
      	String picName="";
      	String title="";
      	String sOpeDate="";
      	String sContnet="";
      	String userName="";
      	String urlTaskName="";
      	User applyUser=UserUtils.get(sa.getCreateBy().getId());
      	User approvalUser=UserUtils.get(sa.getApprovalBy().getId());
      	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      	if(sa.getStatus().equals("0")){
      		picName="pic-approval.jpg";
      		title="您有一个待审批应急申请";
      		sContnet="申请人："+applyUser.getName();
      		sOpeDate=sdf.format(sa.getCreateDate());
      		//sOpeDate=DateUtils.formatDate(sa.getCreateDate(), "yyyy-MM-dd HH:mm:ss");
      		userName=approvalUser.getEmail();
      		urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_approval?user="+userName; 
      	}else if(sa.getResult().equals("0")){
      		title="您的申请未通过审批";
      		picName="pic-no.jpg";
      		sOpeDate=sdf.format(sa.getApprovalDate());
      		//sOpeDate=DateUtils.formatDate(sa.getApprovalDate(), "yyyy-MM-dd HH:mm:ss");
      		sContnet="审批结果：通过";
      		userName=applyUser.getEmail(); 
      		urlTaskName="http://wechat.itma.com.cn/jeesite/f/wxClose";
      	}else if(sa.getResult().equals("1")){
      		title="您的申请已通过审批";
      		picName="pic-yes.jpg";
      		sOpeDate=sdf.format(sa.getApprovalDate());
      		//sOpeDate=DateUtils.formatDate(sa.getApprovalDate(), "yyyy-MM-dd HH:mm:ss");
      		sContnet="审批结果：未通过";
      		userName=applyUser.getEmail();  
      		urlTaskName="http://wechat.itma.com.cn/jeesite/f/wxClose";
      	}
      	String picUrl="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/"+picName; 

    	try{
    		URL url = new URL(urlTaskName); 
    		conn = (HttpURLConnection)url.openConnection();
    		conn.setDoOutput(true); 
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json");
    		
    		JSONObject jo = new JSONObject();
    		jo.put("title",title); 
            jo.put("customer_name",  sContnet);
            jo.put("service_content","申请信息:"+sa.getRemarks()); 
            jo.put("service_reward","发布时间:"+sOpeDate);
            jo.put("url",cilckUrl);
            jo.put("picurl",picUrl ); 
            
    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		//out.write(jo.toString().getBytes("utf-8"));
    		out.write(jo.toString().getBytes());
    		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
    		char[] buffer = new char[1024]; 
    		int count = 0;
    		StringBuilder strBuilder = new StringBuilder(); 
    		while ((count = reader.read(buffer)) > 0) {
    			strBuilder.append(buffer, 0, count); 
    		}
    		ret = strBuilder.toString(); 
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(conn != null ) {
    			conn.disconnect();
    		}
    	}
    	return ret; 
    	
    	
    }		
	
	public static String  postWeixinApply( OaTempTask oaTempTask) { 
      	User applyUser=UserUtils.get(oaTempTask.getCreateBy().getId());
      	User approvalUser=UserUtils.get(oaTempTask.getLeaderId().getId());		
		HttpURLConnection conn = null ; 
    	String ret = "";
      	String cilckUrl="http://wechat.itma.com.cn/jeesite/f/act/form?procInsId="+oaTempTask.getProcInsId()+"&taskId="+oaTempTask.getAct().getTaskId()+"&taskName="+oaTempTask.getAct().getTaskName()+"&taskDefKey="+oaTempTask.getAct().getTaskDefKey()+"&isFinishTask=false";
      	String picName="";
      	String title="";
      	String sOpeDate="";
      	String sContnet="";
      	String userName="";
      	String urlTaskName="";
      	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  		picName="pic-approval.jpg";
  		title="您有一个待审批应急申请";
  		sContnet="申请人："+applyUser.getName();
  		sOpeDate=sdf.format(new Date());
  		//sOpeDate=DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
  		userName=approvalUser.getEmail();
  		urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_approval?agentId=0&user="+userName; 

  
      	String picUrl="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/"+picName; 

    	try{
    		URL url = new URL(urlTaskName); 
    		conn = (HttpURLConnection)url.openConnection();
    		conn.setDoOutput(true); 
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json");
    		
    		JSONObject jo = new JSONObject();
    		jo.put("title",title); 
            jo.put("customer_name",  sContnet);
            jo.put("service_content","申请信息:"+oaTempTask.getContent()); 
            jo.put("service_reward","发布时间:"+sOpeDate);
            jo.put("url",cilckUrl);
            jo.put("picurl",picUrl ); 
            jo.put("agentid", "0"); 
    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		//out.write(jo.toString().getBytes("utf-8"));
    		out.write(jo.toString().getBytes());
    		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
    		char[] buffer = new char[1024]; 
    		int count = 0;
    		StringBuilder strBuilder = new StringBuilder(); 
    		while ((count = reader.read(buffer)) > 0) {
    			strBuilder.append(buffer, 0, count); 
    		}
    		ret = strBuilder.toString(); 
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(conn != null ) {
    			conn.disconnect();
    		}
    	}
    	return ret; 
    	
    	
    }
	
	public static String  postWeixinAudit( OaTempTask oaTempTask) { 
    	
    	HttpURLConnection conn = null ; 
    	String ret = "";
      	String cilckUrl="http://wechat.itma.com.cn/jeesite/f/act/form?procInsId="+oaTempTask.getProcInsId()+"&taskId="+oaTempTask.getAct().getTaskId()+"&taskName=&taskDefKey=finish&isFinishTask=true";	
      	String picName="";
      	String title="";
      	String sOpeDate="";
      	String sContnet="";
      	String urlTaskName="";
      	String userName="";
      	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      	User applyUser=UserUtils.get(oaTempTask.getCreateBy().getId());
      	sOpeDate=sdf.format(new Date());
      	if("no".equals(oaTempTask.getAct().getFlag())){
      		title="您的申请未通过审批";
      		picName="pic-no.jpg";
      		//sOpeDate=DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
      		sContnet="审批结果：未通过";
      		userName=applyUser.getEmail(); 
      	}else if("yes".equals(oaTempTask.getAct().getFlag())){
      		title="您的申请已通过审批";
      		picName="pic-yes.jpg";
      		//sOpeDate=DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
      		sContnet="审批结果：通过";
      		userName=applyUser.getEmail();  
      	}
      	String picUrl="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/"+picName; 
  		urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_approval?agentId=0&user="+userName; 
    	try{
    		URL url = new URL(urlTaskName); 
    		conn = (HttpURLConnection)url.openConnection();
    		conn.setDoOutput(true); 
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/json");
    		
    		JSONObject jo = new JSONObject();
    		jo.put("title",title); 
            jo.put("customer_name",  sContnet);
            jo.put("service_content","审批信息:"+oaTempTask.getAct().getComment()); 
            jo.put("service_reward","发布时间:"+sOpeDate);
            jo.put("url",cilckUrl);
            jo.put("picurl",picUrl ); 
            
    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		out.write(jo.toString().getBytes());
    		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
    		char[] buffer = new char[1024]; 
    		int count = 0;
    		StringBuilder strBuilder = new StringBuilder(); 
    		while ((count = reader.read(buffer)) > 0) {
    			strBuilder.append(buffer, 0, count); 
    		}
    		ret = strBuilder.toString(); 
    	} catch(Exception e) {
    		e.printStackTrace();
    	} finally {
    		if(conn != null ) {
    			conn.disconnect();
    		}
    	}
    	
    	//判断是否机票审批
    	if(oaTempTask.getType().endsWith("4") && "yes".equals(oaTempTask.getAct().getFlag())){
          	picUrl="http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-air.jpg"; 
          	//取机票执行角色用户
          	Role r = new Role();
    		r.setName("机票管理员");
    		List<User> list = userDao.findUserByRoleId(roleDao.getByName(r).getId());
    		for(User userAir:list){
    			userName=userAir.getEmail();
          		urlTaskName="http://wechat.itma.com.cn/jeesite/itma/assign_approval?agentId=0&user="+userName; 
            	try{
            		URL url = new URL(urlTaskName); 
            		conn = (HttpURLConnection)url.openConnection();
            		conn.setDoOutput(true); 
            		conn.setRequestMethod("POST");
            		conn.setRequestProperty("Content-Type", "application/json");
            		
            		JSONObject jo = new JSONObject();
            		jo.put("title","机票审批"); 
                    jo.put("customer_name",  sContnet);
                    jo.put("service_content","审批信息:"+oaTempTask.getAct().getComment()); 
                    jo.put("service_reward","发布时间:"+sOpeDate);
                    jo.put("url",cilckUrl);
                    jo.put("picurl",picUrl ); 
                    
            		OutputStream out = new DataOutputStream(conn.getOutputStream());
            		out.write(jo.toString().getBytes());
            		InputStreamReader reader = new InputStreamReader(conn.getInputStream()); 
            		char[] buffer = new char[1024]; 
            		int count = 0;
            		StringBuilder strBuilder = new StringBuilder(); 
            		while ((count = reader.read(buffer)) > 0) {
            			strBuilder.append(buffer, 0, count); 
            		}
            		ret = strBuilder.toString(); 
            	} catch(Exception e) {
            		e.printStackTrace();
            	} finally {
            		if(conn != null ) {
            			conn.disconnect();
            		}
            	}  
    		}
    		
    	}
    	return ret;     	
    	
    }		
}
