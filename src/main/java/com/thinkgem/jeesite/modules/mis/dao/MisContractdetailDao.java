/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.mis.entity.MisContractdetail;

/**
 * 巡检合同DAO接口
 * @author ym
 * @version 2015-12-11
 */
@MyBatisDao
public interface MisContractdetailDao extends CrudDao<MisContractdetail> {
	
}