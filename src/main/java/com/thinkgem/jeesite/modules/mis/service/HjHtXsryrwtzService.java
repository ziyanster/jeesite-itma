/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.HjHtXsryrwtz;
import com.thinkgem.jeesite.modules.mis.dao.HjHtXsryrwtzDao;

/**
 * 销售人员任务台账Service
 * @author 黄尚弟
 * @version 2015-09-24
 */
@Service
@Transactional(readOnly = true)
public class HjHtXsryrwtzService extends CrudService<HjHtXsryrwtzDao, HjHtXsryrwtz> {

	public HjHtXsryrwtz get(String id) {
		return super.get(id);
	}
	
	public List<HjHtXsryrwtz> findList(HjHtXsryrwtz hjHtXsryrwtz) {
		return super.findList(hjHtXsryrwtz);
	}
	
	public Page<HjHtXsryrwtz> findPage(Page<HjHtXsryrwtz> page, HjHtXsryrwtz hjHtXsryrwtz) {
		return super.findPage(page, hjHtXsryrwtz);
	}
	
	@Transactional(readOnly = false)
	public void save(HjHtXsryrwtz hjHtXsryrwtz) {
		super.save(hjHtXsryrwtz);
	}
	
	@Transactional(readOnly = false)
	public void delete(HjHtXsryrwtz hjHtXsryrwtz) {
		super.delete(hjHtXsryrwtz);
	}
	
}