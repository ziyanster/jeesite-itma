/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.MisChecktask;
import com.thinkgem.jeesite.modules.mis.dao.MisChecktaskDao;

/**
 * 巡检任务管理Service
 * @author ym
 * @version 2015-12-14
 */
@Service
@Transactional(readOnly = true)
public class MisChecktaskService extends CrudService<MisChecktaskDao, MisChecktask> {

	public MisChecktask get(String id) {
		return super.get(id);
	}
	
	public List<MisChecktask> findList(MisChecktask misChecktask) {
		return super.findList(misChecktask);
	}
	
	public Page<MisChecktask> findPage(Page<MisChecktask> page, MisChecktask misChecktask) {
		return super.findPage(page, misChecktask);
	}
	
	@Transactional(readOnly = false)
	public void save(MisChecktask misChecktask) {
		super.save(misChecktask);
	}
	
	@Transactional(readOnly = false)
	public void delete(MisChecktask misChecktask) {
		super.delete(misChecktask);
	}
	
}