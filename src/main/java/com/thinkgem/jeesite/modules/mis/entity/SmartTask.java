/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;
import com.thinkgem.jeesite.modules.sys.entity.User;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 运维任务录入Entity
 * @author 黄尚弟
 * @version 2015-10-13
 */
public class SmartTask extends DataEntity<SmartTask> {
	
	private static final long serialVersionUID = 1L;
	private String taskno;		// 任务编号
	private String customerno;		// 客户名称
	private String taskserialno;		// 客户名称

	private Date incomingtime;		// 呼入时间
	private String incomingway;		// 呼入方式
	private String model;		// 机型
	private String seriesno;		// 系列号
	private String brand;		// 品牌
	private String record;		// 呼叫记录
	private String status;		// 状态
	private Integer integral;		// 积分
	private String servicetype;		// 服务类型
	private String faulttype;		// 故障类型
	private String worktype;       // 工作类型
	private String servicelevel;		// 服务评级
	private String sendtype;		// 消息类型
	private User assignuser;		// 指派用户
	private Date beginIncomingtime;		// 开始 呼入时间
	private Date endIncomingtime;		// 结束 呼入时间

	private String ontimecheck;		// 是否巡检
	private Date checkdate;		//巡检日期
	private String serviceAddr;		//服务地址
	private Date serviceDate;		//服务时间
	
	public SmartTask() {
		super();
	}

	public SmartTask(String id){
		super(id);
	}

	@Length(min=0, max=64, message="任务编号长度必须介于 0 和 64 之间")
	public String getTaskno() {
		return taskno;
	}

	public void setTaskno(String taskno) {
		this.taskno = taskno;
	}
	
	@Length(min=0, max=64, message="客户名称长度必须介于 0 和 64 之间")
	public String getCustomerno() {
		return customerno;
	}

	public void setCustomerno(String customerno) {
		this.customerno = customerno;
	}
	public String getTaskserialno() {
		return taskserialno;
	}

	public void setTaskserialno(String taskserialno) {
		this.taskserialno = taskserialno;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="呼入时间不能为空")
	public Date getIncomingtime() {
		return incomingtime;
	}

	public void setIncomingtime(Date incomingtime) {
		this.incomingtime = incomingtime;
	}
	
	@Length(min=0, max=30, message="呼入方式长度必须介于 0 和 30 之间")
	public String getIncomingway() {
		return incomingway;
	}

	public void setIncomingway(String incomingway) {
		this.incomingway = incomingway;
	}
	
	@Length(min=0, max=50, message="机型长度必须介于 0 和 50 之间")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	@Length(min=0, max=64, message="系列号长度必须介于 0 和 64 之间")
	public String getSeriesno() {
		return seriesno;
	}

	public void setSeriesno(String seriesno) {
		this.seriesno = seriesno;
	}
	
	@Length(min=0, max=50, message="品牌长度必须介于 0 和 50 之间")
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@Length(min=0, max=300, message="呼叫记录长度必须介于 0 和 300 之间")
	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}
	
	@Length(min=0, max=30, message="状态长度必须介于 0 和 30 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getIntegral() {
		return integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}
	
	@Length(min=0, max=30, message="服务类型长度必须介于 0 和 30 之间")
	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	
	@Length(min=0, max=30, message="故障类型长度必须介于 0 和 30 之间")
	public String getFaulttype() {
		return faulttype;
	}

	public void setFaulttype(String faulttype) {
		this.faulttype = faulttype;
	}
	
	public String getWorktype() {
		return worktype;
	}

	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}

	@Length(min=0, max=30, message="服务评级长度必须介于 0 和 30 之间")
	public String getServicelevel() {
		return servicelevel;
	}

	public void setServicelevel(String servicelevel) {
		this.servicelevel = servicelevel;
	}
	
	@Length(min=0, max=1, message="消息类型长度必须介于 0 和 1 之间")
	public String getSendtype() {
		return sendtype;
	}

	public void setSendtype(String sendtype) {
		this.sendtype = sendtype;
	}
	
	public User getAssignuser() {
		return assignuser;
	}

	public void setAssignuser(User assignuser) {
		this.assignuser = assignuser;
	}
	
	public Date getBeginIncomingtime() {
		return beginIncomingtime;
	}

	public void setBeginIncomingtime(Date beginIncomingtime) {
		this.beginIncomingtime = beginIncomingtime;
	}
	
	public Date getEndIncomingtime() {
		return endIncomingtime;
	}

	public void setEndIncomingtime(Date endIncomingtime) {
		this.endIncomingtime = endIncomingtime;
	}

	public String getOntimecheck() {
		return ontimecheck;
	}

	public void setOntimecheck(String ontimecheck) {
		this.ontimecheck = ontimecheck;
	}

	public Date getCheckdate() {
		return checkdate;
	}

	public void setCheckdate(Date checkdate) {
		this.checkdate = checkdate;
	}

	public String getServiceAddr() {
		return serviceAddr;
	}

	public void setServiceAddr(String serviceAddr) {
		this.serviceAddr = serviceAddr;
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

}