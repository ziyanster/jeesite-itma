/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.mis.entity.SmartAttachments;

/**
 * 附件管理DAO接口
 * @author 黄尚弟
 * @version 2015-10-25
 */
@MyBatisDao
public interface SmartAttachmentsDao extends CrudDao<SmartAttachments> {
	
}