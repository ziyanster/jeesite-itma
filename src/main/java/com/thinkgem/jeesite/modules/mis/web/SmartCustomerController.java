/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.beanvalidator.BeanValidators;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.common.utils.excel.ImportExcel;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.mis.service.SmartCustomerService;


/**
 * 客户资料维护Controller
 * @author 黄尚弟
 * @version 2015-10-12
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/smartCustomer")
public class SmartCustomerController extends BaseController {

	@Autowired
	private SmartCustomerService smartCustomerService;
	
	@ModelAttribute
	public SmartCustomer get(@RequestParam(required=false) String id) {
		SmartCustomer entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smartCustomerService.get(id);
		}
		if (entity == null){
			entity = new SmartCustomer();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:smartCustomer:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmartCustomer smartCustomer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmartCustomer> page = smartCustomerService.findPage(new Page<SmartCustomer>(request, response), smartCustomer); 
		model.addAttribute("page", page);
		return "modules/mis/smartCustomerList";
	}

	@RequiresPermissions("mis:smartCustomer:view")
	@RequestMapping(value = "form")
	public String form(SmartCustomer smartCustomer, Model model) {
		model.addAttribute("smartCustomer", smartCustomer);
		return "modules/mis/smartCustomerForm";
	}

	@RequiresPermissions("mis:smartCustomer:edit")
	@RequestMapping(value = "save")
	public String save(SmartCustomer smartCustomer, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartCustomer)){
			return form(smartCustomer, model);
		}
		smartCustomerService.save(smartCustomer);
		addMessage(redirectAttributes, "保存客户资料维护成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartCustomer/?repage";
	}
	
	@RequiresPermissions("mis:smartCustomer:edit")
	@RequestMapping(value = "delete")
	public String delete(SmartCustomer smartCustomer, RedirectAttributes redirectAttributes) {
		smartCustomerService.delete(smartCustomer);
		addMessage(redirectAttributes, "删除客户资料维护成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartCustomer/?repage";
	}
	/**
	 * 导出数据
	 * @param smartCustomer
	 * @param request
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("mis:smartCustomer:view")
    @RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(SmartCustomer smartCustomer, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "客户资料数据"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            List<SmartCustomer> list=smartCustomerService.findList(smartCustomer);
    		 
    		new ExportExcel("客户资料数据", SmartCustomer.class).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出客户资料数据失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mis/smartCustomer/?repage";
    }
	/**
	 * 导入数据
	 * @param file
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("mis:smartCustomer:view")
    @RequestMapping(value = "import", method=RequestMethod.POST)
    public String importFile(MultipartFile file, RedirectAttributes redirectAttributes) {
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<SmartCustomer> list = ei.getDataList(SmartCustomer.class);
			for (SmartCustomer cust : list){
				try{
					//过滤重复ID，缩写，客户名称
					//if ("true".equals(checkLoginName("", cust.getCustomername()))){
						BeanValidators.validateWithException(validator, cust);
						smartCustomerService.save(cust);
						successNum++;
					//}else{
					//	failureMsg.append("<br/>登录名 过滤重复ID，缩写，客户名称已存在; ");
					//	failureNum++;
					//}
				}catch(ConstraintViolationException ex){
					failureMsg.append("<br/>客户名称 "+cust.getCustomername()+" 导入失败：");
					List<String> messageList = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
					for (String message : messageList){
						failureMsg.append(message+"; ");
						failureNum++;
					}
				}catch (Exception ex) {
					failureMsg.append("<br/>客户名称 "+cust.getCustomername()+" 导入失败："+ex.getMessage());
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条客户，导入信息如下：");
			}
			addMessage(redirectAttributes, "已成功导入 "+successNum+" 条客户"+failureMsg);
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入客户失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mis/smartCustomer/?repage";
    }
	/**
	 * 下载导入数据模板
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("mis:smartCustomer:view")
    @RequestMapping(value = "import/template")
    public String importFileTemplate(HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "数据导入模板.xlsx";
    		List<SmartCustomer> list = Lists.newArrayList(); 
    		list.add(new SmartCustomer());
    		new ExportExcel("数据", SmartCustomer.class, 2).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导入模板下载失败！失败信息："+e.getMessage());
		}
		return "redirect:"+Global.getAdminPath()+"/mis/smartCustomer/?repage";
    }	
}