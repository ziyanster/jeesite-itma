/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.SmartAttachments;
import com.thinkgem.jeesite.modules.mis.dao.SmartAttachmentsDao;

/**
 * 附件管理Service
 * @author 黄尚弟
 * @version 2015-10-25
 */
@Service
@Transactional(readOnly = true)
public class SmartAttachmentsService extends CrudService<SmartAttachmentsDao, SmartAttachments> {

	public SmartAttachments get(String id) {
		return super.get(id);
	}
	
	public List<SmartAttachments> findList(SmartAttachments smartAttachments) {
		return super.findList(smartAttachments);
	}
	
	public Page<SmartAttachments> findPage(Page<SmartAttachments> page, SmartAttachments smartAttachments) {
		return super.findPage(page, smartAttachments);
	}
	
	@Transactional(readOnly = false)
	public void save(SmartAttachments smartAttachments) {
		super.save(smartAttachments);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmartAttachments smartAttachments) {
		super.delete(smartAttachments);
	}
	
}