/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.StatsParam;
import com.thinkgem.jeesite.modules.mis.entity.StatsResult;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.cms.dao.ArticleDao;
import com.thinkgem.jeesite.modules.cms.entity.Category;
import com.thinkgem.jeesite.modules.cms.entity.Site;
import com.thinkgem.jeesite.modules.mis.dao.SmartOrderDao;

/**
 * 工单管理Service
 * @author 黄尚弟
 * @version 2015-10-21
 */
@Service
@Transactional(readOnly = true)
public class SmartOrderService extends CrudService<SmartOrderDao, SmartOrder> {

	public SmartOrder get(String id) {
		return super.get(id);
	}
	
	public List<SmartOrder> findList(SmartOrder smartOrder) {
		return super.findList(smartOrder);
	}
	
	public Page<SmartOrder> findPage(Page<SmartOrder> page, SmartOrder smartOrder) {
		return super.findPage(page, smartOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(SmartOrder smartOrder) {
		super.save(smartOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmartOrder smartOrder) {
		super.delete(smartOrder);
	}
	
	@Autowired
	private SmartOrderDao soDao;
	
	public List<StatsResult> findExitStats(Map<String, Object> paramMap){	
		StatsParam param = new StatsParam();		
		
		Date beginDate = DateUtils.parseDate(paramMap.get("beginDate"));
		if (beginDate == null){
			beginDate = DateUtils.setDays(new Date(), 1);
			paramMap.put("beginDate", DateUtils.formatDate(beginDate, "yyyy-MM-dd"));
		}
		param.setBeginDate(beginDate);
		Date endDate = DateUtils.parseDate(paramMap.get("endDate"));
		if (endDate == null){
			endDate = DateUtils.addDays(DateUtils.addMonths(beginDate, 1), -1);
			paramMap.put("endDate", DateUtils.formatDate(endDate, "yyyy-MM-dd"));
		}
		param.setEndDate(endDate);
		
		
		List<StatsResult> list = soDao.findExitStats(param);
		return list;
	}
	
	public List<StatsResult> findStatusStats(Map<String, Object> paramMap){	
		StatsParam param = new StatsParam();		
		
		Date beginDate = DateUtils.parseDate(paramMap.get("beginDate"));
		if (beginDate == null){
			beginDate = DateUtils.setDays(new Date(), 1);
			paramMap.put("beginDate", DateUtils.formatDate(beginDate, "yyyy-MM-dd"));
		}
		param.setBeginDate(beginDate);
		Date endDate = DateUtils.parseDate(paramMap.get("endDate"));
		if (endDate == null){
			endDate = DateUtils.addDays(DateUtils.addMonths(beginDate, 1), -1);
			paramMap.put("endDate", DateUtils.formatDate(endDate, "yyyy-MM-dd"));
		}
		param.setEndDate(endDate);
		
		
		List<StatsResult> list = soDao.findStatusStats(param);
		return list;
	}
	
	public List<StatsResult> findIndexStats(Map<String, Object> paramMap){	
		StatsParam param = new StatsParam();		
		
		Date beginDate = DateUtils.parseDate(paramMap.get("beginDate"));
		if (beginDate == null){
			beginDate = DateUtils.setDays(DateUtils.addMonths(new Date(),-1), 1);
			paramMap.put("beginDate", DateUtils.formatDate(beginDate, "yyyy-MM-dd"));
		}
		param.setBeginDate(beginDate);
		Date endDate = DateUtils.parseDate(paramMap.get("endDate"));
		if (endDate == null){
			endDate = DateUtils.addDays(new Date(), -1);
			paramMap.put("endDate", DateUtils.formatDate(endDate, "yyyy-MM-dd"));
		}
		param.setEndDate(endDate);
		String status = (String)paramMap.get("status");
		if (status != null && !("".equals(status))){
			param.setStatus(status);
		}
		String orderBy = (String)paramMap.get("orderBy");
		if ("ASC".equals(orderBy)){
			param.setOrderBy("ASC");
		}
		
		List<StatsResult> list = soDao.findIndexStats(param);
		return list;
	}
}