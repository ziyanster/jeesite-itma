/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

/**
 * 客户资料维护Entity
 * @author 黄尚弟
 * @version 2015-10-12
 */
public class SmartCustomer extends DataEntity<SmartCustomer> {
	
	private static final long serialVersionUID = 1L;
	private String customerno;		// 客户编号
	private String customername;		// 客户名称
	private String address;		// 客户地址
	private Double longitude;		// 经度
	private Double latitude;		// 纬度
	private String phone;		// 联系电话
	private String personname;		// 联系人
	private String area;		// 所属区域
	private String industry;		// 所属行业
	
	public SmartCustomer() {
		super();
	}

	public SmartCustomer(String id){
		super(id);
	}

	@Length(min=0, max=64, message="客户编号长度必须介于 0 和 64 之间")
	@ExcelField(title="客户编号", align=2, sort=1)
	public String getCustomerno() {
		return customerno;
	}

	public void setCustomerno(String customerno) {
		this.customerno = customerno;
	}
	
	@Length(min=0, max=50, message="客户名称长度必须介于 0 和 50 之间")
	@ExcelField(title="客户名称", align=2, sort=2)
	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
	@Length(min=0, max=300, message="客户地址长度必须介于 0 和 300 之间")
	@ExcelField(title="客户地址", align=2, sort=3)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@ExcelField(title="经度", align=2, sort=4)
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	@ExcelField(title="维度", align=2, sort=5)
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	@Length(min=0, max=30, message="联系电话长度必须介于 0 和 30 之间")
	@ExcelField(title="联系电话", align=2, sort=6)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=50, message="联系人长度必须介于 0 和 50 之间")
	@ExcelField(title="联系人", align=2, sort=7)
	public String getPersonname() {
		return personname;
	}

	public void setPersonname(String personname) {
		this.personname = personname;
	}
	
	@Length(min=0, max=30, message="所属区域长度必须介于 0 和 30 之间")
	@ExcelField(title="所属区域", align=2, sort=8)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	@Length(min=0, max=60, message="所属行业长度必须介于 0 和 60 之间")
	@ExcelField(title="所属行业", align=2, sort=9)
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
}