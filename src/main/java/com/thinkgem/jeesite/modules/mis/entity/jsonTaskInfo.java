package com.thinkgem.jeesite.modules.mis.entity;

import java.util.List;

import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.sys.entity.User;

public class jsonTaskInfo {
	private String orderId;
	private String formId;
	private String customerNo;
	private String customerName;
	private String customerAddress;
	private String addressLink;
	private String customerPhone;
	private String phoneLink;
	private String serviceType;
	private String faultType;
	private String workType;
	private String price;
	private String callTitle;
	private String callLog;
	private String grabOrderLink;
	private String orderTime;
	private String orderResponseTime;
	private String orderResponseTitle;
	private String orderpicLink1;
	private String orderpicLink2;
	private String modelType;
	private String serialNumber;
	private String workContent;
	private String solveProblem;
	private String workPlan;
	private String remarks; 	
	
	// 客户运维部分添加
	private String serviceDate;
	private List<User> engineers;
	private List<SmartCustomer> customers;
	private List<Dict> dicts;
	private List<Dict> dicts1;
	private List<Dict> dicts2;
	private String contract;
	private String callInTime;
	public String getModelType() {
		return modelType;
	}
	public void setModelType(String modelType) {
		this.modelType = modelType;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getWorkContent() {
		return workContent;
	}
	public void setWorkContent(String workContent) {
		this.workContent = workContent;
	}
	public String getSolveProblem() {
		return solveProblem;
	}
	public void setSolveProblem(String solveProblem) {
		this.solveProblem = solveProblem;
	}
	public String getWorkPlan() {
		return workPlan;
	}
	public void setWorkPlan(String workPlan) {
		this.workPlan = workPlan;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getOrderResponseTime() {
		return orderResponseTime;
	}
	public void setOrderResponseTime(String orderResponseTime) {
		this.orderResponseTime = orderResponseTime;
	}
	public String getOrderResponseTitle() {
		return orderResponseTitle;
	}
	public void setOrderResponseTitle(String orderResponseTitle) {
		this.orderResponseTitle = orderResponseTitle;
	}
	public String getOrderpicLink1() {
		return orderpicLink1;
	}
	public void setOrderpicLink1(String orderpicLink1) {
		this.orderpicLink1 = orderpicLink1;
	}
	public String getOrderpicLink2() {
		return orderpicLink2;
	}
	public void setOrderpicLink2(String orderpicLink2) {
		this.orderpicLink2 = orderpicLink2;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getAddressLink() {
		return addressLink;
	}
	public void setAddressLink(String addressLink) {
		this.addressLink = addressLink;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getPhoneLink() {
		return phoneLink;
	}
	public void setPhoneLink(String phoneLink) {
		this.phoneLink = phoneLink;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getFaultType() {
		return faultType;
	}
	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCallTitle() {
		return callTitle;
	}
	public void setCallTitle(String callTitle) {
		this.callTitle = callTitle;
	}
	public String getCallLog() {
		return callLog;
	}
	public void setCallLog(String callLog) {
		this.callLog = callLog;
	}
	public String getGrabOrderLink() {
		return grabOrderLink;
	}
	public void setGrabOrderLink(String grabOrderLink) {
		this.grabOrderLink = grabOrderLink;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(String serviceTime) {
		this.serviceDate = serviceTime;
	}
	public List<User> getEngineers() {
		return engineers;
	}
	public void setEngineers(List<User> engineers) {
		this.engineers = engineers;
	}
	public List<SmartCustomer> getCustomers() {
		return customers;
	}
	public void setCustomers(List<SmartCustomer> customers) {
		this.customers = customers;
	}
	public List<Dict> getDicts() {
		return dicts;
	}
	public void setDicts(List<Dict> dicts) {
		this.dicts = dicts;
	}
	public List<Dict> getDicts1() {
		return dicts1;
	}
	public void setDicts1(List<Dict> dicts1) {
		this.dicts1 = dicts1;
	}
	public List<Dict> getDicts2() {
		return dicts2;
	}
	public void setDicts2(List<Dict> dicts2) {
		this.dicts2 = dicts2;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getCallInTime() {
		return callInTime;
	}
	public void setCallInTime(String callInTime) {
		this.callInTime = callInTime;
	}

}
