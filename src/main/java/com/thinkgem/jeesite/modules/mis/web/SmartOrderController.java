/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.mapper.JsonMapper;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.cms.entity.Category;
import com.thinkgem.jeesite.modules.mis.entity.SmartAttachments;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.mis.entity.StatsResult;
import com.thinkgem.jeesite.modules.mis.service.SmartAttachmentsService;
import com.thinkgem.jeesite.modules.mis.service.SmartOrderService;
import com.thinkgem.jeesite.modules.mis.service.SmartTaskService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 工单管理Controller
 * @author 黄尚弟
 * @version 2015-10-13
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/smartOrder")
public class SmartOrderController extends BaseController {

	@Autowired
	private SmartOrderService smartOrderService;
	@Autowired
	private SmartTaskService smartTaskService;	
	@Autowired
	private SmartAttachmentsService smartAttachmentsService;	
	
	@ModelAttribute
	public SmartOrder get(@RequestParam(required=false) String id) {
		SmartOrder entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smartOrderService.get(id);
		}
		if (entity == null){
			entity = new SmartOrder();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmartOrder smartOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmartOrder> page = smartOrderService.findPage(new Page<SmartOrder>(request, response), smartOrder); 
		model.addAttribute("page", page);
		return "modules/mis/smartReadyOrderList";
	}

	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = "form")
	public String form(SmartOrder smartOrder, Model model) {
		model.addAttribute("smartOrder", smartOrder);
		return "modules/mis/smartReadyOrderForm";
	}
	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = {"unValuedList"})
	public String unValuedList(SmartOrder smartOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartOrder.setIsValued("0");
		smartOrder.setStatus("2");
		smartOrder.setOrderserialno(UserUtils.getUser().getArea());
		Page<SmartOrder> page = smartOrderService.findPage(new Page<SmartOrder>(request, response), smartOrder);
		for (SmartOrder so : page.getList()) {
			so.setAssignedno(UserUtils.get(so.getAssignedno().getId()));
		}
		model.addAttribute("page", page);
		return "modules/mis/smartUnValuedOrderList";
	}

	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = "unValuedForm")
	public String unValuedForm(SmartOrder smartOrder, Model model) {
		smartOrder.setAssignedno(UserUtils.get(smartOrder.getAssignedno().getId()));
		model.addAttribute("smartOrder", smartOrder);
		return "modules/mis/smartUnValuedOrderForm";
	}
	@RequiresPermissions("mis:smartOrder:edit")
	@RequestMapping(value = "unValuedSave")
	public String unValuedSave(SmartOrder smartOrder, Model model) {
		smartOrder.setIsValued("1");
		smartOrder.setValuedPerson(UserUtils.getUser().getId());
		smartOrderService.save(smartOrder);
		model.addAttribute("smartOrder", new SmartOrder());
		//return "modules/mis/smartUnValuedOrderList";
		return "redirect:"+Global.getAdminPath()+"/mis/smartOrder/unValuedList?repage";
	}
	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = {"list_person"})
	public String list_person(SmartOrder smartOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartOrder.setAssignedno(UserUtils.getUser());
		Page<SmartOrder> page = smartOrderService.findPage(new Page<SmartOrder>(request, response), smartOrder); 
		model.addAttribute("page", page);
		return "modules/mis/smartPersonOrderList";
	}

	@RequiresPermissions("mis:smartOrder:edit")
	@RequestMapping(value = "form_person")
	public String form_person(SmartOrder smartOrder, Model model) {
		model.addAttribute("smartOrder", smartOrder);
		String sTaskId=smartOrder.getTaskno();
		model.addAttribute("smartTask", smartTaskService.get(sTaskId));
		SmartAttachments sa=new SmartAttachments();
		sa.setTaskno(sTaskId);
		model.addAttribute("attList", smartAttachmentsService.findList(sa));
		return "modules/mis/smartPersonOrderForm";
	}
	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = "list_exit")
	public String list_exit(SmartOrder smartOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		
		Page<SmartOrder> page = smartOrderService.findPage(new Page<SmartOrder>(request, response), smartOrder); 
		model.addAttribute("page", page);
		return "modules/mis/smartExitOrderList";
	}

	@RequiresPermissions("mis:smartOrder:edit")
	@RequestMapping(value = "chart_exit")
	public String chart_exit(@RequestParam Map<String, Object> paramMap, Model model) {

		List<StatsResult> list_exit = smartOrderService.findExitStats(paramMap) ;
		model.addAttribute("list_exit", list_exit);
		model.addAttribute("jsondata1", JsonMapper.toJsonString(list_exit));
		
		List<StatsResult> list_status = smartOrderService.findStatusStats(paramMap) ;
		model.addAttribute("list_status", list_status);
		model.addAttribute("jsondata2", JsonMapper.toJsonString(list_status));
		
		String sLegend="[";
		for(int j=0;j<list_status.size();j++){
			sLegend=sLegend+"'"+list_status.get(j).getName()+"',";
		}
		for(int i=0;i<list_exit.size();i++){
			sLegend=sLegend+"'"+list_exit.get(i).getName()+"'";
			if(i<(list_exit.size()-1))
				sLegend=sLegend+",";
		}		
		sLegend=sLegend+"]";
		model.addAttribute("legend", sLegend);
		
		model.addAttribute("paramMap", paramMap);		
		return "modules/mis/smartExitOrderForm";
		
	}	
	@RequiresPermissions("mis:smartOrder:view")
	@RequestMapping(value = "chart_index")
	public String chart_index(@RequestParam Map<String, Object> paramMap, Model model) {

		List<StatsResult> list = smartOrderService.findIndexStats(paramMap) ;
		model.addAttribute("list", list);
		String sArray="[[161.2, 51.6,'aaa'], [167.5, 59.0,'bbb'], [159.5, 49.2,'ccc'], [157.0, 63.0,'ddd'], [155.8, 53.6,'eee']]";
		model.addAttribute("jsondata1", sArray);
		
		String sLegend="[";
		for(int i=0;i<list.size();i++){
			sLegend=sLegend+"[";
			sLegend=sLegend+list.get(i).getSumValue()+",";
			sLegend=sLegend+list.get(i).getCountValue()+",";
			sLegend=sLegend+"'"+list.get(i).getName()+"'";
			sLegend=sLegend+"]";
			if(i<(list.size()-1))
				sLegend=sLegend+",";
		}		
		sLegend=sLegend+"]";
		model.addAttribute("arrayData", sLegend);
		
		model.addAttribute("paramMap", paramMap);		
		return "modules/mis/smartIndexOrderForm";
		
	}
	@RequiresPermissions("mis:smartOrder:edit")
	@RequestMapping(value = "save_person")
	public String save_person(SmartOrder smartOrder, Model model, RedirectAttributes redirectAttributes) {
		//if (!beanValidator(model, smartOrder)){
		//	return form(smartOrder, model);
		//}
		if(smartOrder.getOrderserialno().equals(null)||smartOrder.getOrderserialno().equals("")){
			String custArea=DictUtils.getCustomerArea(smartOrder.getCustomerno(), "");
			smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
		}
		//String custArea=DictUtils.getCustomerArea(smartOrder.getCustomerno(), "");
	//	smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
		smartOrderService.save(smartOrder);
		addMessage(redirectAttributes, "保存工单成功");
		return "modules/mis/smartPersonOrderList";
	}	
	@RequiresPermissions("mis:smartOrder:edit")
	@RequestMapping(value = "save")
	public String save(SmartOrder smartOrder, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartOrder)){
			return form(smartOrder, model);
		}
		if(smartOrder.getOrderserialno().equals(null)||smartOrder.getOrderserialno().equals("")){
			String custArea=DictUtils.getCustomerArea(smartOrder.getCustomerno(), "");
			smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
		}
		smartOrderService.save(smartOrder);
		addMessage(redirectAttributes, "保存工单管理成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartOrder/?repage";
	}
	
	@RequiresPermissions("mis:smartOrder:edit")
	@RequestMapping(value = "delete")
	public String delete(SmartOrder smartOrder, RedirectAttributes redirectAttributes) {
		smartOrderService.delete(smartOrder);
		addMessage(redirectAttributes, "删除工单管理成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartOrder/?repage";
	}

}