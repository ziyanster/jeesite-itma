/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.SmartApproval;
import com.thinkgem.jeesite.modules.mis.service.SmartApprovalService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 应急审批Controller
 * @author 黄尚弟
 * @version 2015-11-15
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/smartApproval")
public class SmartApprovalController extends BaseController {

	@Autowired
	private SmartApprovalService smartApprovalService;
	
	@ModelAttribute
	public SmartApproval get(@RequestParam(required=false) String id) {
		SmartApproval entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smartApprovalService.get(id);
		}
		if (entity == null){
			entity = new SmartApproval();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:smartApproval:view")
	@RequestMapping(value = "listApply")
	public String listApply(SmartApproval smartApproval, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartApproval.setCreateBy(UserUtils.getUser());
		Page<SmartApproval> page = smartApprovalService.findPage(new Page<SmartApproval>(request, response), smartApproval); 
		model.addAttribute("page", page);
		return "modules/mis/smartApproval_listApply";
	}
	@RequiresPermissions("mis:smartApproval:view")
	@RequestMapping(value = "listReady")
	public String listReady(SmartApproval smartApproval, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartApproval.setStatus("0");
		Page<SmartApproval> page = smartApprovalService.findPage(new Page<SmartApproval>(request, response), smartApproval); 
		model.addAttribute("page", page);
		return "modules/mis/smartApproval_listReady";
	}	
	@RequiresPermissions("mis:smartApproval:view")
	@RequestMapping(value = "listFinish")
	public String listFinish(SmartApproval smartApproval, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartApproval.setStatus("1");
		Page<SmartApproval> page = smartApprovalService.findPage(new Page<SmartApproval>(request, response), smartApproval); 
		model.addAttribute("page", page);
		return "modules/mis/smartApproval_listFinish";
	}
	@RequiresPermissions("mis:smartApproval:view")
	@RequestMapping(value = "formQuery")
	public String formQuery(SmartApproval smartApproval, Model model) {
		model.addAttribute("smartApproval", smartApproval);
		return "modules/mis/smartApproval_formQuery";
	}
	@RequiresPermissions("mis:smartApproval:view")
	@RequestMapping(value = "formApply")
	public String formApply(SmartApproval smartApproval, Model model) {
		model.addAttribute("smartApproval", smartApproval);
		return "modules/mis/smartApproval_formApply";
	}
	@RequiresPermissions("mis:smartApproval:view")
	@RequestMapping(value = "formApproval")
	public String formApproval(SmartApproval smartApproval, Model model) {
		model.addAttribute("smartApproval", smartApproval);
		return "modules/mis/smartApproval_formApproval";
	}
	@RequiresPermissions("mis:smartApproval:edit")
	@RequestMapping(value = "saveApply")
	public String saveApply(SmartApproval smartApproval, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartApproval)){
			return formApply(smartApproval, model);
		}
		smartApproval.setStatus("0");
		smartApproval.setCreateBy(UserUtils.getUser());
		String approvalUser=DictUtils.getDictLabel("approvalUser", "system_param","");
		smartApproval.setApprovalBy(UserUtils.getByEmail(approvalUser));
	
		smartApprovalService.save(smartApproval);
		addMessage(redirectAttributes, "保存应急申请成功");
		
		DictUtils.postWeixinApproval(smartApproval);
		addMessage(redirectAttributes, "提交微信平台发布应急申请成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartApproval/listApply";
	}
	@RequiresPermissions("mis:smartApproval:edit")
	@RequestMapping(value = "saveApproval")
	public String save(SmartApproval smartApproval, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartApproval)){
			return formApproval(smartApproval, model);
		}
		smartApproval.setStatus("1");
		smartApprovalService.save(smartApproval);
		addMessage(redirectAttributes, "保存应急审批成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartApproval/listFinish";
	}	
	@RequiresPermissions("mis:smartApproval:edit")
	@RequestMapping(value = "agree")
	public String agree(SmartApproval smartApproval, RedirectAttributes redirectAttributes) {
		smartApproval.setStatus("1");
		smartApproval.setResult("1");
		smartApproval.setApprovalBy(UserUtils.getUser());
		smartApproval.setApprovalDate(new Date());		
		smartApprovalService.save(smartApproval);
		addMessage(redirectAttributes, "应急审批完成：同意");
		DictUtils.postWeixinApproval(smartApproval);
		addMessage(redirectAttributes, "提交微信平台发布应急申请成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartApproval/listFinish";
	}
	@RequiresPermissions("mis:smartApproval:edit")
	@RequestMapping(value = "disagree")
	public String disagree(SmartApproval smartApproval, RedirectAttributes redirectAttributes) {
		smartApproval.setStatus("1");
		smartApproval.setResult("0");
		smartApproval.setApprovalBy(UserUtils.getUser());
		smartApproval.setApprovalDate(new Date());
		smartApprovalService.save(smartApproval);
		addMessage(redirectAttributes, "应急审批完成：不同意");
		DictUtils.postWeixinApproval(smartApproval);
		addMessage(redirectAttributes, "提交微信平台发布应急申请成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartApproval/listFinish";
	}
	@RequiresPermissions("mis:smartApproval:edit")
	@RequestMapping(value = "delete")
	public String delete(SmartApproval smartApproval, RedirectAttributes redirectAttributes) {
		smartApprovalService.delete(smartApproval);
		addMessage(redirectAttributes, "删除应急审批成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartApproval/?repage";
	}

}