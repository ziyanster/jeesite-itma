/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.dao.MisChecktaskDao;
import com.thinkgem.jeesite.modules.mis.dao.MisContractDao;
import com.thinkgem.jeesite.modules.mis.entity.MisChecktask;
import com.thinkgem.jeesite.modules.mis.entity.MisContract;
import com.thinkgem.jeesite.modules.mis.service.MisChecktaskService;

/**
 * 巡检任务管理Controller
 * @author ym
 * @version 2015-12-14
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/misChecktask")
public class MisChecktaskController extends BaseController {

	@Autowired
	private MisChecktaskService misChecktaskService;
	
	@ModelAttribute
	public MisChecktask get(@RequestParam(required=false) String id) {
		MisChecktask entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = misChecktaskService.get(id);
		}
		if (entity == null){
			entity = new MisChecktask();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:misChecktask:view")
	@RequestMapping(value = {"list", ""})
	public String list(MisChecktask misChecktask, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MisChecktask> page = misChecktaskService.findPage(new Page<MisChecktask>(request, response), misChecktask); 
		model.addAttribute("page", page);
		return "modules/mis/misChecktaskList";
	}

	@RequiresPermissions("mis:misChecktask:view")
	@RequestMapping(value = "form")
	public String form(MisChecktask misChecktask, Model model) {
		model.addAttribute("misChecktask", misChecktask);
		return "modules/mis/misChecktaskForm";
	}

	@RequiresPermissions("mis:misChecktask:edit")
	@RequestMapping(value = "save")
	public String save(MisChecktask misChecktask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, misChecktask)){
			return form(misChecktask, model);
		}
		String strConId=misChecktask.getContractid();
		MisContractDao mcDao = SpringContextHolder.getBean(MisContractDao.class);
		MisChecktaskDao mctDao = SpringContextHolder.getBean(MisChecktaskDao.class);
		MisChecktask temp=new MisChecktask();
		temp.setContractid(strConId);
		List<MisChecktask> tasklist=	mctDao.findList(temp);
		//System.out.println(temp.getId()+"%%%%%%%%%%%%%%%");
	//	List<MisChecktask> tasklist= mctDao.findList(temp);

		MisContract contract=new MisContract();
		contract=mcDao.get(strConId);
		
		if(misChecktask.getTaskserialno()==null||misChecktask.getTaskserialno().equals("")){
			misChecktask.setCheckdata(contract.getBegindate());
			misChecktask.setTaskserialno(StringUtils.createOrderNo("XJ", "mis_checkTask", "taskSerialNo"));
		}
		misChecktask.setCheckfre(contract.getChecktype());
		
		//将当前时间像前设置
	//	misChecktask.setAdvweek(advweek);
		
		//判断之前是否存在该合同的任务
		boolean b=misChecktask.getIsNewRecord();
		if(b&&tasklist.size()!=0){
			addMessage(redirectAttributes, "已经存在该合同的巡检任务");
		}else{
			misChecktaskService.save(misChecktask);
			addMessage(redirectAttributes, "保存巡检任务成功");
		}
		
		return "redirect:"+Global.getAdminPath()+"/mis/misChecktask/?repage";
	}
	
	@RequiresPermissions("mis:misChecktask:edit")
	@RequestMapping(value = "delete")
	public String delete(MisChecktask misChecktask, RedirectAttributes redirectAttributes) {
		misChecktaskService.delete(misChecktask);
		addMessage(redirectAttributes, "删除巡检任务成功");
		return "redirect:"+Global.getAdminPath()+"/mis/misChecktask/?repage";
	}

}