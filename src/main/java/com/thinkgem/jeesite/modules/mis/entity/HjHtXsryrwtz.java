/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 销售人员任务台账Entity
 * @author 黄尚弟
 * @version 2015-09-24
 */
public class HjHtXsryrwtz extends DataEntity<HjHtXsryrwtz> {
	
	private static final long serialVersionUID = 1L;
	private String yearq;		// 销售季度
	private String ywrydm;		// 业务员代码
	private String ywryxm;		// 业务员姓名
	private Double rwe;		// 任务额
	private Double wce;		// 完成额
	private Double wcbl;		// 完成比例
	private Double fybxed;		// 费用报销额
	private Double sjfybx;		// 实际费用
	private Double pxfy;		// 培训费用
	private Double clfy;		// 差旅费用
	private Double sjyfgzbl;		// 实发工资比
	
	public HjHtXsryrwtz() {
		super();
	}

	public HjHtXsryrwtz(String id){
		super(id);
	}

	@Length(min=1, max=6, message="销售季度长度必须介于 1 和 6 之间")
	public String getYearq() {
		return yearq;
	}

	public void setYearq(String yearq) {
		this.yearq = yearq;
	}
	
	@Length(min=1, max=6, message="业务员代码长度必须介于 1 和 6 之间")
	public String getYwrydm() {
		return ywrydm;
	}

	public void setYwrydm(String ywrydm) {
		this.ywrydm = ywrydm;
	}
	
	@Length(min=0, max=8, message="业务员姓名长度必须介于 0 和 8 之间")
	public String getYwryxm() {
		return ywryxm;
	}

	public void setYwryxm(String ywryxm) {
		this.ywryxm = ywryxm;
	}
	
	public Double getRwe() {
		return rwe;
	}

	public void setRwe(Double rwe) {
		this.rwe = rwe;
	}
	
	public Double getWce() {
		return wce;
	}

	public void setWce(Double wce) {
		this.wce = wce;
	}
	
	public Double getWcbl() {
		return wcbl;
	}

	public void setWcbl(Double wcbl) {
		this.wcbl = wcbl;
	}
	
	public Double getFybxed() {
		return fybxed;
	}

	public void setFybxed(Double fybxed) {
		this.fybxed = fybxed;
	}
	
	public Double getSjfybx() {
		return sjfybx;
	}

	public void setSjfybx(Double sjfybx) {
		this.sjfybx = sjfybx;
	}
	
	public Double getPxfy() {
		return pxfy;
	}

	public void setPxfy(Double pxfy) {
		this.pxfy = pxfy;
	}
	
	public Double getClfy() {
		return clfy;
	}

	public void setClfy(Double clfy) {
		this.clfy = clfy;
	}
	
	public Double getSjyfgzbl() {
		return sjyfgzbl;
	}

	public void setSjyfgzbl(Double sjyfgzbl) {
		this.sjyfgzbl = sjyfgzbl;
	}
	
}