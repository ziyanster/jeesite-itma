/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 运维任务信息Entity
 * @author 黄尚弟
 * @version 2015-10-12
 */
public class SmartServiceDoc extends DataEntity<SmartServiceDoc> {
	
	private static final long serialVersionUID = 1L;
	private String servicedocno;		// 服务单号
	private String customername;		// 客户名称
	private String customerno;		// 客户编号
	private String incomingway;		// 呼入方式
	private Date incomingtime;		// 呼入时间
	private String model;		// 机型
	private String seriesno;		// 系列号
	private String brand;		// 品牌
	private String personname;		// 客户姓名
	private String phone;		// 联系电话
	private String address;		// 地址
	
	public SmartServiceDoc() {
		super();
	}

	public SmartServiceDoc(String id){
		super(id);
	}

	@Length(min=0, max=64, message="服务单号长度必须介于 0 和 64 之间")
	public String getServicedocno() {
		return servicedocno;
	}

	public void setServicedocno(String servicedocno) {
		this.servicedocno = servicedocno;
	}
	
	@Length(min=0, max=50, message="客户名称长度必须介于 0 和 50 之间")
	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
	@Length(min=0, max=64, message="客户编号长度必须介于 0 和 64 之间")
	public String getCustomerno() {
		return customerno;
	}

	public void setCustomerno(String customerno) {
		this.customerno = customerno;
	}
	
	@Length(min=0, max=30, message="呼入方式长度必须介于 0 和 30 之间")
	public String getIncomingway() {
		return incomingway;
	}

	public void setIncomingway(String incomingway) {
		this.incomingway = incomingway;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="呼入时间不能为空")
	public Date getIncomingtime() {
		return incomingtime;
	}

	public void setIncomingtime(Date incomingtime) {
		this.incomingtime = incomingtime;
	}
	
	@Length(min=0, max=50, message="机型长度必须介于 0 和 50 之间")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	@Length(min=0, max=64, message="系列号长度必须介于 0 和 64 之间")
	public String getSeriesno() {
		return seriesno;
	}

	public void setSeriesno(String seriesno) {
		this.seriesno = seriesno;
	}
	
	@Length(min=0, max=50, message="品牌长度必须介于 0 和 50 之间")
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@Length(min=0, max=30, message="客户姓名长度必须介于 0 和 30 之间")
	public String getPersonname() {
		return personname;
	}

	public void setPersonname(String personname) {
		this.personname = personname;
	}
	
	@Length(min=0, max=30, message="联系电话长度必须介于 0 和 30 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=0, max=300, message="地址长度必须介于 0 和 300 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}