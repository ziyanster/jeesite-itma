/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.google.common.collect.Lists;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 巡检合同Entity
 * @author ym
 * @version 2015-12-11
 */
public class MisContract extends DataEntity<MisContract> {
	
	private static final long serialVersionUID = 1L;
	private String customerno;		// 客户名称
	private Date begindate;		// 开始日期
	private Date enddate;		// 结束日期
	private String address;		// 地址
	private String content;		// 服务内容
	private String salesdirector;		// 销售主管
	private String checktype;		// 巡检频次
	private String projectname;		// 项目名称
	private String cuscontact;		// 客户联系人
	private String phone;		// 客户电话
	private Date beginBegindate;		// 开始 开始日期
	private Date endBegindate;		// 结束 开始日期
	private Date beginEnddate;		// 开始 结束日期
	private Date endEnddate;		// 结束 结束日期
	private List<MisContractdetail> misContractdetailList = Lists.newArrayList();		// 子表列表
	
	public MisContract() {
		super();
	}

	public MisContract(String id){
		super(id);
	}

	@Length(min=0, max=64, message="客户名称长度必须介于 0 和 64 之间")
	public String getCustomerno() {
		return customerno;
	}

	public void setCustomerno(String customerno) {
		this.customerno = customerno;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getBegindate() {
		return begindate;
	}

	public void setBegindate(Date begindate) {
		this.begindate = begindate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	
	@Length(min=0, max=200, message="地址长度必须介于 0 和 200 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=0, max=250, message="服务内容长度必须介于 0 和 250 之间")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=0, max=64, message="销售主管长度必须介于 0 和 64 之间")
	public String getSalesdirector() {
		return salesdirector;
	}

	public void setSalesdirector(String salesdirector) {
		this.salesdirector = salesdirector;
	}
	
	@Length(min=0, max=1, message="巡检频次长度必须介于 0 和 1 之间")
	public String getChecktype() {
		return checktype;
	}

	public void setChecktype(String checktype) {
		this.checktype = checktype;
	}
	
	@Length(min=0, max=150, message="项目名称长度必须介于 0 和 150 之间")
	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	
	@Length(min=0, max=64, message="客户联系人长度必须介于 0 和 64 之间")
	public String getCuscontact() {
		return cuscontact;
	}

	public void setCuscontact(String cuscontact) {
		this.cuscontact = cuscontact;
	}
	
	@Length(min=0, max=30, message="客户电话长度必须介于 0 和 30 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Date getBeginBegindate() {
		return beginBegindate;
	}

	public void setBeginBegindate(Date beginBegindate) {
		this.beginBegindate = beginBegindate;
	}
	
	public Date getEndBegindate() {
		return endBegindate;
	}

	public void setEndBegindate(Date endBegindate) {
		this.endBegindate = endBegindate;
	}
		
	public Date getBeginEnddate() {
		return beginEnddate;
	}

	public void setBeginEnddate(Date beginEnddate) {
		this.beginEnddate = beginEnddate;
	}
	
	public Date getEndEnddate() {
		return endEnddate;
	}

	public void setEndEnddate(Date endEnddate) {
		this.endEnddate = endEnddate;
	}
		
	public List<MisContractdetail> getMisContractdetailList() {
		return misContractdetailList;
	}

	public void setMisContractdetailList(List<MisContractdetail> misContractdetailList) {
		this.misContractdetailList = misContractdetailList;
	}
}