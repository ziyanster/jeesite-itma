/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.SmartApproval;
import com.thinkgem.jeesite.modules.mis.dao.SmartApprovalDao;

/**
 * 应急审批Service
 * @author 黄尚弟
 * @version 2015-11-15
 */
@Service
@Transactional(readOnly = true)
public class SmartApprovalService extends CrudService<SmartApprovalDao, SmartApproval> {

	public SmartApproval get(String id) {
		return super.get(id);
	}
	
	public List<SmartApproval> findList(SmartApproval smartApproval) {
		return super.findList(smartApproval);
	}
	
	public Page<SmartApproval> findPage(Page<SmartApproval> page, SmartApproval smartApproval) {
		return super.findPage(page, smartApproval);
	}
	
	@Transactional(readOnly = false)
	public void save(SmartApproval smartApproval) {
		super.save(smartApproval);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmartApproval smartApproval) {
		super.delete(smartApproval);
	}
	
}