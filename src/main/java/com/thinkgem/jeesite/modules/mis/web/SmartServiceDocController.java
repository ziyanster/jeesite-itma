/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.SmartServiceDoc;
import com.thinkgem.jeesite.modules.mis.service.SmartServiceDocService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;

/**
 * 运维任务信息Controller
 * @author 黄尚弟
 * @version 2015-10-12
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/smartServiceDoc")
public class SmartServiceDocController extends BaseController {

	@Autowired
	private SmartServiceDocService smartServiceDocService;
	
	@ModelAttribute
	public SmartServiceDoc get(@RequestParam(required=false) String id) {
		SmartServiceDoc entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smartServiceDocService.get(id);
		}
		if (entity == null){
			entity = new SmartServiceDoc();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:smartServiceDoc:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmartServiceDoc smartServiceDoc, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmartServiceDoc> page = smartServiceDocService.findPage(new Page<SmartServiceDoc>(request, response), smartServiceDoc); 
		model.addAttribute("page", page);
		return "modules/mis/smartServiceDocList";
	}

	@RequiresPermissions("mis:smartServiceDoc:view")
	@RequestMapping(value = "form")
	public String form(SmartServiceDoc smartServiceDoc, Model model) {
		model.addAttribute("smartServiceDoc", smartServiceDoc);
		return "modules/mis/smartServiceDocForm";
	}

	@RequiresPermissions("mis:smartServiceDoc:edit")
	@RequestMapping(value = "save")
	public String save(SmartServiceDoc smartServiceDoc, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartServiceDoc)){
			return form(smartServiceDoc, model);
		}
		smartServiceDocService.save(smartServiceDoc);
		addMessage(redirectAttributes, "保存运维任务信息成功");
		//DictUtils.postWeixinTask(0,"huangsd@itma.com.cn",smartServiceDoc.getCustomername(),smartServiceDoc.getRemarks());
		return "redirect:"+Global.getAdminPath()+"/mis/smartServiceDoc/?repage";
	}
	
	@RequiresPermissions("mis:smartServiceDoc:edit")
	@RequestMapping(value = "delete")
	public String delete(SmartServiceDoc smartServiceDoc, RedirectAttributes redirectAttributes) {
		smartServiceDocService.delete(smartServiceDoc);
		addMessage(redirectAttributes, "删除运维任务信息成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartServiceDoc/?repage";
	}
	


}