/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.SmartAttachments;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.service.SmartAttachmentsService;

/**
 * 附件管理Controller
 * @author 黄尚弟
 * @version 2015-10-25
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/smartAttachments")
public class SmartAttachmentsController extends BaseController {

	@Autowired
	private SmartAttachmentsService smartAttachmentsService;
	
	@ModelAttribute
	public SmartAttachments get(@RequestParam(required=false) String id) {
		SmartAttachments entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smartAttachmentsService.get(id);
		}
		if (entity == null){
			entity = new SmartAttachments();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:smartAttachments:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmartAttachments smartAttachments, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmartAttachments> page = smartAttachmentsService.findPage(new Page<SmartAttachments>(request, response), smartAttachments); 
		model.addAttribute("page", page);
		return "modules/mis/smartAttachmentsList";
	}

	@RequiresPermissions("mis:smartAttachments:view")
	@RequestMapping(value = "form")
	public String form(SmartAttachments smartAttachments, Model model) {
		model.addAttribute("smartAttachments", smartAttachments);
		return "modules/mis/smartAttachmentsForm";
	}

	@RequiresPermissions("mis:smartAttachments:edit")
	@RequestMapping(value = "save")
	public String save(SmartAttachments smartAttachments, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartAttachments)){
			return form(smartAttachments, model);
		}
		smartAttachmentsService.save(smartAttachments);
		addMessage(redirectAttributes, "保存附件管理成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartAttachments/?repage";
	}
	
	@RequiresPermissions("mis:smartAttachments:edit")
	@RequestMapping(value = "delete")
	public String delete(SmartAttachments smartAttachments, RedirectAttributes redirectAttributes) {
		smartAttachmentsService.delete(smartAttachments);
		addMessage(redirectAttributes, "删除附件管理成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartAttachments/?repage";
	}

}