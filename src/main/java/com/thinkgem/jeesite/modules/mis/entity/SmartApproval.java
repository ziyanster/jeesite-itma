/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 应急审批Entity
 * @author 黄尚弟
 * @version 2015-11-15
 */
public class SmartApproval extends DataEntity<SmartApproval> {
	
	private static final long serialVersionUID = 1L;
	private String misno;		// MIS编号
	private String type;		// 申请类型
	private String status;		// 是否审批
	private User approvalBy;		// 审批者
	private Date approvalDate;		// 审批时间
	private String result;		// 是否审批通过
	
	public SmartApproval() {
		super();
	}

	public SmartApproval(String id){
		super(id);
	}

	@Length(min=0, max=64, message="MIS编号长度必须介于 0 和 64 之间")
	public String getMisno() {
		return misno;
	}

	public void setMisno(String misno) {
		this.misno = misno;
	}
	
	@Length(min=0, max=1, message="申请类型长度必须介于 0 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=1, message="是否审批长度必须介于 0 和 1 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public User getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(User approvalBy) {
		this.approvalBy = approvalBy;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	@Length(min=0, max=1, message="是否审批通过长度必须介于 0 和 1 之间")
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}