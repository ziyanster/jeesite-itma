/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

/**
 * 销售任务Entity
 * @author 黄尚弟
 * @version 2015-10-10
 */
public class HjMarkerterTask extends DataEntity<HjMarkerterTask> {
	
	private static final long serialVersionUID = 1L;
	private String salesid;		// 销售ID
	private String salesname;		// 销售姓名	
	private String salerange;		// 市场范围
	private String year;		// 销售年度
	private String task;		// 任务
	private String saleTask;		// 销售任务
	private String zhxgr;		// zhxgr
	private Date zhxgsj;		// zhxgsj
	private String taskPercent;		// 任务比例
	private String conPercent;		// 完成比例
	private String s1Task;		// 任务额
	private String s1TaskCom;		// 完成额
	private String s1;		// 完成比
	private String s2Task;		// 任务额
	private String s2TaskCom;		// 完成额
	private String s2;		// 完成比
	private String s3Task;		// 任务额
	private String s3TaskCom;		// 完成额
	private String s3;		// 完成比
	private String s4Task;		// 任务额
	private String s4TaskCom;		// 完成额
	private String s4;		// 完成比
	
	public HjMarkerterTask() {
		super();
	}

	public HjMarkerterTask(String id){
		super(id);
	}

	@Length(min=1, max=6, message="销售ID长度必须介于 1 和 6 之间")
	@ExcelField(title="销售ID", align=2, sort=1)
	public String getSalesid() {
		return salesid;
	}

	public void setSalesid(String salesid) {
		this.salesid = salesid;
	}
	@ExcelField(title="销售姓名", align=2, sort=2)
	public String getSalesname() {
		return salesname;
	}

	public void setSalesname(String salesname) {
		this.salesname = salesname;
	}	
	@Length(min=1, max=2, message="市场范围长度必须介于 1 和 2 之间")
	@ExcelField(title="市场范围", align=2, sort=3)
	public String getSalerange() {
		return salerange;
	}

	public void setSalerange(String salerange) {
		this.salerange = salerange;
	}
	
	@Length(min=1, max=4, message="销售年度长度必须介于 1 和 4 之间")
	@ExcelField(title="销售年度", align=2, sort=4)
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	@ExcelField(title="销售任务额", align=2, sort=5)	
	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}
	@ExcelField(title="完成任务额", align=2, sort=6)		
	public String getSaleTask() {
		return saleTask;
	}

	public void setSaleTask(String saleTask) {
		this.saleTask = saleTask;
	}
	
	@Length(min=0, max=6, message="zhxgr长度必须介于 0 和 6 之间")
	public String getZhxgr() {
		return zhxgr;
	}

	public void setZhxgr(String zhxgr) {
		this.zhxgr = zhxgr;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getZhxgsj() {
		return zhxgsj;
	}

	public void setZhxgsj(Date zhxgsj) {
		this.zhxgsj = zhxgsj;
	}
	
	@Length(min=0, max=8, message="任务比例长度必须介于 0 和 8 之间")
	public String getTaskPercent() {
		return taskPercent;
	}

	public void setTaskPercent(String taskPercent) {
		this.taskPercent = taskPercent;
	}
	
	@Length(min=0, max=8, message="完成比例长度必须介于 0 和 8 之间")
	@ExcelField(title="完成任务比例", align=2, sort=7)		
	public String getConPercent() {
		return conPercent;
	}

	public void setConPercent(String conPercent) {
		this.conPercent = conPercent;
	}
	@ExcelField(title="一季度任务额", align=2, sort=11)	
	public String getS1Task() {
		return s1Task;
	}

	public void setS1Task(String s1Task) {
		this.s1Task = s1Task;
	}
	@ExcelField(title="一季度完成额", align=2, sort=12)	
	public String getS1TaskCom() {
		return s1TaskCom;
	}

	public void setS1TaskCom(String s1TaskCom) {
		this.s1TaskCom = s1TaskCom;
	}
	@ExcelField(title="一季度完成比", align=2, sort=13)	
	public String getS1() {
		return s1;
	}

	public void setS1(String s1) {
		this.s1 = s1;
	}
	@ExcelField(title="二季度任务额", align=2, sort=21)		
	public String getS2Task() {
		return s2Task;
	}

	public void setS2Task(String s2Task) {
		this.s2Task = s2Task;
	}
	@ExcelField(title="二季度完成额", align=2, sort=22)	
	public String getS2TaskCom() {
		return s2TaskCom;
	}

	public void setS2TaskCom(String s2TaskCom) {
		this.s2TaskCom = s2TaskCom;
	}
	@ExcelField(title="二季度完成比", align=2, sort=23)	
	public String getS2() {
		return s2;
	}

	public void setS2(String s2) {
		this.s2 = s2;
	}
	@ExcelField(title="三季度任务额", align=2, sort=31)	
	public String getS3Task() {
		return s3Task;
	}

	public void setS3Task(String s3Task) {
		this.s3Task = s3Task;
	}
	@ExcelField(title="三季度完成额", align=2, sort=32)		
	public String getS3TaskCom() {
		return s3TaskCom;
	}
	
	public void setS3TaskCom(String s3TaskCom) {
		this.s3TaskCom = s3TaskCom;
	}
	@ExcelField(title="三季度完成比", align=2, sort=33)	
	public String getS3() {
		return s3;
	}

	public void setS3(String s3) {
		this.s3 = s3;
	}
	@ExcelField(title="四季度任务额", align=2, sort=41)		
	public String getS4Task() {
		return s4Task;
	}

	public void setS4Task(String s4Task) {
		this.s4Task = s4Task;
	}
	@ExcelField(title="四季度完成额", align=2, sort=42)		
	public String getS4TaskCom() {
		return s4TaskCom;
	}

	public void setS4TaskCom(String s4TaskCom) {
		this.s4TaskCom = s4TaskCom;
	}
	@ExcelField(title="四季度完成比", align=2, sort=43)		
	public String getS4() {
		return s4;
	}

	public void setS4(String s4) {
		this.s4 = s4;
	}

	
}