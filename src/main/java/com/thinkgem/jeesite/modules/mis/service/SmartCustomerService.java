/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.mis.dao.SmartCustomerDao;

/**
 * 客户资料维护Service
 * @author 黄尚弟
 * @version 2015-10-12
 */
@Service
@Transactional(readOnly = true)
public class SmartCustomerService extends CrudService<SmartCustomerDao, SmartCustomer> {

	public SmartCustomer get(String id) {
		return super.get(id);
	}
	
	public List<SmartCustomer> findList(SmartCustomer smartCustomer) {
		return super.findList(smartCustomer);
	}
	
	public Page<SmartCustomer> findPage(Page<SmartCustomer> page, SmartCustomer smartCustomer) {
		return super.findPage(page, smartCustomer);
	}
	
	@Transactional(readOnly = false)
	public void save(SmartCustomer smartCustomer) {
		super.save(smartCustomer);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmartCustomer smartCustomer) {
		super.delete(smartCustomer);
	}
	
}