/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 销售任务Entity
 * @author 黄尚弟
 * @version 2015-09-27
 */
public class HjHtSwhtbh extends DataEntity<HjHtSwhtbh> {
	
	private static final long serialVersionUID = 1L;
	private String yearsale;	//销售年度
	public String getYearsale() {
		return yearsale;
	}

	public void setYearsale(String yearsale) {
		this.yearsale = yearsale;
	}

	private String swhtbh;		// 商务合同号
	private Date htksrq;		// 合同开始日
	private Date htzzrq;		// 合同终止日
	private String htlx;		// 合同类型
	private String khmc;		// 客户名称
	private String xmmc;		// 项目名称
	private Double htje;		// 合同金额
	private Double yjyje;		// 资金已回款
	private Double wgfy;		// 外购费用
	private Double zjzy;		// 资金占用
	private Double pxfy;		// 培训费用
	private String htlxmc;		// 合同类型
	private String jszxdm;		// 结算中心（盖章公司）
	private String jszxmc;		// 结算中心名称（盖章公司）
	private String ywjgdm;		// 业务机构
	private String ywjgmc;		// 业务机构名称
	private String lsgs;		// 隶属公司
	private String lsgsmc;		// 隶属公司名称
	private String ddgsdm;		// 调度公司
	private String ddgsmc;		// 调度公司名称
	private String htzt;		// 合同状态
	private String htmb;		// 合同模板
	private String zshtbh;		// 正式合同编号
	private String khdm;		// 客户代码
	private String ywrydm;		// 业务人员代码
	private String ywryxm;		// 业务人员
	private String jbryxm;		// 经办人员姓名
	private String sdfsmc;		// 送达方式名称
	private Date sdrq;		// 送达日期
	private String bz;		// 备注
	private String dflxr;		// 对方联系人
	private String bhry;		// 编号人员
	private Date bhrq;		// 编号日期
	private String khhtbh;		// 客户合同编号
	private String yf;		// 乙方
	private String qddd;		// 签订地点
	private String gdry;		// 归档人
	private String zhxgr;		// 最后修改人
	private Date zhxgsj;		// 最后修改时间
	private String zxbz;		// 注销标志
	private String bz2;		// bz2
	private String bz3;		// bz3
	private String kpgsdm;		// kpgsdm
	private String kpgsmc;		// kpgsmc
	private String zdr;		// zdr
	private String spr1;		// spr1
	private String spr2;		// spr2
	private String spr3;		// spr3
	private String spr4;		// spr4
	private String spr5;		// spr5
	private String spr6;		// spr6
	private String spr7;		// spr7
	private String spr8;		// spr8
	private String spr9;		// spr9
	private String gdr;		// gdr
	private String wdbh;		// wdbh
	private String spbz;		// spbz
	private String bmdm;		// bmdm
	private String fgsdm;		// fgsdm
	private String xmfzr;		// xmfzr
	private String xmfzrdm;		// xmfzrdm
	private String xjzq;		// xjzq
	private String yyswhtbh;		// yyswhtbh
	private String zy;		// zy
	private String dykh;		// dykh
	private String dykhdm;		// dykhdm
	private Date zdrq;		// zdrq
	private String zzkhmc;		// zzkhmc
	private String swhtlx;		// swhtlx
	private Double sl;		// 税率
	private Double zjhkl;		// 资金回款率
	private Double ftmlebl;		// 分摊毛利额比例
	private Double mle;		// 毛利额
	private Double avgmle;		// 年平均毛利额	
	private Double mll;		// 毛利率
	private Date beginHtksrq;		// 开始 合同开始日
	private Date endHtksrq;		// 结束 合同开始日
	private Date beginHtzzrq;		// 开始 合同终止日
	private Date endHtzzrq;		// 结束 合同终止日
	
	public HjHtSwhtbh() {
		super();
	}

	public HjHtSwhtbh(String id){
		super(id);
	}

	@Length(min=1, max=20, message="商务合同号长度必须介于 1 和 20 之间")
	public String getSwhtbh() {
		return swhtbh;
	}

	public void setSwhtbh(String swhtbh) {
		this.swhtbh = swhtbh;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="合同开始日不能为空")
	public Date getHtksrq() {
		return htksrq;
	}

	public void setHtksrq(Date htksrq) {
		this.htksrq = htksrq;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="合同终止日不能为空")
	public Date getHtzzrq() {
		return htzzrq;
	}

	public void setHtzzrq(Date htzzrq) {
		this.htzzrq = htzzrq;
	}
	
	@Length(min=1, max=10, message="合同类型长度必须介于 1 和 10 之间")
	public String getHtlx() {
		return htlx;
	}

	public void setHtlx(String htlx) {
		this.htlx = htlx;
	}
	
	@Length(min=1, max=50, message="客户名称长度必须介于 1 和 50 之间")
	public String getKhmc() {
		return khmc;
	}

	public void setKhmc(String khmc) {
		this.khmc = khmc;
	}
	
	@Length(min=1, max=60, message="项目名称长度必须介于 1 和 60 之间")
	public String getXmmc() {
		return xmmc;
	}

	public void setXmmc(String xmmc) {
		this.xmmc = xmmc;
	}
	
	@NotNull(message="合同金额不能为空")
	public Double getHtje() {
		return htje;
	}

	public void setHtje(Double htje) {
		this.htje = htje;
	}
	
	public Double getYjyje() {
		return yjyje;
	}

	public void setYjyje(Double yjyje) {
		this.yjyje = yjyje;
	}
	
	public Double getWgfy() {
		return wgfy;
	}

	public void setWgfy(Double wgfy) {
		this.wgfy = wgfy;
	}
	
	public Double getZjzy() {
		return zjzy;
	}

	public void setZjzy(Double zjzy) {
		this.zjzy = zjzy;
	}
	
	public Double getPxfy() {
		return pxfy;
	}

	public void setPxfy(Double pxfy) {
		this.pxfy = pxfy;
	}
	
	@Length(min=1, max=20, message="合同类型长度必须介于 1 和 20 之间")
	public String getHtlxmc() {
		return htlxmc;
	}

	public void setHtlxmc(String htlxmc) {
		this.htlxmc = htlxmc;
	}
	
	@Length(min=1, max=2, message="结算中心（盖章公司）长度必须介于 1 和 2 之间")
	public String getJszxdm() {
		return jszxdm;
	}

	public void setJszxdm(String jszxdm) {
		this.jszxdm = jszxdm;
	}
	
	@Length(min=1, max=20, message="结算中心名称（盖章公司）长度必须介于 1 和 20 之间")
	public String getJszxmc() {
		return jszxmc;
	}

	public void setJszxmc(String jszxmc) {
		this.jszxmc = jszxmc;
	}
	
	@Length(min=1, max=2, message="业务机构长度必须介于 1 和 2 之间")
	public String getYwjgdm() {
		return ywjgdm;
	}

	public void setYwjgdm(String ywjgdm) {
		this.ywjgdm = ywjgdm;
	}
	
	@Length(min=1, max=20, message="业务机构名称长度必须介于 1 和 20 之间")
	public String getYwjgmc() {
		return ywjgmc;
	}

	public void setYwjgmc(String ywjgmc) {
		this.ywjgmc = ywjgmc;
	}
	
	@Length(min=1, max=2, message="隶属公司长度必须介于 1 和 2 之间")
	public String getLsgs() {
		return lsgs;
	}

	public void setLsgs(String lsgs) {
		this.lsgs = lsgs;
	}
	
	@Length(min=1, max=20, message="隶属公司名称长度必须介于 1 和 20 之间")
	public String getLsgsmc() {
		return lsgsmc;
	}

	public void setLsgsmc(String lsgsmc) {
		this.lsgsmc = lsgsmc;
	}
	
	@Length(min=1, max=2, message="调度公司长度必须介于 1 和 2 之间")
	public String getDdgsdm() {
		return ddgsdm;
	}

	public void setDdgsdm(String ddgsdm) {
		this.ddgsdm = ddgsdm;
	}
	
	@Length(min=1, max=20, message="调度公司名称长度必须介于 1 和 20 之间")
	public String getDdgsmc() {
		return ddgsmc;
	}

	public void setDdgsmc(String ddgsmc) {
		this.ddgsmc = ddgsmc;
	}
	
	@Length(min=1, max=10, message="合同状态长度必须介于 1 和 10 之间")
	public String getHtzt() {
		return htzt;
	}

	public void setHtzt(String htzt) {
		this.htzt = htzt;
	}
	
	@Length(min=1, max=10, message="合同模板长度必须介于 1 和 10 之间")
	public String getHtmb() {
		return htmb;
	}

	public void setHtmb(String htmb) {
		this.htmb = htmb;
	}
	
	@Length(min=1, max=9, message="正式合同编号长度必须介于 1 和 9 之间")
	public String getZshtbh() {
		return zshtbh;
	}

	public void setZshtbh(String zshtbh) {
		this.zshtbh = zshtbh;
	}
	
	@Length(min=1, max=6, message="客户代码长度必须介于 1 和 6 之间")
	public String getKhdm() {
		return khdm;
	}

	public void setKhdm(String khdm) {
		this.khdm = khdm;
	}
	
	@Length(min=1, max=6, message="业务人员代码长度必须介于 1 和 6 之间")
	public String getYwrydm() {
		return ywrydm;
	}

	public void setYwrydm(String ywrydm) {
		this.ywrydm = ywrydm;
	}
	
	@Length(min=1, max=8, message="业务人员长度必须介于 1 和 8 之间")
	public String getYwryxm() {
		return ywryxm;
	}

	public void setYwryxm(String ywryxm) {
		this.ywryxm = ywryxm;
	}
	
	@Length(min=1, max=8, message="经办人员姓名长度必须介于 1 和 8 之间")
	public String getJbryxm() {
		return jbryxm;
	}

	public void setJbryxm(String jbryxm) {
		this.jbryxm = jbryxm;
	}
	
	@Length(min=1, max=30, message="送达方式名称长度必须介于 1 和 30 之间")
	public String getSdfsmc() {
		return sdfsmc;
	}

	public void setSdfsmc(String sdfsmc) {
		this.sdfsmc = sdfsmc;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="送达日期不能为空")
	public Date getSdrq() {
		return sdrq;
	}

	public void setSdrq(Date sdrq) {
		this.sdrq = sdrq;
	}
	
	@Length(min=1, max=500, message="备注长度必须介于 1 和 500 之间")
	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}
	
	@Length(min=1, max=10, message="对方联系人长度必须介于 1 和 10 之间")
	public String getDflxr() {
		return dflxr;
	}

	public void setDflxr(String dflxr) {
		this.dflxr = dflxr;
	}
	
	@Length(min=1, max=6, message="编号人员长度必须介于 1 和 6 之间")
	public String getBhry() {
		return bhry;
	}

	public void setBhry(String bhry) {
		this.bhry = bhry;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="编号日期不能为空")
	public Date getBhrq() {
		return bhrq;
	}

	public void setBhrq(Date bhrq) {
		this.bhrq = bhrq;
	}
	
	@Length(min=0, max=20, message="客户合同编号长度必须介于 0 和 20 之间")
	public String getKhhtbh() {
		return khhtbh;
	}

	public void setKhhtbh(String khhtbh) {
		this.khhtbh = khhtbh;
	}
	
	@Length(min=1, max=60, message="乙方长度必须介于 1 和 60 之间")
	public String getYf() {
		return yf;
	}

	public void setYf(String yf) {
		this.yf = yf;
	}
	
	@Length(min=1, max=60, message="签订地点长度必须介于 1 和 60 之间")
	public String getQddd() {
		return qddd;
	}

	public void setQddd(String qddd) {
		this.qddd = qddd;
	}
	
	@Length(min=0, max=8, message="归档人长度必须介于 0 和 8 之间")
	public String getGdry() {
		return gdry;
	}

	public void setGdry(String gdry) {
		this.gdry = gdry;
	}
	
	@Length(min=1, max=6, message="最后修改人长度必须介于 1 和 6 之间")
	public String getZhxgr() {
		return zhxgr;
	}

	public void setZhxgr(String zhxgr) {
		this.zhxgr = zhxgr;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="最后修改时间不能为空")
	public Date getZhxgsj() {
		return zhxgsj;
	}

	public void setZhxgsj(Date zhxgsj) {
		this.zhxgsj = zhxgsj;
	}
	
	@Length(min=0, max=1, message="注销标志长度必须介于 0 和 1 之间")
	public String getZxbz() {
		return zxbz;
	}

	public void setZxbz(String zxbz) {
		this.zxbz = zxbz;
	}
	
	@Length(min=0, max=100, message="bz2长度必须介于 0 和 100 之间")
	public String getBz2() {
		return bz2;
	}

	public void setBz2(String bz2) {
		this.bz2 = bz2;
	}
	
	@Length(min=0, max=100, message="bz3长度必须介于 0 和 100 之间")
	public String getBz3() {
		return bz3;
	}

	public void setBz3(String bz3) {
		this.bz3 = bz3;
	}
	
	@Length(min=0, max=2, message="kpgsdm长度必须介于 0 和 2 之间")
	public String getKpgsdm() {
		return kpgsdm;
	}

	public void setKpgsdm(String kpgsdm) {
		this.kpgsdm = kpgsdm;
	}
	
	@Length(min=0, max=20, message="kpgsmc长度必须介于 0 和 20 之间")
	public String getKpgsmc() {
		return kpgsmc;
	}

	public void setKpgsmc(String kpgsmc) {
		this.kpgsmc = kpgsmc;
	}
	
	@Length(min=0, max=8, message="zdr长度必须介于 0 和 8 之间")
	public String getZdr() {
		return zdr;
	}

	public void setZdr(String zdr) {
		this.zdr = zdr;
	}
	
	@Length(min=0, max=8, message="spr1长度必须介于 0 和 8 之间")
	public String getSpr1() {
		return spr1;
	}

	public void setSpr1(String spr1) {
		this.spr1 = spr1;
	}
	
	@Length(min=0, max=8, message="spr2长度必须介于 0 和 8 之间")
	public String getSpr2() {
		return spr2;
	}

	public void setSpr2(String spr2) {
		this.spr2 = spr2;
	}
	
	@Length(min=0, max=8, message="spr3长度必须介于 0 和 8 之间")
	public String getSpr3() {
		return spr3;
	}

	public void setSpr3(String spr3) {
		this.spr3 = spr3;
	}
	
	@Length(min=0, max=8, message="spr4长度必须介于 0 和 8 之间")
	public String getSpr4() {
		return spr4;
	}

	public void setSpr4(String spr4) {
		this.spr4 = spr4;
	}
	
	@Length(min=0, max=8, message="spr5长度必须介于 0 和 8 之间")
	public String getSpr5() {
		return spr5;
	}

	public void setSpr5(String spr5) {
		this.spr5 = spr5;
	}
	
	@Length(min=0, max=8, message="spr6长度必须介于 0 和 8 之间")
	public String getSpr6() {
		return spr6;
	}

	public void setSpr6(String spr6) {
		this.spr6 = spr6;
	}
	
	@Length(min=0, max=8, message="spr7长度必须介于 0 和 8 之间")
	public String getSpr7() {
		return spr7;
	}

	public void setSpr7(String spr7) {
		this.spr7 = spr7;
	}
	
	@Length(min=0, max=8, message="spr8长度必须介于 0 和 8 之间")
	public String getSpr8() {
		return spr8;
	}

	public void setSpr8(String spr8) {
		this.spr8 = spr8;
	}
	
	@Length(min=0, max=8, message="spr9长度必须介于 0 和 8 之间")
	public String getSpr9() {
		return spr9;
	}

	public void setSpr9(String spr9) {
		this.spr9 = spr9;
	}
	
	@Length(min=0, max=8, message="gdr长度必须介于 0 和 8 之间")
	public String getGdr() {
		return gdr;
	}

	public void setGdr(String gdr) {
		this.gdr = gdr;
	}
	
	@Length(min=0, max=5, message="wdbh长度必须介于 0 和 5 之间")
	public String getWdbh() {
		return wdbh;
	}

	public void setWdbh(String wdbh) {
		this.wdbh = wdbh;
	}
	
	@Length(min=0, max=1, message="spbz长度必须介于 0 和 1 之间")
	public String getSpbz() {
		return spbz;
	}

	public void setSpbz(String spbz) {
		this.spbz = spbz;
	}
	
	@Length(min=0, max=2, message="bmdm长度必须介于 0 和 2 之间")
	public String getBmdm() {
		return bmdm;
	}

	public void setBmdm(String bmdm) {
		this.bmdm = bmdm;
	}
	
	@Length(min=0, max=2, message="fgsdm长度必须介于 0 和 2 之间")
	public String getFgsdm() {
		return fgsdm;
	}

	public void setFgsdm(String fgsdm) {
		this.fgsdm = fgsdm;
	}
	
	@Length(min=0, max=10, message="xmfzr长度必须介于 0 和 10 之间")
	public String getXmfzr() {
		return xmfzr;
	}

	public void setXmfzr(String xmfzr) {
		this.xmfzr = xmfzr;
	}
	
	@Length(min=0, max=6, message="xmfzrdm长度必须介于 0 和 6 之间")
	public String getXmfzrdm() {
		return xmfzrdm;
	}

	public void setXmfzrdm(String xmfzrdm) {
		this.xmfzrdm = xmfzrdm;
	}
	
	@Length(min=0, max=11, message="xjzq长度必须介于 0 和 11 之间")
	public String getXjzq() {
		return xjzq;
	}

	public void setXjzq(String xjzq) {
		this.xjzq = xjzq;
	}
	
	@Length(min=0, max=20, message="yyswhtbh长度必须介于 0 和 20 之间")
	public String getYyswhtbh() {
		return yyswhtbh;
	}

	public void setYyswhtbh(String yyswhtbh) {
		this.yyswhtbh = yyswhtbh;
	}
	
	@Length(min=0, max=650, message="zy长度必须介于 0 和 650 之间")
	public String getZy() {
		return zy;
	}

	public void setZy(String zy) {
		this.zy = zy;
	}
	
	@Length(min=0, max=50, message="dykh长度必须介于 0 和 50 之间")
	public String getDykh() {
		return dykh;
	}

	public void setDykh(String dykh) {
		this.dykh = dykh;
	}
	
	@Length(min=0, max=6, message="dykhdm长度必须介于 0 和 6 之间")
	public String getDykhdm() {
		return dykhdm;
	}

	public void setDykhdm(String dykhdm) {
		this.dykhdm = dykhdm;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getZdrq() {
		return zdrq;
	}

	public void setZdrq(Date zdrq) {
		this.zdrq = zdrq;
	}
	
	@Length(min=0, max=50, message="zzkhmc长度必须介于 0 和 50 之间")
	public String getZzkhmc() {
		return zzkhmc;
	}

	public void setZzkhmc(String zzkhmc) {
		this.zzkhmc = zzkhmc;
	}
	
	@Length(min=0, max=2, message="swhtlx长度必须介于 0 和 2 之间")
	public String getSwhtlx() {
		return swhtlx;
	}

	public void setSwhtlx(String swhtlx) {
		this.swhtlx = swhtlx;
	}
	
	public Double getSl() {
		return sl;
	}

	public void setSl(Double sl) {
		this.sl = sl;
	}
	
	public Double getZjhkl() {
		return zjhkl;
	}

	public void setZjhkl(Double zjhkl) {
		this.zjhkl = zjhkl;
	}
	
	public Double getFtmlebl() {
		return ftmlebl;
	}

	public void setFtmlebl(Double ftmlebl) {
		this.ftmlebl = ftmlebl;
	}
	
	public Double getMle() {
		return mle;
	}

	public void setMle(Double mle) {
		this.mle = mle;
	}
	
	public Double getMll() {
		return mll;
	}

	public void setMll(Double mll) {
		this.mll = mll;
	}
	
	public Date getBeginHtksrq() {
		return beginHtksrq;
	}

	public void setBeginHtksrq(Date beginHtksrq) {
		this.beginHtksrq = beginHtksrq;
	}
	
	public Date getEndHtksrq() {
		return endHtksrq;
	}

	public void setEndHtksrq(Date endHtksrq) {
		this.endHtksrq = endHtksrq;
	}
		
	public Date getBeginHtzzrq() {
		return beginHtzzrq;
	}

	public void setBeginHtzzrq(Date beginHtzzrq) {
		this.beginHtzzrq = beginHtzzrq;
	}
	
	public Date getEndHtzzrq() {
		return endHtzzrq;
	}

	public void setEndHtzzrq(Date endHtzzrq) {
		this.endHtzzrq = endHtzzrq;
	}

	public Double getAvgmle() {
		return avgmle;
	}

	public void setAvgmle(Double avgmle) {
		this.avgmle = avgmle;
	}



		
}