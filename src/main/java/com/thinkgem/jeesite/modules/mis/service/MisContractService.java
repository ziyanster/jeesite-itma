/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.MisContract;
import com.thinkgem.jeesite.modules.mis.dao.MisContractDao;
import com.thinkgem.jeesite.modules.mis.entity.MisContractdetail;
import com.thinkgem.jeesite.modules.mis.dao.MisContractdetailDao;

/**
 * 巡检合同Service
 * @author ym
 * @version 2015-12-11
 */
@Service
@Transactional(readOnly = true)
public class MisContractService extends CrudService<MisContractDao, MisContract> {

	@Autowired
	private MisContractdetailDao misContractdetailDao;
	
	public MisContract get(String id) {
		MisContract misContract = super.get(id);
		misContract.setMisContractdetailList(misContractdetailDao.findList(new MisContractdetail(misContract)));
		return misContract;
	}
	
	public List<MisContract> findList(MisContract misContract) {
		return super.findList(misContract);
	}
	
	public Page<MisContract> findPage(Page<MisContract> page, MisContract misContract) {
		return super.findPage(page, misContract);
	}
	
	@Transactional(readOnly = false)
	public void save(MisContract misContract) {
		super.save(misContract);
		for (MisContractdetail misContractdetail : misContract.getMisContractdetailList()){
			if (misContractdetail.getId() == null){
				continue;
			}
			if (MisContractdetail.DEL_FLAG_NORMAL.equals(misContractdetail.getDelFlag())){
				if (StringUtils.isBlank(misContractdetail.getId())){
					misContractdetail.setMisContract(misContract);
					misContractdetail.preInsert();
					misContractdetailDao.insert(misContractdetail);
				}else{
					misContractdetail.preUpdate();
					misContractdetailDao.update(misContractdetail);
				}
			}else{
				misContractdetailDao.delete(misContractdetail);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(MisContract misContract) {
		super.delete(misContract);
		misContractdetailDao.delete(new MisContractdetail(misContract));
	}
	
}