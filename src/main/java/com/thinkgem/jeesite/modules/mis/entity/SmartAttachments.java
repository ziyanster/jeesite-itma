/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 附件管理Entity
 * @author 黄尚弟
 * @version 2015-10-25
 */
public class SmartAttachments extends DataEntity<SmartAttachments> {
	
	private static final long serialVersionUID = 1L;
	private String taskno;		// 任务编号
	private String orderid;		// 工单编号
	private String type;		// 附件类型
	
	public SmartAttachments() {
		super();
	}

	public SmartAttachments(String id){
		super(id);
	}

	@Length(min=0, max=64, message="任务编号长度必须介于 0 和 64 之间")
	public String getTaskno() {
		return taskno;
	}

	public void setTaskno(String taskno) {
		this.taskno = taskno;
	}
	
	@Length(min=0, max=64, message="工单编号长度必须介于 0 和 64 之间")
	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	
	@Length(min=0, max=1, message="附件类型长度必须介于 0 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}