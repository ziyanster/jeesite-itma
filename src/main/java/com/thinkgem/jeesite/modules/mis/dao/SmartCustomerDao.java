/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.sys.entity.Dict;

/**
 * 客户资料维护DAO接口
 * @author 黄尚弟
 * @version 2015-10-12
 */
@MyBatisDao
public interface SmartCustomerDao extends CrudDao<SmartCustomer> {
	

}