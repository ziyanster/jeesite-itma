/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import java.util.Date;
import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 工单管理Entity
 * @author 黄尚弟
 * @version 2015-10-21
 */
public class SmartOrder extends DataEntity<SmartOrder> {
	
	private static final long serialVersionUID = 1L;
	private String servicedocno;		// 服务单号
	private String customerno;		// 客户编号
	private String orderserialno;		// 工单流水号
	private String taskno;		// 任务编号
	private String equipmentno;		// 设备编号
	private String isassigned;		// 是否指派
	private User assignedno;		// 受理人
	private String taskcontent;		// 工作内容
	private String thistimejob;		// 本次解决的问题
	private String nexttimeplan;		// 下一步工作计划
	private String opinion;		// 客户意见
	private String advice;		// 客户建议
	private Date responsetime;		// 服务响应时间
	private Date arrivedtime;		// 到达现场时间
	private Date solvetime;		// 问题解决时间
	private User audituser;		// 审批人
	private Date audittime;		// 审批时间
	private String auditopinion;		// 审批意见
	private String status;		// 状态
	private String iscomplete;		// 请用户确认是否完成实施
	private String isappointment;		// 是否在规定时间与客户预约
	private String isontime;		// 是否在预约时间上门服务
	private String isback;		// 是否退单
	private String backremark;		// 退单说明
	private String servicetype;		// servicetype
	private Integer integral;		// 积分
	private String record;		// 呼叫记录
	private Integer timeout;		// 时效（分）
	private String modeltype;		// 机型
	private Date beginCreateDate;		// 开始 创建时间
	private Date endCreateDate;		// 结束 创建时间
	
	private String smsIndex1;		// mis短信记录开始索引
	private String smsIndex2;		// mis短信记录结束索引
	
	private String isValued;       //是否已评分
	private String valuedPerson;	//评分人
	
	public SmartOrder() {
		super();
	}

	public SmartOrder(String id){
		super(id);
	}

	@Length(min=0, max=64, message="服务单号长度必须介于 0 和 64 之间")
	public String getServicedocno() {
		return servicedocno;
	}

	public void setServicedocno(String servicedocno) {
		this.servicedocno = servicedocno;
	}
	
	@Length(min=0, max=64, message="客户编号长度必须介于 0 和 64 之间")
	public String getCustomerno() {
		return customerno;
	}

	public void setCustomerno(String customerno) {
		this.customerno = customerno;
	}
	
	@Length(min=0, max=64, message="任务编号长度必须介于 0 和 64 之间")
	public String getTaskno() {
		return taskno;
	}

	public void setTaskno(String taskno) {
		this.taskno = taskno;
	}
	
	@Length(min=0, max=64, message="设备编号长度必须介于 0 和 64 之间")
	public String getEquipmentno() {
		return equipmentno;
	}

	public void setEquipmentno(String equipmentno) {
		this.equipmentno = equipmentno;
	}
	
	@Length(min=0, max=1, message="是否指派长度必须介于 0 和 1 之间")
	public String getIsassigned() {
		return isassigned;
	}

	public void setIsassigned(String isassigned) {
		this.isassigned = isassigned;
	}
	
	public User getAssignedno() {
		return assignedno;
	}

	public void setAssignedno(User assignedno) {
		this.assignedno = assignedno;
	}
	
	@Length(min=0, max=200, message="工作内容长度必须介于 0 和 200 之间")
	public String getTaskcontent() {
		return taskcontent;
	}

	public void setTaskcontent(String taskcontent) {
		this.taskcontent = taskcontent;
	}
	
	@Length(min=0, max=200, message="本次解决的问题长度必须介于 0 和 200 之间")
	public String getThistimejob() {
		return thistimejob;
	}

	public void setThistimejob(String thistimejob) {
		this.thistimejob = thistimejob;
	}
	
	@Length(min=0, max=200, message="下一步工作计划长度必须介于 0 和 200 之间")
	public String getNexttimeplan() {
		return nexttimeplan;
	}

	public void setNexttimeplan(String nexttimeplan) {
		this.nexttimeplan = nexttimeplan;
	}
	
	@Length(min=0, max=1, message="客户意见长度必须介于 0 和 1 之间")
	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}
	
	@Length(min=0, max=200, message="客户建议长度必须介于 0 和 200 之间")
	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="服务响应时间不能为空")
	public Date getResponsetime() {
		return responsetime;
	}

	public void setResponsetime(Date responsetime) {
		this.responsetime = responsetime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="到达现场时间不能为空")
	public Date getArrivedtime() {
		return arrivedtime;
	}

	public void setArrivedtime(Date arrivedtime) {
		this.arrivedtime = arrivedtime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="问题解决时间不能为空")
	public Date getSolvetime() {
		return solvetime;
	}

	public void setSolvetime(Date solvetime) {
		this.solvetime = solvetime;
	}
	
	public User getAudituser() {
		return audituser;
	}

	public void setAudituser(User audituser) {
		this.audituser = audituser;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="审批时间不能为空")
	public Date getAudittime() {
		return audittime;
	}

	public void setAudittime(Date audittime) {
		this.audittime = audittime;
	}
	
	@Length(min=0, max=200, message="审批意见长度必须介于 0 和 200 之间")
	public String getAuditopinion() {
		return auditopinion;
	}

	public void setAuditopinion(String auditopinion) {
		this.auditopinion = auditopinion;
	}
	
	@Length(min=0, max=1, message="状态长度必须介于 0 和 1 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Length(min=0, max=1, message="请用户确认是否完成实施长度必须介于 0 和 1 之间")
	public String getIscomplete() {
		return iscomplete;
	}

	public void setIscomplete(String iscomplete) {
		this.iscomplete = iscomplete;
	}
	
	@Length(min=0, max=1, message="是否在规定时间与客户预约长度必须介于 0 和 1 之间")
	public String getIsappointment() {
		return isappointment;
	}

	public void setIsappointment(String isappointment) {
		this.isappointment = isappointment;
	}
	
	@Length(min=0, max=1, message="是否在预约时间上门服务长度必须介于 0 和 1 之间")
	public String getIsontime() {
		return isontime;
	}

	public void setIsontime(String isontime) {
		this.isontime = isontime;
	}
	
	@Length(min=0, max=1, message="是否退单长度必须介于 0 和 1 之间")
	public String getIsback() {
		return isback;
	}

	public void setIsback(String isback) {
		this.isback = isback;
	}
	
	@Length(min=0, max=200, message="退单说明长度必须介于 0 和 200 之间")
	public String getBackremark() {
		return backremark;
	}

	public void setBackremark(String backremark) {
		this.backremark = backremark;
	}
	
	@Length(min=0, max=1, message="servicetype长度必须介于 0 和 1 之间")
	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	
	public Integer getIntegral() {
		return integral;
	}

	public void setIntegral(Integer integral) {
		this.integral = integral;
	}
	
	@Length(min=0, max=512, message="呼叫记录长度必须介于 0 和 512 之间")
	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}
	
	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}
	
	@Length(min=0, max=200, message="机型长度必须介于 0 和 200 之间")
	public String getModeltype() {
		return modeltype;
	}

	public void setModeltype(String modeltype) {
		this.modeltype = modeltype;
	}
	
	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}
	
	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}

	public String getSmsIndex1() {
		return smsIndex1;
	}

	public void setSmsIndex1(String smsIndex1) {
		this.smsIndex1 = smsIndex1;
	}

	public String getSmsIndex2() {
		return smsIndex2;
	}

	public void setSmsIndex2(String smsIndex2) {
		this.smsIndex2 = smsIndex2;
	}

	public String getOrderserialno() {
		return orderserialno;
	}

	public void setOrderserialno(String orderserialno) {
		this.orderserialno = orderserialno;
	}

	public String getIsValued() {
		return isValued;
	}

	public void setIsValued(String isValued) {
		this.isValued = isValued;
	}

	public String getValuedPerson() {
		return valuedPerson;
	}

	public void setValuedPerson(String valuedPerson) {
		this.valuedPerson = valuedPerson;
	}
		
}