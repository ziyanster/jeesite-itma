/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.HjHtSwhtbh;
import com.thinkgem.jeesite.modules.mis.entity.HjMarkerterTask;
import com.thinkgem.jeesite.modules.mis.service.HjHtSwhtbhService;
import com.thinkgem.jeesite.modules.mis.service.HjMarkerterTaskService;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 销售任务Controller
 * @author 黄尚弟
 * @version 2015-09-27
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/hjHtSwhtbh")
public class HjHtSwhtbhController extends BaseController {

	@Autowired
	private HjHtSwhtbhService hjHtSwhtbhService;
	@Autowired
	private HjMarkerterTaskService hjMarkerterTaskService;
	
	@ModelAttribute
	public HjHtSwhtbh get(@RequestParam(required=false) String id) {
		HjHtSwhtbh entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = hjHtSwhtbhService.get(id);
		}
		if (entity == null){
			entity = new HjHtSwhtbh();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:hjHtSwhtbh:view")
	@RequestMapping(value = {"list", ""})
	public String list(HjHtSwhtbh hjHtSwhtbh, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<HjHtSwhtbh> pageList = hjHtSwhtbhService.findPage(new Page<HjHtSwhtbh>(request, response), hjHtSwhtbh); 
		model.addAttribute("page", pageList);
		return "modules/mis/hjHtSwhtbhList";
	}
	@RequiresPermissions("mis:hjHtSwhtbh:view")
	@RequestMapping(value = {"list", "tasklist"})
	public String tasklist(HjHtSwhtbh hjHtSwhtbh, HttpServletRequest request, HttpServletResponse response, Model model) {
		String saleID	 = UserUtils.getUser().getLoginName();
		String saleYear  = hjHtSwhtbh.getBz3();
		//hjHtSwhtbh.setBz3("");

		hjHtSwhtbh.setYwrydm(saleID);
		Page<HjHtSwhtbh> pageList = hjHtSwhtbhService.findPage(new Page<HjHtSwhtbh>(request, response), hjHtSwhtbh); 
		if(pageList.getList().size()>0){
			if(saleYear == null)
				model.addAttribute("page", pageList);
			else{
				int year_sale=Integer.parseInt(saleYear);		
				Page<HjHtSwhtbh> page =new Page<HjHtSwhtbh>();
				int countTotal=pageList.getList().size();
				int count=0;
				int year_htks,year_htzz;
				for(int i=0;i<countTotal;i++){
					year_htks  =pageList.getList().get(i).getHtksrq().getYear()+1900;
					year_htzz  =pageList.getList().get(i).getHtzzrq().getYear()+1900;
					if(year_sale>=year_htks && year_sale<=year_htzz){
						page.getList().add(pageList.getList().get(i));
						count++;
					}
				}
				page.setCount(count);
				model.addAttribute("page", page);			
			}
	
			
			HjMarkerterTask hjMarkerterTask=new HjMarkerterTask();
			hjMarkerterTask.setSalesid(saleID);
			hjMarkerterTask.setYear(saleYear);
			Page<HjMarkerterTask> pageTask = hjMarkerterTaskService.findPage(new Page<HjMarkerterTask>(request, response), hjMarkerterTask); 
			model.addAttribute("pageTask", pageTask);	
		}
		return "modules/mis/personTaskList";
	}
	@RequiresPermissions("mis:hjHtSwhtbh:view")
	@RequestMapping(value = "form")
	public String form(HjHtSwhtbh hjHtSwhtbh, Model model) {
		model.addAttribute("hjHtSwhtbh", hjHtSwhtbh);
		return "modules/mis/hjHtSwhtbhForm";
	}

	@RequiresPermissions("mis:hjHtSwhtbh:edit")
	@RequestMapping(value = "save")
	public String save(HjHtSwhtbh hjHtSwhtbh, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, hjHtSwhtbh)){
			return form(hjHtSwhtbh, model);
		}
		hjHtSwhtbhService.save(hjHtSwhtbh);
		addMessage(redirectAttributes, "保存销售任务成功");
		return "redirect:"+Global.getAdminPath()+"/mis/hjHtSwhtbh/?repage";
	}
	
	@RequiresPermissions("mis:hjHtSwhtbh:edit")
	@RequestMapping(value = "delete")
	public String delete(HjHtSwhtbh hjHtSwhtbh, RedirectAttributes redirectAttributes) {
		hjHtSwhtbhService.delete(hjHtSwhtbh);
		addMessage(redirectAttributes, "删除销售任务成功");
		return "redirect:"+Global.getAdminPath()+"/mis/hjHtSwhtbh/?repage";
	}

}