/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.cms.entity.Category;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.StatsParam;
import com.thinkgem.jeesite.modules.mis.entity.StatsResult;
import com.thinkgem.jeesite.modules.sys.entity.User;

/**
 * 工单管理DAO接口
 * @author 黄尚弟
 * @version 2015-10-21
 */
@MyBatisDao
public interface SmartOrderDao extends CrudDao<SmartOrder> {
	public List<StatsResult> findExitStats(StatsParam param);	
	public List<StatsResult> findStatusStats(StatsParam param);	
	public List<StatsResult> findIndexStats(StatsParam param);
}