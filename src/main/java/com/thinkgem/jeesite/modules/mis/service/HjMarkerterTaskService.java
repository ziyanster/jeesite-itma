/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.HjMarkerterTask;
import com.thinkgem.jeesite.modules.mis.dao.HjMarkerterTaskDao;

/**
 * 销售任务Service
 * @author 黄尚弟
 * @version 2015-10-10
 */
@Service
@Transactional(readOnly = true)
public class HjMarkerterTaskService extends CrudService<HjMarkerterTaskDao, HjMarkerterTask> {

	public HjMarkerterTask get(String id) {
		return super.get(id);
	}
	
	public List<HjMarkerterTask> findList(HjMarkerterTask hjMarkerterTask) {
		return super.findList(hjMarkerterTask);
	}
	
	public Page<HjMarkerterTask> findPage(Page<HjMarkerterTask> page, HjMarkerterTask hjMarkerterTask) {
		return super.findPage(page, hjMarkerterTask);
	}
	
	@Transactional(readOnly = false)
	public void save(HjMarkerterTask hjMarkerterTask) {
		super.save(hjMarkerterTask);
	}
	
	@Transactional(readOnly = false)
	public void delete(HjMarkerterTask hjMarkerterTask) {
		super.delete(hjMarkerterTask);
	}
	
}