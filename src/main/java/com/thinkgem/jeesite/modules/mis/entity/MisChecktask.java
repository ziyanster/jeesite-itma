/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 巡检任务管理Entity
 * @author ym
 * @version 2015-12-14
 */
public class MisChecktask extends DataEntity<MisChecktask> {
	
	private static final long serialVersionUID = 1L;
	private String contractid;		// 合同编号
	private String taskserialno;		// 任务流水号
	private Date checkdata;		// 巡检日期
	private String weekremind;		// 提前一周提醒
	private String dayremind;		// 提前一天提醒
	private String morningremind;		// 早上提醒
	private String nightremind;		// 晚上提醒
	private String checkfre;		// 巡检频次
	private String isenabled;		// 是否停用
	private Date beginCheckdata;		// 开始 巡检日期
	private Date endCheckdata;		// 结束 巡检日期
	
	private Date advweek;		// 开始 巡检日期
	private Date advday;		// 结束 巡检日期
	private Date advmorning;		// 开始 巡检日期
	private Date advnight;		// 结束 巡检日期
	
	public Date getAdvweek() {
		return advweek;
	}

	public void setAdvweek(Date advweek) {
		this.advweek = advweek;
	}

	public Date getAdvday() {
		return advday;
	}

	public void setAdvday(Date advday) {
		this.advday = advday;
	}

	public Date getAdvmorning() {
		return advmorning;
	}

	public void setAdvmorning(Date advmorning) {
		this.advmorning = advmorning;
	}

	public Date getAdvnight() {
		return advnight;
	}

	public void setAdvnight(Date advnight) {
		this.advnight = advnight;
	}

	public MisChecktask() {
		super();
	}

	public MisChecktask(String id){
		super(id);
	}

	@Length(min=0, max=64, message="合同编号长度必须介于 0 和 64 之间")
	public String getContractid() {
		return contractid;
	}

	public void setContractid(String contractid) {
		this.contractid = contractid;
	}
	
	@Length(min=0, max=64, message="任务流水号长度必须介于 0 和 64 之间")
	public String getTaskserialno() {
		return taskserialno;
	}

	public void setTaskserialno(String taskserialno) {
		this.taskserialno = taskserialno;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCheckdata() {
		return checkdata;
	}

	public void setCheckdata(Date checkdata) {
		this.checkdata = checkdata;
	}
	
	@Length(min=0, max=1, message="提前一周提醒长度必须介于 0 和 1 之间")
	public String getWeekremind() {
		return weekremind;
	}

	public void setWeekremind(String weekremind) {
		this.weekremind = weekremind;
	}
	
	@Length(min=0, max=1, message="提前一天提醒长度必须介于 0 和 1 之间")
	public String getDayremind() {
		return dayremind;
	}

	public void setDayremind(String dayremind) {
		this.dayremind = dayremind;
	}
	
	@Length(min=0, max=1, message="早上提醒长度必须介于 0 和 1 之间")
	public String getMorningremind() {
		return morningremind;
	}

	public void setMorningremind(String morningremind) {
		this.morningremind = morningremind;
	}
	
	@Length(min=0, max=1, message="晚上提醒长度必须介于 0 和 1 之间")
	public String getNightremind() {
		return nightremind;
	}

	public void setNightremind(String nightremind) {
		this.nightremind = nightremind;
	}
	
	@Length(min=0, max=1, message="巡检频次长度必须介于 0 和 1 之间")
	public String getCheckfre() {
		return checkfre;
	}

	public void setCheckfre(String checkfre) {
		this.checkfre = checkfre;
	}
	
	@Length(min=0, max=1, message="是否停用长度必须介于 0 和 1 之间")
	public String getIsenabled() {
		return isenabled;
	}

	public void setIsenabled(String isenabled) {
		this.isenabled = isenabled;
	}
	
	public Date getBeginCheckdata() {
		return beginCheckdata;
	}

	public void setBeginCheckdata(Date beginCheckdata) {
		this.beginCheckdata = beginCheckdata;
	}
	
	public Date getEndCheckdata() {
		return endCheckdata;
	}

	public void setEndCheckdata(Date endCheckdata) {
		this.endCheckdata = endCheckdata;
	}
		
}