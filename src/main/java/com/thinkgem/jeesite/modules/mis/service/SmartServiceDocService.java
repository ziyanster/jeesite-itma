/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.SmartServiceDoc;
import com.thinkgem.jeesite.modules.mis.dao.SmartServiceDocDao;

/**
 * 运维任务信息Service
 * @author 黄尚弟
 * @version 2015-10-12
 */
@Service
@Transactional(readOnly = true)
public class SmartServiceDocService extends CrudService<SmartServiceDocDao, SmartServiceDoc> {

	public SmartServiceDoc get(String id) {
		return super.get(id);
	}
	
	public List<SmartServiceDoc> findList(SmartServiceDoc smartServiceDoc) {
		return super.findList(smartServiceDoc);
	}
	
	public Page<SmartServiceDoc> findPage(Page<SmartServiceDoc> page, SmartServiceDoc smartServiceDoc) {
		return super.findPage(page, smartServiceDoc);
	}
	
	@Transactional(readOnly = false)
	public void save(SmartServiceDoc smartServiceDoc) {
		super.save(smartServiceDoc);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmartServiceDoc smartServiceDoc) {
		super.delete(smartServiceDoc);
	}
	
}