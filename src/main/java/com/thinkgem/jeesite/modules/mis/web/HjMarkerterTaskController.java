/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.utils.excel.ExportExcel;
import com.thinkgem.jeesite.modules.mis.entity.HjHtSwhtbh;
import com.thinkgem.jeesite.modules.mis.entity.HjMarkerterTask;
import com.thinkgem.jeesite.modules.mis.service.HjHtSwhtbhService;
import com.thinkgem.jeesite.modules.mis.service.HjMarkerterTaskService;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 销售任务Controller
 * @author 黄尚弟
 * @version 2015-10-10
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/hjMarkerterTask")
public class HjMarkerterTaskController extends BaseController {
	@Autowired
	private HjHtSwhtbhService hjHtSwhtbhService;
	@Autowired
	private HjMarkerterTaskService hjMarkerterTaskService;
	
	@ModelAttribute
	public HjMarkerterTask get(@RequestParam(required=false) String id) {
		HjMarkerterTask entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = hjMarkerterTaskService.get(id);
		}
		if (entity == null){
			entity = new HjMarkerterTask();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:hjMarkerterTask:view")
	@RequestMapping(value = {"list", ""})
	public String list(HjMarkerterTask hjMarkerterTask, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<HjMarkerterTask> page = hjMarkerterTaskService.findPage(new Page<HjMarkerterTask>(request, response), hjMarkerterTask); 
		model.addAttribute("page", page);
		return "modules/mis/hjMarkerterTaskList";
	}
	@RequiresPermissions("mis:hjMarkerterTask:view")
	@RequestMapping(value = "persontasklist")
	public String persontasklist(HjHtSwhtbh hjHtSwhtbh, HttpServletRequest request, HttpServletResponse response, Model model) {
		String loginID   = UserUtils.getUser().getLoginName();
		String saleID	 = hjHtSwhtbh.getYwrydm();
		String saleYear  = hjHtSwhtbh.getBz3();
		hjHtSwhtbh.setBz3("");
		//判断是否为指定销售查询数据
		if(saleID==null){
			saleID=loginID;
		}
		hjHtSwhtbh.setYwrydm(saleID);
		List<HjHtSwhtbh> list=hjHtSwhtbhService.findList(hjHtSwhtbh);
		List<HjHtSwhtbh> pageList =Lists.newArrayList();
		double dYearMLE=0;
		if(list.size()>0){
			int year_sale=Integer.parseInt(saleYear);		
			Page<HjHtSwhtbh> page =new Page<HjHtSwhtbh>();
			int countTotal=list.size();
			int year_htks,year_htzz;
			for(int i=0;i<countTotal;i++){
				year_htks  =list.get(i).getHtksrq().getYear()+1900;
				year_htzz  =list.get(i).getHtzzrq().getYear()+1900 ;
				if(
						((year_sale==year_htks) && (year_sale==year_htzz)) 
					||  ((year_sale>=year_htks) && (year_sale<year_htzz))
				){
					pageList.add(list.get(i));
					dYearMLE=dYearMLE+list.get(i).getAvgmle();
				}
			}
			model.addAttribute("page", pageList);			
		}else{
			model.addAttribute("page", null);
		}
		dYearMLE=dYearMLE/10000;
		DecimalFormat df=new DecimalFormat(".####");
		model.addAttribute("yearMle", df.format(dYearMLE));
			
		HjMarkerterTask hjMarkerterTask=new HjMarkerterTask();
		hjMarkerterTask.setSalesid(saleID);
		hjMarkerterTask.setYear(saleYear);
		Page<HjMarkerterTask> pageTask = hjMarkerterTaskService.findPage(new Page<HjMarkerterTask>(request, response), hjMarkerterTask); 
		model.addAttribute("pageTask", pageTask);	

		return "modules/mis/personTaskInfo";
	}
	@RequiresPermissions("mis:hjMarkerterTask:view")
	@RequestMapping(value = "form")
	public String form(HjMarkerterTask hjMarkerterTask, Model model) {
		model.addAttribute("hjMarkerterTask", hjMarkerterTask);
		return "modules/mis/hjMarkerterTaskForm";
	}

	@RequiresPermissions("mis:hjMarkerterTask:edit")
	@RequestMapping(value = "save")
	public String save(HjMarkerterTask hjMarkerterTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, hjMarkerterTask)){
			return form(hjMarkerterTask, model);
		}
		hjMarkerterTaskService.save(hjMarkerterTask);
		addMessage(redirectAttributes, "保存销售任务成功");
		return "redirect:"+Global.getAdminPath()+"/mis/hjMarkerterTask/?repage";
	}
	
	@RequiresPermissions("mis:hjMarkerterTask:edit")
	@RequestMapping(value = "delete")
	public String delete(HjMarkerterTask hjMarkerterTask, RedirectAttributes redirectAttributes) {
		hjMarkerterTaskService.delete(hjMarkerterTask);
		addMessage(redirectAttributes, "删除销售任务成功");
		return "redirect:"+Global.getAdminPath()+"/mis/hjMarkerterTask/?repage";
	}
	/**
	 * 导出用户数据
	 * @param user
	 * @param request
	 * @param response
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("mis:hjMarkerterTask:view")
    @RequestMapping(value = "export", method=RequestMethod.POST)
	public String exportFile(HjMarkerterTask hjMarkerterTask, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
		try {
            String fileName = "销售任务数据"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            List<HjMarkerterTask> list=hjMarkerterTaskService.findList(hjMarkerterTask);
    		//Page<HjMarkerterTask> page = hjMarkerterTaskService.findPage(new Page<HjMarkerterTask>(request, response), hjMarkerterTask); 
    		new ExportExcel("销售任务数据", HjMarkerterTask.class).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			addMessage(redirectAttributes, "导出销售任务数据失败！失败信息："+e.getMessage());
		}
		//return "redirect:" + adminPath + "/sys/user/list?repage";
		return "redirect:"+Global.getAdminPath()+"/mis/hjMarkerterTask/?repage";
    }
}