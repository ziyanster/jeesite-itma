/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.mis.dao.SmartTaskDao;

/**
 * 运维任务录入Service
 * @author 黄尚弟
 * @version 2015-10-13
 */
@Service
@Transactional(readOnly = true)
public class SmartTaskService extends CrudService<SmartTaskDao, SmartTask> {

	public SmartTask get(String id) {
		return super.get(id);
	}
	
	public List<SmartTask> findList(SmartTask smartTask) {
		return super.findList(smartTask);
	}
	
	public Page<SmartTask> findPage(Page<SmartTask> page, SmartTask smartTask) {
		return super.findPage(page, smartTask);
	}
	
	@Transactional(readOnly = false)
	public void save(SmartTask smartTask) {
		super.save(smartTask);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmartTask smartTask) {
		super.delete(smartTask);
	}
	
}