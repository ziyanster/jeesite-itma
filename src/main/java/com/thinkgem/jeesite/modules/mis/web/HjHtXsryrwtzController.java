/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.HjHtXsryrwtz;
import com.thinkgem.jeesite.modules.mis.service.HjHtXsryrwtzService;

/**
 * 销售人员任务台账Controller
 * @author 黄尚弟
 * @version 2015-09-24
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/hjHtXsryrwtz")
public class HjHtXsryrwtzController extends BaseController {

	@Autowired
	private HjHtXsryrwtzService hjHtXsryrwtzService;
	
	@ModelAttribute
	public HjHtXsryrwtz get(@RequestParam(required=false) String id) {
		HjHtXsryrwtz entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = hjHtXsryrwtzService.get(id);
		}
		if (entity == null){
			entity = new HjHtXsryrwtz();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:hjHtXsryrwtz:view")
	@RequestMapping(value = {"list", ""})
	public String list(HjHtXsryrwtz hjHtXsryrwtz, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<HjHtXsryrwtz> page = hjHtXsryrwtzService.findPage(new Page<HjHtXsryrwtz>(request, response), hjHtXsryrwtz); 
		model.addAttribute("page", page);
		return "modules/mis/hjHtXsryrwtzList";
	}

	@RequiresPermissions("mis:hjHtXsryrwtz:view")
	@RequestMapping(value = "form")
	public String form(HjHtXsryrwtz hjHtXsryrwtz, Model model) {
		model.addAttribute("hjHtXsryrwtz", hjHtXsryrwtz);
		return "modules/mis/hjHtXsryrwtzForm";
	}

	@RequiresPermissions("mis:hjHtXsryrwtz:edit")
	@RequestMapping(value = "save")
	public String save(HjHtXsryrwtz hjHtXsryrwtz, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, hjHtXsryrwtz)){
			return form(hjHtXsryrwtz, model);
		}
		hjHtXsryrwtzService.save(hjHtXsryrwtz);
		addMessage(redirectAttributes, "保存销售人员任务台账成功");
		return "redirect:"+Global.getAdminPath()+"/mis/hjHtXsryrwtz/?repage";
	}
	
	@RequiresPermissions("mis:hjHtXsryrwtz:edit")
	@RequestMapping(value = "delete")
	public String delete(HjHtXsryrwtz hjHtXsryrwtz, RedirectAttributes redirectAttributes) {
		hjHtXsryrwtzService.delete(hjHtXsryrwtz);
		addMessage(redirectAttributes, "删除销售人员任务台账成功");
		return "redirect:"+Global.getAdminPath()+"/mis/hjHtXsryrwtz/?repage";
	}

}