package com.thinkgem.jeesite.modules.mis.entity;

public class jsonTask {
	private	String	orderId;
	private	String	orderHref;
	private	String	orderTitle;
	private	String	orderSubTitle;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderHref() {
		return orderHref;
	}
	public void setOrderHref(String orderHref) {
		this.orderHref = orderHref;
	}
	public String getOrderTitle() {
		return orderTitle;
	}
	public void setOrderTitle(String orderTitle) {
		this.orderTitle = orderTitle;
	}
	public String getOrderSubTitle() {
		return orderSubTitle;
	}
	public void setOrderSubTitle(String orderSubTitle) {
		this.orderSubTitle = orderSubTitle;
	}
	public String getOrderPrice() {
		return orderPrice;
	}
	public void setOrderPrice(String orderPrice) {
		this.orderPrice = orderPrice;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	private	String	orderPrice;
	private	String	orderTime;
}
