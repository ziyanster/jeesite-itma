/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.mis.service.SmartOrderService;
import com.thinkgem.jeesite.modules.mis.service.SmartTaskService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 运维任务录入Controller
 * @author 黄尚弟
 * @version 2015-10-12
 */
@Controller
@RequestMapping(value = "${adminPath}/mis/smartTask")
public class SmartTaskController extends BaseController {

	@Autowired
	private SmartTaskService smartTaskService;
	@Autowired
	private SmartOrderService smartOrderService;
	
	@ModelAttribute
	public SmartTask get(@RequestParam(required=false) String id) {
		SmartTask entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smartTaskService.get(id);
		}
		if (entity == null){
			entity = new SmartTask();
		}
		return entity;
	}
	
	@RequiresPermissions("mis:smartTask:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmartTask smartTask, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartTask.setStatus("0");//编辑状态
		Page<SmartTask> page = smartTaskService.findPage(new Page<SmartTask>(request, response), smartTask); 
		model.addAttribute("page", page);
		return "modules/mis/smartIncomeTaskList";
	}
	
	@RequiresPermissions("mis:smartTask:view")
	@RequestMapping(value = "readylist")
	public String readyList(SmartTask smartTask, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartTask.setStatus("0");//编辑状态
		Page<SmartTask> page = smartTaskService.findPage(new Page<SmartTask>(request, response), smartTask); 
		model.addAttribute("page", page);
		return "modules/mis/smartSetTaskList";
	}	
	
	@RequiresPermissions("mis:smartTask:view")
	@RequestMapping(value = "checkinlist")
	public String checkinList(SmartTask smartTask, HttpServletRequest request, HttpServletResponse response, Model model) {
		smartTask.setStatus("1");//编辑状态
		smartTask.setSendtype("1");//抢单状态
		Page<SmartTask> page = smartTaskService.findPage(new Page<SmartTask>(request, response), smartTask); 
		model.addAttribute("page", page);
		return "modules/mis/smartCheckinTaskList";
	}	
	
	@RequiresPermissions("mis:smartTask:view")
	@RequestMapping(value = "form")
	public String form(SmartTask smartTask, Model model) {
		model.addAttribute("smartTask", smartTask);
		return "modules/mis/smartIncomeTaskForm";
	}

	@RequiresPermissions("mis:smartTask:view")
	@RequestMapping(value = "readyForm")
	public String readyForm(SmartTask smartTask, Model model) {
		model.addAttribute("smartTask", smartTask);
		return "modules/mis/smartSetTaskForm";
	}	
	
	@RequiresPermissions("mis:smartTask:view")
	@RequestMapping(value = "checkinForm")
	public String checkinForm(SmartTask smartTask, Model model) {
		model.addAttribute("smartTask", smartTask);
		return "modules/mis/smartCheckinTaskForm";
	}
	
	@RequiresPermissions("mis:smartTask:edit")
	@RequestMapping(value = "save")
	public String save(SmartTask smartTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartTask)){
			return form(smartTask, model);
		}
		smartTask.setStatus("0");//编辑状态
		smartTaskService.save(smartTask);
		addMessage(redirectAttributes, "保存运维任务录入成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartTask/?repage";
	}
	
	@RequiresPermissions("mis:smartTask:edit")
	@RequestMapping(value = "readySave")
	public String readySave(SmartTask smartTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartTask)){
			return readyForm(smartTask, model);
		}
		smartTask.setStatus("1");//预处理状态
		smartTaskService.save(smartTask);
		addMessage(redirectAttributes, "保存运维任务预处理成功");
		String custName=DictUtils.getCustomerName(smartTask.getCustomerno(), "");
		String custArea=DictUtils.getCustomerArea(smartTask.getCustomerno(), "");
		String weixinUser="";	
		String sendType=smartTask.getSendtype();
		if(sendType.equals("0")){//指派任务
			String weixinUserid=smartTask.getAssignuser().getId();		
			weixinUser=UserUtils.get(weixinUserid).getEmail();
			
			SmartOrder smartOrder=new SmartOrder();
			//SmartCustomer smartCustomer=new SmartCustomer();
			
			smartOrder.setServicedocno(IdGen.uuid());
			smartOrder.setCustomerno(smartTask.getCustomerno());
			smartOrder.setTaskno(smartTask.getId());
			smartOrder.setStatus("0");//已受理状态
			smartOrder.setIsassigned(smartTask.getSendtype());
			smartOrder.setAssignedno(smartTask.getAssignuser());
			smartOrder.setIntegral(smartTask.getIntegral());
			smartOrder.setServicetype(smartTask.getServicetype());
			smartOrder.setRecord(smartTask.getRecord());
			smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
			smartOrderService.save(smartOrder);
			addMessage(redirectAttributes, "保存指派工单管理成功");
			//return "redirect:"+Global.getAdminPath()+"/mis/smartOrder/?repage";			
		}
		DictUtils.postWeixinTask(smartTask.getId(),sendType,weixinUser,custName,smartTask.getRecord(),smartTask.getIntegral());
		addMessage(redirectAttributes, "提交微信平台发布任务成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartTask/readylist?repage";
		//return "modules/mis/smartSetTaskList";//
	}
	
	
	@RequiresPermissions("mis:smartTask:edit")
	@RequestMapping(value = "delete")
	public String delete(SmartTask smartTask, RedirectAttributes redirectAttributes) {
		smartTaskService.delete(smartTask);
		addMessage(redirectAttributes, "删除运维任务录入成功");
		return "redirect:"+Global.getAdminPath()+"/mis/smartTask/?repage";
	}

}