/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.mis.entity.HjHtSwhtbh;
import com.thinkgem.jeesite.modules.mis.dao.HjHtSwhtbhDao;

/**
 * 销售任务Service
 * @author 黄尚弟
 * @version 2015-09-27
 */
@Service
@Transactional(readOnly = true)
public class HjHtSwhtbhService extends CrudService<HjHtSwhtbhDao, HjHtSwhtbh> {

	public HjHtSwhtbh get(String id) {
		return super.get(id);
	}
	
	public List<HjHtSwhtbh> findList(HjHtSwhtbh hjHtSwhtbh) {
		return super.findList(hjHtSwhtbh);
	}
	
	public Page<HjHtSwhtbh> findPage(Page<HjHtSwhtbh> page, HjHtSwhtbh hjHtSwhtbh) {
		return super.findPage(page, hjHtSwhtbh);
	}
	
	@Transactional(readOnly = false)
	public void save(HjHtSwhtbh hjHtSwhtbh) {
		super.save(hjHtSwhtbh);
	}
	
	@Transactional(readOnly = false)
	public void delete(HjHtSwhtbh hjHtSwhtbh) {
		super.delete(hjHtSwhtbh);
	}
	
}