/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.mis.entity.HjMarkerterTask;

/**
 * 销售任务DAO接口
 * @author 黄尚弟
 * @version 2015-10-10
 */
@MyBatisDao
public interface HjMarkerterTaskDao extends CrudDao<HjMarkerterTask> {
	
}