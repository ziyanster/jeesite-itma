package com.thinkgem.jeesite.modules.mis.entity;

public class jsonOrder {
	private	String taskId;
	private	String taskHref;
	private	String orderNo;
	private	String orderPicSrc;
	private	String orderTitle;
	private	String taskcontent;
	private	String orderDate;
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getTaskHref() {
		return taskHref;
	}
	public void setTaskHref(String taskHref) {
		this.taskHref = taskHref;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderPicSrc() {
		return orderPicSrc;
	}
	public void setOrderPicSrc(String orderPicSrc) {
		this.orderPicSrc = orderPicSrc;
	}
	public String getOrderTitle() {
		return orderTitle;
	}
	public void setOrderTitle(String orderTitle) {
		this.orderTitle = orderTitle;
	}
	public String getTaskcontent() {
		return taskcontent;
	}
	public void setTaskcontent(String taskcontent) {
		this.taskcontent = taskcontent;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
}
