/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.mis.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 巡检合同Entity
 * @author ym
 * @version 2015-12-11
 */
public class MisContractdetail extends DataEntity<MisContractdetail> {
	
	private static final long serialVersionUID = 1L;
	private MisContract misContract;		// 合同编号 父类
	private String model;		// 机型
	private String productdes;		// 产品描述
	private String qty;		// 数量
	private String servicetime;		// 服务时限
	private String serialno;		// 序列号
	private String useful;		// 用途
	
	public MisContractdetail() {
		super();
	}

	public MisContractdetail(String id){
		super(id);
	}

	public MisContractdetail(MisContract misContract){
		this.misContract = misContract;
	}

	@Length(min=0, max=64, message="合同编号长度必须介于 0 和 64 之间")
	public MisContract getMisContract() {
		return misContract;
	}

	public void setMisContract(MisContract misContract) {
		this.misContract = misContract;
	}
	
	@Length(min=0, max=100, message="机型长度必须介于 0 和 100 之间")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	@Length(min=0, max=200, message="产品描述长度必须介于 0 和 200 之间")
	public String getProductdes() {
		return productdes;
	}

	public void setProductdes(String productdes) {
		this.productdes = productdes;
	}
	
	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
	
	@Length(min=0, max=100, message="服务时限长度必须介于 0 和 100 之间")
	public String getServicetime() {
		return servicetime;
	}

	public void setServicetime(String servicetime) {
		this.servicetime = servicetime;
	}
	
	@Length(min=0, max=100, message="序列号长度必须介于 0 和 100 之间")
	public String getSerialno() {
		return serialno;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	
	@Length(min=0, max=150, message="用途长度必须介于 0 和 150 之间")
	public String getUseful() {
		return useful;
	}

	public void setUseful(String useful) {
		this.useful = useful;
	}
	
}