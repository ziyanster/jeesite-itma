﻿/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.oa.service;

import java.util.List;
import java.util.Map;


import org.activiti.engine.TaskService;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.oa.entity.OaTempTask;
import com.thinkgem.jeesite.modules.oa.entity.TestAudit;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.oa.dao.OaTempTaskDao;
import com.thinkgem.jeesite.modules.oa.dao.TestAuditDao;

/**
 * 临时审批任务Service
 * @author 黄尚弟
 * @version 2015-11-22
 */
@Service
@Transactional(readOnly = true)
public class OaTempTaskService extends CrudService<OaTempTaskDao, OaTempTask> {
	@Autowired
	private ActTaskService actTaskService;
	@Autowired
	private TaskService taskService;	
	@Autowired
	private SystemService systemService;
	public OaTempTask get(String id) {
		return super.get(id);
	}
	
	public OaTempTask getByProcInsId(String procInsId) {
		return dao.getByProcInsId(procInsId);
	}
	
	public List<OaTempTask> findList(OaTempTask oaTempTask) {
		return super.findList(oaTempTask);
	}
	
	public Page<OaTempTask> findPage(Page<OaTempTask> page, OaTempTask oaTempTask) {
		return super.findPage(page, oaTempTask);
	}
	/*
	@Transactional(readOnly = false)
	public void save(OaTempTask oaTempTask) {
		super.save(oaTempTask);
	}
	*/
	@Transactional(readOnly = false)
	public void delete(OaTempTask oaTempTask) {
		super.delete(oaTempTask);
	}
	
	/**
	 * 审核新增或编辑
	 */
	@Transactional(readOnly = false)
	public void save(OaTempTask oaTempTask) {
		
		// 申请发起
		if (StringUtils.isBlank(oaTempTask.getId())){
			oaTempTask.preInsert();
			dao.insert(oaTempTask);
			
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("approval", systemService.getUser(oaTempTask.getLeaderId().getId()).getLoginName());
			// 启动流程

			String procInsId=actTaskService.startProcess(ActUtils.PD_TEMP_AUDIT[0], ActUtils.PD_TEMP_AUDIT[1], oaTempTask.getId(), oaTempTask.getContent(),vars);
			oaTempTask.setProcInsId(procInsId);
			//提交微信平台发布应急申请
	      	//User applyUser=UserUtils.get(oaTempTask.getCreateBy().getId());
			//String userId=applyUser.getLoginName();
	      	User weixin_user=UserUtils.get(oaTempTask.getLeaderId().getId());
			String userId=weixin_user.getLoginName();			
			Act nullact=new Act();
			List<Act> actList = actTaskService.todoList(nullact,userId);
			for (Act act : actList) {
				if(act.getProcInsId().equals(oaTempTask.getProcInsId())){
					oaTempTask.setAct(act);
					break;
				}
			}

			DictUtils.postWeixinApply(oaTempTask);
		}
		
		// 重新编辑申请		
		else{
			oaTempTask.preUpdate();
			dao.update(oaTempTask);

			oaTempTask.getAct().setComment(("yes".equals(oaTempTask.getAct().getFlag())?"[重申] ":"[销毁] ")+oaTempTask.getAct().getComment());
			
			// 完成流程任务
			Map<String, Object> vars = Maps.newHashMap();
			vars.put("pass", "yes".equals(oaTempTask.getAct().getFlag())? "1" : "0");
			actTaskService.complete(oaTempTask.getAct().getTaskId(), oaTempTask.getAct().getProcInsId(), oaTempTask.getAct().getComment(), oaTempTask.getContent(), vars);
			if("1".equals(oaTempTask.getAct().getFlag())){//重申
				//提交微信平台发布应急申请
				DictUtils.postWeixinApply(oaTempTask);	
			}
		}
		

	}

	/**
	 * 审核审批保存
	 */
	@Transactional(readOnly = false)
	public void auditSave(OaTempTask oaTempTask) {
		
		// 设置意见
		oaTempTask.getAct().setComment(("yes".equals(oaTempTask.getAct().getFlag())?"[同意] ":"[驳回] ")+oaTempTask.getAct().getComment());
		
		oaTempTask.preUpdate();
		// 对不同环节的业务逻辑进行操作
		String taskDefKey = oaTempTask.getAct().getTaskDefKey();

		// 审核环节
		if ("audit".equals(taskDefKey)){
			oaTempTask.setLeaderText(oaTempTask.getAct().getComment());
			dao.updateLeadText(oaTempTask);
		}
		
		// 未知环节，直接返回
		else{
			return;
		}
		
		// 提交流程任务
		Map<String, Object> vars = Maps.newHashMap();
		vars.put("pass", "yes".equals(oaTempTask.getAct().getFlag())? "1" : "0");
		actTaskService.complete(oaTempTask.getAct().getTaskId(), oaTempTask.getAct().getProcInsId(), oaTempTask.getAct().getComment(), vars);
		OaTempTask ott=getByProcInsId(oaTempTask.getAct().getProcInsId());
		ott.setAct(oaTempTask.getAct());
		//提交微信平台发布应急审批
		DictUtils.postWeixinAudit(ott);
		
	}
}