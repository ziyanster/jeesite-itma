/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.oa.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.oa.entity.OaTempTask;
import com.thinkgem.jeesite.modules.oa.entity.TestAudit;

/**
 * 临时审批任务DAO接口
 * @author 黄尚弟
 * @version 2015-11-22
 */
@MyBatisDao
public interface OaTempTaskDao extends CrudDao<OaTempTask> {
	public OaTempTask getByProcInsId(String procInsId);
	
	public int updateInsId(OaTempTask oaTempTask);
	
	public int updateLeadText(OaTempTask oaTempTask);

}