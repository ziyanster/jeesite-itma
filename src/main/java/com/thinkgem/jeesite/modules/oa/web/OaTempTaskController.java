/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.oa.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.oa.entity.OaTempTask;
import com.thinkgem.jeesite.modules.oa.entity.TestAudit;
import com.thinkgem.jeesite.modules.oa.service.OaTempTaskService;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 临时审批任务Controller
 * @author 黄尚弟
 * @version 2015-11-22
 */
@Controller
@RequestMapping(value = "${adminPath}/oa/oaTempTask")
public class OaTempTaskController extends BaseController {

	@Autowired
	private OaTempTaskService oaTempTaskService;
	
	@ModelAttribute
	public OaTempTask get(@RequestParam(required=false) String id) {
		OaTempTask entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = oaTempTaskService.get(id);
		}
		if (entity == null){
			entity = new OaTempTask();
		}
		return entity;
	}
	
	
	@RequiresPermissions("oa:oaTempTask:view")
	@RequestMapping(value = {"list", ""})
	public String list(OaTempTask oaTempTask, HttpServletRequest request, HttpServletResponse response, Model model) {
//		User user = UserUtils.getUser();
//		if (!user.isAdmin()){
//			oaTempTask.setCreateBy(user);
//		}		
		Page<OaTempTask> page = oaTempTaskService.findPage(new Page<OaTempTask>(request, response), oaTempTask); 
		model.addAttribute("page", page);
		return "modules/oa/oaTempTaskList";
	}

	@RequiresPermissions("oa:oaTempTask:view")
	@RequestMapping(value = "form")
	public String form(OaTempTask oaTempTask, Model model) {
		
		String view = "oaTempTaskForm";
		
		// 查看审批申请单
		if (StringUtils.isNotBlank(oaTempTask.getId())){//.getAct().getProcInsId())){

			// 环节编号
			String taskDefKey = oaTempTask.getAct().getTaskDefKey();
			
			// 查看工单
			if(oaTempTask.getAct().isFinishTask()){
				view = "oaTempTaskView";
			}
			// 修改环节
			else if ("modify".equals(taskDefKey)){
				view = "oaTempTaskForm";
			}
			// 审核环节
			else if ("audit".equals(taskDefKey)){
				view = "oaTempTaskAudit";
			}
		}

		model.addAttribute("oaTempTask", oaTempTask);
		return "modules/oa/" + view;		
	}

	@RequiresPermissions("oa:oaTempTask:edit")
	@RequestMapping(value = "save")
	public String save(OaTempTask oaTempTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, oaTempTask)){
			return form(oaTempTask, model);
		}
		oaTempTaskService.save(oaTempTask);
		addMessage(redirectAttributes, "保存临时审批任务成功");
		
		return "redirect:" + adminPath + "/act/task/todo/";
	}

	/**
	 * 工单执行（完成任务）
	 * @param model
	 * @return
	 */
	@RequiresPermissions("oa:testAudit:edit")
	@RequestMapping(value = "saveAudit")
	public String saveAudit(OaTempTask oaTempTask, Model model) {
		if (StringUtils.isBlank(oaTempTask.getAct().getFlag())
				|| StringUtils.isBlank(oaTempTask.getAct().getComment())){
			addMessage(model, "请填写审核意见。");
			return form(oaTempTask, model);
		}
		oaTempTaskService.auditSave(oaTempTask);
		return "redirect:" + adminPath + "/act/task/todo/";
	}
	
	@RequiresPermissions("oa:oaTempTask:edit")
	@RequestMapping(value = "delete")
	public String delete(OaTempTask oaTempTask, RedirectAttributes redirectAttributes) {
		oaTempTaskService.delete(oaTempTask);
		addMessage(redirectAttributes, "删除临时审批任务成功");
		return "redirect:"+Global.getAdminPath()+"/oa/oaTempTask/?repage";
	}

}