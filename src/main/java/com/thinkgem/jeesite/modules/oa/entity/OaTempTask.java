/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.oa.entity;

import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.common.persistence.ActEntity;


/**
 * 临时审批任务Entity
 * @author 黄尚弟
 * @version 2015-11-22
 */
public class OaTempTask extends ActEntity<OaTempTask> {
	
	private static final long serialVersionUID = 1L;
	private String type;		// 申请类型
	private String misno;		// MIS编号
	private String content;		// 申请原因
	private User leaderId;		// 审批者
	private String leaderText;		// 审批结果
	private String image;		// 缩略图
	
	public OaTempTask() {
		super();
	}

	public OaTempTask(String id){
		super(id);
	}

	@Length(min=0, max=1, message="申请类型长度必须介于 0 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=64, message="MIS编号长度必须介于 0 和 64 之间")
	public String getMisno() {
		return misno;
	}

	public void setMisno(String misno) {
		this.misno = misno;
	}
	
	@Length(min=0, max=255, message="申请原因长度必须介于 0 和 255 之间")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public User getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(User leaderId) {
		this.leaderId = leaderId;
	}
	
	@Length(min=0, max=255, message="审批结果长度必须介于 0 和 255 之间")
	public String getLeaderText() {
		return leaderText;
	}

	public void setLeaderText(String leaderText) {
		this.leaderText = leaderText;
	}
	
	@Length(min=0, max=255, message="缩略图长度必须介于 0 和 255 之间")
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}