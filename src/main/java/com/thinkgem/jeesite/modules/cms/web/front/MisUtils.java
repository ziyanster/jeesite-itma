package com.thinkgem.jeesite.modules.cms.web.front;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinksoft.wechat.utils.HttpUtils;

public class MisUtils {

	// 日志记录
	private static Logger logger = LoggerFactory.getLogger(MisUtils.class);

	/**
	 * 发送起始短信
	 * 
	 * */
	public static String sendStartSms(SmartOrder so, SmartTask st,Date outDate) {
		try {
			String sCustName = DictUtils.getCustomer(st.getCustomerno())
					.getCustomername();
			sCustName = StringUtils.isBlank(sCustName) ? "" : sCustName;
			String record = StringUtils.isBlank(st.getRecord()) ? "" : st
					.getRecord();
			outDate = outDate == null ? new Date() : outDate;
			// 在受理的同时，向mis系统添加短信记录，保证报销流程的进行
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss");
			String url = DictUtils.getDictLabel("url", "sys_misSmsUrl", "");
			String pattern = ";;;";
			// 按网页格式编码url
			String content = "s%20" + URLEncoder.encode(sCustName, "UTF-8")
					+ "%20" + URLEncoder.encode(record, "UTF-8") + "%20"
					+ URLEncoder.encode("来自微信", "UTF-8");
			url = url + "?a=1&&gogo=" + so.getAssignedno().getMobile()
					+ pattern + content + pattern + sdf.format(outDate)
					+ pattern + "4" + pattern + st.getSendtype();
			HttpURLConnection conn = (HttpURLConnection) new URL(url)
					.openConnection();
			logger.debug("存短信的URL为：" + url);
			String s = HttpUtils.retrieveInput(conn);
			s = s.replace("\r\n", "").replace("\"", "").replace(" ", "");
			if (conn != null) {
				conn.disconnect();
			}
			if (!StringUtils.isBlank(s)) {
				int p = s.indexOf("value=");
				return s.substring(p + 6, p + 13);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		return "";
	}

	/**
	 * 短信派遣
	 * 
	 * */
	public static void assignSms(SmartOrder so, SmartTask st) {
		try {
			// 用来模拟短信派遣
			// http://192.168.43.17/mis/gzzl/gzzl_sms_pq_bf_save.jsp?eid=140033&smsid=短信编号&work_content=工作内容&stype=WH&pqh=客户号-日期WH001&eventid=null&ename=王飞宇
			String url = DictUtils.getDictList("sys_misSmsPqUrl").get(0)
					.getRemarks();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String pqh = so.getCustomerno() + "-"
					+ sdf.format(so.getResponsetime()) + st.getWorktype()
					+ "001";
			url = url
					+ "?eid="
					+ so.getAssignedno().getNo()
					+ "&smsid="
					+ so.getSmsIndex1()
					+ "&work_content="
					+ pqh
					+ "%20"
					+ URLEncoder.encode(st.getRecord(), "UTF-8")
					+ "%20"
					+ URLEncoder.encode(DictUtils.getDictLabel(
							st.getWorktype(), "mis_workType", ""), "UTF-8")
					+ "&stype=" + st.getWorktype() + "&pqh=" + pqh
					+ "&eventid=null&ename="
					+ URLEncoder.encode(so.getAssignedno().getName(), "UTF-8");

			HttpURLConnection conn = (HttpURLConnection) new URL(url)
					.openConnection();
			logger.debug("短信派遣的URL为：" + url);
			String s = HttpUtils.retrieveInput(conn);
			s = s.replace("\r\n", "").replace("\"", "").replace(" ", "");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 发送结束短信
	 * 
	 * */
	public static String sendEndSms(SmartOrder so, SmartTask st,
			String workContent) {
		User user = UserUtils.get(so.getAssignedno().getId());
		String sCustName = DictUtils.getCustomer(st.getCustomerno())
				.getCustomername();
		sCustName = StringUtils.isBlank(sCustName) ? "" : sCustName;
		// 在受理的同时，向mis系统添加短信记录，保证报销流程的进行
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss");
		String url = DictUtils.getDictLabel("url", "sys_misSmsUrl", "");
		String pattern = ";;;";
		HttpURLConnection conn = null;
		try {
			// 按网页格式编码url
			String content = "e%20" + URLEncoder.encode(sCustName, "UTF-8")
					+ "%20" + URLEncoder.encode(workContent, "UTF-8") + "%20"
					+ URLEncoder.encode("来自微信", "UTF-8");
			url = url + "?a=1&&gogo=" + user.getMobile() + pattern + content
					+ pattern + sdf.format(new Date()) + pattern + "4"
					+ pattern + st.getSendtype();
			conn = (HttpURLConnection) new URL(url).openConnection();
			logger.debug("存短信的URL为：" + url);
			String s = HttpUtils.retrieveInput(conn);

			// 发送之后的结果进行解析，找出短信编号
			s = s.replace("\r\n", "").replace("\"", "").replace(" ", "");
			int p = s.indexOf("value=");
			if (conn != null) {
				conn.disconnect();
			}
			return s.substring(p + 6, p + 13);

		} catch (IOException e) {
			logger.debug("存短信异常");
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 处理短信
	 * 
	 * */
	public static String resolveSms(SmartOrder so, SmartTask st,Date endTime,String workway,
			String workResult) {
		try {
			// 必须完成了开始和结束，才能进行
			if (!StringUtils.isBlank(so.getSmsIndex1())
					&& !StringUtils.isBlank(so.getSmsIndex2())) {
				User user = UserUtils.get(so.getAssignedno().getId());
				// 进行短信合并
				SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy-MM-dd");
				String smsCombineUrl = DictUtils
						.getDictList("sys_misWorkAssistInput_lin").get(0)
						.getRemarks();
				String os = System.getProperty("os.name");
				if (os.toLowerCase().startsWith("win")) {
					smsCombineUrl = DictUtils
							.getDictList("sys_misWorkAssistInput_win").get(0)
							.getRemarks();
				}
				String query0 = "?eId="
						+ user.getNo()
						+ "&eName="
						+ URLEncoder.encode(user.getName(), "UTF-8")
						+ "&customerNo="
						+ st.getCustomerno()
						+ "&customerName="
						+ URLEncoder.encode(
								DictUtils.getCustomer(st.getCustomerno())
										.getCustomername(), "UTF-8")
						+ "&workContent=s%20e%20"
						+ URLEncoder.encode(st.getRecord().replace("\n", "")
								+ " " + so.getTaskcontent(), "UTF-8")
						+ "&smsindex1="
						+ so.getSmsIndex1()
						+ "&workType=" + st.getWorktype()
						+ "&smsindex2="
						+ so.getSmsIndex2()
						+ "&dxlx=%E5%8F%8A%E6%97%B6%E7%9F%AD%E4%BF%A1&gzfs="+ URLEncoder.encode(workway, "UTF-8")
						+ "&hdate1=" + sdf0.format(so.getResponsetime())
						+ "&hdate2=" + sdf0.format(endTime) + "&end_state=0";
				String inUrl0 = smsCombineUrl + query0;

				HttpURLConnection conn = (HttpURLConnection) new URL(inUrl0)
						.openConnection();

				String pqh = HttpUtils.retrieveInput(conn).trim()
						.replace("\r\n", "").replace("\"", "").replace(" ", "");
				if (!StringUtils.isBlank(pqh)) {
					int p = pqh.indexOf("pqh=");
					pqh = pqh.substring(p + 4);
				}
				logger.debug("mis短信归并存盘的执行地址：" + inUrl0 + "，派遣号为：" + pqh);
				if (conn != null) {
					conn.disconnect();
				}
				return pqh;
			} else {
				return "";
			}
		} catch (Exception e) {
			logger.debug("mis短信归并存盘异常");
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 生成报告
	 * 
	 * */
	public static void generateReport(SmartOrder so, SmartTask st,
			String dealProg, String workResult, String pqh, String arriveTime,
			String backTime, HttpServletRequest request) {
		try {
			// 必须完成了开始和结束，才能进行
			if (!StringUtils.isBlank(so.getSmsIndex1())
					&& !StringUtils.isBlank(so.getSmsIndex2())) {
				User user = UserUtils.get(so.getAssignedno().getId());
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd%20HH:mm");
				backTime = StringUtils.isBlank(backTime) ? sdf1.format(new Date()) : backTime;
						
				if (!"XJ".equals(st.getWorktype())) {
					// 进行工作报告的填写
					String workReportUrl = DictUtils
							.getDictList("sys_misWorkReportUrl").get(0)
							.getRemarks();
					
					// http://192.168.43.17/mis/xzsw/wxWorkReportInput.jsp
					// ?reporterId=030112&cutomerNo=D00621&serviceType=%E4%BF%9D%E4%BF%AE%E5%90%88%E5%90%8C&workContent=%E7%B3%BB%E7%BB%9F%E6%95%85%E9%9A%9C&problemDes=adbad24243543
					// &dealProg=sdsdsswewe&personName=%E7%8E%8B%E9%A3%9E%E5%AE%87&faultType=1&workResult=1
					// &hjsj=2016-01-13%2011:23&xysj=2016-01-13%2011:23&ddsj=2016-01-13%2011:23&wcsj=2016-01-13%2011:23
					String query = "?reporterId="
							+ user.getNo()
							+ "&cutomerNo="
							+ st.getCustomerno()
							+ "&serviceType="
							+ URLEncoder.encode(
									DictUtils.getDictLabel(so.getServicetype(),
											"mis_servicetype", ""), "UTF-8");
					query += "&workContent="
							+ URLEncoder.encode(so.getTaskcontent(), "UTF-8")
							+ "&problemDes="
							+ URLEncoder.encode(so.getThistimejob(), "UTF-8");
					query += "&dealProg="
							+ URLEncoder.encode(dealProg, "UTF-8")
							+ "&personName="
							+ URLEncoder.encode(
									DictUtils.getCustomer(st.getCustomerno())
											.getPersonname(), "UTF-8");
					query += "&faultType=" + st.getFaulttype() + "&workResult="
							+ workResult + "&pqh=" + pqh + "&workType="+ st.getWorktype();
					query += "&hjsj=" + sdf1.format(st.getIncomingtime())
							+ "&xysj=" + sdf1.format(so.getResponsetime())
							+ "&ddsj=" + arriveTime.replace(" ", "%20")
							+ "&fhsj=" + (backTime + "").replace(" ", "%20")
							+ "&wcsj=" + sdf1.format(new Date());
					;
					String inUrl = workReportUrl + query;

					HttpURLConnection conn = (HttpURLConnection) new URL(inUrl)
							.openConnection();
					HttpUtils.retrieveInput(conn);
					logger.debug("mis工作报告存盘的执行地址：" + inUrl);
					if (conn != null) {
						conn.disconnect();
					}
				} else {
					// 巡检报告
					String device = request.getParameter("device");
					String errDevice = request.getParameter("errDevice");
					String qtsb = request.getParameter("qtsb");
					
					String xjReportUrl = DictUtils
							.getDictList("sys_misXjReportUrl").get(0)
							.getRemarks();
					// http://192.168.43.17/mis/xzsw/wxXjReportInput.jsp
					// ?reporterId=030112&cutomerNo=D00621&serviceType=%E4%BF%9D%E4%BF%AE%E5%90%88%E5%90%8C&workContent=%E7%B3%BB%E7%BB%9F%E6%95%85%E9%9A%9C
					// &personName=%E7%8E%8B%E9%A3%9E%E5%AE%87&workResult=1
					// &cfsj=2016-01-13%2011:23&ddsj=2016-01-13%2011:23&wcsj=2016-01-13%2011:23&fhsj=2016-01-13%2011:23
					// &qtsb=2xcsdfe&device=02$111111$sdfs;01$22222222$sdfs&errDevice=
					String query = "?reporterId="
							+ user.getNo()
							+ "&cutomerNo="
							+ st.getCustomerno()
							+ "&serviceType="
							+ URLEncoder.encode(
									DictUtils.getDictLabel(so.getServicetype(),
											"mis_servicetype", ""), "UTF-8");
					query += "&workContent="
							+ URLEncoder.encode(so.getTaskcontent() + "-" + so.getThistimejob() + "-" + dealProg, "UTF-8");
					query += "&personName="
							+ URLEncoder.encode(
									DictUtils.getCustomer(st.getCustomerno())
											.getPersonname(), "UTF-8")+ "&workType="+ st.getWorktype();
					query += "&workResult=" + workResult + "&pqh=" + pqh;
					query += "&cfsj=" + sdf1.format(so.getResponsetime())
							+ "&ddsj=" + arriveTime.replace(" ", "%20")
							+ "&fhsj=" + (backTime + "").replace(" ", "%20")
							+ "&wcsj=" + sdf1.format(new Date());
					query += "&qtsb=" + URLEncoder.encode(qtsb, "UTF-8") + "&device=" + URLEncoder.encode(device, "UTF-8")
							+ "&errDevice=" + URLEncoder.encode(errDevice, "UTF-8");
					;
					String inUrl = xjReportUrl + query;

					HttpURLConnection conn = (HttpURLConnection) new URL(inUrl)
							.openConnection();
					HttpUtils.retrieveInput(conn);
					logger.debug("mis工作报告存盘的执行地址：" + inUrl);
					if (conn != null) {
						conn.disconnect();
					}
					
				}
			}
		} catch (Exception e) {
			logger.debug("保存工作报告异常");
			e.printStackTrace();
		}
	}

	/**
	 * 查询巡检报告设备
	 * 
	 * */
	public static String searchXjReports(String custNo){
		String url = DictUtils.getDictList("sys_misXjRepList_lin").get(0).getRemarks();
		String os = System.getProperty("os.name");
		if(os.toLowerCase().startsWith("win")){
			url = DictUtils.getDictList("sys_misXjRepList_win").get(0).getRemarks();
		}

		HttpURLConnection conn;
		try {
			//http://192.168.43.17/mis/xzsw/wxGetXjReports-lin.jsp?khdm=D00132
			url = url + "?khdm="+custNo;
			conn = (HttpURLConnection) new URL(url)
					.openConnection();
			logger.debug("查找相关的巡检报告的URL为："+url);
			String s = HttpUtils.retrieveInput(conn);
			return s.replace("\r\n", "").replace("\"", "").replace(" ", "");
		}
		catch(Exception e){
			e.printStackTrace();
			return "";
		}
	}
}
