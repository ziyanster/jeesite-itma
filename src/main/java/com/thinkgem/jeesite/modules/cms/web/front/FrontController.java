/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.cms.web.front;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.mapper.JsonMapper;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.servlet.ValidateCodeServlet;
import com.thinkgem.jeesite.common.utils.Encodes;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.cms.entity.Article;
import com.thinkgem.jeesite.modules.cms.entity.Category;
import com.thinkgem.jeesite.modules.cms.entity.Comment;
import com.thinkgem.jeesite.modules.cms.entity.Link;
import com.thinkgem.jeesite.modules.cms.entity.Site;
import com.thinkgem.jeesite.modules.cms.service.ArticleDataService;
import com.thinkgem.jeesite.modules.cms.service.ArticleService;
import com.thinkgem.jeesite.modules.cms.service.CategoryService;
import com.thinkgem.jeesite.modules.cms.service.CommentService;
import com.thinkgem.jeesite.modules.cms.service.LinkService;
import com.thinkgem.jeesite.modules.cms.service.SiteService;
import com.thinkgem.jeesite.modules.cms.utils.CmsUtils;
import com.thinkgem.jeesite.modules.mis.dao.SmartOrderDao;
import com.thinkgem.jeesite.modules.mis.entity.SmartApproval;
import com.thinkgem.jeesite.modules.mis.entity.SmartAttachments;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.mis.entity.jsonReturn;
import com.thinkgem.jeesite.modules.mis.service.SmartApprovalService;
import com.thinkgem.jeesite.modules.mis.service.SmartAttachmentsService;
import com.thinkgem.jeesite.modules.mis.service.SmartCustomerService;
import com.thinkgem.jeesite.modules.mis.service.SmartOrderService;
import com.thinkgem.jeesite.modules.mis.service.SmartTaskService;
import com.thinkgem.jeesite.modules.oa.entity.OaTempTask;
import com.thinkgem.jeesite.modules.oa.service.OaTempTaskService;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.OfficeService;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinksoft.wechat.utils.HttpUtils;

/**
 * 网站Controller
 * @author ThinkGem
 * @version 2013-5-29
 */
@Controller
@RequestMapping(value = "${frontPath}")
public class FrontController extends BaseController{
	
	@Autowired
	private ArticleService articleService;
	@Autowired
	private ArticleDataService articleDataService;
	@Autowired
	private LinkService linkService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private SiteService siteService;
	private static SmartOrderDao  soDao = SpringContextHolder.getBean(SmartOrderDao.class);	
	/**
	 * 网站首页
	 */
	@RequestMapping
	public String index(Model model) {
		Site site = CmsUtils.getSite(Site.defaultSiteId());
		model.addAttribute("site", site);
		model.addAttribute("isIndex", true);
		return "modules/cms/front/themes/"+site.getTheme()+"/frontIndex";
	}
	
	/**
	 * 网站首页
	 */
	@RequestMapping(value = "index-{siteId}${urlSuffix}")
	public String index(@PathVariable String siteId, Model model) {
		if (siteId.equals("1")){
			return "redirect:"+Global.getFrontPath();
		}
		Site site = CmsUtils.getSite(siteId);
		// 子站有独立页面，则显示独立页面
		if (StringUtils.isNotBlank(site.getCustomIndexView())){
			model.addAttribute("site", site);
			model.addAttribute("isIndex", true);
			return "modules/cms/front/themes/"+site.getTheme()+"/frontIndex"+site.getCustomIndexView();
		}
		// 否则显示子站第一个栏目
		List<Category> mainNavList = CmsUtils.getMainNavList(siteId);
		if (mainNavList.size() > 0){
			String firstCategoryId = CmsUtils.getMainNavList(siteId).get(0).getId();
			return "redirect:"+Global.getFrontPath()+"/list-"+firstCategoryId+Global.getUrlSuffix();
		}else{
			model.addAttribute("site", site);
			return "modules/cms/front/themes/"+site.getTheme()+"/frontListCategory";
		}
	}
	
	/**
	 * 内容列表
	 */
	@RequestMapping(value = "list-{categoryId}${urlSuffix}")
	public String list(@PathVariable String categoryId, @RequestParam(required=false, defaultValue="1") Integer pageNo,
			@RequestParam(required=false, defaultValue="15") Integer pageSize, Model model) {
		Category category = categoryService.get(categoryId);
		if (category==null){
			Site site = CmsUtils.getSite(Site.defaultSiteId());
			model.addAttribute("site", site);
			return "error/404";
		}
		Site site = siteService.get(category.getSite().getId());
		model.addAttribute("site", site);
		// 2：简介类栏目，栏目第一条内容
		if("2".equals(category.getShowModes()) && "article".equals(category.getModule())){
			// 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
			List<Category> categoryList = Lists.newArrayList();
			if (category.getParent().getId().equals("1")){
				categoryList.add(category);
			}else{
				categoryList = categoryService.findByParentId(category.getParent().getId(), category.getSite().getId());
			}
			model.addAttribute("category", category);
			model.addAttribute("categoryList", categoryList);
			// 获取文章内容
			Page<Article> page = new Page<Article>(1, 1, -1);
			Article article = new Article(category);
			page = articleService.findPage(page, article, false);
			if (page.getList().size()>0){
				article = page.getList().get(0);
				article.setArticleData(articleDataService.get(article.getId()));
				articleService.updateHitsAddOne(article.getId());
			}
			model.addAttribute("article", article);
            CmsUtils.addViewConfigAttribute(model, category);
            CmsUtils.addViewConfigAttribute(model, article.getViewConfig());
			return "modules/cms/front/themes/"+site.getTheme()+"/"+getTpl(article);
		}else{
			List<Category> categoryList = categoryService.findByParentId(category.getId(), category.getSite().getId());
			// 展现方式为1 、无子栏目或公共模型，显示栏目内容列表
			if("1".equals(category.getShowModes())||categoryList.size()==0){
				// 有子栏目并展现方式为1，则获取第一个子栏目；无子栏目，则获取同级分类列表。
				if(categoryList.size()>0){
					category = categoryList.get(0);
				}else{
					// 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
					if (category.getParent().getId().equals("1")){
						categoryList.add(category);
					}else{
						categoryList = categoryService.findByParentId(category.getParent().getId(), category.getSite().getId());
					}
				}
				model.addAttribute("category", category);
				model.addAttribute("categoryList", categoryList);
				// 获取内容列表
				if ("article".equals(category.getModule())){
					Page<Article> page = new Page<Article>(pageNo, pageSize);
					//System.out.println(page.getPageNo());
					page = articleService.findPage(page, new Article(category), false);
					model.addAttribute("page", page);
					// 如果第一个子栏目为简介类栏目，则获取该栏目第一篇文章
					if ("2".equals(category.getShowModes())){
						Article article = new Article(category);
						if (page.getList().size()>0){
							article = page.getList().get(0);
							article.setArticleData(articleDataService.get(article.getId()));
							articleService.updateHitsAddOne(article.getId());
						}
						model.addAttribute("article", article);
			            CmsUtils.addViewConfigAttribute(model, category);
			            CmsUtils.addViewConfigAttribute(model, article.getViewConfig());
						return "modules/cms/front/themes/"+site.getTheme()+"/"+getTpl(article);
					}
				}else if ("link".equals(category.getModule())){
					Page<Link> page = new Page<Link>(1, -1);
					page = linkService.findPage(page, new Link(category), false);
					model.addAttribute("page", page);
				}
				String view = "/frontList";
				if (StringUtils.isNotBlank(category.getCustomListView())){
					view = "/"+category.getCustomListView();
				}
	            CmsUtils.addViewConfigAttribute(model, category);
                site =siteService.get(category.getSite().getId());
                //System.out.println("else 栏目第一条内容 _2 :"+category.getSite().getTheme()+","+site.getTheme());
				return "modules/cms/front/themes/"+siteService.get(category.getSite().getId()).getTheme()+view;
				//return "modules/cms/front/themes/"+category.getSite().getTheme()+view;
			}
			// 有子栏目：显示子栏目列表
			else{
				model.addAttribute("category", category);
				model.addAttribute("categoryList", categoryList);
				String view = "/frontListCategory";
				if (StringUtils.isNotBlank(category.getCustomListView())){
					view = "/"+category.getCustomListView();
				}
	            CmsUtils.addViewConfigAttribute(model, category);
				return "modules/cms/front/themes/"+site.getTheme()+view;
			}
		}
	}

	/**
	 * 内容列表（通过url自定义视图）
	 */
	@RequestMapping(value = "listc-{categoryId}-{customView}${urlSuffix}")
	public String listCustom(@PathVariable String categoryId, @PathVariable String customView, @RequestParam(required=false, defaultValue="1") Integer pageNo,
			@RequestParam(required=false, defaultValue="15") Integer pageSize, Model model) {
		Category category = categoryService.get(categoryId);
		if (category==null){
			Site site = CmsUtils.getSite(Site.defaultSiteId());
			model.addAttribute("site", site);
			return "error/404";
		}
		Site site = siteService.get(category.getSite().getId());
		model.addAttribute("site", site);
		List<Category> categoryList = categoryService.findByParentId(category.getId(), category.getSite().getId());
		model.addAttribute("category", category);
		model.addAttribute("categoryList", categoryList);
        CmsUtils.addViewConfigAttribute(model, category);
		return "modules/cms/front/themes/"+site.getTheme()+"/frontListCategory"+customView;
	}

	/**
	 * 显示内容
	 */
	@RequestMapping(value = "view-{categoryId}-{contentId}${urlSuffix}")
	public String view(@PathVariable String categoryId, @PathVariable String contentId, Model model) {
		Category category = categoryService.get(categoryId);
		if (category==null){
			Site site = CmsUtils.getSite(Site.defaultSiteId());
			model.addAttribute("site", site);
			return "error/404";
		}
		model.addAttribute("site", category.getSite());
		if ("article".equals(category.getModule())){
			// 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
			List<Category> categoryList = Lists.newArrayList();
			if (category.getParent().getId().equals("1")){
				categoryList.add(category);
			}else{
				categoryList = categoryService.findByParentId(category.getParent().getId(), category.getSite().getId());
			}
			// 获取文章内容
			Article article = articleService.get(contentId);
			if (article==null || !Article.DEL_FLAG_NORMAL.equals(article.getDelFlag())){
				return "error/404";
			}
			// 文章阅读次数+1
			articleService.updateHitsAddOne(contentId);
			// 获取推荐文章列表
			List<Object[]> relationList = articleService.findByIds(articleDataService.get(article.getId()).getRelation());
			// 将数据传递到视图
			model.addAttribute("category", categoryService.get(article.getCategory().getId()));
			model.addAttribute("categoryList", categoryList);
			article.setArticleData(articleDataService.get(article.getId()));
			model.addAttribute("article", article);
			model.addAttribute("relationList", relationList); 
            CmsUtils.addViewConfigAttribute(model, article.getCategory());
            CmsUtils.addViewConfigAttribute(model, article.getViewConfig());
            Site site = siteService.get(category.getSite().getId());
            model.addAttribute("site", site);
//			return "modules/cms/front/themes/"+category.getSite().getTheme()+"/"+getTpl(article);
            return "modules/cms/front/themes/"+site.getTheme()+"/"+getTpl(article);
		}
		return "error/404";
	}
	
	/**
	 * 内容评论
	 */
	@RequestMapping(value = "comment", method=RequestMethod.GET)
	public String comment(String theme, Comment comment, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Comment> page = new Page<Comment>(request, response);
		Comment c = new Comment();
		c.setCategory(comment.getCategory());
		c.setContentId(comment.getContentId());
		c.setDelFlag(Comment.DEL_FLAG_NORMAL);
		page = commentService.findPage(page, c);
		model.addAttribute("page", page);
		model.addAttribute("comment", comment);
		return "modules/cms/front/themes/"+theme+"/frontComment";
	}
	
	/**
	 * 内容评论保存
	 */
	@ResponseBody
	@RequestMapping(value = "comment", method=RequestMethod.POST)
	public String commentSave(Comment comment, String validateCode,@RequestParam(required=false) String replyId, HttpServletRequest request) {
		if (StringUtils.isNotBlank(validateCode)){
			if (ValidateCodeServlet.validate(request, validateCode)){
				if (StringUtils.isNotBlank(replyId)){
					Comment replyComment = commentService.get(replyId);
					if (replyComment != null){
						comment.setContent("<div class=\"reply\">"+replyComment.getName()+":<br/>"
								+replyComment.getContent()+"</div>"+comment.getContent());
					}
				}
				comment.setIp(request.getRemoteAddr());
				comment.setCreateDate(new Date());
				comment.setDelFlag(Comment.DEL_FLAG_AUDIT);
				commentService.save(comment);
				return "{result:1, message:'提交成功。'}";
			}else{
				return "{result:2, message:'验证码不正确。'}";
			}
		}else{
			return "{result:2, message:'验证码不能为空。'}";
		}
	}
	
	/**
	 * 站点地图
	 */
	@RequestMapping(value = "map-{siteId}${urlSuffix}")
	public String map(@PathVariable String siteId, Model model) {
		Site site = CmsUtils.getSite(siteId!=null?siteId:Site.defaultSiteId());
		model.addAttribute("site", site);
		return "modules/cms/front/themes/"+site.getTheme()+"/frontMap";
	}

    private String getTpl(Article article){
        if(StringUtils.isBlank(article.getCustomContentView())){
            String view = null;
            Category c = article.getCategory();
            boolean goon = true;
            do{
                if(StringUtils.isNotBlank(c.getCustomContentView())){
                    view = c.getCustomContentView();
                    goon = false;
                }else if(c.getParent() == null || c.getParent().isRoot()){
                    goon = false;
                }else{
                    c = c.getParent();
                }
            }while(goon);
            return StringUtils.isBlank(view) ? Article.DEFAULT_TEMPLATE : view;
        }else{
            return article.getCustomContentView();
        }
    }
	
	@RequestMapping(value = "mobileTaskForm")
	public String mobileTaskForm(SmartTask smartTask, Model model) {
		smartTask.setIncomingway("0");//默认值：电话
		smartTask.setIncomingtime(new Date());//默认值：当前时间
		model.addAttribute("smartTask", smartTask);
		return "modules/cms/front/themes/weixin/mobileIncomeTaskForm";		
	}    
	@Autowired
	private SmartTaskService smartTaskService;
	@Autowired
	private SmartOrderService smartOrderService;
	
	@RequestMapping(value = "mobileTaskSave")
	public String mobileTaskSave(SmartTask smartTask,Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smartTask)){
			return mobileTaskForm(smartTask, model);
		}
		String sEmail=smartTask.getRemarks();
		User assignuser=UserUtils.getByEmail(sEmail);
		smartTask.setRemarks("");
		smartTask.setAssignuser(assignuser);
		smartTask.setStatus("2");//预处理状态
		smartTask.setSendtype("0");//指派任务
		smartTask.setIntegral(100);
		smartTaskService.save(smartTask);
		addMessage(redirectAttributes, "保存运维任务预处理成功");
		String custName=DictUtils.getCustomerName(smartTask.getCustomerno(), "");
		String weixinUser="";	
		String sendType=smartTask.getSendtype();
		String custArea=DictUtils.getCustomerArea(smartTask.getCustomerno(), "");
			String weixinUserid=smartTask.getAssignuser().getId();		
			weixinUser=UserUtils.get(weixinUserid).getEmail();
			SmartOrder smartOrder=new SmartOrder();
			
			smartOrder.setServicedocno(IdGen.uuid());
			smartOrder.setCustomerno(smartTask.getCustomerno());
			smartOrder.setTaskno(smartTask.getId());
			smartOrder.setStatus("1");//已受理状态
			smartOrder.setIsassigned(smartTask.getSendtype());
			smartOrder.setAssignedno(smartTask.getAssignuser());
			smartOrder.setIntegral(smartTask.getIntegral());
			smartOrder.setServicetype(smartTask.getServicetype());
			smartOrder.setRecord(smartTask.getRecord());
			smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
			smartOrderService.save(smartOrder);
			
			custName = StringUtils.isBlank(custName) ? "" : custName;
			String record = StringUtils.isBlank(smartTask.getRecord()) ? ""
					: smartTask.getRecord();
			if (!StringUtils.isBlank(smartOrder.getAssignedno().getMobile())) {
				// 在受理的同时，向mis系统添加短信记录，保证报销流程的进行
				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd%20HH:mm:ss");
				String url = DictUtils.getDictLabel("url", "sys_misSmsUrl",
						"");
				String pattern = ";;;";
				HttpURLConnection conn;
				try {
					// 按网页格式编码url
					String content = "s%20"
							+ URLEncoder.encode(custName, "UTF-8") + "%20"
							+ URLEncoder.encode(record, "UTF-8")+ "%20" + URLEncoder.encode("应急任务", "UTF-8");
					url = url + "?a=1&&gogo="
							+ smartOrder.getAssignedno().getMobile() + pattern
							+ content + pattern + sdf.format(new Date())
							+ pattern + "4" + pattern
							+ sendType;
					conn = (HttpURLConnection) new URL(url)
							.openConnection();
					String s = HttpUtils.retrieveInput(conn);
					if (!StringUtils.isBlank(s)) {
						conn.disconnect();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				logger.error("用户没有维护手机号，未向mis进行短信报备");
			}
			
			
			addMessage(redirectAttributes, "保存指派工单管理成功");
			//return "redirect:"+Global.getAdminPath()+"/mis/smartOrder/?repage";			

		//DictUtils.postWeixinTask(smartTask.getId(),sendType,weixinUser,custName,smartTask.getRecord(),smartTask.getIntegral());
		//addMessage(redirectAttributes, "提交微信平台发布任务成功");
		String sRedirectUrl="http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/receiptOrderInput.html?orderId="+smartOrder.getTaskno()+"&formId="+smartOrder.getId();
		return "redirect:"+sRedirectUrl;
		//return "modules/mis/smartSetTaskList";//
	}	
	
	@Autowired
	private SmartAttachmentsService smartAttachmentsService;	
	@RequestMapping(value = "saveAttachments")
	public String saveAttachments(String formId,String attUrl) {
		jsonReturn jr=new jsonReturn();		
		jr.setFormId(formId);
		if(formId==null ){
			jr.setErrCode("1");//失败
			jr.setErrMsg("保存附件失败，未获取单号");		
		}else{			
			SmartAttachments smartAttachments=new SmartAttachments();
			smartAttachments.setTaskno(soDao.get(formId).getTaskno());			
			smartAttachments.setOrderid(formId);
			smartAttachments.setRemarks(attUrl);;
			smartAttachments.setType("0");
			smartAttachmentsService.save(smartAttachments);
			jr.setErrCode("0");//成功
			jr.setErrMsg("保存附件成功");						
		}
		String sRet="jsonpCallback("+ JsonMapper.toJsonString(jr)+")";
		return sRet;
	}		
	/**
	 * 客户选择选择标签（iconselect.tag）
	 */
	@RequestMapping(value = "customerselect")
	public String customerselect(SmartCustomer smartCustomer,HttpServletRequest request, Model model) {
		model.addAttribute("value", request.getParameter("value"));
		return "modules/cms/front/themes/weixin/tagCustomerselect";
	}
	@Autowired
	private SmartCustomerService smartCustomerService;	
	@RequestMapping(value = "customerSearch")
	public String customerSearch(SmartCustomer smartCustomer, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmartCustomer> page = smartCustomerService.findPage(new Page<SmartCustomer>(request, response), smartCustomer); 
		model.addAttribute("page", page);
		return "modules/cms/front/themes/weixin/tagCustomerselect";
	}	
	
	@RequestMapping(value = "knowledgeIndex")
	public String knowledgeIndex() {
		return "modules/cms/front/themes/weixin/knowledgeIndex";
	}
	
	@RequestMapping(value = "knowledgeTree")
	public String knowledgeTree(Model model) {
		model.addAttribute("categoryList", categoryService.findByParentIdsLike("2"));
		return "modules/cms/front/themes/weixin/knowledgeTree";
	}
	
	@RequestMapping(value = "knowledgeNone")
	public String knowledgeNone() {
		return "modules/cms/front/themes/weixin/knowledgeNone";
	}	
	@RequestMapping(value = "knowledgeArticle")
	public String knowledgeArticle(Article article, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<Article> page = articleService.findPage(new Page<Article>(request, response), article, true); 
        model.addAttribute("page", page);
		return "modules/cms/front/themes/weixin/articleList";
	}
	@RequestMapping(value = "smartMap")
	public String smartMap() {
		return "modules/cms/front/themes/weixin/smartMap";
	}
	
	@Autowired
	private SmartApprovalService smartApprovalService;
	@RequestMapping(value = "formApproval")
	public String formApproval(String id, Model model) {
		SmartApproval entity = null;
		entity = smartApprovalService.get(id);
		model.addAttribute("smartApproval", entity);
		return "modules/cms/front/themes/weixin/weixinApproval";
	}	
	@RequestMapping(value = "agreeApproval")
	public String agreeApproval(String id) {
		SmartApproval smartApproval = null;
		smartApproval = smartApprovalService.get(id);
		smartApproval.setStatus("1");
		smartApproval.setResult("1");
		smartApproval.setApprovalDate(new Date());		
		smartApprovalService.save(smartApproval);
		DictUtils.postWeixinApproval(smartApproval);
		return "modules/cms/front/themes/weixin/wxClose";
	}

	@RequestMapping(value = "disagreeApproval")
	public String disagreeApproval(String id) {
		SmartApproval smartApproval = null;
		smartApproval = smartApprovalService.get(id);		
		smartApproval.setStatus("1");
		smartApproval.setResult("0");
		smartApproval.setApprovalDate(new Date());
		smartApprovalService.save(smartApproval);
		DictUtils.postWeixinApproval(smartApproval);

		return "modules/cms/front/themes/weixin/wxClose";
	}
	@RequestMapping(value = "wxClose")
	public String wxClose() {
		return "modules/cms/front/themes/weixin/wxClose";
	}
	
	@Autowired
	private ActTaskService actTaskService;	
	/**
	 * 获取待办列表
	 * @param procDefKey 流程定义标识
	 * @return
	 */
	@RequestMapping(value = "/act/todo")
	public String todoList(Act act, String userEmail,HttpServletResponse response, Model model) throws Exception {
		String userId=UserUtils.getByEmail(userEmail).getLoginName();
		List<Act> list = actTaskService.todoList(act,userId);
		model.addAttribute("list", list);
		return "modules/cms/front/themes/weixin/actTaskTodoList";
	}
	
	/**
	 * 获取已办任务
	 * @param page
	 * @param procDefKey 流程定义标识
	 * @return
	 */
	@RequestMapping(value = "/act/historic")
	public String historicList(Act act, String userEmail,HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		Page<Act> page = new Page<Act>(request, response);
		String userId=UserUtils.getByEmail(userEmail).getLoginName();
		page = actTaskService.historicList(page, act,userId);
		model.addAttribute("page", page);
		return "modules/cms/front/themes/weixin/actTaskHistoricList";
	}
	/**
	 * 获取流转历史列表
	 * @param procInsId 流程实例
	 * @param startAct 开始活动节点名称
	 * @param endAct 结束活动节点名称
	 */
	@RequestMapping(value = "/act/histoicFlow")
	public String histoicFlow(Act act, String startAct, String endAct, Model model){
		if (StringUtils.isNotBlank(act.getProcInsId())){
			List<Act> histoicFlowList = actTaskService.histoicFlowList(act.getProcInsId(), startAct, endAct);
			model.addAttribute("histoicFlowList", histoicFlowList);
		}
		return "modules/cms/front/themes/weixin/actTaskHistoricFlow";
	}
	/**
	 * 获取流程表单
	 * @param taskId	任务ID
	 * @param taskName	任务名称
	 * @param taskDefKey 任务环节标识
	 * @param procInsId 流程实例ID
	 * @param procDefId 流程定义ID
	 */
	@RequestMapping(value = "/act/form")
	public String actform(String taskId,String taskName,String procInsId,String taskDefKey ,boolean isFinishTask, HttpServletRequest request, Model model){
		
		String view = "oaTempTaskForm";
		OaTempTask oaTempTask=oaTempTaskService.getByProcInsId(procInsId);
		 Act act=oaTempTask.getAct();
		 act.setTaskId(taskId);
		 act.setTaskName(taskName);
		 act.setTaskDefKey(taskDefKey);
		 act.setProcInsId(procInsId);
		 oaTempTask.setAct(act);
		// 查看审批申请单
		if (StringUtils.isNotBlank(oaTempTask.getId())){//.getAct().getProcInsId())){

			// 环节编号
			
			// 查看工单
			if(isFinishTask){
				view = "oaTempTaskView";
			}
			// 修改环节
			else if ("modify".equals(taskDefKey)){
				view = "oaTempTaskForm";
			}
			// 审核环节
			else if ("audit".equals(taskDefKey)){
				view = "oaTempTaskAudit";
			}
		}

		model.addAttribute("oaTempTask", oaTempTask);

		return "modules/cms/front/themes/weixin/" + view;	
		

	}
	@Autowired
	private OaTempTaskService oaTempTaskService;
	@RequestMapping(value = "/oa/form")
	public String oaform(OaTempTask oaTempTask, Model model) {
		String view = "oaTempTaskForm";
		
		// 查看审批申请单
		if (StringUtils.isNotBlank(oaTempTask.getId())){//.getAct().getProcInsId())){

			// 环节编号
			String taskDefKey = oaTempTask.getAct().getTaskDefKey();
			
			// 查看工单
			if(oaTempTask.getAct().isFinishTask()){
				view = "oaTempTaskView";
			}
			// 修改环节
			else if ("modify".equals(taskDefKey)){
				view = "oaTempTaskForm";
			}
			// 审核环节
			else if ("audit".equals(taskDefKey)){
				view = "oaTempTaskAudit";
			}
		}

		model.addAttribute("oaTempTask", oaTempTask);
		return "modules/cms/front/themes/weixin/" + view;		
	}

	@RequestMapping(value = "/oa/save")
	public String save(OaTempTask oaTempTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, oaTempTask)){
			return oaform(oaTempTask, model);
		}
		String userEmail=oaTempTask.getRemarks();
		User createBy=UserUtils.getByEmail(userEmail);
		oaTempTask.setCreateBy(createBy);
		oaTempTask.setCreateDate(new Date());
		oaTempTaskService.save(oaTempTask);
		addMessage(redirectAttributes, "保存临时审批任务成功");
		
		return "modules/cms/front/themes/weixin/wxClose";
	}


	@RequestMapping(value = "/oa/saveAudit")
	public String saveAudit(OaTempTask oaTempTask,Model model) {
		if (StringUtils.isBlank(oaTempTask.getAct().getFlag())
				|| StringUtils.isBlank(oaTempTask.getAct().getComment())){
			addMessage(model, "请填写审核意见。");
			return oaform(oaTempTask, model);
		}
		
		oaTempTaskService.auditSave(oaTempTask);
		return "modules/cms/front/themes/weixin/wxClose";
	}
	
	@Autowired
	private SystemService systemService;	
	
	@ResponseBody
	@RequestMapping(value = "/oa/treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String officeId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<User> list = systemService.findUserByOfficeId(officeId);
		for (int i=0; i<list.size(); i++){
			User e = list.get(i);
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", "u_"+e.getId());
			map.put("pId", officeId);
			map.put("name", StringUtils.replace(e.getName(), " ", ""));
			mapList.add(map);
		}
		return mapList;
	}
	
	@RequestMapping(value = "/oa/treeselect")
	public String treeselect(HttpServletRequest request, Model model) {
		model.addAttribute("url", request.getParameter("url")); 	// 树结构数据URL
		model.addAttribute("extId", request.getParameter("extId")); // 排除的编号ID
		model.addAttribute("checked", request.getParameter("checked")); // 是否可复选
		model.addAttribute("selectIds", request.getParameter("selectIds")); // 指定默认选中的ID
		model.addAttribute("isAll", request.getParameter("isAll")); 	// 是否读取全部数据，不进行权限过滤
		model.addAttribute("module", request.getParameter("module"));	// 过滤栏目模型（仅针对CMS的Category树）
		return "modules/cms/front/themes/weixin/tagTreeselect";
	}
	
	
	
	/**
	 * 读取带跟踪的图片
	 */
	@RequestMapping(value = "/act/trace/photo/{procDefId}/{execId}")
	public void tracePhoto(@PathVariable("procDefId") String procDefId, @PathVariable("execId") String execId, HttpServletResponse response) throws Exception {
		InputStream imageStream = actTaskService.tracePhoto(procDefId, execId);
		
		// 输出资源内容到相应对象
		byte[] b = new byte[1024];
		int len;
		while ((len = imageStream.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
	}
}
