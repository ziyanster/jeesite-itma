/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.cms.web.front;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.mapper.JsonMapper;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.SpringContextHolder;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.cms.utils.WiexinSignUtil;
import com.thinkgem.jeesite.modules.mis.dao.MisContractDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartAttachmentsDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartOrderDao;
import com.thinkgem.jeesite.modules.mis.dao.SmartTaskDao;
import com.thinkgem.jeesite.modules.mis.entity.MisContract;
import com.thinkgem.jeesite.modules.mis.entity.SmartAttachments;
import com.thinkgem.jeesite.modules.mis.entity.SmartCustomer;
import com.thinkgem.jeesite.modules.mis.entity.SmartOrder;
import com.thinkgem.jeesite.modules.mis.entity.SmartTask;
import com.thinkgem.jeesite.modules.mis.entity.StatsParam;
import com.thinkgem.jeesite.modules.mis.entity.StatsResult;
import com.thinkgem.jeesite.modules.mis.entity.jsonReturn;
import com.thinkgem.jeesite.modules.mis.entity.jsonTask;
import com.thinkgem.jeesite.modules.mis.entity.jsonTaskInfo;
import com.thinkgem.jeesite.modules.mis.service.SmartAttachmentsService;
import com.thinkgem.jeesite.modules.mis.service.SmartCustomerService;
import com.thinkgem.jeesite.modules.mis.service.SmartOrderService;
import com.thinkgem.jeesite.modules.mis.service.SmartTaskService;
import com.thinkgem.jeesite.modules.sys.dao.UserDao;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.DictUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import com.thinksoft.wechat.utils.HttpUtils;

/**
 * 微信Controller
 * 
 * @author ThinkGem
 * @version 2014-02-28
 */
@Controller
@RequestMapping(value = "${frontPath}/weixin")
public class WeixinController extends BaseController {
	@Autowired
	private SmartOrderService smartOrderService;
	@Autowired
	private SmartTaskService smartTaskService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private SmartAttachmentsService smartAttachmentsService;	
	@Autowired
	private SmartCustomerService smartCustomerService;	

	private static SmartTaskDao stDao = SpringContextHolder
			.getBean(SmartTaskDao.class);
	private static SmartOrderDao soDao = SpringContextHolder
			.getBean(SmartOrderDao.class);
	private static SmartAttachmentsDao saDao = SpringContextHolder
			.getBean(SmartAttachmentsDao.class);
	private static UserDao uDao = SpringContextHolder
			.getBean(UserDao.class);
	private static MisContractDao mcDao = SpringContextHolder
			.getBean(MisContractDao.class);

	// 抢单任务列表查询
	@RequestMapping(value = "grabTaskList")
	public String grabTaskList(String serviceType, String clickNum,
			HttpServletRequest request, HttpServletResponse response) {
		int pageIndex = 1;
		String strJsonP = "";
		

		if (serviceType == null && clickNum == null) {
			strJsonP = "jsonpCallback("
					+ "{\"order\":"
					+ DictUtils.getReadyGrabList("", 1)// 收费服务,第一页
					+ "}" + ")";
		} else {
			pageIndex = Integer.parseInt(clickNum);
			strJsonP = DictUtils.getReadyGrabList(serviceType, pageIndex);
			strJsonP = "jsonpCallback(" + "{\"order\":" + strJsonP
						+ "}" + ")";
		}
		return renderString(response, strJsonP, "application/json");
	}

	// 指派任务列表查询
	@RequestMapping(value = "assignTaskList")
	public String assignTaskList(String userId, String serviceType,
			String clickNum, HttpServletRequest request,
			HttpServletResponse response) {
		int pageIndex = 1;
		String strJsonP = "";
		
//		00	保修合同
//		01	销售合同
//		02	售前服务
//		03	收费服务
//		04	免费服务
//		05	计次合同
		
		if (serviceType == null && clickNum == null) {
			strJsonP = "jsonpCallback("
					+ "{\"order\":"
					+ DictUtils.getReadyAssignList(userId, "", 1)// 收费服务,第一页
					+ "}" + ")";
		} else {
			pageIndex = Integer.parseInt(clickNum);
			strJsonP = DictUtils.getReadyAssignList(userId, serviceType,
					pageIndex);
			strJsonP = "jsonpCallback(" + "{\"order\":" + strJsonP
						+ "}" + ")";
		}
		return renderString(response, strJsonP, "application/json");
	}

	@RequestMapping(value = "GrabTask")
	public String GrabTask(String orderId, String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		jsonReturn jr = new jsonReturn();
		jr.setOrderId(orderId);// 任务id
		if (orderId == null || userId == null) {
			jr.setErrCode("1");// 失败
			jr.setErrMsg("抢单失败，未获取任务单号或用户ID");
		} else {
			// 查询任务状态是否=1，待处理
			SmartTask st_in = new SmartTask();
			st_in.setId(orderId);
			SmartTask st_out = stDao.get(st_in);

			if (st_out == null) {// 状态已更新
				jr.setErrCode("1");// 失败
				jr.setErrMsg("抢单失败，未查询到此任务");
			} else if (!st_out.getStatus().equals("1")) { // 待抢单
				jr.setErrCode("1");// 失败
				jr.setErrMsg("抢单失败，此单已被受理");
			} else {
				// 更新任务状态
				st_out.setStatus("2");// 抢单成功
				stDao.update(st_out);
				// 新增一条工单
				SmartOrder smartOrder = new SmartOrder();
				String custArea = DictUtils.getCustomerArea(
						st_out.getCustomerno(), "HBQ");
				smartOrder.setServicedocno(IdGen.uuid());
				smartOrder.setCustomerno(st_out.getCustomerno());
				smartOrder.setTaskno(st_out.getId());
				smartOrder.setStatus("1");// 已受理状态
				smartOrder.setIsassigned(st_out.getSendtype());
				smartOrder.setAssignedno(UserUtils.getByEmail(userId));
				smartOrder.setIntegral(st_out.getIntegral());
				smartOrder.setServicetype(st_out.getServicetype());
				smartOrder.setRecord(st_out.getRecord());
				smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea,
						"smart_order", "orderSerialNo"));
				smartOrderService.save(smartOrder);

				SmartOrder so_in = new SmartOrder();
				so_in.setTaskno(orderId);// 任务id
				so_in.setAssignedno(UserUtils.getByEmail(userId));
				so_in.setStatus("1");
				List<SmartOrder> listOrder = soDao.findList(so_in);
				if (listOrder.size() == 1) {// 未查询到符合条件工单
					jr.setFormId(listOrder.get(0).getId());// 工单id
					jr.setOrderId(orderId);// 任务id
				}
				jr.setErrCode("0");// 成功
				jr.setErrMsg("抢单任务受理成功");

				if (!StringUtils.isBlank(so_in.getAssignedno().getMobile())) {
					String smsIndex = MisUtils.sendStartSms(smartOrder, st_out, new Date());
					if(!StringUtils.isBlank(smsIndex)){
						smartOrder.setSmsIndex1(smsIndex);
						soDao.update(smartOrder);
						MisUtils.assignSms(smartOrder, st_out);
					}
				} else {
					logger.error("用户"+so_in.getAssignedno().getName()+"没有维护手机号，未向mis进行短信报备");
				}
				String sCustName = DictUtils
						.getCustomer(st_out.getCustomerno()).getCustomername();
				sCustName = StringUtils.isBlank(sCustName) ? "" : sCustName;
				// 发送成功消息
				DictUtils.postWeixinTask(st_out.getId(), "3", userId,
						sCustName, st_out.getRecord(), st_out.getIntegral());
			}
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	//服务排名任务详情查询
	@RequestMapping(value = "getServiceRankingContent")
	public String getServiceRankingContent(
			HttpServletRequest request, HttpServletResponse response){
		StatsParam param = new StatsParam();				
		//获得前一天日期
		Date dNow = new Date();   //当前时间
		Date dBefore = new Date();
		Calendar calendar = Calendar.getInstance(); //得到日历
		calendar.setTime(dNow);//把当前时间赋给日历
		calendar.add(Calendar.DAY_OF_MONTH, -1);  //设置为前一天
		dBefore = calendar.getTime();   //得到前一天的时间
		String beginDate = DateUtils.formatDate(dBefore, "yyyy-MM-dd");
		String endDate = DateUtils.formatDate(dNow, "yyyy-MM-dd");
		Date begin = DateUtils.parseDate(beginDate);
		param.setBeginDate(begin);
		Date end = DateUtils.parseDate(endDate);
		param.setEndDate(end);
		List<StatsResult> listYesterday = soDao.findIndexStats(param);
		for (StatsResult statsResult : listYesterday) {
			String status = statsResult.getStatus();
			if (("0").equals(status)) {
				status = "待受理";
			}else if ("1".equals(status)) {
				status = "已受理";
			}else if ("2".equals(status)) {
				status = "已回单";
			}else if ("3".equals(status)) {
				status = "已评价";
			}else if ("4".equals(status)) {
				status = "主动退单";
			}else if ("5".equals(status)) {
				status = "超时退单";
			}else if ("9".equals(status)) {
				status = "关闭";
			}
			statsResult.setStatus(status);
		}

		calendar.setTime(dNow);//把当前时间赋给日历
		calendar.set(Calendar.DAY_OF_MONTH, 1);  //设置为当前月第一天
		dBefore = calendar.getTime();   //得到当月1号的时间
		
		beginDate = DateUtils.formatDate(dBefore, "yyyy-MM-dd");
		endDate = DateUtils.formatDate(dNow, "yyyy-MM-dd");
		begin = DateUtils.parseDate(beginDate);
		param.setBeginDate(begin);
		end = DateUtils.parseDate(endDate);
		param.setEndDate(end);
		
		List<StatsResult> listCurMonth = soDao.findIndexStats(param);
		for (StatsResult statsResult : listCurMonth) {
			String status = statsResult.getStatus();
			if (("0").equals(status)) {
				status = "待受理";
			}else if ("1".equals(status)) {
				status = "已受理";
			}else if ("2".equals(status)) {
				status = "已回单";
			}else if ("3".equals(status)) {
				status = "已评价";
			}else if ("4".equals(status)) {
				status = "主动退单";
			}else if ("5".equals(status)) {
				status = "超时退单";
			}else if ("9".equals(status)) {
				status = "关闭";
			}
			statsResult.setStatus(status);
		}
		
		String sRet = "jsonpCallback({ \"listYesterday\":" + JsonMapper.toJsonString(listYesterday) + ",\"listCurMonth\":" + JsonMapper.toJsonString(listCurMonth) + "})";
		return renderString(response, sRet, "application/json");
	}

	/**
	 * 获取巡检报告
	 * */
	@RequestMapping(value="getXjReports")
	public String getXjReports(String custNo,
			HttpServletRequest request, HttpServletResponse response){
		jsonTaskInfo jtinfo = new jsonTaskInfo();
		
		String s = MisUtils.searchXjReports(custNo);
		if(!StringUtils.isBlank(s)){
			//XJ1503007$2015-02-05$周海亮$华为DS5600;XJ1503008$2015-02-24$彭广$3584_L53;XJ1503008$2015-02-24$彭广$3584_L32;XJ1503008$2015-02-24$彭广$IBM9119-595;XJ1503008$2015-02-24$彭广$IBM9119-595;XJ1503008$2015-02-24$彭广$浪潮AS1000G2_3;
			List<Dict> list = new ArrayList<Dict>();
			String[] reports = s.split(";");
			for (String str : reports) {
				if(StringUtils.isBlank(str)){
					continue;
				}
				String[] content = str.split("\\$");
				Dict sc = new Dict();
				sc.setLabel(content[0].trim());
				sc.setValue(content[1].trim());
				sc.setDescription(content[2].trim());
				sc.setRemarks(content[3].trim());
				sc.setType(content[4].trim());
				list.add(sc);
			}
			jtinfo.setDicts(list);	
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	

	// 工程师应急任务基本内容
	@RequestMapping(value="getMyEmergencyTaskContent")
	public String getMyEmergencyTaskContent(String userId,
			HttpServletRequest request, HttpServletResponse response){
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		// 查询任务
		String sRet = "";
		jsonTaskInfo jtinfo = new jsonTaskInfo();
		
		User user = UserUtils.getByEmail(userId);
		String url = DictUtils.getDictList("sys_misCustList_lin").get(0).getRemarks();
		String os = System.getProperty("os.name");
		if(os.toLowerCase().startsWith("win")){
			url = DictUtils.getDictList("sys_misCustList_win").get(0).getRemarks();
		}
		HttpURLConnection conn;
		try {
			//http://192.168.43.17/mis/gzzl/wxGetMyCustomers.jsp?eId=140033
			url = url + "?eId="+user.getNo();
			conn = (HttpURLConnection) new URL(url)
					.openConnection();
			logger.debug("查找自己常用客户的URL为："+url);
			String s = HttpUtils.retrieveInput(conn);
			s = s.replace("\r\n", "").replace("\"", "").replace(" ", "");

			//D00018-太仓农村商业银行;B00048-中国农业银行股份有限公司深圳市分行;D00621-中国烟草总公司海南省公司;B00232-中国银行吉林市分行;
			List<SmartCustomer> list = new ArrayList<SmartCustomer>();
			String[] customers = s.split(";");
			for (String str : customers) {
				if(StringUtils.isBlank(str)){
					continue;
				}
				String[] id_name = str.split("-");
				SmartCustomer sc = new SmartCustomer();
				sc.setCustomerno(id_name[0].trim());
				sc.setCustomername(id_name[1].trim());
				list.add(sc);
			}
			jtinfo.setCustomers(list);
			
			jtinfo.setDicts(DictUtils.getDictList("mis_workType"));
			jtinfo.setDicts1(DictUtils.getDictList("mis_incomingway"));			
			
		}
		catch(Exception e){
			
		}
		
		sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	// 查找客户列表
	@RequestMapping(value = "searchCustomers")
	public String searchCustomers(String custName,
			HttpServletRequest request, HttpServletResponse response){
		SmartCustomer smartCustomer = new SmartCustomer();
		smartCustomer.setCustomername(custName);
		List<SmartCustomer> list = smartCustomerService.findList(smartCustomer);
		jsonTaskInfo jtinfo = new jsonTaskInfo();
		jtinfo.setCustomers(list);
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";
		return renderString(response, sRet, "application/json");
	}

	// 保存工程师一般任务
	@RequestMapping(value = "saveNormalTask")
	public String saveNormalTask(HttpServletRequest request,HttpServletResponse response){
		jsonReturn jr = new jsonReturn();
		String userId = request.getParameter("userId");
		String custNo = request.getParameter("custNo");
		String workType = request.getParameter("workType");
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		String workStat = request.getParameter("workStat");
		String workway = request.getParameter("workway");
		String record = request.getParameter("record");
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		if("02".equals(workType)){
			workType = "ZC";
		}else if("01".equals(workType)){
			workType = "FZ";
		}else{
			workType = "GW";			
		}
		
		User assignuser= UserUtils.getByEmail(userId);

		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SmartTask smartTask = new SmartTask();
		smartTask.setAssignuser(assignuser);
		smartTask.setSendtype("0");//指派任务
		smartTask.setCustomerno(custNo);
				
		Date outDate = null;
		Date endDate = null;
		try {
			outDate = sdf1.parse(startTime);
			endDate = sdf1.parse(endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		smartTask.setRecord(record);
		smartTask.setWorktype(workType);

		SmartOrder smartOrder=new SmartOrder();			
		smartOrder.setServicedocno(IdGen.uuid());
		smartOrder.setCustomerno(smartTask.getCustomerno());
		smartOrder.setAssignedno(smartTask.getAssignuser());
		smartOrder.setResponsetime(outDate);
		smartOrder.setRecord(smartTask.getRecord());
		smartOrder.setTaskcontent("");
		//smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
				
		if (!StringUtils.isBlank(assignuser.getMobile())) {
			String smsIndex = MisUtils.sendStartSms(smartOrder, smartTask, outDate);
			if(!StringUtils.isBlank(smsIndex)){
				smartOrder.setSmsIndex1(smsIndex);
				// 派遣
				MisUtils.assignSms(smartOrder, smartTask);
				String smsIndex1 = MisUtils.sendEndSms(smartOrder, smartTask, record);
				smartOrder.setSmsIndex2(smsIndex1);
				// 处理短信
				MisUtils.resolveSms(smartOrder, smartTask,endDate,workway, workStat);
				
				jr.setErrCode("0");//成功
			}
		} else {
			logger.error("用户"+assignuser.getName()+"没有维护手机号，未向mis进行短信报备");
		}
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	// 保存工程师应急任务
	@RequestMapping(value = "saveEmergencyTask")
	public String saveEmergencyTask(String userId,String custNo,String workType,String callType,String callLog,String callTime,
			String outTime,HttpServletRequest request, HttpServletResponse response) throws ParseException{
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SmartTask smartTask = new SmartTask();
		User assignuser= UserUtils.getByEmail(userId);
		smartTask.setRemarks("");
		smartTask.setAssignuser(assignuser);
		smartTask.setStatus("2");//预处理状态
		smartTask.setSendtype("0");//指派任务
		smartTask.setIntegral(100);
		smartTask.setCustomerno(custNo);
		Date outDate = null;
		try {
			smartTask.setIncomingtime(sdf1.parse(callTime));
			outDate = sdf1.parse(outTime);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		smartTask.setIncomingway(callType);
		smartTask.setRecord(callLog);
		smartTask.setWorktype(workType);
		smartTaskService.save(smartTask);
		String custName=DictUtils.getCustomerName(smartTask.getCustomerno(), "");

		String custArea=DictUtils.getCustomerArea(smartTask.getCustomerno(), "HBQ");
		SmartOrder smartOrder=new SmartOrder();			
		smartOrder.setServicedocno(IdGen.uuid());
		smartOrder.setCustomerno(smartTask.getCustomerno());
		smartOrder.setTaskno(smartTask.getId());
		smartOrder.setStatus("1");//已受理状态
		smartOrder.setIsassigned(smartTask.getSendtype());
		smartOrder.setAssignedno(smartTask.getAssignuser());
		smartOrder.setIntegral(smartTask.getIntegral());
		smartOrder.setServicetype(smartTask.getServicetype());
		smartOrder.setResponsetime(outDate);
		smartOrder.setRecord(smartTask.getRecord());
		smartOrder.setOrderserialno(StringUtils.createOrderNo(custArea, "smart_order", "orderSerialNo"));
		smartOrderService.save(smartOrder);
			
		custName = StringUtils.isBlank(custName) ? "" : custName;
		if (!StringUtils.isBlank(smartOrder.getAssignedno().getMobile())) {
			String smsIndex = MisUtils.sendStartSms(smartOrder, smartTask, outDate);
			if(!StringUtils.isBlank(smsIndex)){
				smartOrder.setSmsIndex1(smsIndex);
				soDao.update(smartOrder);
				// 派遣
				MisUtils.assignSms(smartOrder, smartTask);
	
				jr.setErrCode("0");//成功
				jr.setFormId(smartOrder.getId());
				jr.setOrderId(smartOrder.getTaskno());
			}
		} else {
			logger.error("用户"+smartOrder.getAssignedno().getName()+"没有维护手机号，未向mis进行短信报备");
		}
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	// 抢单任务详情查询
	@RequestMapping(value = "getGrabTaskContent")
	public String getGrabTaskContent(String orderId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 查询任务
		String sRet = "";
		jsonReturn jr = new jsonReturn();
		jr.setOrderId(orderId);

		SmartTask st_in = new SmartTask();
		st_in.setId(orderId);
		SmartTask st_out = stDao.get(st_in);

		if (st_out == null) {// 任务id不存在
			jr.setErrCode("1");// 失败
			jr.setErrMsg("任务编码无效");
			sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		} else {
			SmartOrder so_in = new SmartOrder();
			so_in.setTaskno(orderId);
			List<SmartOrder> soList = soDao.findList(so_in);
			SmartOrder so = null;
			if(soList.size()>0){
				so = soList.get(0);
			}
			
			
			jsonTaskInfo jtinfo = new jsonTaskInfo();
			jtinfo.setCustomerNo(st_out.getCustomerno());
			jtinfo.setOrderId(st_out.getId());
			jtinfo.setCustomerName(DictUtils
					.getCustomer(st_out.getCustomerno()).getCustomername());
			jtinfo.setCustomerAddress(DictUtils.getCustomer(
					st_out.getCustomerno()).getAddress());
			jtinfo.setAddressLink("http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/trafficRoutes.html");
			jtinfo.setCustomerPhone(DictUtils.getCustomer(
					st_out.getCustomerno()).getPhone());
			jtinfo.setPhoneLink("tel:"+DictUtils.getCustomer(st_out.getCustomerno())
					.getPhone());
			jtinfo.setServiceType(DictUtils.getDictLabel(
					st_out.getServicetype(), "mis_servicetype", ""));
			jtinfo.setFaultType(DictUtils.getDictLabel(st_out.getFaulttype(),
					"mis_faulttype", ""));
			jtinfo.setWorkType(st_out.getWorktype());
			jtinfo.setDicts(DictUtils.getDictList("mis_servicetype"));
			jtinfo.setDicts1(DictUtils.getDictList("mis_faulttype"));
			
			jtinfo.setPrice(Integer.toString(st_out.getIntegral()==null?0:st_out.getIntegral()));
			jtinfo.setCallTitle("呼叫记录");
			jtinfo.setCallLog(st_out.getRecord());
			jtinfo.setGrabOrderLink("http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/receiptOrder.html");
			if(so!=null){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				jtinfo.setOrderTime(sdf.format(so.getCreateDate()==null?new Date():so.getCreateDate()));
				jtinfo.setOrderResponseTime(sdf.format(so.getResponsetime()==null?new Date():so.getResponsetime()));
				jtinfo.setCallInTime(sdf.format(st_out.getIncomingtime()==null?new Date():st_out.getIncomingtime()));
			}
			jtinfo.setOrderResponseTitle("");

			Page<SmartAttachments> pageParam = new Page<SmartAttachments>();
			pageParam.setPageSize(2);
			pageParam.setPageNo(1);
			SmartAttachments sa = new SmartAttachments();
			sa.setTaskno(st_out.getId());
			sa.setPage(pageParam);
			List<SmartAttachments> listAttr = saDao.findList(sa);

			if (listAttr.size() > 0) {
				jtinfo.setOrderpicLink1(listAttr.get(0).getRemarks());
			}
			if (listAttr.size() > 1) {
				jtinfo.setOrderpicLink2(listAttr.get(1).getRemarks());
			}

			sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";

		}
		return renderString(response, sRet, "application/json");
	}

	// 工单详情查询
	@RequestMapping(value = "getFormContent")
	public String getFormContent(String formId, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		// 查询任务
		String sRet = "";
		jsonReturn jr = new jsonReturn();
		jr.setFormId(formId);

		SmartOrder formInfor = soDao.get(new SmartOrder(formId));
		if (formInfor == null) {// 工单id不存在
			jr.setErrCode("1");// 失败
			jr.setErrMsg("任务编码无效");
			sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		} else {

			String sTaskId = formInfor.getTaskno();
			jr.setOrderId(sTaskId);
			SmartTask taskInfor = stDao.get(new SmartTask(sTaskId));

			jsonTaskInfo jtinfo = new jsonTaskInfo();
			jtinfo.setOrderId(sTaskId);
			jtinfo.setFormId(formId);
			jtinfo.setCustomerName(DictUtils.getCustomer(
					taskInfor.getCustomerno()).getCustomername());
			jtinfo.setCustomerAddress(DictUtils.getCustomer(
					taskInfor.getCustomerno()).getAddress());
			jtinfo.setAddressLink("http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/trafficRoutes.html");
			jtinfo.setCustomerPhone(DictUtils.getCustomer(
					taskInfor.getCustomerno()).getPhone());
			jtinfo.setPhoneLink(DictUtils
					.getCustomer(taskInfor.getCustomerno()).getPhone());
			jtinfo.setServiceType(DictUtils.getDictLabel(
					taskInfor.getServicetype(), "mis_servicetype", ""));
			jtinfo.setFaultType(DictUtils.getDictLabel(
					taskInfor.getFaulttype(), "mis_faulttype", ""));
			jtinfo.setPrice(Integer.toString(taskInfor.getIntegral()));
			jtinfo.setCallTitle("呼叫记录");
			jtinfo.setCallLog(taskInfor.getRecord());
			jtinfo.setGrabOrderLink("http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/receiptOrder.html");
			jtinfo.setOrderResponseTime("");
			jtinfo.setOrderResponseTitle("");

			Page<SmartAttachments> pageParam = new Page<SmartAttachments>();
			pageParam.setPageSize(2);
			pageParam.setPageNo(1);
			SmartAttachments sa = new SmartAttachments();
			sa.setTaskno(sTaskId);
			sa.setOrderid(formId);
			sa.setPage(pageParam);
			List<SmartAttachments> listAttr = saDao.findList(sa);

			if (listAttr.size() > 0) {
				jtinfo.setOrderpicLink1(listAttr.get(0).getRemarks());
			}
			if (listAttr.size() > 1) {
				jtinfo.setOrderpicLink2(listAttr.get(1).getRemarks());
			}

			jtinfo.setModelType(formInfor.getModeltype());// 机型modelType
			jtinfo.setSerialNumber(formInfor.getEquipmentno());// 系列号serialNumber
			jtinfo.setWorkContent(formInfor.getTaskcontent());// 工作内容workContent
			jtinfo.setSolveProblem(formInfor.getThistimejob());// 本次解决的问题solveProblem
			jtinfo.setWorkPlan(formInfor.getNexttimeplan());// 下一步工作计划workPlan
			jtinfo.setRemarks(formInfor.getRemarks());// 备注remarks

			sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";

		}
		return renderString(response, sRet, "application/json");
	}
	
	// 客户-应急任务查询内容
	@RequestMapping(value = "getCustEmergencyTaskContent")
	public String getCustEmergencyTaskContent(String custId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(custId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		// 查询任务
		String sRet = "";
		jsonTaskInfo jtinfo = new jsonTaskInfo();
		SmartCustomer customer = DictUtils.getCustomer(custId);
		jtinfo.setCustomerName(customer.getCustomername());
		jtinfo.setCustomerAddress(customer.getAddress());
			
		sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";

		return renderString(response, sRet, "application/json");
	}

	// 工程师受理的任務內容
	@RequestMapping(value = "excuteTaskFromEngineer")
	public String excuteTaskFromEngineer(String userId,String orderId,
			HttpServletRequest request, HttpServletResponse response){
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		SmartOrder so = soDao.get(orderId);
		SmartTask st = stDao.get(so.getTaskno());
		if (so!=null && so.getStatus().equals("1")) { // 待抢单
			jr.setErrCode("0");// 失败
			jr.setErrMsg("工单已受理");
			String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, sRet, "application/json");
		} 
		so.setResponsetime(new Date());
		so.setStatus("1");
		so.setIsassigned("0");//指派任务
		soDao.update(so);

		User user = UserUtils.getByEmail(userId);
		String sCustName = DictUtils
				.getCustomer(st.getCustomerno()).getCustomername();
		sCustName = StringUtils.isBlank(sCustName) ? "" : sCustName;
		if (!StringUtils.isBlank(user.getMobile())) {
			
		} else {
			logger.error("用户"+so.getAssignedno().getName()+"没有维护手机号，未向mis进行短信报备");
		}
		// 发送成功消息
		DictUtils.postWeixinTask("", "99", DictUtils.getCustomer(st.getCustomerno()).getCustomerno(),
				"", "尊敬的客户：\r\n 您提交的服务申请已获得受理，内容如下：\r\n已指派"+user.getName()+"工程师（"+user.getMobile()+"）提供技术服务，请保持您的通讯畅通，以便我们预约上门时间。", 0);
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}
	// 工程师受理的任務內容
	@RequestMapping(value = "getTaskDetailFromEngineer")
	public String getTaskDetailFromEngineer(String userId,String orderId,
			HttpServletRequest request, HttpServletResponse response){
		jsonTaskInfo jtinfo = new jsonTaskInfo();
		SmartOrder so = soDao.get(orderId);
		String taskId = so.getTaskno();		
		// 任务相关
		SmartTask st = stDao.get(taskId);
		jtinfo.setCallLog(st.getRecord());
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		jtinfo.setServiceDate(sdf.format(st.getServiceDate()));		
		// 获取客户信息
		SmartCustomer customer = DictUtils.getCustomer(st.getCustomerno());
		jtinfo.setCustomerName(customer.getCustomername());

		// 将客户所传图像检索出来
		SmartAttachments sa_in = new SmartAttachments();
		sa_in.setTaskno(taskId);
		List<SmartAttachments> list = smartAttachmentsService.findList(sa_in);
		List<Dict> dicts = new ArrayList<Dict>();
		for (SmartAttachments smartAttachments : list) {
			Dict di = new Dict();
			di.setLabel(smartAttachments.getRemarks());
			dicts.add(di);
		}
		jtinfo.setDicts(dicts);
				
		// 获取该客户的维保合同
		MisContract mc_in = new MisContract();
		mc_in.setCustomerno(st.getCustomerno());
		// 设置为当年第一天
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		c.clear();
		c.set(Calendar.YEAR, year);
		mc_in.setBeginBegindate(c.getTime());
		// 每年，维保合同只有一个
		List<MisContract> mc_list = mcDao.findList(mc_in);
		if(mc_list != null && mc_list.size() > 0){
			MisContract mc = mc_list.get(0);
			jtinfo.setContract("开始时间：" + sdf.format(mc.getBegindate()) + "，结束时间：" + sdf.format(mc.getEnddate()) + "\r\n合同内容：" + mc.getContent());
		}
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	
	// 項目經理指派任務內容
	@RequestMapping(value = "getTaskDetailFromProjMnger")
	public String getTaskDetailFromProjMnger(String userId,String taskId,
			HttpServletRequest request, HttpServletResponse response){
		jsonTaskInfo jtinfo = new jsonTaskInfo();
		// 任务相关
		SmartTask st = stDao.get(taskId);
		jtinfo.setCallLog(st.getRecord());
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		jtinfo.setServiceDate(sdf.format(st.getServiceDate()));		
		// 获取客户信息
		SmartCustomer customer = DictUtils.getCustomer(st.getCustomerno());
		jtinfo.setCustomerName(customer.getCustomername());
		User user=new User();
		user.setArea(customer.getArea());
		jtinfo.setEngineers(uDao.findList(user));
		jtinfo.setDicts(DictUtils.getDictList("mis_servicetype"));
						
		// 获取该客户的维保合同
		MisContract mc_in = new MisContract();
		mc_in.setCustomerno(st.getCustomerno());
		// 设置为当年第一天
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		c.clear();
		c.set(Calendar.YEAR, year);
		mc_in.setBeginBegindate(c.getTime());
		// 每年，维保合同只有一个
		List<MisContract> mc_list = mcDao.findList(mc_in);
		if(mc_list != null && mc_list.size() > 0){
			MisContract mc = mc_list.get(0);
			jtinfo.setContract("开始时间：" + sdf.format(mc.getBegindate()) + "，结束时间：" + sdf.format(mc.getEnddate()) + "\r\n合同内容：" + mc.getContent());
		}
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jtinfo) + ")";
		return renderString(response, sRet, "application/json");
	}
	// 項目經理指派任務生成工單
	@RequestMapping(value = "createOrderByProjMnger")
	public String createOrderByProjMnger(String userId,String taskId,String serviceType,String engineers,
			HttpServletRequest request, HttpServletResponse response){
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		// 任务相关
		SmartTask st = stDao.get(taskId);
		if (st.getStatus().equals("1")) { // 待抢单
			jr.setErrCode("1");// 失败
			jr.setErrMsg("任务已指派");
			String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, sRet, "application/json");
		} 
		String[] array = engineers.split(",");
		for (String str : array) {
			if(StringUtils.isBlank(str)){
				continue;
			}			
			SmartOrder so = new SmartOrder();
			so.setTaskno(taskId);
			so.setAssignedno(uDao.get(str));
			so.setCustomerno(st.getCustomerno());
			so.setCreateBy(UserUtils.get(userId));
			so.setCreateDate(new Date());
			so.setServicetype(serviceType);
			so.setServicedocno(IdGen.uuid());
			so.setStatus("0");//未受理状态
			so.setIsassigned(st.getSendtype());
			so.setRecord(st.getRecord());
			so.setOrderserialno(StringUtils.createOrderNo(DictUtils.getCustomerArea(st.getCustomerno(), "HBQ"), "smart_order", "orderSerialNo"));
			smartOrderService.save(so);	
			
			DictUtils.postWeixinTask(so.getId(), "13", uDao.get(str).getEmail(), DictUtils.getCustomerName(st.getCustomerno(),""), st.getRecord(), 0);
		}

		st.setStatus("1");
		stDao.update(st);
		jr.setErrCode("0");// 成功
		jr.setErrMsg("指派任务发布成功");
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}
	
	// 客户-应急任务查询内容
	@RequestMapping(value = "custEmergencyTaskSave")
	public String custEmergencyTaskSave(String custId,String record,String serviceAddr,String serviceDate,String imgs,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ParseException {
		jsonReturn jr = new jsonReturn();
		SmartTask smartTask = new SmartTask();
		smartTask.setCustomerno(custId);
		smartTask.setIncomingtime(new Date());
		smartTask.setCreateDate(new Date());
		smartTask.setIncomingway("3");//微信
		smartTask.setRecord(record);//问题
		smartTask.setServiceAddr(serviceAddr);//服务地址
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm");
		smartTask.setServiceDate(sdf.parse(serviceDate));//服务地址
		smartTask.setStatus("0");//预处理状态
		smartTask.setIntegral(0);//积分
		smartTaskService.save(smartTask);
		jr.setErrCode("0");// 成功
		jr.setErrMsg("应急任务发布成功");

		User inU = new User();
		// 用户类型为  项目经理
		inU.setUserType(DictUtils.getDictValue("项目经理", "sys_user_type", ""));
		
		String[] imgUrls = imgs.split(";");
		for (String str : imgUrls) {
			SmartAttachments smartAttachments=new SmartAttachments();
			smartAttachments.setTaskno(smartTask.getId());			
			smartAttachments.setOrderid("");
			smartAttachments.setRemarks(str);;
			smartAttachments.setType("0");
			smartAttachmentsService.save(smartAttachments);
		}		
		
		List<User> projMngers = uDao.findList(inU); //所有项目经理

		SmartCustomer customer = DictUtils.getCustomer(custId); //获得当前客户
		for (User user : projMngers) {
			// 与当前客户区域一致的项目经理，会被发送一个消息
			if(customer.getArea().equals(user.getArea())){
				DictUtils.postWeixinTask(smartTask.getId(), "12", user.getEmail(), customer.getCustomername(), record, 0);
			}
		}		
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}
	// 受理指派任务
	@RequestMapping(value = "assignTask")
	public String assignTask(String orderId, String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		jr.setOrderId(orderId);// 任务id
		if (orderId == null || userId == null) {
			jr.setErrCode("1");// 失败
			jr.setErrMsg("受理失败，未获取任务单号或用户ID");
		} else {

			SmartOrder so_in = new SmartOrder();
			so_in.setTaskno(orderId);// 任务id
			so_in.setAssignedno(UserUtils.getByEmail(userId));
			so_in.setStatus("0");

			List<SmartOrder> listOrder = soDao.findList(so_in);
			if (listOrder.size() ==0) {// 未查询到符合条件工单
				jr.setErrCode("1");// 失败
				jr.setErrMsg("未查询到符合条件工单");
			} else {

				SmartOrder so_out = listOrder.get(0);
				if(so_out.getStatus()=="1"){
					jr.setErrCode("1");// 失败
					jr.setErrMsg("此工单已受理");
					String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
					return renderString(response, sRet, "application/json");
				}
				
				// 更新订单状态
				Date nowtime = new Date();
				so_out.setStatus("1");// 受理成功
				so_out.setResponsetime(nowtime);// 响应时间
				soDao.update(so_out);

				// 更新任务状态
				SmartTask st_in = new SmartTask();
				st_in.setId(so_out.getTaskno());
				SmartTask st_out = stDao.get(st_in);
				st_out.setStatus("2");// 受理成功
				stDao.update(st_out);

				if (!StringUtils.isBlank(so_in.getAssignedno().getMobile())) {
					// 在受理的同时，向mis系统添加短信记录，保证报销流程的进行					
					String smsIndex = MisUtils.sendStartSms(so_out, st_out ,null);
					if(!StringUtils.isBlank(smsIndex)){
						so_out.setSmsIndex1(smsIndex);
						soDao.update(so_out);
						MisUtils.assignSms(so_out, st_out);
					}
				} else {
					logger.error("用户"+so_out.getAssignedno().getName()+"没有维护手机号，未向mis进行短信报备");
				}

				jr.setOrderId(orderId);// 任务id
				jr.setFormId(so_out.getId());// 工单id
				jr.setErrCode("0");// 成功
				jr.setErrMsg("指派任务受理成功");
			}
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}

	// 填写回单—维护服务报告
	@RequestMapping(value = "fillReceiptOrder")
	public String fillReceiptOrder(String formId, String arriveTime, String backTime, String workContent, String solveProblem,String dealProg,
				String workResult,String serviceType,String faultType,String imgs, HttpServletRequest request,
			HttpServletResponse response) {
		jsonReturn jr = new jsonReturn();
		jr.setFormId(formId);// 任务id
		if (formId == null) {
			jr.setErrCode("1");// 失败
			jr.setErrMsg("保存维护服务报告失败，未获取单号");
		} else {
			SmartOrder so_out = soDao.get(formId);
			if (so_out == null) {
				jr.setErrCode("1");// 失败
				jr.setErrMsg("保存维护服务报告失败，未检索到工单单号");
				String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
				return renderString(response, sRet, "application/json");
			} else {
				so_out.setStatus("2");// 已回单
				so_out.setRemarks("");// 备注remarks
				so_out.setTaskcontent(workContent);// 工作内容workContent
				so_out.setThistimejob(solveProblem);// 本次解决的问题solveProblem
				so_out.setNexttimeplan("");// 下一步工作计划workPlan
				so_out.setEquipmentno("");// 系列号serialNumber
				so_out.setModeltype("");// 机型modelType
				if(!StringUtils.isBlank(serviceType))
				{
					so_out.setServicetype(serviceType);
				}
				
				SimpleDateFormat f = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm");
				Date backDate = null;
				try {
					backDate = f.parse(backTime);
					so_out.setArrivedtime(f.parse(arriveTime));
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
				soDao.update(so_out);// 更新工单

				String sTaskId = so_out.getTaskno();
				SmartTask st_in = new SmartTask();
				st_in.setId(sTaskId);
				SmartTask st_out = stDao.get(st_in);
				if(!StringUtils.isBlank(faultType))
				{
					st_out.setFaulttype(faultType);
					stDao.update(st_out);
				}
				User user = UserUtils.get(so_out.getAssignedno().getId());
				String sCustName = DictUtils
						.getCustomer(st_out.getCustomerno()).getCustomername();
				sCustName = StringUtils.isBlank(sCustName) ? "" : sCustName;
				workContent = StringUtils.isBlank(workContent) ? ""
						: workContent;

				String[] imgUrls = imgs.split(";");
				for (String str : imgUrls) {
					if(StringUtils.isBlank(str)){
						continue;
					}
					SmartAttachments smartAttachments=new SmartAttachments();
					smartAttachments.setTaskno(st_out.getId());			
					smartAttachments.setOrderid(so_out.getId());
					smartAttachments.setRemarks(str);;
					smartAttachments.setType("0");
					smartAttachmentsService.save(smartAttachments);
				}			
				// 如果没有填写结束短信，则进行发送结束短信，并归并及填写工作报告
				if (!StringUtils.isBlank(user.getMobile())&& !StringUtils.isBlank(so_out.getSmsIndex1()) && StringUtils.isBlank(so_out.getSmsIndex2())) {
					String smsIndex = MisUtils.sendEndSms(so_out, st_out, workContent);
					if(!StringUtils.isBlank(smsIndex)){
						so_out.setSmsIndex2(smsIndex);
						soDao.update(so_out);
						String pqh = MisUtils.resolveSms(so_out, st_out,backDate,"现场工作", workResult);
						if(!StringUtils.isBlank(pqh)){
							MisUtils.generateReport(so_out, st_out, dealProg, workResult, pqh, arriveTime, backTime,request);
						}	
					}
				} else {
					if(StringUtils.isBlank(user.getMobile()))
					{
						logger.error("用户"+user.getName()+"没有维护手机号，未向mis进行短信报备");
					}else{
						logger.error("已在mis完成工作报告，上条结束短信的Index为："+so_out.getSmsIndex2());						
					}
				}
				jr.setFormId(so_out.getId());// 工单id
				jr.setErrCode("0");// 成功
				jr.setErrMsg("保存维护服务报成功");
			}
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}

	// 待填写工单列表查询
	@RequestMapping(value = "unfillReceiptOrder")
	public String unfillReceiptOrder(String userId, String searchParam,
			HttpServletRequest request, HttpServletResponse response) {
		List<SmartOrder> listOrder;
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		String redirectStr = "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/receiptOrderInput.html";
		if (userId == null) {
			jr.setOrderId(userId);
			jr.setErrCode("1");// 失败
			jr.setErrMsg("查询待填写工单列表失败,用户ID不存在");
			String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, sRet, "application/json");
		}
		
		List<jsonTask> jsonOrderList = Lists.newArrayList();

		SmartOrder so = new SmartOrder();
		User user = UserUtils.getByEmail(userId);
		so.setAssignedno(user);
		so.setStatus("1");// 已受理，未回单
		if (searchParam != null) {// 关键字查询
			so.setRecord(searchParam);
		}
		listOrder = soDao.findList(so);
		for (int i = 0; i < listOrder.size(); i++) {
			SmartOrder so_out = listOrder.get(i);
			jsonTask jsonT = new jsonTask();

			jsonT.setOrderId(so_out.getId());
			jsonT.setOrderHref(redirectStr + "?formId=" + so_out.getId()
					+ "&orderId=" + so_out.getTaskno());
			jsonT.setOrderPrice(so_out.getIntegral() + "");
			jsonT.setOrderSubTitle(DictUtils.getCustomerName(
					so_out.getCustomerno(), ""));
			jsonT.setOrderTime(DateUtils.formatDateTime(so_out.getCreateDate()));
			jsonT.setOrderTitle(so_out.getRecord());
			jsonOrderList.add(jsonT);

		}
		jr.setOrderId(userId);
		jr.setErrCode("0");// 失败
		jr.setErrMsg("查询待填写工单完成");
		String strJsonP = "jsonpCallback({" + "\"status\":"
				+ JsonMapper.toJsonString(jr) + ",\"listTtemData\":"
				+ JsonMapper.toJsonString(jsonOrderList) + "})";
		return renderString(response, strJsonP, "application/json");
	}

	// 待填写评价列表查询
	@RequestMapping(value = "unEvaluationOrderList")
	public String unEvaluationOrderList(String userId,
			HttpServletRequest request, HttpServletResponse response) {
		List<SmartOrder> listOrder;
		jsonReturn jr = new jsonReturn();
		if(StringUtils.isBlank(userId)){
			jr.setErrCode("1");//失败
			jr.setErrMsg("用户在微信端授权未识别，未获取到用户相关信息");
			String r = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
			return renderString(response, r, "application/json");			
		}
		String redirectStr = "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/evaluationOrderDetail.html";

		List<jsonTask> jsonOrderList = Lists.newArrayList();

		SmartOrder so = new SmartOrder();
//		User user = UserUtils.getByEmail(userId);
//		if (user == null) {
//			jr.setOrderId(userId);
//			jr.setErrCode("1");// 失败
//			jr.setErrMsg("查询待填写评价列表失败,用户ID不存在");
//			String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
//			return renderString(response, sRet, "application/json");
//		}
//		so.setAssignedno(user);
		so.setCustomerno(userId);
		so.setStatus("2");// 已填写回单
		listOrder = soDao.findList(so);
		for (int i = 0; i < listOrder.size(); i++) {
			SmartOrder so_out = listOrder.get(i);
			jsonTask jsonT = new jsonTask();
			jsonT.setOrderId(so_out.getId());
			jsonT.setOrderHref(redirectStr + "?taskId=" + so_out.getTaskno()+"&orderId="+so_out.getId());
			jsonT.setOrderPrice(so_out.getIntegral()+"");
			jsonT.setOrderSubTitle(DictUtils.getCustomerName(
					so_out.getCustomerno(), ""));
			jsonT.setOrderTime(DateUtils.formatDateTime(so_out.getCreateDate()));
			jsonT.setOrderTitle(so_out.getRecord());
			jsonOrderList.add(jsonT);

		}
		jr.setOrderId(userId);
		jr.setErrCode("0");// 失败
		jr.setErrMsg("查询待填写评价列表完成");
		String strJsonP = "jsonpCallback({" + "\"status\":"
				+ JsonMapper.toJsonString(jr) + ",\"listTtemData\":"
				+ JsonMapper.toJsonString(jsonOrderList) + "})";
		return renderString(response, strJsonP, "application/json");
	}

	// 回单评价
	@RequestMapping(value = "evaluationOrderDetail")
	public String evaluationOrderDetail(String formId, String completeTask,
			String completeAppointmen, String completeCal,
			String taskEvaluation, String signature,
			HttpServletRequest request, HttpServletResponse response) {
		jsonReturn jr = new jsonReturn();
		jr.setFormId(formId);// 任务id
		if (formId == null) {
			jr.setErrCode("1");// 失败
			jr.setErrMsg("保存回单评价失败，未获取单号");
		} else {
			SmartOrder so_out = soDao.get(formId);
			if (so_out == null) {
				jr.setErrCode("1");// 失败
				jr.setErrMsg("保存回单评价失败，未检索到工单单号");
			} else {
				so_out.setStatus("3");// 已评价
				so_out.setIscomplete(completeTask); // 请用户确认是否完成实施
				so_out.setIsappointment(completeAppointmen); // 是否在规定时间与客户预约
				so_out.setIsontime(completeCal); // 是否在预约时间上门服务
				so_out.setOpinion(taskEvaluation); // 客户意见
				so_out.setAdvice(signature); // 客户建议
				soDao.update(so_out);// 更新工单
				jr.setFormId(so_out.getId());// 工单id
				jr.setErrCode("0");// 成功
				jr.setErrMsg("保存回单评价成功");
			}
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}

	// 退单
	@RequestMapping(value = "quitOrder")
	public String quitOrder(String formId, String reason, String quitType,
			HttpServletRequest request, HttpServletResponse response) {
		jsonReturn jr = new jsonReturn();
		jr.setFormId(formId);// 任务id
		if (formId == null) {
			jr.setErrCode("1");// 失败
			jr.setErrMsg("保存退单失败，未获取单号");
		} else {
			SmartOrder so_out = soDao.get(formId);
			if (so_out == null) {
				jr.setErrCode("1");// 失败
				jr.setErrMsg("保存退单失败，未检索到工单单号");
			} else {
				String weixinUserid = so_out.getAssignedno().getId();
				String weixinUser = UserUtils.get(weixinUserid).getEmail();
				so_out.setStatus(quitType); // 退单状态
				so_out.setIsback("0"); // 是否退单
				so_out.setBackremark(reason); // 退单说明
				soDao.update(so_out);// 更新工单
				// 更新任务状态
				String sTaskId = so_out.getTaskno();
				SmartTask st_in = new SmartTask();
				st_in.setId(sTaskId);
				SmartTask st_out = stDao.get(st_in);
				st_out.setStatus("0");// 待处理
				stDao.update(st_out);
				// 发送退单消息
				String sCustName = DictUtils
						.getCustomer(st_out.getCustomerno()).getCustomername();
				DictUtils.postWeixinTask(formId, quitType, weixinUser,
						sCustName, reason, st_out.getIntegral());
				jr.setFormId(so_out.getId());// 工单id
				jr.setErrCode("0");// 成功
				jr.setErrMsg("保存退单成功");
			}
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(jr) + ")";
		return renderString(response, sRet, "application/json");
	}

	/**
	 * 
	 * 
	 * @param signature
	 *            微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
	 * @param timestamp
	 *            时间戳
	 * @param nonce
	 *            随机数
	 * @param echostr
	 *            随机数
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public String get(String signature, String timestamp, String nonce,
			String echostr, HttpServletRequest request) {

		System.out
				.println("=============================================== get start");
		for (Object o : request.getParameterMap().keySet()) {
			System.out.println(o + " = " + request.getParameter((String) o));
		}
		System.out
				.println("=============================================== get end");

		// 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
		if (WiexinSignUtil.checkSignature(signature, timestamp, nonce)) {
			return echostr;
		}

		return "";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public String post(String signature, String timestamp, String nonce,
			String echostr, HttpServletRequest request) {
		System.out
				.println("=============================================== post start");
		for (Object o : request.getParameterMap().keySet()) {
			System.out.println(o + " = " + request.getParameter((String) o));
		}
		System.out
				.println("=============================================== post end");
		StringBuilder result = new StringBuilder();
		result.append("<xml>" + "<ToUserName><![CDATA[toUser]]></ToUserName>"
				+ "<FromUserName><![CDATA[fromUser]]></FromUserName>"
				+ "<CreateTime>12345678</CreateTime>"
				+ "<MsgType><![CDATA[text]]></MsgType>"
				+ "<Content><![CDATA[你好]]></Content>" + "</xml>");
		return result.toString();
	}

}
