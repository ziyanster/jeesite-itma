package com.thinksoft.wechat.service.impl.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.ManageUserService;
import com.thinksoft.wechat.utils.HttpUtils;

public class ManageUserServiceImpl extends ManageUserService {
	
	@Autowired
	private WechatToken token ; 
	
	@Override
	public List<String> getAllUser() {
        return getAllOpenId(); 
	}
	
	@Override
	public String getUserId(String url, String code) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String addGroup(String groupInfo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String addUser(String userInfo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getUserInfo(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDepartmentUserInfoList(String departmentId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String inviteFriend(String userId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override 
	public String deleteUser(String userId) {
		return null; 
	}
	
	@Override 
	public String updateUser(String userInfo) {
		return null; 
	}
	
	private List<String> getAllOpenId() {
		int totalCount = 0; 
		String nextId = ""; 
		int currentCount = 0; 
		List<String> ret = new ArrayList<String>(); 	
		do {	
			HttpURLConnection conn = null; 
			try {
				URL url = new URL(buildGetOpenIdUrl(nextId));
				conn = (HttpURLConnection)url.openConnection();
				conn.setRequestMethod("GET");
				if (conn.getResponseCode() == 200) {		
					String jsonStr = HttpUtils.retrieveInput(conn); 
				    JSONObject jo = new JSONObject(jsonStr); 
				    totalCount = jo.getInt("total"); 
				    currentCount += jo.getInt("count"); 
				    nextId = jo.getString("next_openid"); 
				    JSONArray ja = jo.getJSONObject("data").getJSONArray("openid"); 
				    for (int i = 0; i < ja.length(); ++i) {
				    	ret.add(ja.getString(i)); 
				    }
				} else {
					break; 
				}
		
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace(); 
			}
			
		}while(currentCount < totalCount); 
		return ret; 
	}
	
	private String buildGetOpenIdUrl(String nextId) {
		String HTTP_SERVER = "https://api.weixin.qq.com/cgi-bin/user/get?"; 
		String ACCESS_TOKEN = "access_token=" + token.getToken(); 
		String NEXT_OPEN_ID = "next_openid=" + nextId; 
		StringBuilder strBuilder = new StringBuilder(); 
		strBuilder.append(HTTP_SERVER); 
		strBuilder.append(ACCESS_TOKEN); 
		strBuilder.append("&"); 
		strBuilder.append(NEXT_OPEN_ID); 
		return strBuilder.toString(); 
	}



}
