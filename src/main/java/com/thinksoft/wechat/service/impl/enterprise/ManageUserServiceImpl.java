package com.thinksoft.wechat.service.impl.enterprise;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.thinksoft.wechat.models.CorpInfo;
import com.thinksoft.wechat.models.WechatError;
import com.thinksoft.wechat.models.WechatGroup;
import com.thinksoft.wechat.models.WechatInvitation;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.models.WechatUser;
import com.thinksoft.wechat.service.ManageUserService;
import com.thinksoft.wechat.utils.HttpUtils;

public class ManageUserServiceImpl extends ManageUserService {

	private final static String KEY_USER_ID = "UserId";
	//ssprivate final static String KEY_OPEN_ID = "OpenId";

	@Autowired
	private WechatToken token;

	@Autowired
	private CorpInfo corpInfo;

	@Override
	public List<String> getAllUser() {
		return null;
	}

	@Override
	public String getUserId(String url, String code) {
		if (code == null || code.equals("")) {
			return buildRedirectUrl(url);
		} else {
			HttpsURLConnection conn = null;
			String ret = "";
			try {
				URL queryUrl = new URL(buildQueryUserUrl(code));
				conn = (HttpsURLConnection) queryUrl.openConnection();
				if (conn.getResponseCode() == 200) {
					ret = HttpUtils.retrieveInput(conn);
					JSONObject jo = new JSONObject(ret);
					if(jo.has("errcode")){
						ret = "";
					}else
					{
						ret = jo.getString(KEY_USER_ID);
					}
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return ret;
		}
	}

	@Override
	public String addGroup(String groupInfo) {
		HttpsURLConnection conn = null;
		groupInfo = WechatGroup.toJsonString(WechatGroup.fromJsonString(groupInfo));
		String ret = "";
		try {
			URL url = new URL(buildCreateGroupUrl());
			conn = (HttpsURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(groupInfo, conn);
			ret = HttpUtils.retrieveInput(conn);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	@Override
	public String addUser(String userInfo) {
		HttpsURLConnection conn = null;
		userInfo = WechatUser.toJsonString(WechatUser.fromJsonString(userInfo));
		String ret = "";
		try {
			URL url = new URL(buildCreateUserUrl());
			conn = (HttpsURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(userInfo, conn);
			ret = HttpUtils.retrieveInput(conn);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}
	
	@Override
	public String updateUser(String userInfo) {
		HttpsURLConnection conn = null;
		userInfo = WechatUser.toJsonString(WechatUser.fromJsonString(userInfo));
		String ret = "";
		try {
			URL url = new URL(buildUpdateUserUrl());
			conn = (HttpsURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(userInfo, conn);
			ret = HttpUtils.retrieveInput(conn);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}
	
	@Override 
	public String deleteUser(String userId) {
		HttpsURLConnection conn = null;
		String ret = "";
		try {
			URL url = new URL(buildDeleteUserUrl(userId));
			conn = (HttpsURLConnection) url.openConnection();
			if (conn.getResponseCode() == 200) {
				ret = HttpUtils.retrieveInput(conn);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public String getUserInfo(String userId) {
		HttpsURLConnection conn = null;
		String ret = "";
		try {
			URL url = new URL(buildGetUserInfoUrl(userId));
			conn = (HttpsURLConnection) url.openConnection();
			conn.connect();
			if (conn.getResponseCode() == 200) {
				String tmp = HttpUtils.retrieveInput(conn);
				WechatError errorMessage = WechatError.fromJsonString(tmp);
				if (errorMessage.isError()) {
					ret = WechatError.toJsonString(errorMessage);
				} else {
					WechatUser user = WechatUser.fromJsonString(tmp);
					ret = WechatUser.toJsonString(user);
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	@Override
	public String getDepartmentUserInfoList(String departmentId) {
		HttpsURLConnection conn = null;
		String ret = "";
		try {
			URL url = new URL(buildGetDepartmentUserInfoListUrl(departmentId));
			conn = (HttpsURLConnection) url.openConnection();
			conn.connect();
			if (conn.getResponseCode() == 200) {
				final String KEY_USER_LIST = "userlist";
				String tmp = HttpUtils.retrieveInput(conn);
				WechatError errorMessage = WechatError.fromJsonString(tmp);
				if (errorMessage.isError()) {
					ret = WechatError.toJsonString(errorMessage);
				} else {
					JSONObject users = new JSONObject(tmp);
					JSONArray userList = users.getJSONArray(KEY_USER_LIST);
					JSONArray retDatas = new JSONArray();
					for (int i = 0; i < userList.length(); ++i) {
						WechatUser user = WechatUser.fromJson(userList
								.getJSONObject(i));
						retDatas.put(WechatUser.toJson(user));
					}
					ret = retDatas.toString();
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
            e.printStackTrace(); 
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	@Override
	public String inviteFriend(String userId) {	
		WechatInvitation invitation = new WechatInvitation(); 
		invitation.setUserId(userId);
		String ret = ""; 
		HttpsURLConnection conn = null; 
		try {
			URL url = new URL(buildInviteFriendUrl());
			conn = (HttpsURLConnection)url.openConnection(); 
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(WechatInvitation.toJsonString(invitation), conn);
			ret = HttpUtils.retrieveInput(conn); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if( conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}
	
	

	private String buildRedirectUrl(String url) {
		StringBuilder builder = new StringBuilder();
		builder.append("https://open.weixin.qq.com/connect/oauth2/authorize?appid=");
		builder.append(corpInfo.getCorpId());
		builder.append("&redirect_uri=");
		builder.append(url);
		builder.append("&response_type=code&scope=snsapi_base#wechat_redirect");
		return builder.toString();
	}

	private String buildQueryUserUrl(String code) {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String CODE = "code=" + code;
		StringBuilder builder = new StringBuilder();
		builder.append(HTTP_SERVER);
		builder.append(ACCESS_TOKEN);
		builder.append("&");
		builder.append(CODE);
		return builder.toString();
	}

	private String buildCreateGroupUrl() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/department/create?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		return HTTP_SERVER + ACCESS_TOKEN;
	}

	private String buildCreateUserUrl() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/user/create?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		return HTTP_SERVER + ACCESS_TOKEN;
	}

	private String buildGetUserInfoUrl(String usrId) {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/user/get?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String USER_ID = "userid=" + usrId;
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&");
		strBuilder.append(USER_ID);
		return strBuilder.toString();
	}

	private String buildGetDepartmentUserInfoListUrl(String departmentId) {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/user/list?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String DEPARTMENT_ID = "department_id=" + departmentId;
		String FETCH_CHILD = "fetch_child=0";
		String STATUS = "status=0";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&");
		strBuilder.append(DEPARTMENT_ID);
		strBuilder.append("&");
		strBuilder.append(FETCH_CHILD);
		strBuilder.append("&");
		strBuilder.append(STATUS);
		return strBuilder.toString();
	}

	private String buildInviteFriendUrl() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/invite/send?"; 
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		return HTTP_SERVER + ACCESS_TOKEN;
	}
	
	private String buildDeleteUserUrl(String userId) {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/user/delete?"; 
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String USER_ID = "userid=" + userId;
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&");
		strBuilder.append(USER_ID);
		return strBuilder.toString();
	}
	
	private String buildUpdateUserUrl() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/user/update?"; 
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		return HTTP_SERVER + ACCESS_TOKEN; 
	}
}
