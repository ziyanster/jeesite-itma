package com.thinksoft.wechat.service.impl.enterprise;


import org.springframework.beans.factory.annotation.Autowired;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.ManageMediaService;


public class ManageMediaServiceImpl extends ManageMediaService {
	
	@Autowired 
	private WechatToken token; 
	
	@Override
	protected  String buildFetchImageUrl(String mediaId) {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/media/get?";
		String ACCESS_TOKEN = "access_token="+ token.getToken(); 
		String MEDIA_ID= "media_id="+ mediaId; 
		StringBuilder strBuilder = new StringBuilder(); 
		strBuilder.append(HTTP_SERVER); 
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&"); 
		strBuilder.append(MEDIA_ID); 
		return strBuilder.toString(); 
	}
	
	@Override
	protected String buildUploadImageUrl() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String TYPE = "type=image";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&");
		strBuilder.append(TYPE);
		return strBuilder.toString();
	}

	@Override
	protected String buildUploadNewsUrl() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/material/add_mpnews?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		return strBuilder.toString();
	}

}
