package com.thinksoft.wechat.service.impl.service;

import com.thinksoft.wechat.models.WechatEvent;
import com.thinksoft.wechat.models.WelcomeMessage;
import com.thinksoft.wechat.service.EventHandler;

public class EventHandlerImpl extends EventHandler {
	
	@Override
	public  String handleEvent(WechatEvent evt) {
		if( evt != null) {
			String event = evt.getEvent(); 
			
			if (event.equals("subscribe")) {
				return onSubscribe(evt); 
			}
			else if (event.equals("unsubscribe")) {
				return onUnsubscribe(evt); 
			}			
		}
		return ""; 
	}
	
	@Override
	protected String onSubscribe(WechatEvent evt) {
		String message = new WelcomeMessage.MessageBuilder().build(evt).toXml(); 
		return message; 
	}
	
	@Override
	protected String onUnsubscribe (WechatEvent evt) {
		return ""; 
	}

}
