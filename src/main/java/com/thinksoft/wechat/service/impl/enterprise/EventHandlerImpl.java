package com.thinksoft.wechat.service.impl.enterprise;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinksoft.wechat.models.WechatEvent;
import com.thinksoft.wechat.models.WelcomeMessage;
import com.thinksoft.wechat.service.EventHandler;
import com.thinksoft.wechat.utils.CryptionUtil;

public class EventHandlerImpl extends EventHandler {
	
	@Autowired
	private CryptionUtil cryptionUtil; 
	
	@Override
	public  String handleEvent(WechatEvent evt) {
		if( evt != null) {
			String event = evt.getEvent(); 
			
			if (event.equals("subscribe")) {
				return onSubscribe(evt); 
			}
			else if (event.equals("unsubscribe")) {
				return onUnsubscribe(evt); 
			}			
		}
		return ""; 
	}
	
	@Override
	protected String onSubscribe(WechatEvent evt) {
		String message = new WelcomeMessage.MessageBuilder().build(evt).toXml(); 
		message = cryptionUtil.encryptMsg(message); 
		System.out.println(message); 
		return message; 
	}
	
	@Override
	protected String onUnsubscribe (WechatEvent evt) {
		return ""; 
	}

}
