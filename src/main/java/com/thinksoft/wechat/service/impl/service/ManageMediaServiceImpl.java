package com.thinksoft.wechat.service.impl.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.ManageMediaService;

public class ManageMediaServiceImpl extends ManageMediaService{

	@Autowired
	private WechatToken token;

	@Override
	protected String buildUploadImageUrl() {
		String HTTP_SERVER = "https://api.weixin.qq.com/cgi-bin/media/upload?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String TYPE = "type=image";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&");
		strBuilder.append(TYPE);
		return strBuilder.toString();
	}
	
	@Override
	protected String buildUploadNewsUrl() {
		String HTTP_SERVER = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?";
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(HTTP_SERVER);
		strBuilder.append(ACCESS_TOKEN);
		return strBuilder.toString();
	}

	@Override
	protected String buildFetchImageUrl(String mediaId) {
		// TODO Auto-generated method stub
		return null;
	}

}
