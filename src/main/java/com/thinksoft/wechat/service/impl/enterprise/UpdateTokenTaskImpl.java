package com.thinksoft.wechat.service.impl.enterprise;

import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.UpdateTokenTask;

public class UpdateTokenTaskImpl extends UpdateTokenTask {

	@Override
	protected String buildURLString() {
		String HTTP_SERVER = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?"; 
		String CORP_ID = "corpid=wx57f9d1c45c99a814"; 
		String SECRET = "corpsecret=kM2FjpC2jNul7VkWH7PCh4ouKn5kWssCOmB-Sibjz7wVqec-VtldYdvR7fp_TIr8"; 
		StringBuilder strBuilder = new StringBuilder(); 
		strBuilder.append(HTTP_SERVER); 
		strBuilder.append(CORP_ID); 
		strBuilder.append("&"); 
		strBuilder.append(SECRET); 
		return strBuilder.toString(); 
	}

	@Override
	protected String buildJsTicketUrl(WechatToken token) {
		String HTTP_SERVER="https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?";
		String ACCESS_TOKEN = "access_token="+token.getToken(); 
		return HTTP_SERVER + ACCESS_TOKEN; 
	}

}
