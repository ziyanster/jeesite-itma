package com.thinksoft.wechat.service.impl.service;


import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.UpdateTokenTask;

public class UpdateTokenTaskImpl extends UpdateTokenTask {

    @Override
	protected  String buildURLString() {
		String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?";
		String GRANT_TYPE="grant_type=client_credential"; 
		String APP_ID = "appid=wxe4b9808e26605858"; 
		String APP_SECRET = "secret=40de3b637599fdb4548eb0611033e95f"; 
		StringBuilder strBuilder = new StringBuilder(); 
		strBuilder.append(TOKEN_URL); 
		strBuilder.append(GRANT_TYPE);
		strBuilder.append("&"); 
		strBuilder.append(APP_ID); 
		strBuilder.append("&"); 
		strBuilder.append(APP_SECRET); 
		return strBuilder.toString(); 
	}

	@Override
	protected String buildJsTicketUrl(WechatToken token) {
		String HTTP_SERVER = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?"; 
		String ACCESS_TOKEN = "access_token=" + token.getToken();
		String TYPE="type=jsapi"; 
		StringBuilder strBuilder = new StringBuilder(); 
		strBuilder.append(HTTP_SERVER); 
		strBuilder.append(ACCESS_TOKEN);
		strBuilder.append("&"); 
		strBuilder.append(TYPE); 
		return strBuilder.toString(); 
	}
}
