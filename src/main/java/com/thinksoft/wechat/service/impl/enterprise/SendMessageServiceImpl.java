package com.thinksoft.wechat.service.impl.enterprise;

import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinksoft.wechat.models.WechatMessage;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.ManageUserService;
import com.thinksoft.wechat.service.SendMessageService;

public class SendMessageServiceImpl extends SendMessageService {
	
	@Autowired
	private WechatToken token ; 
		
	@Autowired
	private ManageUserService manageUserService;

	@Override
	protected String buildSendToAllUrl() {
	     return buildSendToUserUrl(); 
	}

	@Override
	protected String buildSendToUserUrl() {
		String HTTP_SERVER="https://qyapi.weixin.qq.com/cgi-bin/message/send?";
		String TOKEN = "access_token=" + token.getToken();
		return HTTP_SERVER + TOKEN ;
	}
	
	@Override
	protected String buildSendToSomeoneUrl() {
	     return buildSendToUserUrl(); 
	}

	@Override
	protected String buildSendAllJsonStr(WechatMessage message, String agentId) {
        JSONObject jo = new JSONObject(); 
        jo.put("touser", "@all");
        jo.put("msgtype", "news");
        jo.put("agentid", Integer.parseInt(agentId)); 
        JSONObject ja = new JSONObject(message.toJson());
        jo.put("news", ja); 
		return jo.toString();
	}
	
	@Override
	protected String buildSendOneJsonStr(WechatMessage message, String agentId, String user) {
        JSONObject jo = new JSONObject(); 
        jo.put("touser", user);
        jo.put("msgtype", "news");
        jo.put("agentid", Integer.parseInt(agentId)); 
        JSONObject ja = new JSONObject(message.toJson());
        jo.put("news", ja); 
		return jo.toString();
	}
	
	@Override
	protected String buildSendSomeoneJsonStr(WechatMessage message,
			String agentId, String users,String parties) {
        JSONObject jo = new JSONObject(); 
        jo.put("touser", users);
        jo.put("toparty", parties);
        jo.put("msgtype", "news");
        jo.put("agentid", Integer.parseInt(agentId)); 
        JSONObject ja = new JSONObject(message.toJson());
        jo.put("news", ja); 
		return jo.toString();
	}



}
