package com.thinksoft.wechat.service.impl.service;

import java.util.List;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinksoft.wechat.models.WechatMessage;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.ManageMediaService;
import com.thinksoft.wechat.service.ManageUserService;
import com.thinksoft.wechat.service.SendMessageService;


public class SendMessageServiceImpl extends SendMessageService {
	
	@Autowired
	private WechatToken token ; 
	
	@Autowired
	private ManageMediaService manageMediaService; 
	
	@Autowired
	private ManageUserService manageUserService; 
	

    @Override	
	protected String buildSendToAllUrl() {		
		String HTTP_SERVER = "https://api.weixin.qq.com/cgi-bin/message/mass/send?"; 
		String ACCESS_TOKEN = "access_token=" + token.getToken(); 
		return HTTP_SERVER + ACCESS_TOKEN; 
	}
    
    @Override 
    protected String buildSendToUserUrl() {
    	return ""; 
    }
	
    @Override 
	protected  String buildSendAllJsonStr (WechatMessage message , String agentId) {	
		JSONObject jo = new JSONObject(); 
		//media_id
		String media_id = manageMediaService.uploadNews(message); 
		JSONObject newsJo = new JSONObject(); 
		newsJo.put("media_id", media_id); 
		jo.put("mpnews", newsJo); 
		//open_id
		List<String> openIdList = manageUserService.getAllUser();
		JSONArray openIdArray = new JSONArray(); 
		for (String item : openIdList) {
			openIdArray.put(item); 
		}
		jo.put("touser", openIdArray); 
		//msgtype
		jo.put("msgtype", "mpnews");
		return jo.toString();
	}
    
    @Override 
    protected String buildSendOneJsonStr (WechatMessage message , String agentId,  String user){
    	return ""; 
    }

	@Override
	protected String buildSendToSomeoneUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String buildSendSomeoneJsonStr(WechatMessage message,
			String agentId, String users, String parties) {
		// TODO Auto-generated method stub
		return null;
	}


}
