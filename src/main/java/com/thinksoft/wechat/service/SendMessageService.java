package com.thinksoft.wechat.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.thinksoft.wechat.models.WechatMessage;
import com.thinksoft.wechat.utils.HttpUtils;


public abstract class SendMessageService {
	
	public  String sendMessageToAll(WechatMessage message, String agentId) {
		HttpURLConnection conn = null; 
		String ret = ""; 
		try {
			URL url = new URL(buildSendToAllUrl());
			conn = (HttpURLConnection)url.openConnection();
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(buildSendAllJsonStr(message, agentId), conn);
			ret = HttpUtils.retrieveInput(conn); 
			System.out.println(ret); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null ) {
				conn.disconnect();
			}
		}
		return ret; 
	}
	public  String sendMessageToSomeone(WechatMessage message, String agentId,String users ,String parties) {
		HttpURLConnection conn = null; 
		String ret = ""; 
		try {
			URL url = new URL(buildSendToSomeoneUrl());
			conn = (HttpURLConnection)url.openConnection();
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(buildSendSomeoneJsonStr(message, agentId ,users ,parties), conn);
			ret = HttpUtils.retrieveInput(conn); 
			System.out.println(ret); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null ) {
				conn.disconnect();
			}
		}
		return ret; 
	}
	
	public  String sendMessage(WechatMessage message , String agentId, String user) {
		HttpURLConnection conn = null; 
		String ret = ""; 
		try {
			URL url = new URL(buildSendToAllUrl());
			conn = (HttpURLConnection)url.openConnection();
			HttpUtils.enableJsonPost(conn);
			HttpUtils.postJson(buildSendOneJsonStr(message, agentId, user), conn);
			ret = HttpUtils.retrieveInput(conn); 
			System.out.println(ret); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null ) {
				conn.disconnect();
			}
		}
		return ret; 
	}
	
	protected abstract String buildSendToAllUrl(); 
	
	protected abstract String buildSendToUserUrl(); 
	
	protected abstract String buildSendToSomeoneUrl();
	
	protected abstract String buildSendAllJsonStr (WechatMessage message, String agentId); 
	
	protected abstract String buildSendSomeoneJsonStr(WechatMessage message, String agentId, String users,String parties); 
	
	protected abstract String buildSendOneJsonStr(WechatMessage message, String agentId, String user); 
}
