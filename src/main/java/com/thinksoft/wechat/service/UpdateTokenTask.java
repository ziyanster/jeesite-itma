package com.thinksoft.wechat.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinksoft.wechat.models.WechatJsTicket;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.utils.HttpUtils;

public abstract class UpdateTokenTask {
	
	private final static String KEY_TOKEN = "access_token"; 
	
	private final static String KEY_EXPIRED_SECONDS = "expires_in"; 
	
	private final static String KEY_JS_TICKET = "ticket"; 
	
	@Autowired
	private WechatToken token;
	
	@Autowired
	private WechatJsTicket ticket; 
		
	public void updateToken() {
		HttpsURLConnection conn = null; 
	    try {
			URL url = new URL(buildURLString());
			conn = (HttpsURLConnection)url.openConnection(); 
			conn.connect();	
			System.out.println("responseCode = " + conn.getResponseCode());
			if(conn.getResponseCode() == 200 ){
		        String ret = HttpUtils.retrieveInput(conn);  
				JSONObject jo = new JSONObject(ret); 
				System.out.println(jo);
				token.setToken(jo.getString(KEY_TOKEN));
				token.setExpiredSeconds(jo.getInt(KEY_EXPIRED_SECONDS));
				updateJsTicket(token); 
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e ) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}
	
	private  void updateJsTicket(WechatToken token) {
		HttpsURLConnection conn = null; 
	    try {
			URL url = new URL(buildJsTicketUrl(token));
			conn = (HttpsURLConnection)url.openConnection(); 
			conn.connect();	
			System.out.println("responseCode = " + conn.getResponseCode());
			if(conn.getResponseCode() == 200 ){
		        String ret = HttpUtils.retrieveInput(conn);  
				JSONObject jo = new JSONObject(ret); 
				System.out.println(jo); 
                ticket.setTicket(jo.getString(KEY_JS_TICKET));
                ticket.setExpiredSeconds(jo.getInt(KEY_EXPIRED_SECONDS));
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e ) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}
	
	protected abstract String buildJsTicketUrl(WechatToken token); 

	protected abstract  String buildURLString() ; 
}
