package com.thinksoft.wechat.service;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.activiti.engine.impl.util.json.JSONObject;

import com.thinksoft.wechat.models.WechatMessage;
import com.thinksoft.wechat.utils.HttpUtils;

public abstract  class ManageMediaService {
		
	private final  String BOUNDARY = "----------" + System.currentTimeMillis();

	public String fetchImage(String mediaId) {
		HttpsURLConnection conn = null; 
		String ret = ""; 
		try {
			URL url = new URL(buildFetchImageUrl(mediaId)); 
			conn = (HttpsURLConnection) url.openConnection(); 
			conn.connect();
			if(conn.getResponseCode() == 200 ) {
                File dir = new File(getClass().getResource("/").getPath()); 
                String filePath = dir.getAbsolutePath().replace("WEB-INF/classes", "static/itma/wechat/images/");
                String disposition = conn.getHeaderField("Content-disposition"); 
                if(disposition != null) {
                	String fileName = disposition.split("=")[1].replace("\"", "");
                	filePath += fileName; 
                	ret = fileName; 
                }
				HttpUtils.saveBinaryFile(conn.getInputStream(), filePath);
			}		
		} catch (MalformedURLException e){
			e.printStackTrace();
		} catch (IOException e ) {
			e.printStackTrace(); 
		} finally {
			if(conn != null ) {
				conn.disconnect();
			}
		}
		return ret;
	}
	
	public String uploadImage(String fileName) {
		HttpURLConnection conn = null;
		String ret = ""; 
		try {
			URL url = new URL(buildUploadImageUrl());
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableFormPost(conn, BOUNDARY);	
			HttpUtils.postImage(fileName,BOUNDARY, conn);
            ret = HttpUtils.retrieveInput(conn); 
            System.out.println(ret);
            JSONObject jo = new JSONObject(ret); 
            ret = jo.getString("media_id"); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
            if (conn != null) {
            	conn.disconnect();
            }
		}
		return ret;
	}
	
	public String uploadNews(WechatMessage message) {
		HttpURLConnection conn = null; 
		String ret = "";
		try {
			URL url = new URL(buildUploadNewsUrl());
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn); 
			HttpUtils.postJson(message.toJson(), conn); 
			ret = HttpUtils.retrieveInput(conn); 
			JSONObject jo = new JSONObject(ret); 
			ret = jo.getString("media_id"); 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
            if (conn != null) {
            	conn.disconnect();
            }
		}	
		return ret; 
	}

	protected abstract  String buildUploadImageUrl() ; 
	
	protected abstract  String buildUploadNewsUrl() ; 
	
	protected abstract  String buildFetchImageUrl(String mediaId);  
	//public abstract String fetchImage(String mediaId); 

}
