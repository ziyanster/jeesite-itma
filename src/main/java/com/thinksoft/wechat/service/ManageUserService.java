package com.thinksoft.wechat.service;

import java.util.List;


public abstract class ManageUserService {
		
	public abstract  List<String> getAllUser(); 
	
	public abstract  String getUserId(String url, String code); 
	
	public abstract  String addGroup(String groupInfo);  
	
	public abstract  String addUser(String userInfo);
	
	public abstract  String updateUser(String userInfo); 
	
	public abstract  String deleteUser(String userId); 
	
	public abstract  String getUserInfo(String userId); 
	
	public abstract  String getDepartmentUserInfoList(String departmentId); 
	
	public abstract  String inviteFriend(String userId); 
}
