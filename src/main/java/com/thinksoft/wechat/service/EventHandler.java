package com.thinksoft.wechat.service;

import com.thinksoft.wechat.models.WechatEvent;

public abstract class  EventHandler {
	
	abstract public  String handleEvent(WechatEvent evt) ; 
	
	abstract protected String onSubscribe(WechatEvent evt) ; 
	
	abstract protected  String onUnsubscribe (WechatEvent evt) ; 
}
