package com.thinksoft.wechat.controller;


import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thinksoft.wechat.models.AssignTaskMessage;
import com.thinksoft.wechat.models.PromptMessage;
import com.thinksoft.wechat.models.WechatApproval;
import com.thinksoft.wechat.models.WechatEvent;
import com.thinksoft.wechat.models.WechatEventBuilder;
import com.thinksoft.wechat.models.WechatJsTicket;
import com.thinksoft.wechat.models.WechatMessage;
import com.thinksoft.wechat.models.WechatNews;
import com.thinksoft.wechat.service.EventHandler;
import com.thinksoft.wechat.service.ManageMediaService;
import com.thinksoft.wechat.service.ManageUserService;
import com.thinksoft.wechat.service.SendMessageService;
import com.thinksoft.wechat.utils.CryptionUtil;
import com.thinksoft.wechat.utils.HttpUtils;

@Controller
public class WechatController {
	
	@Autowired
	private EventHandler eventHandler;
	
	@Autowired
	private SendMessageService sendMessageService ; 
	
	@Autowired
	private ManageUserService manageUserService;
	
	@Autowired
	private ManageMediaService manageMediaService; 
	
	@Autowired
	private CryptionUtil cryptionUtil; 
	
	@Autowired
	private WechatJsTicket ticket; 
	
	@ModelAttribute
	public void enableCors(HttpServletResponse response ) {
		response.setHeader("Access-Control-Allow-Origin", "*");
	}
	
	@ResponseBody
	@RequestMapping(value="/itma" , method=RequestMethod.POST)
	public String receiveEvents(HttpServletRequest request ) {	
        String content = HttpUtils.retrieveInput(request); 
        content = cryptionUtil.decryptMsg(content, request.getParameterMap()); 
        Document doc = parseContent(content); 
        WechatEvent evt = WechatEventBuilder.build(doc); 
        return eventHandler.handleEvent(evt);
	}
	
	@ResponseBody
	@RequestMapping(value="/itma", method=RequestMethod.GET)
	public String verifyWechatUrl(HttpServletRequest request) {
		System.out.println("verifyWechatUrl....");  
		return cryptionUtil.verifyUrl(request.getParameterMap());
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/grab_task" ,method=RequestMethod.POST)
	public String receiveGrabTask(HttpServletRequest request, @RequestParam("agentId") String agentId ) {
		String content = HttpUtils.retrieveInput(request); 
		WechatMessage msg = (new AssignTaskMessage.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessageToAll(msg, agentId); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/assign_task" , method=RequestMethod.POST)
	public String receiveAssignTask (HttpServletRequest request,  @RequestParam("agentId") String agentId, @RequestParam("user") String user) {
		System.out.println("user = " + user); 
		String content = HttpUtils.retrieveInput(request); 
		WechatMessage msg = (new AssignTaskMessage.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessage(msg, agentId, user); 
	} 
	
	@ResponseBody
	@RequestMapping(value="/itma/service_ranking" , method=RequestMethod.POST)
	public String receiveServiceRanking (HttpServletRequest request,  @RequestParam("agentId") String agentId, @RequestParam("user") String user) {
		System.out.println("user = " + user); 
		String content = HttpUtils.retrieveInput(request); 
		WechatMessage msg = (new AssignTaskMessage.MessageBuilder(manageMediaService)).build2(content); 
		return sendMessageService.sendMessage(msg, agentId, user); 
	} 

	@ResponseBody
	@RequestMapping(value="/itma/assign_approval" , method=RequestMethod.POST)
	public String receiveAssignApproval (HttpServletRequest request,@RequestParam("agentId") String agentId, @RequestParam("user") String user) {
		System.out.println("user = " + user); 
		String content = HttpUtils.retrieveInput(request); 
		WechatMessage msg = (new WechatApproval.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessage(msg, agentId, user); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/send_prompt", method=RequestMethod.POST)
	public String sendPrompt(HttpServletRequest request,@RequestParam("agentId") String agentId, @RequestParam("user") String user) {
		String content = HttpUtils.retrieveInput(request);
		WechatMessage msg = (new PromptMessage.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessage(msg, agentId,user); 
	}

	@ResponseBody
	@RequestMapping(value="/itma/send_news", method=RequestMethod.POST)
	public String sendNews(HttpServletRequest request, @RequestParam("agentId") String agentId) {
		String content = HttpUtils.retrieveInput(request);
		WechatMessage msg = (new WechatNews.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessageToAll(msg, agentId); 
	}
	@ResponseBody
	@RequestMapping(value="/itma/send_news_tosomeone", method=RequestMethod.POST)
	public String sendNewsToSomeone(HttpServletRequest request, @RequestParam("agentId") String agentId,@RequestParam("users") String users,@RequestParam("parties") String parties) {
		String content = HttpUtils.retrieveInput(request);
		WechatMessage msg = (new WechatNews.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessageToSomeone(msg, agentId ,users ,parties); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/get_jsticket", method=RequestMethod.GET)
	public String getJSTicket(HttpServletRequest request){
		return ticket.getTicket(); 
	}

	@ResponseBody
	@RequestMapping(value="/itma/get_userid", method=RequestMethod.GET)
	public String getUserId(@RequestParam(value="url",required=false) String url, 	
                            @RequestParam(value="code", required=false) String code,
                            HttpServletResponse response) {
		if(url != null ) {
			//System.out.println("url=" + url); 
		}
		
		if (code != null) {
			//System.out.println("code = " + code); 
		}
		
        String ret = manageUserService.getUserId(url, code); 
        
        try {
            if(code == null){
                HttpURLConnection conn = (HttpURLConnection) new URL(ret).openConnection();
        		String r = HttpUtils.retrieveInput(conn);
        		
            }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
        //System.out.println(ret); 
        
//        if(code != null ) {
//        	response.addCookie(new Cookie("user_id", ret));
//        }
        return ret; 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/fetch_media")
	public String fetchImage(HttpServletRequest request , 
			@RequestParam(value="media_id", required=true) String media_id) {
		String url = request.getRequestURL().toString(); 
		url = url.replace("/itma/fetch_media", "/static/itma/wechat/images/");
		url += manageMediaService.fetchImage(media_id);
		System.out.println(url);
		return url;
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/create_group")
	public String createGroup(HttpServletRequest request ){
		String groupInfo = HttpUtils.retrieveInput(request); 
		return manageUserService.addGroup(groupInfo); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/create_user")
	public String createUser(HttpServletRequest request) {
		String userInfo = HttpUtils.retrieveInput(request); 
		return manageUserService.addUser(userInfo); 
	}
	
	
	@ResponseBody
	@RequestMapping(value="/itma/update_user")
	public String updateUser(HttpServletRequest request) {
		String userInfo = HttpUtils.retrieveInput(request); 
		return manageUserService.updateUser(userInfo); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/delete_user", method=RequestMethod.GET) 
	public String deleteUser(@RequestParam(value="user", required=true) String user) {
		return manageUserService.deleteUser(user); 
	}

	@ResponseBody
	@RequestMapping(value="/itma/get_userinfo")
	public String getUserInfo(@RequestParam(value="user", required=true) String user) {
		return manageUserService.getUserInfo(user); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/get_departmentusers")
	public String getDepartmentUserInfoList( @RequestParam(value="department", required=true) String department) {
		return manageUserService.getDepartmentUserInfoList(department); 
	}
	
	@ResponseBody
	@RequestMapping(value="/itma/invite_friend", method=RequestMethod.GET)
	public String inviteFriend( @RequestParam(value="user",required=true) String user) {
		return manageUserService.inviteFriend(user); 
	}
	
	private Document  parseContent(String content) {
		Document ret = null; 
		try {
			if(content.equals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")){
				content = content +"<root></root>";
			}
			ret = DocumentHelper.parseText(content);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return ret; 
	}

	
	@ResponseBody
	@RequestMapping(value="/oa" , method=RequestMethod.POST)
	public String receiveOAEvents(HttpServletRequest request ) {	
        String content = HttpUtils.retrieveInput(request); 
        content = cryptionUtil.decryptMsg(content, request.getParameterMap()); 
        Document doc = parseContent(content); 
        WechatEvent evt = WechatEventBuilder.build(doc); 
        return eventHandler.handleEvent(evt);
	}
	
	@ResponseBody
	@RequestMapping(value="/oa", method=RequestMethod.GET)
	public String verifyOaWechatUrl(HttpServletRequest request) {
		System.out.println("verifyWechatUrl....");  
		return cryptionUtil.verifyUrl(request.getParameterMap());
	}
	
	@ResponseBody
	@RequestMapping(value="/oa/assign_approval" , method=RequestMethod.POST)
	public String receiveOaApproval (HttpServletRequest request, @RequestParam("agentId") String agentId, @RequestParam("user") String user) {
		System.out.println("user = " + user); 
		String content = HttpUtils.retrieveInput(request); 
		WechatMessage msg = (new WechatApproval.MessageBuilder(manageMediaService)).build(content); 
		return sendMessageService.sendMessage(msg,agentId, user); 
	}	
}
