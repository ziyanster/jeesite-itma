package com.thinksoft.wechat.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

import javax.servlet.http.HttpServletRequest;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;

public class HttpUtils {
	
	public static String retrieveInput(HttpURLConnection conn){
		String ret = ""; 
		try {
			char[] buffer = new char[1024]; 
			int readCount = 0 ; 
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			StringBuilder strBuilder = new StringBuilder(); 		
			while ((readCount = in.read(buffer)) > 0) {
				strBuilder.append(buffer, 0, readCount);
			}
			ret = strBuilder.toString(); 
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return ret; 
	}
	public static String retrieveInput(HttpURLConnection conn,String charset){
		String ret = ""; 
		try {
			char[] buffer = new char[1024]; 
			int readCount = 0 ; 
			InputStreamReader in = new InputStreamReader(conn.getInputStream(),charset);
			StringBuilder strBuilder = new StringBuilder(); 		
			while ((readCount = in.read(buffer)) > 0) {
				strBuilder.append(buffer, 0, readCount);
			}
			ret = strBuilder.toString(); 
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return ret; 
	}
	
	public static String retrieveInput(HttpServletRequest request) {
		BufferedReader reader = null; 
		StringBuilder strBuilder = new StringBuilder(); 
		try{
			reader = new BufferedReader(new InputStreamReader(request.getInputStream())); 
			char[] buffer = new char[1024]; 
			int readCount = 0; 
			while ((readCount = reader.read(buffer)) > 0 ){
				strBuilder.append(buffer, 0, readCount); 
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null ) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}	
		return strBuilder.toString(); 
	}
	
	public static void enableFormPost(HttpURLConnection conn, String boundary) {	
		try {
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:39.0) Gecko/20100101 Firefox/39.0");
			conn.setRequestProperty("Charset", "UTF-8"); 
			conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
			conn.setRequestMethod("POST");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	}
	
	public static  void enableJsonPost(HttpURLConnection conn ){
		try {
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	}
	
	public static void postJson(String jsonStr,  HttpURLConnection conn) throws IOException {
		OutputStream out = new DataOutputStream(conn.getOutputStream());
		out.write(jsonStr.getBytes("utf-8"));
		out.flush(); 
		out.close(); 
	}
	
	public static String postJsonResp(String jsonStr,  HttpURLConnection conn) throws IOException{
		OutputStream out = new DataOutputStream(conn.getOutputStream());
		out.write(jsonStr.getBytes("utf-8"));
		out.flush(); 
		out.close(); 
		StringBuffer sb = new StringBuffer();   
		String readLine;   
		BufferedReader responseReader;   
		// 处理响应流，必须与服务器响应流输出的编码一致   
		responseReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));   
		while ((readLine = responseReader.readLine()) != null) {   
			sb.append(readLine).append("\n");   
		}   
		responseReader.close();   
		return sb.toString();
	}
	
	public static  void postImage(String fileName, String boundary, HttpURLConnection conn) throws IOException {		
		File file = new File(fileName); 
		OutputStream out = new DataOutputStream(conn.getOutputStream());
		String contentType = "image/jpeg";
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("--").append(boundary).append("\r\n");
		strBuilder.append("Content-Disposition: form-data;  name=\"media\"; filename=\"" + file.getName() + "\"\r\n");
		strBuilder.append("Content-Type:" + contentType + "\r\n\r\n");
	    out.write(strBuilder.toString().getBytes("utf-8"));	    
		DataInputStream ins = new DataInputStream(new FileInputStream (file)); 
		byte[] buffer = new byte[1024]; 
		int readCount = 0; 
		while ((readCount = ins.read(buffer)) != -1) {
			out.write(buffer, 0, readCount);
		}
	    out.write(("\r\n--" + boundary + "--\r\n").getBytes("utf-8")); 
		out.flush();
		out.close();
		ins.close();
	}
	
	public static void postFile(String fileName,String contentType,InputStream fis, String boundary, HttpURLConnection conn) throws IOException {	
		OutputStream out = new DataOutputStream(conn.getOutputStream());  
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("--").append(boundary).append("\r\n");
		strBuilder.append("Content-Disposition: form-data;  name=\"media\"; filename=\"" + fileName + "\"\r\n");
		strBuilder.append("Content-Type:" + contentType + "\r\n\r\n");
	    out.write(strBuilder.toString().getBytes("utf-8"));	    
		DataInputStream ins = new DataInputStream(fis); 
		byte[] buffer = new byte[1024]; 
		int readCount = 0; 
		while ((readCount = ins.read(buffer)) != -1) {
			out.write(buffer, 0, readCount);
		}
	    out.write(("\r\n--" + boundary + "--\r\n").getBytes("utf-8")); 
		out.flush();
		out.close();
		ins.close();
	}
	
	public static void saveBinaryFile(InputStream ins , String fileName) {
		BufferedOutputStream bos = null; 
        try {
        	File file = new File(fileName);
        	if(!file.exists()) {
        		file.createNewFile(); 
        	}
        	bos = new BufferedOutputStream(new FileOutputStream(file)); 
			byte[] buffer = new byte[1024]; 
			int count = 0; 
			while ((count = ins.read(buffer, 0, 1024)) > 0) {
				bos.write(buffer, 0, count);
			}
			bos.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(bos != null ) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
