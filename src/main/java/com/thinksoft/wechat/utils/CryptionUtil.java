package com.thinksoft.wechat.utils;

import java.util.Calendar;
import java.util.Map;
import java.util.Random;
import com.qq.weixin.mp.aes.AesException;
import com.qq.weixin.mp.aes.WXBizMsgCrypt;

public class CryptionUtil {
	
    private WXBizMsgCrypt crypt; 
    
    public CryptionUtil( WXBizMsgCrypt crypt ) {
    	this.crypt = crypt; 
    }
    
    public String decryptMsg(String msg, Map paramMap) {
    	String signature = ((String[])paramMap.get("msg_signature"))[0];
    	String timestamp = ((String[])paramMap.get("timestamp"))[0]; 
    	String nonce = ((String[])paramMap.get("nonce"))[0]; 
    	String ret =""; 
    	try {
			ret = crypt.DecryptMsg(signature, timestamp, nonce, msg);
		} catch (AesException e) {
			e.printStackTrace();
		} 
    	return ret; 
    }
    
    public String verifyUrl(Map paramMap) {
    	String ret = ""; 
    	String signature = ((String[])paramMap.get("msg_signature"))[0];
    	String timestamp = ((String[])paramMap.get("timestamp"))[0]; 
    	String nonce = ((String[])paramMap.get("nonce"))[0];
    	String echoStr = ((String[])paramMap.get("echostr"))[0];
    	try {
			 ret = crypt.VerifyURL(signature, timestamp, nonce, echoStr);
		} catch (AesException e) {
			e.printStackTrace();
		}
    	return ret; 
    }
    
    public String encryptMsg(String msg) {
    	long time = Calendar.getInstance().getTimeInMillis(); 
    	String timestamp = String.valueOf(time); 
    	String nonce = String.valueOf(new Random(time).nextInt()); 
    	String ret = ""; 
    	try {
			ret = crypt.EncryptMsg(msg, timestamp, nonce);
		} catch (AesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  	
    	return ret; 
    }
}
