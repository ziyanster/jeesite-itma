package com.thinksoft.wechat.models;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;

import com.thinksoft.wechat.service.ManageMediaService;

public class PromptMessage extends WechatMessage {
	
	private String title; 
	private String description ; 
	private String picUrl ; 
	
	
	private PromptMessage ( MessageBuilder builder) {
		title = builder.title; 
		description = builder.description; 
		picUrl = builder.picUrl; 
	}
		
	public static class MessageBuilder {	
		private ManageMediaService service;	
		private String title; 
		private String description ; 
		private String picUrl ; 
		
		public MessageBuilder( ManageMediaService service) {
			this.service = service; 
		}
		
		private String buildDescription(JSONObject jo) {
			String serviceContent = jo.getString("service_content");
			StringBuilder strBuilder = new StringBuilder(); 
			strBuilder.append(serviceContent + "\n"); 
			return strBuilder.toString(); 
		}

		public PromptMessage  build ( String jsonStr) {	
			JSONObject jo = new JSONObject(jsonStr); 
			title = jo.getString("title"); 
			description = buildDescription(jo); 
			picUrl = jo.getString("picurl"); 
			return new PromptMessage(this); 
		}
	}
	
	@Override
	public String toJson() {
		JSONObject jo = new JSONObject();
		jo.put("title", title); 
        jo.put("description", description);
        jo.put("picurl", picUrl); 
		JSONArray ja = new JSONArray(); 
		ja.put(jo); 
		JSONObject articles = new JSONObject(); 
		articles.put("articles", ja); 
		return articles.toString(); 
	}

	@Override
	public String toXml() {
		// TODO Auto-generated method stub
		return "";
	}

}
