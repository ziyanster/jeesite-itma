package com.thinksoft.wechat.models;

public class WechatToken {
	
	private String token ; 
	
	private int expiredSeconds ;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getExpiredSeconds() {
		return expiredSeconds;
	}

	public void setExpiredSeconds(int expiredSeconds) {
		this.expiredSeconds = expiredSeconds;
	} 

}
