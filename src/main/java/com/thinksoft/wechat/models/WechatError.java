package com.thinksoft.wechat.models;

import org.activiti.engine.impl.util.json.JSONObject;

public class WechatError {
	
	private final static String KEY_ERROR_CODE = "errcode"; 
	
	private final static String KEY_ERROR_MESSAGE = "errmsg"; 
	
	private int errorCode = 0 ; 
	
	private String errorMessage = "";

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	} 
	
	public boolean isError() {
		return  errorCode != 0 ;  
	}
	
	public static  WechatError fromJson(JSONObject jsonError) {
		WechatError error = new WechatError(); 
		if (jsonError.has(KEY_ERROR_CODE)) {
			error.setErrorCode(jsonError.getInt(KEY_ERROR_CODE));
		}
		
		if (jsonError.has(KEY_ERROR_MESSAGE)) {
			error.setErrorMessage(jsonError.getString(KEY_ERROR_MESSAGE));
		}
		return error; 
	}
	
	public static WechatError fromJsonString (String jsonStr) {
		JSONObject jo = new JSONObject(jsonStr); 
		return fromJson(jo); 
	}
	
	public static JSONObject toJson( WechatError wechatError) {
		JSONObject jo = new JSONObject();
		jo.put(KEY_ERROR_CODE, wechatError.getErrorCode()); 
		jo.put(KEY_ERROR_MESSAGE, wechatError.getErrorMessage()); 
		return jo; 
	}
	
	public static String toJsonString( WechatError  wechatError) {
		JSONObject jo = toJson(wechatError); 
		return jo.toString(); 
	}
}
