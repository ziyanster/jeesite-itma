package com.thinksoft.wechat.models;

import org.activiti.engine.impl.util.json.JSONObject;

public class WechatGroup {

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	} 

	
	public static WechatGroup fromJsonString ( String groupInfo) {
		JSONObject jo = new JSONObject(groupInfo); 
		return fromJson(jo); 
	}
	
	public static WechatGroup fromJson(JSONObject jo) {
		WechatGroup group = new WechatGroup(); 
		group.setId(jo.getString(KEY_GROUP_ID));
		group.setName(jo.getString(KEY_GROUP_NAME));
		group.setOrder(jo.getString(KEY_GROUP_ORDER));
		group.setParentId(jo.getString(KEY_GROUP_PARENT_ID));
		return group; 
	}
	
	public static JSONObject toJson(WechatGroup group){
		JSONObject jo = new JSONObject();
		jo.put(KEY_GROUP_ID, group.getId()); 
		jo.put(KEY_GROUP_NAME, group.getName()); 
		jo.put(KEY_GROUP_PARENT_ID, group.getParentId()); 
		jo.put(KEY_GROUP_ORDER, group.getOrder()); 	
		return jo; 
	} 
	
	public static String toJsonString( WechatGroup group) {
		JSONObject jo = toJson(group);
		return jo.toString(); 
	}
	
	
	private String id ; 
	private String name; 
	private String parentId; 
	private String order;
	
	private final static String KEY_GROUP_ID = "id"; 
	
	private final static String KEY_GROUP_NAME = "name"; 
	
	private final static String KEY_GROUP_PARENT_ID = "parentid";
	
	private final static String KEY_GROUP_ORDER = "order"; 

}
