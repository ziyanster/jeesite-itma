package com.thinksoft.wechat.models;

import org.dom4j.Document;
import org.dom4j.Element;

public class WechatEventBuilder {
	
	public static WechatEvent build( Document doc) {
		if(doc==null){
			return null;
		}
		Element root = doc.getRootElement(); 
		String msgType = root.element("MsgType").getText(); 
		String event = root.element("Event").getText(); 
		
		if(msgType.equals("event")) {
			if (event.equals("subscribe") || event.equals("unsubscribe")) {
				return new SubscribeEvent.EventBuilder().build(doc); 
			}
		}
        return null; 
	} 

}
