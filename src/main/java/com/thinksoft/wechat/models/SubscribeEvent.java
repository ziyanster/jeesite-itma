package com.thinksoft.wechat.models;

import org.dom4j.Document;
import org.dom4j.Element;

public class SubscribeEvent extends WechatEvent {

	private String toUserName;

	private String fromUserName;

	private long createTime;

	private String msgType;

	private String event;
	
	
    @Override
	public String getToUserName() {
		return toUserName;
	}

    @Override
	public String getFromUserName() {
		return fromUserName;
	}

	public long getCreateTime() {
		return createTime;
	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public String getEvent() {
		return event;
	}

	private  SubscribeEvent(EventBuilder builder) {	
		toUserName = builder.toUserName; 
		fromUserName = builder.fromUserName; 
		createTime = builder.createTime; 
		msgType = builder.msgType; 
		event = builder.event; 
	}
	
	public static class EventBuilder {		
		private String toUserName;
		private String fromUserName;
		private long createTime;
		private String msgType;
		private String event;
		
		private void doParse(Document doc) {          
			Element root = doc.getRootElement(); 
			toUserName = root.element("ToUserName").getText(); 
			fromUserName = root.element("FromUserName").getText(); 
			createTime = Integer.valueOf(root.element("CreateTime").getText());
			msgType = root.element("MsgType").getText(); 
			event = root.element("Event").getText(); 
		} 
		
		public WechatEvent build(Document doc) {
			doParse(doc); 
			return new SubscribeEvent(this); 
		}
	}

}
