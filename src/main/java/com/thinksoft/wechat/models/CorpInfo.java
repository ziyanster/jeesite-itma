package com.thinksoft.wechat.models;

public class CorpInfo {
	
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getEncryptToken() {
		return encryptToken;
	}

	public void setEncryptToken(String encryptToken) {
		this.encryptToken = encryptToken;
	}

	public String getAesKey() {
		return aesKey;
	}

	public void setAesKey(String aesKey) {
		this.aesKey = aesKey;
	}

	private String corpId; 
	
	private String  encryptToken; 
	
	private String aesKey; 
}
