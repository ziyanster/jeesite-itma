package com.thinksoft.wechat.models;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import com.thinksoft.wechat.service.ManageMediaService;

public class WechatNews extends WechatMessage {

	private String title; 
	private String description ; 
	private String url; 
	private String picUrl ; 
	
	
	private WechatNews ( MessageBuilder builder) {
		title = builder.title; 
		description = builder.description; 
		url = builder.url; 
		picUrl = builder.picUrl; 
	}
		
	public static class MessageBuilder {	
		private ManageMediaService service;	
		private String title; 
		private String description ; 
		private String url; 
		private String picUrl ; 
		
		public MessageBuilder( ManageMediaService service) {
			this.service = service; 
		}
		
		private String buildDescription(JSONObject jo) {
			String customerName = jo.getString("customer_name"); 
			String serviceContent = jo.getString("service_content");
			String serviceReward = jo.getString("service_reward");
			StringBuilder strBuilder = new StringBuilder(); 
			strBuilder.append(customerName + "\n");
			strBuilder.append(serviceContent + "\n"); 
			strBuilder.append(serviceReward + "\n");
			return strBuilder.toString(); 
		}

		public WechatNews  build ( String jsonStr) {	
			JSONObject jo = new JSONObject(jsonStr); 
			title = jo.getString("title"); 
			description = buildDescription(jo); 
			url = jo.getString("url"); 
			picUrl = jo.getString("picurl"); 
			return new WechatNews(this); 
		}
	}
	
	@Override
	public String toJson() {
		JSONObject jo = new JSONObject();
		jo.put("title", title); 
        jo.put("description", description);
        jo.put("url", url); 
        jo.put("picurl", picUrl); 
		JSONArray ja = new JSONArray(); 
		ja.put(jo); 
		JSONObject articles = new JSONObject(); 
		articles.put("articles", ja); 
		return articles.toString(); 
	}

	@Override
	public String toXml() {
		// TODO Auto-generated method stub
		return "";
	}


}
