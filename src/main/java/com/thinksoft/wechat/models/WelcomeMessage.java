package com.thinksoft.wechat.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;


public class WelcomeMessage extends WechatMessage {
	
	private String toUserName ; 
	private String fromUserName; 
	private long createTime; 
	private String msgType ; 
    private List<Article> articles ; 
    
    private static class Article {
    	private String title; 
    	private String description; 
    	private String picUrl; 
    	private String url; 
    	
    	public Element toXmlElement() {
    		Document doc = DocumentHelper.createDocument(); 
    		Element root = doc.addElement("item"); 
    		root.addElement("Title").addCDATA(title); 
    		root.addElement("Description").addCDATA(description); 
    		root.addElement("PicUrl").addCDATA(picUrl); 
    		root.addElement("Url").addCDATA(url); 
    		return root; 
    	}
    }

    private WelcomeMessage(MessageBuilder builder) {
    	toUserName = builder.toUserName; 
    	fromUserName = builder.fromUserName; 
    	createTime = builder.createTime; 
    	msgType = builder.msgType; 
    	articles = builder.articles; 
    }

    
    public static class MessageBuilder {		
    	private String toUserName ; 
    	private String fromUserName; 
    	private long createTime; 
        private String msgType = "news"; 
        private List<Article> articles = new ArrayList<Article>(); 
    	
    	public WechatMessage build (WechatEvent evt) {
    		toUserName = evt.getFromUserName(); 
    		fromUserName = evt.getToUserName(); 
    		createTime = Calendar.getInstance().getTimeInMillis(); 
    		Article article = new Article(); 
            article.title = "欢迎使用智能运维服务平台";
            article.description = "使用方便\n不需要电脑和网线，只要一部可以上网的手机，即使是上班也可以处理一些紧急的事务\n";
            article.description += "高效快捷\n施工现场就可以处理工单，资源信息；上班路上就可以查看报表。"; 
            article.picUrl = "http://wechat.itma.com.cn/jeesite/static/itma/wechat/images/pic-huanying.jpg"; 
            article.url = "http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/login.html"; 
    		articles.add(article); 
    		return new WelcomeMessage(this); 
    	}
    }
    
    @Override 
    public String toXml() {
    	Document doc = DocumentHelper.createDocument(); 
    	Element root = doc.addElement("xml"); 
    	root.addElement("ToUserName").addCDATA(toUserName); 
    	root.addElement("FromUserName").addCDATA(fromUserName); 
    	root.addElement("CreateTime").addText(String.valueOf(createTime)); 
    	root.addElement("MsgType").addCDATA(msgType); 
    	int articleCount = articles.size(); 
    	root.addElement("ArticleCount").addText(String.valueOf(articleCount)); 
    	Element articleList = root.addElement("Articles"); 
    	for (Article article : articles) {
    		Element a = article.toXmlElement(); 
    		articleList.add(a); 
    	}
        return root.asXML(); 
    }

	@Override
	public String toJson() {
		// TODO Auto-generated method stub
		return "";
	}
}
