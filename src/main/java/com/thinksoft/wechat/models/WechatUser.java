package com.thinksoft.wechat.models;

import java.util.ArrayList;
import java.util.List; 

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;

public class WechatUser {
	
	private String  userId ; 
	private String  name;
	private List<Integer> departments; 
	private String  position; 
	private String  mobile; 
	private String  gender ; //1 : man  2: women
	private String  email; 
	private String  wechatId; 
	private String  avatarUrl; //url
	private String  extraAttr; //jsonString
	
	private final static String KEY_USER_ID = "userid"; 
	private final static String KEY_USER_NAME = "name"; 
	private final static String KEY_USER_POSITION = "position"; 
	private final static String KEY_USER_DEPARTMENT = "department"; 
	private final static String KEY_USER_MOBILE = "mobile";
	private final static String KEY_USER_GENDER = "gender"; 
	private final static String KEY_USER_EMAIL = "email";
	private final static String KEY_USER_WECHAT_ID = "weixinid";
	//private final static String KEY_USER_AVATAR_ID = "avatar_mediaid"; 
	private final static String KEY_USER_AVATAR_URL = "avatar_url"; 
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Integer> getDepartments() {
		return departments;
	}
	public void setDepartments(List<Integer> departments) {
		this.departments = departments;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWechatId() {
		return wechatId;
	}
	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public String getExtraAttr() {
		return extraAttr;
	}
	public void setExtraAttr(String extraAttr) {
		this.extraAttr = extraAttr;
	}
	
	public static WechatUser fromJson(JSONObject jo) {
		WechatUser user = new WechatUser(); 
		if(jo.has(KEY_USER_ID)) {
			user.setUserId(jo.getString(KEY_USER_ID));
		} 
		if (jo.has(KEY_USER_NAME)) {
			user.setName(jo.getString(KEY_USER_NAME)); 
		}    	
		if (jo.has(KEY_USER_POSITION)) {
			user.setPosition(jo.getString(KEY_USER_POSITION));
		}
		if (jo.has(KEY_USER_DEPARTMENT)) {
			JSONArray departList = jo.getJSONArray(KEY_USER_DEPARTMENT); 
			ArrayList<Integer> departs = new ArrayList<Integer>(); 
			for (int i = 0; i < departList.length(); ++i ) {
				departs.add(departList.getInt(i)); 
			}
			user.setDepartments(departs);
		}
		if(jo.has(KEY_USER_MOBILE)) {
			user.setMobile(jo.getString(KEY_USER_MOBILE));
		}
		if(jo.has(KEY_USER_EMAIL)) {
			user.setEmail(jo.getString(KEY_USER_EMAIL));
		}
	    if(jo.has(KEY_USER_GENDER)) {
			user.setGender(jo.getString(KEY_USER_GENDER));
	    }
	    if(jo.has(KEY_USER_WECHAT_ID)) {
	    	user.setWechatId(jo.getString(KEY_USER_WECHAT_ID));
	    }
	    if(jo.has(KEY_USER_AVATAR_URL)) {
			user.setAvatarUrl(jo.getString(KEY_USER_AVATAR_URL));
	    }
		return user; 
	}

	public static WechatUser fromJsonString(String userInfo) {
		JSONObject jo = new JSONObject(userInfo);
        return WechatUser.fromJson(jo); 
	}
	
	public static JSONObject toJson(WechatUser user) {
		JSONObject jo = new JSONObject(); 
		jo.put(KEY_USER_ID, user.getUserId());
		jo.put(KEY_USER_NAME, user.getName());
		jo.put(KEY_USER_POSITION, user.getPosition());
		jo.put(KEY_USER_MOBILE, user.getMobile()); 
		jo.put(KEY_USER_EMAIL, user.getEmail());
		jo.put(KEY_USER_GENDER, user.getGender()); 
		jo.put(KEY_USER_MOBILE, user.getMobile()); 
		jo.put(KEY_USER_WECHAT_ID, user.getWechatId()); 
		JSONArray ja = new JSONArray(); 
		List<Integer> departs = user.getDepartments(); 
		for (int i = 0; i < departs.size(); ++i ) {
			ja.put(departs.get(i)); 
		}
		jo.put(KEY_USER_DEPARTMENT, ja); 
		return jo; 
	}
	
	public static String toJsonString(WechatUser user) {
       JSONObject jo = WechatUser.toJson(user); 
		return jo.toString(); 
	}
	

}
