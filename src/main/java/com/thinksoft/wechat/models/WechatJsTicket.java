package com.thinksoft.wechat.models;

public class WechatJsTicket {
	
	private String ticket;
	
	private int expiredSeconds; 

	public int getExpiredSeconds() {
		return expiredSeconds;
	}

	public void setExpiredSeconds(int expiredSeconds) {
		this.expiredSeconds = expiredSeconds;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	} 
}
