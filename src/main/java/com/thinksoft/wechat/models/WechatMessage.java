package com.thinksoft.wechat.models;

public abstract class WechatMessage {
	
	public abstract  String toXml() ;
	
	public abstract String toJson() ;

}
