package com.thinksoft.wechat.models;

import java.io.File;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;

import com.thinksoft.wechat.service.ManageMediaService;


public class GrabTaskMessage extends WechatMessage {
	
	private String thumb_media_id; 	
	private String title ; 
	private String digest; 
	private String content; 
	private String content_source_url 	; 
	private int show_cover_pic ; 
	private String picPath; 
	
	public String getThumb_media_id() {
		return thumb_media_id;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public String getContent_source_url 	() {
		return content_source_url 	;
	}

	public int getShow_cover_pic() {
		return show_cover_pic;
	}

	public String getPicPath() {
		return picPath;
	}


	private GrabTaskMessage(GrabTaskMessage.MessageBuilder builder) {		
		thumb_media_id = builder.thumb_media_id; 
		title = builder.title; 
		content = builder.content; 
		digest = builder.digest; 
		content_source_url 	 = builder.content_source_url 	; 
		show_cover_pic = builder.show_cover_pic;
		picPath = builder.picPath; 
	}
		
	public static class MessageBuilder {	
		private ManageMediaService service ; 	
		private String thumb_media_id; 
		private String title; 
		private String content; 
		private String digest; 
		private String content_source_url 	; 
		private int show_cover_pic ; 
		private String picPath; 
		
		public MessageBuilder(ManageMediaService service) {
			this.service = service; 
		}
		
		private String getThumbMediaId( String picPath) {
		    String ret = service.uploadImage(picPath); 
		    return ret; 
		}
		
		private String buildDigest(JSONObject jo){
			String customerName = jo.getString("customer_name"); 
			String serviceContent = jo.getString("service_content");
			String serviceReward = jo.getString("service_reward");
			StringBuilder strBuilder = new StringBuilder(); 
			strBuilder.append("任务概述\n");
			strBuilder.append("客户名称：" + customerName + "\n");
			strBuilder.append("服务内容：" + serviceContent + "\n"); 
			strBuilder.append("服务积分：" + serviceReward + "\n");
			return strBuilder.toString(); 
		}
		
		private String buildContent(JSONObject jo) {
			String customerName = jo.getString("customer_name"); 
			String serviceContent = jo.getString("service_content");
			String serviceReward = jo.getString("service_reward");
			StringBuilder strBuilder = new StringBuilder(); 
			strBuilder.append("任务概述<br/>");
			strBuilder.append("客户名称：" + customerName + "<br/>");
			strBuilder.append("服务内容：" + serviceContent + "<br/>"); 
			strBuilder.append("服务积分：" + serviceReward + "<br/>");
			return strBuilder.toString(); 
		}
		
		public WechatMessage build( String jsonStr) {		
			JSONObject jo = new JSONObject(jsonStr); 
			title = jo.getString("title"); 
			content = buildContent(jo); 
			digest = buildDigest(jo); 
			content_source_url="http://wechat.itma.com.cn/jeesite/static/itma/wechat/html/grabOrder.html";
			show_cover_pic = 1;			
			File directory = new File(getClass().getResource("/").getPath()); 
			picPath = directory.getAbsolutePath().replace("WEB-INF/classes", "static/itma/wechat/images/pic-qiangdan.jpg"); 
			System.out.println(picPath);		
			thumb_media_id = getThumbMediaId(picPath); 
			return new GrabTaskMessage(this); 
		}
	}
	
	@Override
	public String toJson() {
		JSONObject jo = new JSONObject();
		jo.put("thumb_media_id", thumb_media_id); 
		jo.put("title", title); 
		jo.put("content", content);
		jo.put("show_cover_pic", String.valueOf(show_cover_pic)); 
		jo.put("content_source_url", content_source_url); 
		jo.put("digest", digest); 
		JSONArray ja = new JSONArray(); 
		ja.put(jo); 
		JSONObject articles = new JSONObject(); 
		articles.put("articles", ja); 
		return articles.toString(); 
	}

	@Override
	public String toXml() {
		// TODO Auto-generated method stub
		return "";
	}

	
}
