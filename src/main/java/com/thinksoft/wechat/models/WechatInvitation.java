package com.thinksoft.wechat.models;

import org.activiti.engine.impl.util.json.JSONObject;

public class WechatInvitation {
	
	private String userId ; 
	
	private final static String KEY_USER_ID = "userid";

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public static JSONObject toJson( WechatInvitation invitation) {
		JSONObject jo = new JSONObject(); 
		jo.put(KEY_USER_ID, invitation.getUserId()); 
		return jo; 
	} 
	
	public static String toJsonString( WechatInvitation invitation) {
		JSONObject jo = toJson(invitation); 
		return jo.toString(); 
	}

}
