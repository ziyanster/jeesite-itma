package com.itma.general;

public enum UploadOption {
	IMG("image"),	
	VOI("voice"),
	VID("video"),
	FIL("file");
	String _value;
	UploadOption(String value){
		this._value = value;
	}
	
	@Override
	public String toString(){
		return _value + "";
	}

}
