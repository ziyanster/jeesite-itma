package com.itma.general;

public enum SearchUserOption {
	ALL(0),	
	FOLLOWED(1),
	FORBIDDEN(2),
	UNFOLLOWED(4);
	int _value;
	SearchUserOption(int value){
		this._value = value;
	}
	
	@Override
	public String toString(){
		return _value + "";
	}
}
