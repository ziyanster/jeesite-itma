package com.itma.general;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.mapper.JsonMapper;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.service.OfficeService;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.utils.HttpUtils;

@Controller
@RequestMapping(value = "${adminPath}/wechat")
public class WechatSyncController extends BaseController {

	@Autowired
	private WechatToken token ; 
	
	@Autowired
	private OfficeService officeService;
	
	@Autowired
	private SystemService sysService;
	
	@RequestMapping("/depIndex")
	public String depIndex(Model model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		return "modules/wechat/depIndex";
	}
	@RequestMapping("/depSync")
	public String depSync(Model model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		return "modules/wechat/depSync";
	}
	
	@RequestMapping(value = {"depList", ""})
	public String list(String depId, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<User> list = new ArrayList<User>();
		if(StringUtils.isBlank(depId)){
			model.addAttribute("list", list);
			return "modules/wechat/depList";			
		}
		String ret = GeneralHelper.getUserListByDep(token.getToken(), depId, "1", SearchUserOption.ALL);
		JSONObject jo = new JSONObject(ret);
		JSONArray jr = jo.getJSONArray("userlist");
		for (int i = 0; i < jr.length(); i++) {
			JSONObject o = jr.getJSONObject(i);
			User user = new User();
			user.setId(o.optString("userid"));
			user.setName(o.optString("name"));
			user.setWeChatId(o.optString("weixinid"));
			user.setLoginName(o.optString("position"));
			user.setEmail(o.optString("email"));
			user.setMobile(o.optString("mobile"));
			list.add(user);
		}
		model.addAttribute("list", list);
		return "modules/wechat/depList";
	}
	@RequestMapping(value = {"depForm", ""})
	public String form(HttpServletRequest request, HttpServletResponse response, Model model) {
		Office office = new Office();
		Office pOffice = new Office();
		office.setId(request.getParameter("depId"));
		pOffice.setId(request.getParameter("parentid"));
		office.setParent(pOffice);
		office.setName(request.getParameter("name"));
		office.setSort(0);
		model.addAttribute("office", office);
		return "modules/wechat/depForm";
	}

	@ResponseBody
	@RequestMapping("/doUpload")
	public String doUpload(MultipartFile file,HttpServletRequest request, HttpServletResponse response){
		String ret = "";
		try {
			ret = GeneralHelper.uploadTempMedia(token.getToken(), file.getName(),file.getContentType(),file.getInputStream(),UploadOption.FIL);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(ret) + ")";
		return renderString(response, sRet, "application/json");
	}

	@RequestMapping("/uploadAndSync")
	public String uploadAndSync(MultipartFile file,HttpServletRequest request, HttpServletResponse response){
		String ret = "";
		try {
			ret = GeneralHelper.uploadTempMedia(token.getToken(), file.getOriginalFilename(),file.getContentType(),file.getInputStream(),UploadOption.FIL);
			if(ret != ""){
				ret = GeneralHelper.asyncBatchCoverDep(token.getToken(), ret);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "modules/wechat/depSync";
	}

	@ResponseBody
	@RequestMapping("/deleteDepartment")
	public String deleteDepartment(HttpServletRequest request, HttpServletResponse response){
		String ret = "";
		try {
			String id = request.getParameter("depId");
			GeneralHelper.deleteDepartment(token.getToken(),id);
			ret = "Success";			
		} catch (Exception e) {
			ret = "Failed:" + e.getMessage();
		}
		
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(ret) + ")";
		return renderString(response, sRet, "application/json");
	}

	@ResponseBody
	@RequestMapping("/syncDepartment")
	public String syncDepartment(HttpServletRequest request, HttpServletResponse response){
		String ret = "";
		try {
			List<Office> list = new ArrayList<Office>();
			for (Office office : officeService.findAll()) {
				if(office.getParentId().equals("1")){
					list.add(office);
				}
			}
			GeneralHelper.syncDepartments(token.getToken(),list);
			ret = "Success";
			
		} catch (Exception e) {
			ret = "Failed:" + e.getMessage();
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(ret) + ")";
		return renderString(response, sRet, "application/json");
	}
	@ResponseBody
	@RequestMapping("/syncUser")
	public String syncUser(HttpServletRequest request, HttpServletResponse response){
		String ret = "";
		try {
			String depId = request.getParameter("depId");
			List<User> list = sysService.findUserByOfficeId(depId);
			GeneralHelper.syncUsers(token.getToken(),depId,list);
			ret = "Success";
			
		} catch (Exception e) {
			ret = "Failed:" + e.getMessage();
		}
		String sRet = "jsonpCallback(" + JsonMapper.toJsonString(ret) + ")";
		return renderString(response, sRet, "application/json");
	}

	@ResponseBody
	@RequestMapping("/depTreeData")
	public List<Map<String, Object>> depTreeData(HttpServletRequest request,HttpServletResponse response){
		List<Map<String, Object>> mapList = Lists.newArrayList();
		String ret = GeneralHelper.getDepList(token.getToken());
		JSONObject jo = new JSONObject(ret);
		JSONArray jr = jo.getJSONArray("department");
		for (int i = 0; i < jr.length(); i++) {
			JSONObject o = jr.getJSONObject(i);

			Map<String, Object> map = Maps.newHashMap();
			map.put("id", o.getString("id"));
			map.put("pId", o.getString("parentid"));
			map.put("name", o.getString("name"));
			mapList.add(map);
		}
		return mapList;	
	}
}
