package com.itma.general;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinksoft.wechat.utils.HttpUtils;

public class GeneralHelper {
	private static final String BOUNDARY = "----------"
			+ System.currentTimeMillis();

	private static final String CREATE_DEP = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={0}";

	private static final String UPDATE_DEP = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token={0}";

	private static final String DELETE_DEP = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token={0}&id={1}";

	private static final String ASYNC_COVER_DEP = "https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty?access_token={0}";

	private static final String GET_DEP_LIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={0}&id={1}";

	private static final String GET_USERS_BY_DEP = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={0}&department_id={1}&fetch_child={2}&status={3}";

	private static final String GET_USER = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={0}&userid={1}";

	private static final String CREATE_USER = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={0}";

	private static final String UPDATE_USER = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={0}";

	private static final String BATCH_DELETE_USER = "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token={0}";

	private static final String UPLOAD_TEMP_MEDIA = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={0}&type={1}";

	/**
	 * 获取部门列表
	 * 
	 * @param urlPattern
	 *            URL地址模型
	 * @param params
	 *            参数列表，进行查找替换
	 * */
	public static String getUrlFromPattern(String urlPattern,
			List<String> params) {
		String returnStr = urlPattern;
		for (int i = 0; i < params.size(); i++) {
			returnStr = returnStr.replace("{" + i + "}", params.get(i));
		}
		return returnStr;
	}

	/**
	 * 获取部门列表
	 * 
	 * @param token
	 *            获取微信数据的token
	 * */
	public static String getDepList(String token) {
		String ret = "";
		HttpURLConnection conn = null;
		try {

			List<String> list = new ArrayList<String>();
			list.add(token);
			list.add("1");

			URL url = new URL(GeneralHelper.getUrlFromPattern(GET_DEP_LIST_URL,
					list));
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-type",
					"application/json; charset=utf-8");
			conn.setRequestMethod("GET");
			ret = HttpUtils.retrieveInput(conn, "utf-8");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	/**
	 * 获取部门下成员
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param depId
	 *            部门Id
	 * @param isRecursion
	 *            是否递归 0-否 1-是
	 * @param userStatus
	 *            0获取全部成员，1获取已关注成员列表，2获取禁用成员列表，4获取未关注成员列表。status可叠加，未填写则默认为4
	 * */
	public static String getUserListByDep(String token, String depId,
			String isRecursion, SearchUserOption userStatus) {
		String ret = "";
		HttpURLConnection conn = null;
		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			list.add(depId);
			list.add(isRecursion);
			list.add(userStatus.toString());

			URL url = new URL(GeneralHelper.getUrlFromPattern(GET_USERS_BY_DEP,
					list));
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-type",
					"application/json; charset=utf-8");
			conn.setRequestMethod("GET");
			ret = HttpUtils.retrieveInput(conn, "utf-8");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	/**
	 * 获取成员
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param userId
	 *            用户Id
	 * */
	public static String getUser(String token, String userId) {
		String ret = "";
		HttpURLConnection conn = null;
		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			list.add(userId);

			URL url = new URL(GeneralHelper.getUrlFromPattern(GET_USER, list));
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Content-type",
					"application/json; charset=utf-8");
			conn.setRequestMethod("GET");
			ret = HttpUtils.retrieveInput(conn, "utf-8");

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	/**
	 * 更新成员
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param user
	 *            用户
	 * @param depList
	 *            用户所在部门列表
	 * @param isNew
	 *            是否是更新
	 * */
	public static String editUser(String token, User user,
			List<String> depList, boolean isNew) {
		String ret = "";
		HttpURLConnection conn = null;
		try {
			List<String> list = new ArrayList<String>();
			list.add(token);

			URL url = null;
			if (!isNew) {
				url = new URL(
						GeneralHelper.getUrlFromPattern(UPDATE_USER, list));
			} else {
				url = new URL(
						GeneralHelper.getUrlFromPattern(CREATE_USER, list));
			}
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			JSONObject jo = new JSONObject();
			jo.put("userid", user.getEmail() + "");
			if (!"".equals(user.getName())) {
				jo.put("name", user.getName());
			}
			if (!"".equals(user.getMobile())) {
				jo.put("mobile", user.getMobile());
			}
			if (!"".equals(user.getEmail())) {
				jo.put("email", user.getEmail());
			}
			if (depList != null && depList.size() > 0) {
				int[] dep = new int[depList.size()];
				for (int i = 0; i < depList.size(); i++) {
					dep[i] = Integer.parseInt(depList.get(i));
				}
				jo.put("department", dep);
			}
			ret = HttpUtils.postJsonResp(jo.toString(), conn);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	/**
	 * 根据部门同步用户
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param depId
	 *            部门ID
	 * @param list
	 *            用户列表
	 * */
	public static void syncUsers(String token, String depId, List<User> list) {
		HttpURLConnection conn = null;
		try {
			// 获得所有人
			String usersJson = getUserListByDep(token, "1", "1",
					SearchUserOption.ALL);
			JSONObject jo = new JSONObject(usersJson);
			JSONArray jr = jo.getJSONArray("userlist");
			for (User u : list) {
				boolean hasThisPerson = false;
				JSONObject thisPerson = null;
				for (int i = 0; i < jr.length(); i++) {
					JSONObject o = jr.getJSONObject(i);
					String userId = o.optString("userid");
					// 找到这个人
					if ((u.getEmail()+"").equals(userId)) {
						hasThisPerson = true;
						thisPerson = o;
					}
				}
				List<String> desDepList = new ArrayList<String>();
				if (hasThisPerson) {
					JSONArray oriDepList = thisPerson
							.optJSONArray("department");
					boolean isContain = false;
					for (int j = 0; j < oriDepList.length(); j++) {
						String depid = oriDepList.getInt(j) + "";
						if (depid.equals(depId)) {
							isContain = true;
							break;
						}
						desDepList.add(depid);
					}
					if (isContain) {
						continue;
					} else {
						desDepList.add(depId);
						String ret = editUser(token, u, desDepList, false);
						System.out.println(ret);
						Thread.sleep(1000);
					}
				} else {
					desDepList.add(depId);
					String ret = editUser(token, u, desDepList, true);
					System.out.println(ret);
					Thread.sleep(1000);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	/**
	 * 同步部门
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param misOffice
	 *            部门列表
	 * */
	public static void syncDepartments(String token, List<Office> misOffice) {
		HttpURLConnection conn = null;

		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			URL url = new URL(GeneralHelper.getUrlFromPattern(CREATE_DEP, list));
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			for (Office office : misOffice) {
				JSONObject jo = new JSONObject();
				jo.put("name", office.getName());
				jo.put("parentid", Integer.parseInt(office.getParentId()));
				jo.put("id", Integer.parseInt(office.getId()));
				jo.put("order", office.getSort());
				String ret = HttpUtils.postJsonResp(jo.toString(), conn);
				System.out.println(ret);
				Thread.sleep(1000);
			}
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	/**
	 * 更新部门
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param depId
	 *            部门Id
	 * */
	public static void updateDepartment(String token, Office office) {
		HttpURLConnection conn = null;

		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			URL url = new URL(GeneralHelper.getUrlFromPattern(UPDATE_DEP, list));
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			JSONObject jo = new JSONObject();
			jo.put("name", office.getName());
			jo.put("parentid", office.getParentId());
			jo.put("id", office.getId());
			jo.put("order", office.getSort());
			HttpUtils.postJson(jo.toString(), conn);

		} catch (Exception e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

	}

	/**
	 * 删除部门
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param depId
	 *            部门Id
	 * */
	public static void deleteDepartment(String token, String depId) {
		HttpURLConnection conn = null;

		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			list.add(depId);
			URL url = new URL(GeneralHelper.getUrlFromPattern(DELETE_DEP, list));
			conn = (HttpURLConnection) url.openConnection();
			String r = HttpUtils.retrieveInput(conn, "utf-8");
			System.out.println(r);
			// {"errcode":60005,"errmsg":"department contains user"}
			// 部门内仍有用户，不能删除，则更新所含有的用户不在此部门内
			JSONObject jo = new JSONObject(r);
			String errMsg = jo.optString("errmsg");
			if (!"ok".equals(errMsg)) {
				if (errMsg.contains("department contains user")) {
					bacthRemoveUserFromDep(token, depId);
					deleteDepartment(token, depId);
				}
			}
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

	}

	/**
	 * 批量移除用户
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param depId
	 *            部门Id
	 * */
	public static void bacthRemoveUserFromDep(String token, String depId) {
		HttpURLConnection conn = null;

		try {
			String usersJson = getUserListByDep(token, depId, "1",
					SearchUserOption.ALL);
			JSONObject jo = new JSONObject(usersJson);
			JSONArray jr = jo.getJSONArray("userlist");
			for (int i = 0; i < jr.length(); i++) {
				JSONObject o = jr.getJSONObject(i);
				String userId = o.optString("userid");
				JSONArray oriDepList = o.optJSONArray("department");
				List<String> desDepList = new ArrayList<String>();
				for (int j = 0; j < oriDepList.length(); j++) {
					String depid = oriDepList.getString(j);
					if (depId.equals(depid)) {
						continue;
					} else {
						desDepList.add(depid);
					}
				}
				if (desDepList.size() == 0) {
					desDepList.add("1");
				}
				User user = new User();
				user.setId(userId);
				editUser(token, user, desDepList, false);
			}
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

	}

	/**
	 * 批量删除用户
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param userIdList
	 *            用户Id列表
	 * */
	public static void bacthDeleteUser(String token, List<String> userIdList) {
		HttpURLConnection conn = null;
		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			URL url = new URL(GeneralHelper.getUrlFromPattern(
					BATCH_DELETE_USER, list));
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			JSONObject jo = new JSONObject();
			jo.put("useridlist", userIdList);
			String ret = HttpUtils.postJsonResp(jo.toString(), conn);
			System.out.println(ret);
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

	}

	/**
	 * 上传临时文件
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param fileName
	 *            文件名
	 * @param contentType
	 *            文件类型
	 * @param fis
	 *            文件流
	 * */
	public static String uploadTempMedia(String token, String fileName,
			String contentType, InputStream fis, UploadOption uo) {
		HttpURLConnection conn = null;
		String ret = "";
		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			list.add(uo.toString());
			URL url = new URL(GeneralHelper.getUrlFromPattern(
					UPLOAD_TEMP_MEDIA, list));
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableFormPost(conn, BOUNDARY);
			HttpUtils.postFile(fileName, contentType, fis, BOUNDARY, conn);
			ret = HttpUtils.retrieveInput(conn);
			System.out.println(ret);
			JSONObject jo = new JSONObject(ret);
			String errMsg = jo.optString("errmsg");
			if (!"ok".equals(errMsg)) {
				ret = jo.getString("media_id");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;
	}

	/**
	 * 异步批量覆盖部门
	 * 
	 * @param token
	 *            获取微信数据的token
	 * @param mediaId
	 *            在微信上传资源后的资源id
	 * */
	public static String asyncBatchCoverDep(String token, String mediaId) {
		HttpURLConnection conn = null;
		String ret = "";
		try {
			List<String> list = new ArrayList<String>();
			list.add(token);
			URL url = new URL(GeneralHelper.getUrlFromPattern(ASYNC_COVER_DEP,
					list));
			conn = (HttpURLConnection) url.openConnection();
			HttpUtils.enableJsonPost(conn);
			JSONObject jo = new JSONObject();
			jo.put("media_id", mediaId);
			ret = HttpUtils.postJsonResp(jo.toString(), conn);
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return ret;

	}
}
