package com.thinksoft.wechat.models;

import static org.junit.Assert.*;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.Before;
import org.junit.Test;

import com.qq.weixin.mp.aes.AesException;
import com.qq.weixin.mp.aes.WXBizMsgCrypt;

public class TestSubscribeEvent {
	
	String signature = "477715d11cdb4164915debcba66cb864d751f3e6";
	// String sReqTimeStamp = HttpUtils.ParseUrl("timestamp");
	String timestamp = "1409659813";
	// String sReqNonce = HttpUtils.ParseUrl("nonce");
	String nonce = "1372623149";
	// post请求的密文数据
	// sReqData = HttpUtils.PostData();
	String postData = "<xml><ToUserName><![CDATA[wx5823bf96d3bd56c7]]></ToUserName><Encrypt><![CDATA[RypEvHKD8QQKFhvQ6QleEB4J58tiPdvo+rtK1I9qca6aM/wvqnLSV5zEPeusUiX5L5X/0lWfrf0QADHHhGd3QczcdCUpj911L3vg3W/sYYvuJTs3TUUkSUXxaccAS0qhxchrRYt66wiSpGLYL42aM6A8dTT+6k4aSknmPj48kzJs8qLjvd4Xgpue06DOdnLxAUHzM6+kDZ+HMZfJYuR+LtwGc2hgf5gsijff0ekUNXZiqATP7PF5mZxZ3Izoun1s4zG4LUMnvw2r+KqCKIw+3IQH03v+BCA9nMELNqbSf6tiWSrXJB3LAVGUcallcrw8V2t9EL4EhzJWrQUax5wLVMNS0+rUPA3k22Ncx4XXZS9o0MBH27Bo6BpNelZpS+/uh9KsNlY6bHCmJU9p8g7m3fVKn28H3KDYA5Pl/T8Z1ptDAVe0lXdQ2YoyyH2uyPIGHBZZIs2pDBS8R07+qN+E7Q==]]></Encrypt><AgentID><![CDATA[218]]></AgentID></xml>"; 

	String TOKEN = "QDG6eK"; 
	String CORP_ID = "wx5823bf96d3bd56c7"; 
	String AES_KEY = "jWmYm7qr5nMoAUwZRjGtBxmz3KA1tkAj3ykkR6q2B2C";  
	
	WXBizMsgCrypt wxcpt = null; 
	
	@Before	
	public void setup() {
		try {
			wxcpt = new WXBizMsgCrypt(TOKEN, AES_KEY, CORP_ID);
		} catch (AesException e) {
			e.printStackTrace();
		} 
	}
			
	@Test
	public void test() {
		
		try {	
			String msg = wxcpt.DecryptMsg(signature, timestamp, nonce, postData);
			Document doc = DocumentHelper.parseText(msg);
			WechatEvent evt = new SubscribeEvent.EventBuilder().build(doc); 
			assert(evt != null); 
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (AesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
