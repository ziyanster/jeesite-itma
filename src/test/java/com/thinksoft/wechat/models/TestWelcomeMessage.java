package com.thinksoft.wechat.models;

import static org.junit.Assert.*;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.Test;

public class TestWelcomeMessage {

	@Test
	public void test() {
		String text = 
				"<xml><ToUserName><![CDATA[huangsd@itma.com.cn]]></ToUserName><FromUserName><![CDATA[13910296751]]></FromUserName><CreateTime>111111</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[subscribe]]></Event><MsgId>1</MsgId></xml>"; 
		try {
			Document doc = DocumentHelper.parseText(text);
			WechatEvent evt = new SubscribeEvent.EventBuilder().build(doc); 
			assert(evt != null); 	
			String msg = new WelcomeMessage.MessageBuilder().build(evt).toXml(); 
			System.out.println(msg); 
		} catch (DocumentException e) {
			e.printStackTrace();
		} 

	}

}
