package com.thinksoft.wechat.models;

import static org.junit.Assert.*;

import org.activiti.engine.impl.util.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import com.thinksoft.wechat.service.ManageMediaService;
import com.thinksoft.wechat.service.impl.enterprise.ManageMediaServiceImpl;


public class TestGrabTaskMessage {
	
	private JSONObject jo; 
	private ManageMediaService service; 
	private WechatToken token; 
	
	@Before
	public void setup() {
		jo = new JSONObject(); 
		jo.put("title", "任务提醒，赶紧抢单啊！");
		jo.put("customer_name", "河北中行数据中心"); 
	    jo.put("service_content", "机柜设备调试电源模块");
	    jo.put("service_reward", "50");
	    
	    token = new WechatToken(); 
	    token.setToken("ucWHx_ZPp_g1q-e51U0-xEuM6JsjRoP6zQ5MV3W3ERdaZCB6ScEC7c6ryklOEP-fq5soEaRB5IdLfO028ItenthyqRz_P7CFGd__KyCh22s");
	    token.setExpiredSeconds(7200);	    
	    service = new ManageMediaServiceImpl();

	}

	@Test
	public void test() {
		GrabTaskMessage m = (GrabTaskMessage)(new GrabTaskMessage.MessageBuilder(service)).build(jo.toString());
		String ret = m.toJson(); 
		System.out.println(ret);
	}

}
