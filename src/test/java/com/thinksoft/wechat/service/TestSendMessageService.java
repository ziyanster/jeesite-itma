package com.thinksoft.wechat.service;

import static org.junit.Assert.*;

import org.activiti.engine.impl.util.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.thinksoft.wechat.models.GrabTaskMessage;
import com.thinksoft.wechat.models.WechatMessage;
import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.impl.enterprise.ManageMediaServiceImpl;
import com.thinksoft.wechat.service.impl.enterprise.SendMessageServiceImpl;
import com.thinksoft.wechat.service.impl.service.ManageUserServiceImpl;

public class TestSendMessageService {
	
	private JSONObject jo; 
	private ManageMediaService uploadService; 
	private ManageUserService userService; 
	private WechatToken token; 
	
	@Before
	public void setup() {
		jo = new JSONObject(); 
		jo.put("title", "任务提醒，赶紧抢单啊！");
		jo.put("customer_name", "河北中行数据中心"); 
	    jo.put("service_content", "机柜设备调试电源模块");
	    jo.put("service_reward", "50");
	    
	    token = new WechatToken(); 
	    token.setToken("ucWHx_ZPp_g1q-e51U0-xEuM6JsjRoP6zQ5MV3W3ERdaZCB6ScEC7c6ryklOEP-fq5soEaRB5IdLfO028ItenthyqRz_P7CFGd__KyCh22s");
	    token.setExpiredSeconds(7200);
	    uploadService = new ManageMediaServiceImpl();
	    userService = new ManageUserServiceImpl(); 
	}

	@Test
	public void test() {
		WechatMessage msg = (new GrabTaskMessage.MessageBuilder(uploadService)).build(jo.toString());
		SendMessageService service = new SendMessageServiceImpl(); 
		service.sendMessageToAll(msg, "0");
		
	}

}
