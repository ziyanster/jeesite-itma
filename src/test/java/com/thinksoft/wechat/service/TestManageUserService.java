package com.thinksoft.wechat.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.thinksoft.wechat.models.WechatToken;
import com.thinksoft.wechat.service.impl.service.ManageUserServiceImpl;

public class TestManageUserService {
	
	private  WechatToken token ; 
	
	@Before
	public void setup() {
		token = new WechatToken();
		token.setToken("ucWHx_ZPp_g1q-e51U0-xEuM6JsjRoP6zQ5MV3W3ERdaZCB6ScEC7c6ryklOEP-fq5soEaRB5IdLfO028ItenthyqRz_P7CFGd__KyCh22s");
		token.setExpiredSeconds(7200);
	}

	@Test
	public void test() {
        ManageUserService service = new ManageUserServiceImpl(); 
        List<String> users = service.getAllUser(); 
        assert(users.size() > 0); 
        
        for (String item :users) {
        	System.out.println(item); 
        }
	}

}
