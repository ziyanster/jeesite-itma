SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS checkTask;
DROP TABLE IF EXISTS contract;
DROP TABLE IF EXISTS contractDetail;
DROP TABLE IF EXISTS hj_ht_swhtbh;




/* Create Tables */

CREATE TABLE checkTask
(
	-- 编号
	id varchar(64) COMMENT '编号',
	-- 创建者
	create_by varchar(64) COMMENT '创建者',
	-- 创建时间
	create_date datetime COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) COMMENT '更新者',
	-- 更新时间
	update_date datetime COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记
	del_flag char(1) COMMENT '删除标记',
	-- 合同编号
	contractId varchar(64) COMMENT '合同编号',
	-- 任务流水号
	taskSerialNo varchar(64) COMMENT '任务流水号',
	-- 巡检日期
	checkData datetime COMMENT '巡检日期',
	-- 提前一周提醒
	weekRemind char COMMENT '提前一周提醒',
	-- 提前一天提醒
	dayRemind char COMMENT '提前一天提醒',
	-- 早上提醒
	morningRemind char COMMENT '早上提醒',
	-- 晚上提醒
	nightRemind char COMMENT '晚上提醒',
	-- 巡检频次
	checkFre char COMMENT '巡检频次',
	-- 0可用1不可用
	isEnabled char COMMENT '0可用1不可用',
	-- 提前一周
	advWeek varchar(100) COMMENT '提前一周',
	-- 提前一天提醒时间
	advDay varchar(100) COMMENT '提前一天提醒时间',
	-- 早上提醒
	advMorning varchar(100) COMMENT '早上提醒',
	-- 当天晚上提醒时间
	advNight varchar(100) COMMENT '当天晚上提醒时间'
);


CREATE TABLE contract
(
	-- 编号
	id varchar(64) COMMENT '编号',
	-- 创建者
	create_by varchar(64) COMMENT '创建者',
	-- 创建时间
	create_date datetime COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) COMMENT '更新者',
	-- 更新时间
	update_date datetime COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记
	del_flag char(1) COMMENT '删除标记',
	-- 客户编号
	customerNo varchar(64) COMMENT '客户编号',
	-- 合同开始日期
	beginDate datetime COMMENT '合同开始日期',
	-- 合同结束日期
	endDate datetime COMMENT '合同结束日期',
	-- 地址
	address varchar(200) COMMENT '地址',
	-- 服务内容
	content varchar(250) COMMENT '服务内容',
	-- 销售主管
	salesDirector varchar(64) COMMENT '销售主管',
	-- 巡检频次（周，两周，月，两个月，季度）
	checkType char COMMENT '巡检频次（周，两周，月，两个月，季度）',
	-- 项目名称
	projectName varchar(150) COMMENT '项目名称',
	-- 客户联系人
	cusContact varchar(64) COMMENT '客户联系人',
	-- 客户电话
	phone varchar(30) COMMENT '客户电话'
);


CREATE TABLE contractDetail
(
	-- 编号
	id varchar(64) COMMENT '编号',
	-- 创建者
	create_by varchar(64) COMMENT '创建者',
	-- 创建时间
	create_date datetime COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) COMMENT '更新者',
	-- 更新时间
	update_date datetime COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记
	del_flag char(1) COMMENT '删除标记',
	-- 合同编号
	contractId varchar(64) COMMENT '合同编号',
	-- 机型
	model varchar(100) COMMENT '机型',
	-- 产品描述
	productDes varchar(200) COMMENT '产品描述',
	-- 数量
	qty decimal(19,2) COMMENT '数量',
	-- 服务时限
	serviceTime varbinary(100) COMMENT '服务时限',
	-- 序列号
	serialNo varchar(100) COMMENT '序列号',
	-- 用途
	useful varchar(150) COMMENT '用途'
);


CREATE TABLE hj_ht_swhtbh
(
	-- 商务合同编号
	SWHTBH varchar(20) BINARY NOT NULL COMMENT '商务合同编号',
	-- 合同类型
	HTLX varchar(10) BINARY NOT NULL COMMENT '合同类型',
	-- 合同类型名称
	HTLXMC varchar(20) BINARY NOT NULL COMMENT '合同类型名称',
	-- 结算中心（盖章公司）
	JSZXDM char(2) BINARY NOT NULL COMMENT '结算中心（盖章公司）',
	-- 结算中心名称（盖章公司）
	JSZXMC varchar(20) BINARY NOT NULL COMMENT '结算中心名称（盖章公司）',
	-- 业务机构
	YWJGDM char(2) BINARY NOT NULL COMMENT '业务机构',
	-- 业务机构名称
	YWJGMC varchar(20) BINARY NOT NULL COMMENT '业务机构名称',
	-- 隶属公司
	LSGS char(2) BINARY NOT NULL COMMENT '隶属公司',
	-- 隶属公司名称
	LSGSMC varchar(20) BINARY NOT NULL COMMENT '隶属公司名称',
	-- 调度公司
	DDGSDM char(2) BINARY NOT NULL COMMENT '调度公司',
	-- 调度公司名称
	DDGSMC varchar(20) BINARY NOT NULL COMMENT '调度公司名称',
	-- 合同状态
	HTZT varchar(10) BINARY NOT NULL COMMENT '合同状态',
	-- 合同模板
	HTMB varchar(10) BINARY NOT NULL COMMENT '合同模板',
	-- 正式合同编号
	ZSHTBH char(9) BINARY COMMENT '正式合同编号',
	-- 客户代码
	KHDM char(6) BINARY NOT NULL COMMENT '客户代码',
	-- 客户名称
	KHMC varchar(50) BINARY NOT NULL COMMENT '客户名称',
	-- 合同开始日期
	HTKSRQ date NOT NULL COMMENT '合同开始日期',
	-- 合同终止日期
	HTZZRQ date NOT NULL COMMENT '合同终止日期',
	-- 业务人员代码
	YWRYDM char(6) BINARY NOT NULL COMMENT '业务人员代码',
	-- 业务人员姓名
	YWRYXM varchar(8) BINARY NOT NULL COMMENT '业务人员姓名',
	-- 经办人员姓名
	JBRYXM varchar(8) BINARY NOT NULL COMMENT '经办人员姓名',
	-- 送达方式名称
	SDFSMC varchar(30) BINARY NOT NULL COMMENT '送达方式名称',
	-- 送达日期
	SDRQ date NOT NULL COMMENT '送达日期',
	-- 合同金额
	HTJE decimal(12,2) NOT NULL COMMENT '合同金额',
	-- 备注
	BZ varchar(500) BINARY COMMENT '备注',
	-- 对方联系人
	DFLXR varchar(10) BINARY NOT NULL COMMENT '对方联系人',
	-- 编号人员
	BHRY char(6) BINARY NOT NULL COMMENT '编号人员',
	-- 编号日期
	BHRQ date NOT NULL COMMENT '编号日期',
	-- 客户合同编号
	KHHTBH varchar(20) BINARY COMMENT '客户合同编号',
	-- 项目名称
	XMMC varchar(60) BINARY NOT NULL COMMENT '项目名称',
	-- 乙方
	YF varchar(60) BINARY NOT NULL COMMENT '乙方',
	-- 签订地点
	QDDD varchar(60) BINARY NOT NULL COMMENT '签订地点',
	-- 归档人
	GDRY varchar(8) BINARY COMMENT '归档人',
	-- 最后修改人
	ZHXGR char(6) BINARY NOT NULL COMMENT '最后修改人',
	-- 最后修改时间
	ZHXGSJ date NOT NULL COMMENT '最后修改时间',
	-- 注销标志
	ZXBZ char BINARY COMMENT '注销标志',
	BZ2 varchar(100) BINARY,
	BZ3 varchar(100) BINARY,
	KPGSDM char(2) BINARY,
	KPGSMC varchar(20) BINARY,
	ZDR varchar(8) BINARY,
	SPR1 varchar(8) BINARY,
	SPR2 varchar(8) BINARY,
	SPR3 varchar(8) BINARY,
	SPR4 varchar(8) BINARY,
	SPR5 varchar(8) BINARY,
	SPR6 varchar(8) BINARY,
	SPR7 varchar(8) BINARY,
	SPR8 varchar(8) BINARY,
	SPR9 varchar(8) BINARY,
	GDR varchar(8) BINARY,
	WDBH char(5) BINARY,
	SPBZ char BINARY,
	BMDM char(2) BINARY,
	FGSDM char(2) BINARY,
	XMFZR char(10) BINARY,
	XMFZRDM char(6) BINARY,
	XJZQ int,
	YYSWHTBH varchar(20) BINARY,
	ZY varchar(650) BINARY,
	DYKH varchar(50) BINARY,
	DYKHDM char(6) BINARY,
	ZDRQ date,
	ZZKHMC varchar(50) BINARY,
	SWHTLX varchar(2) BINARY,
	-- 资金占用
	ZJZY decimal(12,2) COMMENT '资金占用',
	-- 外购费用
	WGFY decimal(12,2) COMMENT '外购费用',
	-- 编号
	id varchar(64) COMMENT '编号',
	-- 创建者
	create_by varchar(64) COMMENT '创建者',
	-- 创建时间
	create_date datetime COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) COMMENT '更新者',
	-- 更新时间
	update_date datetime COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记
	del_flag char(1) COMMENT '删除标记',
	PRIMARY KEY (SWHTBH)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;



